Developer Readme
================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   runbooks/developer_workstation_setup

System Setup
------------

:doc:`Developer workstation setup <runbooks/developer_workstation_setup>`


Code Style
----------

Follow the Google code style

https://google.github.io/styleguide/pyguide.html

