tessa.widgets package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tessa.widgets.data

Submodules
----------

tessa.widgets.data\_grid module
-------------------------------

.. automodule:: tessa.widgets.data_grid
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.figures module
----------------------------

.. automodule:: tessa.widgets.figures
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.mapping module
----------------------------

.. automodule:: tessa.widgets.mapping
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.model\_editors module
-----------------------------------

.. automodule:: tessa.widgets.model_editors
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.reports module
----------------------------

.. automodule:: tessa.widgets.reports
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.summary\_tables module
------------------------------------

.. automodule:: tessa.widgets.summary_tables
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.ui module
-----------------------

.. automodule:: tessa.widgets.ui
   :members:
   :undoc-members:
   :show-inheritance:

tessa.widgets.utilities module
------------------------------

.. automodule:: tessa.widgets.utilities
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tessa.widgets
   :members:
   :undoc-members:
   :show-inheritance:
