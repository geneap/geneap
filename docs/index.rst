.. Tessa documentation master file, created by
   sphinx-quickstart on Fri Jun 10 18:12:23 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tessa's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
