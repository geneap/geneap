Overview of server/service architecture
=======================================

GOALS/PLAN

How to set up dev, testing environment.

Looking to use Ubuntu images, could be docker, multipass, LXD...
For local dev, Docker should make sense although pretty slow to get started. Still not a bad way to
practice getting the command sequence right. Could be a good way to create a build as well since
for that don't need much power or to have the file dependencies working (like the tiledb).

Can then install tessa as a wheel archive, nicer that doing it from source. 
To actually do the build, can use poetry or not, doesn't really matter. Actually using setuptools
might even be better since it's easy to use cython later (which I think will be needed eventually).

Completely different step is the deploy. This is where I was getting confused but I think i get it now.
Setup.py or poetry are for the build stage. Poetry does a lot of stuff about pinning dependencies, cool
but that doesn't by itself solve the deploy part. It could be used for a source deploy where you pull
the git repo and run poetry install inside, if the .lock file is there then it will reproduce the dev env.
Not super convinced by that tho. Could do something like poetry install deps then install the wheel. 
Others suggest building a docker image with everything in.

Still feels like poetry is for dev and build, not run. Docker based workflows just move the problem,
you still need to set up the environment in the image, just that has an easier workflow but you are still
doing pseudo linux system setup, but then you have to run your images on something using yet another tool.
Usually advertised docker compose or AWS cloudformation or OpenStack Heat (which is just a clone). These
are all YAML bullshit. Not that yaml is the issue per se just that they all embed bash scripts in the yaml
which also becomes a bizzaro DSL with 'dict entries' that are actually commands or templating tools or
some shit. Even very simple cases do this. Why not just use a python script from the start if you need
dynamic capabilities, Openstack has full python APIs.

I think (and seeing some blogs to support) that this yaml shit is not the way to go. Rather write a 
shell script or python script and if necessary have that script be executed, but not do some wierd
templating of a script in yaml. Only real drawback with the python approach is that you still need an 
entry point where you are running in a python env that has all the cloud APIs installed, but I think
this is anyway going to be a separate C&C machine (like your own laptop). 


So where that leaves us:
Goal is to get tessa building as a wheel, and have a way to express the dependencies. The build can be
setup.py or poetry doesn't matter (actually see many projects have a bit of everything). Point is that
you get a wheel out of it. 
Then deploy is a matter of:
- Set up the server environment. Could be a pre-existing one set up by hand, e.g. in case where we run
everything on one mainframe.
- Set up the python environment. 
- Copy the built wheel file and pip install it into the environment
- Running gunicorn/uvicorn/fastapi with systemd
- Running the voila server with systemd and the right target notebook file.
  - ISSUE: if we are building a wheel package, what about the notebook files. Can the be embedded in the
  package? Should they be? Might be easier still to do a source install and skip the build step for now
   
Installing python deps could be a pip install requirements.txt. But if you're anyway doing that
(i.e. there isn't a special deploy thing linked to the poetry build tool) then why not use conda or
mamba directly and get better management of big binary dependencies like BLAS. Since we're in hard 
core computing realm a lot of this webshit doesn't apply, we don't just have a handful of friendly deps
we have BLAS and GDAL and all that shit that conda/mamba was made for. Question is simply then whether 
to go with both conda+mamba (advantage of having deb repo for conda with security updates).

Another idea is using latest ubuntu can use the system python 3.10. Like this idea for getting auto 
updates, but could be a pain in terms of advanced dependencies. can use pyenv as well, main advantage is
to play nice with peotry since poetry doesn't like to be in a 'nested venv' if you try to initialise
it using a conda environment (it seems conda environments sort of are venvs and also sort of aren't).


## Server

WIP and thoughts

First version can replicate the Edelweiss server setup more or less.

Thinking to start by running everything including DB on one cloud instance. Why? traditionally
would have dedicated instance for DB. Because: at the moment we have basically no concurrant users.
So the load is serial - db, compute, etc... So we can get much better utilisation by hosting everything
on the same instance. The alternative is the extreme microservices approach but the size of the instances
people use is effin tiny - fine if you are serving some stupid TODO app but we need muscle. 

nginx still best proxy ? Has add value of also being a web server.
Think in this first version I still want to have the jhub setup.
-> what are the alternatives? Could just server a plain Voila, maybe with basic auth. 
Or something like adding ONLY voila to the jupyter_server (so there is no notebook support).
Idea being that I don't want every UI user to have notebook access.
OR make them two separate services. For consultancy work would like to have a jhub but 'private'.


Currently for testing we put voila in CDS Dashboards. At the moment a bit annoying we can't also
use with Jhub 2 (which would give user roles and things). 

For 'production' don't like too much the idea of dangling voila off the end of a hub environment, seems
uncecessarily complicated.

At some point would want to have a 'dev' env with a JHub like edelweiss for our own use and a 'prod' 
with just APIs and UIs (could be voila but eventually something a bit better baked)

Right now happy to stick to using edelweiss for development work, more powerful and know where we are 
with it. 

So the 'server' env is mainly for API host and the 'production' UI. Therefore doesn't need a jupyterhub
environment.


## Voila


## JupyterHub

Jupyterhub extensions

## JupyterLab 

Jupyterlab extensions
Widgets (including matplotlib ones)
Git

