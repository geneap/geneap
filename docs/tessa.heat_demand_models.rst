tessa.heat\_demand\_models package
==================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   tessa.heat_demand_models.data

Submodules
----------

tessa.heat\_demand\_models.load\_curves module
----------------------------------------------

.. automodule:: tessa.heat_demand_models.load_curves
   :members:
   :undoc-members:
   :show-inheritance:

tessa.heat\_demand\_models.model\_ch module
-------------------------------------------

.. automodule:: tessa.heat_demand_models.model_ch
   :members:
   :undoc-members:
   :show-inheritance:

tessa.heat\_demand\_models.old\_lc module
-----------------------------------------

.. automodule:: tessa.heat_demand_models.old_lc
   :members:
   :undoc-members:
   :show-inheritance:

tessa.heat\_demand\_models.yearly\_demand module
------------------------------------------------

.. automodule:: tessa.heat_demand_models.yearly_demand
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tessa.heat_demand_models
   :members:
   :undoc-members:
   :show-inheritance:
