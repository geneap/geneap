tessa.core package
==================


Module contents
---------------

.. automodule:: tessa.core
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

tessa.core.serialisation module
-------------------------------

.. automodule:: tessa.core.serialisation
   :members:
   :undoc-members:
   :show-inheritance:

tessa.core.units module
-----------------------

.. automodule:: tessa.core.units
   :members:
   :undoc-members:
   :show-inheritance:

