from abc import abstractmethod
from typing import Optional

import pandera as pa
from geopandas import GeoDataFrame
from pandera.typing import DataFrame, DateTime, Index, Series
from pydantic import BaseModel, ConfigDict, Field, field_serializer, field_validator, model_validator
from pydantic.dataclasses import dataclass


class ClimateRecord(pa.DataFrameModel):
    timestamp: Index[DateTime] = pa.Field(check_name=True, coerce=True)
    year: Series[int] = pa.Field(coerce=True)
    day_of_year: Series[int] = pa.Field(coerce=True)
    hour_of_day: Series[int] = pa.Field(coerce=True)
    temp2m: Series[float] = pa.Field(coerce=True)
    globalirrad: Optional[Series[float]] = pa.Field(coerce=True)
    t_ext_1d: Series[float] = pa.Field(coerce=True)
    avg_3d: Series[float] = pa.Field(coerce=True)
    t_ext_bin: Series[float] = pa.Field(coerce=True)


class ClimateData(BaseModel):
    # id: UUID4 = Field(default_factory=uuid.uuid4)
    schema_version: int = 1
    source: str
    # Order is important, make sure hdd/cdd validators run after climate is set
    timeseries: DataFrame[ClimateRecord] = Field(repr=False)
    heating_degree_days: float | None = None
    cooling_degree_days: float | None = None
    model_config = ConfigDict(from_attributes=True)

    @model_validator(mode="after")
    def _generate_degree_days(self):
        if self.heating_degree_days is None:
            ts = self.timeseries
            if ts is None:
                raise ValueError("Must provide climate time series to generate hdd")
            from .common import calculate_heating_degree_days

            self.heating_degree_days = calculate_heating_degree_days(self.timeseries["temp2m"])

        if self.cooling_degree_days is None:
            ts = self.timeseries
            if ts is None:
                raise ValueError("Must provide climate time series to generate cdd")
            from .common import calculate_cdd

            self.cooling_degree_days = calculate_cdd(self.timeseries["temp2m"])

        return self

    @field_validator("timeseries", mode="before")
    def _validate_timeseries(cls, v):
        if v is not None:
            if v.index.name != "timestamp":
                v.index.name = "timestamp"
        return v

    #
    # @field_serializer("timeseries")
    # def _serialize_timeseries(self, timeseries):
    #     return timeseries.to_dict()


@dataclass(kw_only=True)
class ClimateDataSource:
    name = "default"

    @abstractmethod
    def load(self, buildings: GeoDataFrame, year: int | None = None) -> ClimateData | None:
        pass

    @abstractmethod
    def load_location(self, lat=None, lon=None, year: int | None = None) -> ClimateData | None:
        pass
