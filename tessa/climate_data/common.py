import numpy as np
import xarray as xr
from pandas import DataFrame, Series


def calculate_cdd(temperatures, cdd_threshold=18.3):
    """
    Calculate cooling degree days
    Args:
        temperatures:
        cdd_threshold:

    Returns:

    """
    t_over = temperatures[temperatures > cdd_threshold]
    t_over = t_over - cdd_threshold
    t_over = t_over[t_over > 0]

    return t_over.sum()


def calculate_hdd(temperatures, hdd_threshold=12):
    """
    Calculate heating degree days
    Args:
        temperatures:
        hdd_threshold:

    Returns:

    """
    t_over = temperatures[temperatures < hdd_threshold]
    t_over = hdd_threshold - t_over
    t_over = t_over[t_over > 0]

    return t_over.sum()


def calculate_heating_degree_days(temperatures, t_heating_on=16, t_heating_setpoint=20):
    """
    Calculates heating degree days for the given climate data using a
    *double* threshold where the outdoor temperature must be below a given value
    but the DD is calculated relative to a setpoint.

    The calculation is
    sum(t_heating_setpoint - t_ext) where t_ext <= t_ref (temperature of heating cut-in)
        Originally 2 values were calculated with:
    t_heating_setpoint = 18,  t_ref = 12
    t_heating_setpoint = 20,  t_ref = 16

    Currently defaulting to 20, 16

    Args:
        temperatures:
        t_heating_on:
        t_heating_setpoint:

    Returns:

    """
    try:
        daily_mean = temperatures.resample("1D").mean()
    except TypeError:
        # If we don't have a datetime index, just assume that it's already daily mean
        daily_mean = temperatures

    # t_heating_on = 12
    # t_heating_setpoint = 18
    # degree_day_12_18 = degree_day_double_threshold(daily_mean, t_heating_on, t_heating_setpoint)
    degree_day = heating_degree_day_double_threshold(daily_mean, t_heating_on, t_heating_setpoint)
    return degree_day


def heating_degree_day_double_threshold(daily_mean, t_heating_on, t_heating_setpoint):
    is_heating_day = daily_mean <= t_heating_on
    degree_day_daily = (t_heating_setpoint - daily_mean) * is_heating_day
    return degree_day_daily.sum()


def calculate_climate_correction(heating_degree_days_real, heating_degree_days_reference):
    """
    Function to calculate the climate correction factor for heating demands.
    This is the ratio between the heating degree days calculated for the climate chosen
    by the user and the reference heating degree days.
    Note that this is a climate correction for the DJ16-20.

    Args:
        heating_degree_days_real: float or Series
        heating_degree_days_reference: float

    Returns:
        same type as heat_degree_days_real
    """

    clim_correction = heating_degree_days_real / heating_degree_days_reference
    return clim_correction


def add_temp_mean_and_bins(climate_hourly: DataFrame):
    """Compute daily average of used climate and add temperature bins (quantize to 2 deg bin)"""
    climate_daily = climate_hourly[["temp2m"]].resample("1d").mean().rename(columns={"temp2m": "t_ext_1d"})
    climate_daily["t_ext_1d"] = climate_daily["t_ext_1d"].ffill().bfill()
    climate_daily["avg_3d"] = climate_daily["t_ext_1d"].rolling(3).mean()
    climate_daily["avg_3d"] = climate_daily["avg_3d"].ffill().bfill()
    # climate_hourly = climate_hourly.rename(columns={'temp2m': 't_ext'})
    climate_hourly = climate_hourly.join(climate_daily.resample("1h").ffill()).ffill()
    # Setup T_ext Bin for random deviation as the temperature rounded to the nearest even number
    climate_hourly["t_ext_bin"] = 2 * np.round(climate_hourly["temp2m"] / 2)
    climate_hourly["t_ext_bin"].ffill().bfill().astype(int)

    # Put the Text_bin value = -99 for all values <= 0
    climate_hourly.loc[climate_hourly["t_ext_bin"] <= 0, "t_ext_bin"] = -99
    return climate_hourly


def smooth_16_hours_between_consecutive_months(weather_timeseries: DataFrame | Series | xr.DataArray):
    """For TMY data processing, smooths time series data across month boundaries
    when a TMY is assembled from typical months from different years.

    Args:
        weather_timeseries: time series of temperatures or other climate variables to be smoothed
    """
    try:
        year = weather_timeseries["time.year"].values[0]
    except KeyError:
        year = weather_timeseries.index.year[0]

    # NOTE had to add type ignore here to silence mypy
    weather_timeseries.loc[f"{year}-01-31T16" : f"{year}-02-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-02-28T16" : f"{year}-03-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-03-31T16" : f"{year}-04-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-04-30T16" : f"{year}-05-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-05-31T16" : f"{year}-06-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-06-30T16" : f"{year}-07-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-07-31T16" : f"{year}-08-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-08-31T16" : f"{year}-09-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-09-30T16" : f"{year}-10-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-10-31T16" : f"{year}-11-01T07"] = np.nan  # type: ignore
    weather_timeseries.loc[f"{year}-11-30T16" : f"{year}-12-01T07"] = np.nan  # type: ignore

    if hasattr(weather_timeseries, "interpolate"):
        weather_timeseries = weather_timeseries.interpolate(method="cubic")
    else:
        weather_timeseries = weather_timeseries.interpolate_na(dim="time", method="cubic")
    return weather_timeseries
