import io
from dataclasses import dataclass
from datetime import datetime
from typing import Optional

import pandas as pd
import requests
import xarray as xr
from geoalchemy2 import Geometry
from pandera.typing import DataFrame
from pandera.typing.geopandas import GeoDataFrame
from requests.adapters import HTTPAdapter, Retry
from sqlalchemy import Column, LargeBinary, String, text
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, declared_attr

from tessa.core.orm import Base

from .common import (
    add_temp_mean_and_bins,
    smooth_16_hours_between_consecutive_months,
)
from .models import ClimateData, ClimateDataSource, ClimateRecord

# def load_climate_correction(engine, station, year):
#     climate = load_sia_climate(station, year, engine)
#     real201 = calculate_heating_degree_days(climate['temp2m'])
#     ref_climate_data_ge = load_ge_norm_climate()
#     ge201 = ref_climate_data_ge['del_t_ref_dj_16_20'].sum()
#     clim_correction = calculate_climate_correction(real201, ge201)
#     return clim_correction


@dataclass(kw_only=True)
class SingleTableSource(ClimateDataSource):
    """
    Allows to provide a fixed climate data file instead of loading from
    a data source based on building locations
    """

    name = "Fixed data source"

    def __init__(self, data):
        self.data = ClimateData(source=self.name, timeseries=DataFrame[ClimateRecord](data))

    def load(self, buildings, year: int | None = None) -> ClimateData:
        return self.data

    def load_location(self, lat=None, lon=None, year: int | None = None) -> ClimateData:
        return self.data


class OneBuildingClimateStation(Base):
    # __tablename__ = "onebuildingclimatestation"
    __table_args__ = {"schema": "climate"}
    id = Column(String, primary_key=True, index=True)  # is the WMO code
    name = Column(String)
    comments_1 = Column(String)
    comments_2 = Column(String)
    tmyx = Column(LargeBinary)
    geom = Column(Geometry(geometry_type="POINTZ", srid=4326))

    @declared_attr
    def __tablename__(cls) -> str:
        return "onebuildingclimatestation"

    @property
    def tmyx_table(self):
        return pd.read_parquet(io.BytesIO(self.tmyx))


@dataclass(kw_only=True)
class OneBuildingTMYSource(ClimateDataSource):
    """

    Args:
        database: SQLAlchemy engine/session
        override_year: TMY data picks months from different years. When loading, you can
        suppy the year argument, if override_year is True the year in the time stamp will be replaced
        with the supplied year value
    """

    engine: Engine
    tmy_reference_year = 2015
    name = "OneBuilding TMY"
    override_year = False

    def load(self, buildings: GeoDataFrame, year: int | None = None) -> ClimateData | None:
        """
        Load climate data for the buildings. The location for the climate data
        is apprxoimated using the shapely representative_point() function

        Args:
            buildings:
            year:

        Returns:

        """
        b = buildings.geometry.to_crs(4326).unary_union.representative_point()
        lon, lat = b.x, b.y
        return self.load_location(lat=lat, lon=lon, year=year)

    def load_location(self, lat=None, lon=None, year: int | None = None) -> ClimateData | None:
        """

        Args:
            lat:
            lon:
            year:

        Returns:

        """
        # TODO: (c)heck validity bounds.
        stid = self.get_closest_climate_station_id(lat=lat, lon=lon)

        with Session(bind=self.engine) as session:
            st_data = session.get(OneBuildingClimateStation, stid)
        if st_data is None:
            return None
        clim = st_data.tmyx_table.copy()
        clim["day_of_year"] = clim.timestamp_synthetic.dt.day_of_year

        clim = (
            clim.rename(
                columns={
                    "timestamp_synthetic": "timestamp",
                    "dry_bulb_temperature": "temp2m",
                    "global_horizontal_radiation": "globalirrad",
                    "hour": "hour_of_day",
                }
            )
            .drop(["id", "days_since_last_snowfall"], axis=1)
            .set_index("timestamp")
        )
        clim["hour_of_day"] -= 1
        clim = add_temp_mean_and_bins(clim)
        if self.override_year and year is not None:
            clim.index = pd.Index([t.replace(year=year) for t in clim.index])
        else:
            clim.index = pd.Index([t.replace(year=self.tmy_reference_year) for t in clim.index])
        clim.index.name = "timestamp"
        timeseries = DataFrame[ClimateRecord](clim)
        return ClimateData(source=self.name, timeseries=timeseries)

    def get_closest_climate_station_id(self, *, lat, lon):
        """
        For each point, get the closest climate station
        This uploads the x,y points to a temporary table to do the search,
        therefore, it isn't very fast. The idea is to be very general, e.g. in the
        case you want to upload new buildings, just need the location.
        """
        sql = """
        SELECT
            id
        FROM (SELECT st_setsrid(st_point(:lon, :lat), 4326) as point_bld) pnt
        CROSS JOIN LATERAL (
          SELECT id
          FROM climate.onebuildingclimatestation
          ORDER BY st_distance(onebuildingclimatestation.geom, point_bld)
          LIMIT 1
        ) climate_stations;
        """
        with self.engine.begin() as conn:
            result = conn.execute(text(sql), dict(lon=lon, lat=lat))
            stations = result.fetchall()
            if len(stations) > 0:
                return stations[0][0]
        return None


@dataclass(kw_only=True)
class EUPvgisTMYSource(ClimateDataSource):
    """TMY data from EU PVGIS web service

    Load the TMY information and generate a consistent timestamp
    """

    tmy_reference_year = 2015
    tmy_url = "https://re.jrc.ec.europa.eu/api/tmy"
    name = "EU PVGIS TMY data"
    override_year: bool = False

    def __post_init__(self):
        """

        Args:
            override_year: TMY data picks months from different years. When loading, you can
            suppy the year argument, if override_year is True the year in the time stamp will be replaced
            with the supplied year value
        """
        self._session = requests.Session()
        retries = Retry(total=5, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])
        self._session.mount("https://", HTTPAdapter(max_retries=retries))

    def load(self, buildings: GeoDataFrame, year: int | None = None) -> ClimateData | None:
        # approx_centre = buildings[['lon', 'lat']].mean()
        b = buildings.geometry.to_crs(4326)
        lon, lat = b.x.mean(), b.y.mean()
        return self.load_location(lat, lon, year=year)

    def load_location(self, lat=None, lon=None, year: int | None = None) -> ClimateData | None:
        if self.override_year and year is not None:
            year_midpoint = year
        else:
            year_midpoint = self.tmy_reference_year

        result = self._request_api_data(lat, lon)
        if result is None:
            return None

        clim = pd.DataFrame(result["outputs"]["tmy_hourly"])
        clim.columns = [c.lower().replace("(", "_").replace(")", "") for c in clim.columns]

        clim["year_tmy"], clim["month"], clim["day"], clim["hour_of_day"] = zip(
            *clim.time_utc.apply(self._proc_time)
        )

        clim["year"] = year_midpoint

        clim["timestamp"] = clim.apply(lambda r: datetime(r.year, r.month, r.day, r.hour_of_day), axis=1)

        clim["day_of_year"] = clim.timestamp.dt.day_of_year

        clim = clim.rename(columns={"t2m": "temp2m", "g_h": "globalirrad"}).set_index("timestamp")
        clim = add_temp_mean_and_bins(clim)
        return ClimateData(source=self.name, timeseries=DataFrame[ClimateRecord](clim))

    def _request_api_data(self, lat, lon):
        # TODO: (c)ached fetch function with rounding to nearest 100m
        # TODO: (h)andle failed fetch.
        payload = {"outputformat": "json", "lat": lat, "lon": lon}
        r = self._session.get(self.tmy_url, params=payload)
        if r.status_code != 200:
            return None
        result = r.json()
        return result

    @staticmethod
    def _proc_time(t):
        year = int(t[:4])
        month = int(t[4:6])
        day = int(t[6:8])
        hour = int(t[9:10])
        return year, month, day, hour


@dataclass(kw_only=True)
class SwissFutureTMYSource(ClimateDataSource):
    """Climate data source for future TMY data
    using the 2018 Swiss regional climate model (CMIP5 round)

    Defaults to using embedded climate scenario data (small enough to ship in the package)
    """

    name = "Swiss CMIP6 adjusted TMY"
    tmy_source: ClimateDataSource
    climate_anomalies_uri: str | None = None

    def __post_init__(self):
        """

        Args:
            tmy_source: Another data source that provides TMY, such as OneBuilding or Eu PVGIS
            climate_anomalies_uri: location for the climate anomaly file, otherwise use embedded data.
        """
        if self.climate_anomalies_uri is None:
            from importlib.resources import as_file, files

            from . import data

            with as_file(
                files(data).joinpath("swiss_climate_scenarios_20y_mean_monthly_anomaly.nc")
            ) as climate_ref_file:
                climate_anomalies_uri = climate_ref_file
                self.climate_anomalies_uri = str(climate_anomalies_uri)
        else:
            self.climate_anomalies_uri = str(self.climate_anomalies_uri)

        # Eventually could make this open on demand, but usually we want XR to load the metadata on init
        # to reduce latency.
        # Climate data should be 20 year monthly anomalies
        self.monthly_anomalies = xr.open_dataset(self.climate_anomalies_uri)
        self.rcps = list(self.monthly_anomalies.rcp.values)

    def load(self, buildings: GeoDataFrame, year: int | None = None, rcp="rcp45") -> ClimateData | None:
        b = buildings.geometry.to_crs(4326)
        lon, lat = b.x.mean(), b.y.mean()
        return self.load_location(lat=lat, lon=lon, year=year, rcp=rcp)

    def load_location(self, lat=None, lon=None, year: int | None = None, rcp="rcp45") -> ClimateData | None:
        if rcp not in self.rcps:
            raise ValueError(f"rcp should be in {self.rcps}")
        anom = self.monthly_anomalies.sel(lat=lat, lon=lon, year=year, method="nearest", drop=True)
        anom = anom.sel(rcp=rcp, drop=True)

        anom_df = anom.tas.to_dataframe()
        anom_df = anom_df.rename(columns=dict(tas="temp2m_anomaly"))

        tmy = self.tmy_source.load_location(lat=lat, lon=lon, year=year)
        if tmy is None:
            # No data found for this location
            return None

        clim = tmy.timeseries
        if "month" not in clim.columns:
            # must at least have datetime index
            clim["month"] = clim.index.month

        clim = clim.join(anom_df, on="month")
        clim["temp2m_base"] = clim["temp2m"].copy()
        clim["temp2m"] = clim["temp2m"] + clim["temp2m_anomaly"]
        clim["temp2m"] = smooth_16_hours_between_consecutive_months(clim["temp2m"].copy())
        return ClimateData(source=self.name, timeseries=DataFrame[ClimateRecord](clim))
