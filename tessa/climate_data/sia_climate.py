from dataclasses import dataclass

import geopandas as gpd
import numpy as np
import pandas as pd
from geopandas import GeoSeries
from pandera.typing import DataFrame
from pandera.typing.geopandas import GeoDataFrame
from sqlalchemy import text
from sqlalchemy.engine import Engine

from . import calculate_climate_correction, calculate_heating_degree_days
from .common import add_temp_mean_and_bins
from .models import ClimateData, ClimateDataSource, ClimateRecord


def extract_climate_station_data(station, year, data: pd.DataFrame) -> pd.DataFrame:
    """
    If the user does not provide a climate file, this function allows the user to choose
    a weather station and a year to generate a climate. It should be noted that for the moment only the year 2015 is complete.
    The weather data comes from IDAWEB.

    Args:
    station :  These are the 3 initial letters for the choice of the weather station.
               There are 39 weather stations.

    year :     Year in 4 numbers.

    data :     Data from the dictionary containing all parameters of the model.
               It's the output of the load_model_param function.
               model_param['param_clim_data_idaweb_2015']

    Returns:
    DataFrame: With columns 'stationid', 'timestamp', 'year', 'day_of_year',
               'hour_of_day', 'temp2m', 'globalirrad', 'is_h_day_ref_dj_16_20',
               'is_h_day_ref_dj_12_18', 'del_t_real_dj_16_20', 'del_t_real_dj_12_18']
    """
    climate_station = station
    climate_year = year
    climate_data = data
    climate_data = climate_data.loc[climate_data["stationid"] == climate_station]
    climate_data = climate_data.loc[climate_data["year"] == climate_year]
    climate_data = climate_data.copy()
    climate_data = climate_data.sort_index()
    return climate_data


def load_sia_climate(station, year, engine):
    sql = """
    select * from climate.climate_idaweb_2015
    where "StationID" = :station_id
    and year = :year
    order by timestamp
    """
    climate = pd.read_sql(text(sql), engine, params={"station_id": station, "year": year})
    climate.columns = [c.lower() for c in climate.columns]
    # Canonical index is the timestamp, should be already sorted from the query
    climate = climate.set_index("timestamp")
    return climate


def get_climate_station(stationid) -> str:
    """
    Select the SIA climate station for the set of buildings, where a station id column exists
    where buildings span several cantons/cliamte stations, choose the one where there are the
    most buildings associated.

    Args:
    stationid

    Returns:
    str
     SIA climate station id
    """
    station = stationid.value_counts().index[0]
    return station


def load_closest_sia_climate_stations(points: GeoDataFrame | GeoSeries, engine):
    """
    For each point, get the closest climate station
    This uploads the x,y points to a temporary table to do the search,
    therefore, it isn't very fast. The idea is to be very general, e.g. in the
    case you want to upload new buildings, just need the location.

    This uses an sqlalchemy transaction to create and drop the temp table.
    """

    sql = """
    with p as (
        SELECT id, st_setsrid(st_point(x, y), 2056) as point_bld
        FROM pnt)
    SELECT
    id,
        stationid
    FROM p
    CROSS JOIN LATERAL (
      SELECT stationid, point
      FROM climate.clim_sia_stations
      ORDER BY st_distance(clim_sia_stations.point, point_bld)
      LIMIT 1
    ) climate_stations;
    """

    with engine.begin() as conn:
        conn.execute(text("CREATE TEMP TABLE pnt ( id int, x float, y float) ON COMMIT DROP;"))

        # Need to use manual insert b/c using pandas to_sql won't find the temporary table
        # TODO: (d)on't depend on x,y name?
        points = points.geometry.to_crs(2056)
        for idx, row in points.items():
            conn.execute(text("INSERT INTO pnt VALUES (:id, :x, :y)"), dict(id=idx, x=row.x, y=row.y))

        stations = pd.read_sql(sql, conn).set_index("id")
    return stations


@dataclass(kw_only=True)
class SIAClimateData(ClimateDataSource):
    """
    Utility for getting SIA standard climate data
    """

    name = "SIA/Meteosuisse"
    database: Engine

    def load(self, buildings: pd.DataFrame | gpd.GeoDataFrame, year: int | None = 2015) -> ClimateData:
        if year is None:
            year = 2015
        climate_station = self._get_climate_station(buildings)
        temperatures = load_sia_climate(climate_station, year, self.database)
        if len(temperatures) == 0:
            raise RuntimeError("Could not find climate data for buildings")
        temperatures = add_temp_mean_and_bins(temperatures)
        # Decide we don't need the station ID
        temperatures = temperatures.drop("stationid", axis="columns")
        return ClimateData(source=self.name, timeseries=DataFrame[ClimateRecord](temperatures))

    def load_location(self, lat=None, lon=None, year: int | None = 2015) -> ClimateData:
        if year is None:
            year = 2015

        sql = """
        SELECT
            stationid
        FROM (SELECT st_transform(st_setsrid(st_point(:lon, :lat), 4326), 2056) as point_bld) pnt
        CROSS JOIN LATERAL (
          SELECT stationid, point
          FROM climate.clim_sia_stations
          ORDER BY st_distance(clim_sia_stations.point, point_bld)
          LIMIT 1
        ) climate_stations;
        """
        with self.database.begin() as conn:
            climate_station = conn.execute(text(sql), dict(lon=lon, lat=lat)).fetchall()[0][0]

        temperatures = load_sia_climate(climate_station, year, self.database)
        if len(temperatures) == 0:
            raise RuntimeError("Could not find climate data for buildings")
        temperatures = add_temp_mean_and_bins(temperatures)
        # Decide we don't need the station ID
        temperatures = temperatures.drop("stationid", axis="columns")
        return ClimateData(source=self.name, timeseries=DataFrame[ClimateRecord](temperatures))

    def _get_climate_station(self, buildings, station_col="stationid"):
        if station_col in buildings.columns and not (pd.isna(buildings[station_col]).all()):
            climate_station = get_climate_station(buildings["stationid"])
        elif hasattr(buildings, "geometry"):
            stations = load_closest_sia_climate_stations(buildings.geometry, self.database)
            climate_station = get_climate_station(stations.stationid)
        # elif x_col in buildings.columns and y_col in buildings.columns:
        #     # NOTE: this give a list of stations just keep the most 'popular' one
        #     stations = load_closest_sia_climate_stations(buildings.geometry, self.database)
        #     climate_station = get_climate_station(stations.stationid)
        else:
            raise ValueError(
                "Could not find climate station ID or building location x, y columns in input table"
            )
        return climate_station


def load_default_climate_data_file():
    from importlib.resources import as_file, files

    from . import data

    # Default climate
    with as_file(files(data).joinpath("climate_idaweb_2015.csv")) as climate_ref_file:
        default_climates = pd.read_csv(climate_ref_file, sep=",")

    default_climates.columns = [c.lower() for c in default_climates.columns]

    default_climates["is_h_day_ref_dj_16_20"] = np.where(default_climates["temp2m"] <= 16, 1, 0)
    default_climates["is_h_day_ref_dj_12_18"] = np.where(default_climates["temp2m"] <= 12, 1, 0)

    default_climates["del_t_real_dj_16_20"] = (20 - default_climates["temp2m"]) * default_climates[
        "is_h_day_ref_dj_16_20"
    ]
    default_climates["del_t_real_dj_12_18"] = (18 - default_climates["temp2m"]) * default_climates[
        "is_h_day_ref_dj_12_18"
    ]

    default_climates["timestamp"] = pd.to_datetime(default_climates["timestamp"])

    return default_climates.set_index("timestamp")


def load_climate_correction_from_file(station=None, year=2015, climate=None, reference_climate=None):
    """
    Mainly used for testing (to avoid needing the DB)
    Args:
        station:
        year:
        climate:

    Returns:

    """
    clim_data_idaweb = load_default_climate_data_file()
    if climate is None:
        climate = extract_climate_station_data(station, year, clim_data_idaweb)
    real201 = calculate_heating_degree_days(climate["temp2m"])
    if reference_climate is None:
        from ..heat_demand_models.ch_tessa.model_parameters import load_ge_norm_climate

        reference_climate = load_ge_norm_climate()
    # ge201 = ref_climate_data_ge['t_air'].sum()
    ge201 = calculate_heating_degree_days(reference_climate["t_air"])
    clim_correction = calculate_climate_correction(real201, ge201)
    return clim_correction
