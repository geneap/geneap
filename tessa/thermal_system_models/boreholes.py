from collections.abc import Iterable

import numpy as np
import pandas as pd
import pygfunction as gt
import xarray as xr


def calculate_cop(ita, T_h, T_c):
    cop = ita * (T_h + 273.15) / (T_h - T_c)
    return cop


def calculate_cop_chiller(ita, T_h, T_c):
    cop = ita * (T_c + 273.15) / (T_h - T_c)
    return cop


def define_bhe(
    x: list,
    y: list,
    k_s: float,
    H: float,
    T_suf: float,
    T_gradient: float,
    # fluid parameters
    fluid_type,
    fluid_percent,
    *,
    D=2.0,
    r_b=0.065,
    # Pipe dimensions
    r_out=0.020,
    r_in=0.01775,
    D_s=0.0400,
    # r_out=0.0211,
    # r_in=0.0145,
    # D_s=0.040,
    epsilon=1.0e-6,
    k_g=1.0,
    k_p=0.4,
):
    """

    Args:
        x: boreholes x positions
        y: boreholes y positions
        k_s: Soil thermal conductivity (in W/m-K).
        H: Borehole length (m)
        T_suf: Surface undisturbed ground temperature
        T_gradient: temperature gradient (K/m)
        fluid_type: see pygfunction.media
        fluid_percent: Mass fraction of the mixing fluid added to water (in %)
        D: Borehole buried depth from ground surface to top of borehole (m)
        r_b: Borehole radius (m)
        r_out:  Pipe outer radius (m)
        r_in:  Pipe inner radius (m)
        D_s: Shank spacing (m)
        epsilon:  Pipe roughness (m)
        k_g: Grout thermal conductivity (W/m.K)
        k_p: Pipe thermal conductivity (W/m.K)
    Returns:

    """
    # Temperature at Borehole mid point
    T_g = T_suf + T_gradient * H / 2

    # Pipe positions within hole assuming Double U-tube configuration
    tube_pos = [(-D_s, 0.0), (0.0, -D_s), (D_s, 0.0), (0.0, D_s)]

    # ----------------
    # Fluid properties
    # ----------------

    # Total fluid mass flow rate per borehole (kg/s). Equivalent to around 1 m3/h
    m_flow_borehole = 0.28

    # Total fluid mass flow rate (kg/s)
    m_flow_network = m_flow_borehole * len(x)
    fluid = gt.media.Fluid(fluid_str=fluid_type, percent=fluid_percent, T=T_g)

    # Fluid specific isobaric heat capacity (J/kg.K)
    cp_f = fluid.cp

    # Fluid density (kg/m3)
    rho_f = fluid.rho

    # Fluid dynamic viscosity (kg/m.s)
    mu_f = fluid.mu

    # Fluid thermal conductivity (W/m.K)
    k_f = fluid.k

    bore_field = [gt.boreholes.Borehole(H, D, r_b, ix, iy) for ix, iy in zip(x, y)]

    # Pipe thermal resistance
    R_p = gt.pipes.conduction_thermal_resistance_circular_pipe(r_in, r_out, k_p)

    # Fluid to inner pipe wall thermal resistance (Double U-tube in parallel)
    m_flow_pipe = m_flow_borehole / 2
    h_f = gt.pipes.convective_heat_transfer_coefficient_circular_pipe(
        m_flow_pipe, r_in, mu_f, rho_f, k_f, cp_f, epsilon
    )
    R_f = 1.0 / (h_f * 2 * np.pi * r_in)

    # Double U-tube (parallel), same for all boreholes in the bore field
    u_tubes = []
    for borehole in bore_field:
        u_tube = gt.pipes.MultipleUTube(
            tube_pos, r_in, r_out, borehole, k_s, k_g, R_f + R_p, nPipes=2, config="parallel"
        )
        u_tubes.append(u_tube)
    R_b = u_tube.effective_borehole_thermal_resistance(m_flow_borehole, cp_f)
    # Build a network object from the list of UTubes
    network = gt.networks.Network(bore_field, u_tubes, m_flow_network=m_flow_network, cp_f=cp_f)

    return bore_field, network, cp_f, rho_f, R_b


def calculate_R_long_term(
    x: Iterable, y: Iterable, H, n_year, T_suf, T_gradient, fluid_type, fluid_percent, alpha, k_s
):
    """

    Args:
        x: boreholes x positions
        y: boreholes y positions
        H: Borehole length (m)
        n_year:
        T_suf:
        T_gradient:
        alpha:
        k_s: Soil thermal conductivity (in W/m-K).

    Returns:

    """
    # g-Function calculation options
    # nSegments = 8

    bhefield, network, _, _, R_b = define_bhe(x, y, k_s, H, T_suf, T_gradient, fluid_type, fluid_percent)

    g_LT = gt.gfunction.gFunction(bhefield, alpha, n_year * 8760 * 3600.0)
    R_LT = g_LT.gFunc[0] / (2 * np.pi * k_s)
    return R_LT, R_b


def calculate_R_seasonal(alpha, k_s):
    r_b = 0.065
    delta = np.sqrt(alpha * 8760 * 3600 / np.pi)  # penetration depth
    r_pb = r_b * np.sqrt(2) / delta
    R_seasonal = np.sqrt(np.square(np.log(2 / r_pb) - 0.5772) + np.pi * np.pi / 16) / (2 * np.pi * k_s)
    return R_seasonal


def calculate_R_pulse(t_pulse, alpha, k_s):
    # t_pulse unit h
    r_b = 0.065
    R_pulse = (np.log(4 * alpha * t_pulse * 3600 / r_b / r_b) - 0.5772) / (4 * np.pi * k_s)
    return R_pulse


def sizing_bhe(
    bhe_lc_total,
    H_guess,
    x,
    y,
    n_year,
    T_suf,
    T_gradient,
    fluid_type,
    fluid_percent,
    alpha,
    k_s,
    *,
    T_f_max: float,
    T_f_min: float,
    R_seas: float,
    R_pulse: float,
):
    N_bhe = len(x)

    demands_rough = bhe_lc_total.total_rough.copy(deep=True)
    # Only supply 60% of peak by BHE
    # demands_rough = demands_rough.clip(np.nan, demands_rough.max() * 0.6)

    q_mean = demands_rough.mean()
    q_month_h = demands_rough.resample("M").mean().max()
    q_peak_h = demands_rough.max()
    q_month_c = np.minimum(demands_rough.resample("M").mean().min(), 0.0)
    q_peak_c = np.minimum(demands_rough.min(), 0.0)

    # Calculate total length of borehole required
    cap_bhe_h = np.nan
    cap_bhe_c = np.nan
    T_min_1st = np.nan
    T_max_1st = np.nan

    H_old = H_guess
    H_new = 0.0
    n_ite = 0
    while (H_new <= H_old * 0.999) or (H_new >= H_old * 1.001):
        if n_ite > 0:
            H_old = H_new
        R_LT, R_b = calculate_R_long_term(
            x,
            y,
            H=H_old,
            n_year=n_year,
            T_suf=T_suf,
            T_gradient=T_gradient,
            fluid_type=fluid_type,
            fluid_percent=fluid_percent,
            alpha=alpha,
            k_s=k_s,
        )
        T_g = T_suf + T_gradient * H_old / 2
        H_h_ideal = (
            (
                np.maximum(q_mean, 0) * R_LT
                + (q_month_h - np.max(q_mean, 0)) * R_seas
                + (q_peak_h - q_month_h) * R_pulse
                + q_peak_h * R_b
            )
            * 1000
            / (T_g - T_f_min)
            / N_bhe
        )
        # print('q_mean', 'R_LT', 'q_month_h', 'R_seas', 'q_peak_h', 'R_pulse', 'R_b', 'T_g - T_f_min', 'N_bhe')
        # print(q_mean, R_LT, q_month_h, R_seas, q_peak_h, R_pulse, R_b, T_g - T_f_min, N_bhe)
        H_c_ideal = (
            np.absolute(
                np.minimum(q_mean, 0) * R_LT
                + (q_month_c - np.minimum(q_mean, 0)) * R_seas
                + (q_peak_c - q_month_c) * R_pulse
                + q_peak_c * R_b
            )
            * 1000
            / (T_f_max - T_g)
            / N_bhe
        )
        H_new = np.maximum(H_h_ideal, H_c_ideal)
        # print('H_old', H_old, 'H_new', H_new)
        cap_bhe_h = q_peak_h * H_new / H_h_ideal * 1.00  # 1% margin
        cap_bhe_c = q_peak_c * H_new / H_c_ideal * 1.00
        # Extreme borehole wall temperature in the first year, cooresponds to cap_bhe_h
        T_min_1st = T_g - (
            (T_g - T_f_min)
            * ((q_month_h - np.max(q_mean, 0)) * R_seas + (q_peak_h - q_month_h) * R_pulse)
            / (
                np.maximum(q_mean, 0) * R_LT
                + (q_month_h - np.max(q_mean, 0)) * R_seas
                + (q_peak_h - q_month_h) * R_pulse
                # + q_peak_h * R_b
            )
        )
        # coorespond to cap_bhe_c
        T_max_1st = T_g + (
            (T_f_max - T_g)
            * np.absolute((q_month_c - np.minimum(q_mean, 0)) * R_seas + (q_peak_c - q_month_c) * R_pulse)
            / np.absolute(
                np.minimum(q_mean, 0) * R_LT
                + (q_month_c - np.minimum(q_mean, 0)) * R_seas
                + (q_peak_c - q_month_c) * R_pulse
                + q_peak_c * R_b
            )
        )

        n_ite = n_ite + 1
        # print('iteration:', n_ite)
        if n_ite > 10:
            break  # Even though not converged, report the latest version of results
    #             sys.exit('Error! Not able to converge on N_bhe.')

    # cap_bhe_h # capacity for heating, unit:kW
    # cap_bhe_c # capacity for cooling, unit:kW
    # T_min_1st # min T_b at the max heating power during the first year of operation
    # T_max_1st # max T_b at the max cooling power during the first year of operation
    # q_peak_h # Peak heating power
    # q_peak_c
    # qxR_h = (
    # np.maximum(q_mean, 0) * R_LT
    # + (q_month_h - np.max(q_mean, 0)) * R_seas
    # + (q_peak_h - q_month_h) * R_pulse
    # + q_peak_h * R_b
    # )
    # qmeanxRlt_h = np.maximum(q_mean, 0) * R_LT

    return (
        H_new,
        H_h_ideal,
        H_c_ideal,
        R_LT,
        R_b,
        cap_bhe_h,
        cap_bhe_c,
        T_min_1st,
        T_max_1st,
        q_peak_h,
        q_peak_c,
        # qxR_h,
        # qmeanxRlt_h,
    )


def sizing_bhe_for_changing_demands(
    bhe_lc_total_50y,
    H_guess,
    x,
    y,
    n_year,
    T_suf,
    T_gradient,
    fluid_type,
    fluid_percent,
    alpha,
    k_s,
    *,
    T_f_max,
    T_f_min,
    R_seas,
    R_pulse,
):
    N_bhe = len(x)

    demands_rough_50y = bhe_lc_total_50y.total_rough.copy(deep=True)

    # Calculate q_mean, q_month, q_peak
    q_mean_50y = demands_rough_50y.groupby(demands_rough_50y.index.year).mean().values
    demands_rough_50y_monthmean = demands_rough_50y.resample("M").mean()
    q_month_h_50y = demands_rough_50y_monthmean.groupby(demands_rough_50y_monthmean.index.year).max().values
    q_peak_h_50y = demands_rough_50y.groupby(demands_rough_50y.index.year).max().values
    q_month_c_50y = np.minimum(
        0.0, demands_rough_50y_monthmean.groupby(demands_rough_50y_monthmean.index.year).min().values
    )
    q_peak_c_50y = np.minimum(0, demands_rough_50y.groupby(demands_rough_50y.index.year).min().values)

    # Calculate total length of borehole required
    cap_bhe_h = np.nan
    cap_bhe_c = np.nan
    T_min_1st = np.nan
    T_max_1st = np.nan

    H_old = H_guess
    H_new = 0.0
    n_ite = 0
    while (H_new <= H_old * 0.999) or (H_new >= H_old * 1.001):
        if n_ite > 0:
            H_old = H_new
        # Calculate R_lt for 1 to n years
        R_LT_lifespan = np.zeros(n_year)
        for i in range(len(R_LT_lifespan)):
            R_LT_lifespan[i], R_b = calculate_R_long_term(
                x,
                y,
                H=H_old,
                n_year=i + 1,
                T_suf=T_suf,
                T_gradient=T_gradient,
                fluid_type=fluid_type,
                fluid_percent=fluid_percent,
                alpha=alpha,
                k_s=k_s,
            )
        # Calculate T_g
        T_g = T_suf + T_gradient * H_old / 2
        # Evaluate the extreme value of q_x*R_x for each year
        qxR_h_lifespan = np.zeros(n_year)
        qxR_c_lifespan = np.zeros(n_year)
        qmeanxRlt_lifespan = np.zeros(n_year)
        for j in np.arange(0, n_year):
            i_qmean_times_Rlt = q_mean_50y[0] * R_LT_lifespan[j - n_year]
            for i in np.arange(1, j + 1):  # step-wise q_mean
                i_qmean_times_Rlt = (
                    i_qmean_times_Rlt + (q_mean_50y[i] - q_mean_50y[i - 1]) * R_LT_lifespan[-i + j - n_year]
                )
            qmeanxRlt_lifespan[j] = i_qmean_times_Rlt
            qxR_h_lifespan[j] = (
                np.maximum(i_qmean_times_Rlt, 0)
                + (q_month_h_50y[j] - np.maximum(q_mean_50y[j], 0)) * R_seas
                + (q_peak_h_50y[j] - q_month_h_50y[j]) * R_pulse
                + q_peak_h_50y[j] * R_b
            )
            qxR_c_lifespan[j] = (
                np.minimum(i_qmean_times_Rlt, 0)
                + (q_month_c_50y[j] - np.minimum(q_mean_50y[j], 0)) * R_seas
                + (q_peak_c_50y[j] - q_month_c_50y[j]) * R_pulse
                + q_peak_c_50y[j] * R_b
            )
        # Evaluate H_ideal
        H_h_ideal = qxR_h_lifespan.max() * 1000 / (T_g - T_f_min) / N_bhe
        H_c_ideal = qxR_c_lifespan.max() * 1000 / (T_f_max - T_g) / N_bhe
        H_new = np.maximum(H_h_ideal, H_c_ideal)
        # print('H_old', H_old, 'H_new', H_new)
        cap_bhe_h = q_peak_h_50y.max() * H_new / H_h_ideal * 1.00
        cap_bhe_c = q_peak_c_50y.min() * H_new / H_c_ideal * 1.00
        # Extreme borehole wall temperature in the first year, cooresponds to cap_bhe_h
        T_min_1st = np.nan
        # coorespond to cap_bhe_c
        T_max_1st = np.nan

        n_ite = n_ite + 1
        # print('iteration:', n_ite)
        if n_ite > 10:
            break  # Even though not converged, report the latest version of results
    #             sys.exit('Error! Not able to converge on N_bhe.')

    # cap_bhe_h # capacity for heating, unit:kW
    # cap_bhe_c # capacity for cooling, unit:kW
    # T_min_1st # min T_b at the max heating power during the first year of operation
    # T_max_1st # max T_b at the max cooling power during the first year of operation
    # q_peak_h # Peak heating power
    # q_peak_c
    return (
        H_new,
        H_h_ideal,
        H_c_ideal,
        R_LT_lifespan[-1],
        R_b,
        cap_bhe_h,
        cap_bhe_c,
        T_min_1st,
        T_max_1st,
        q_peak_h_50y.max(),
        q_peak_c_50y.min(),
        # qxR_h_lifespan,
        # qmeanxRlt_lifespan,
        T_g,
    )


def sim_bhe_zarrella_ht(
    load_curve_orig,
    T_suf,
    T_gradient,
    fluid_type,
    fluid_percent,
    x,
    y,
    H_bhe,
    alpha,
    k_s,
    cap_bhe_h,
    cap_bhe_c,
    q_peak_h,
    q_peak_c,
    delta_T_dhc,
    timestep,
    ita_wshp,
    T_load_ht,
    T_load_lt,
    T_load_c,
):
    """
    This version set a minimum flow rate
    Cimmino's assumption for no flow condition:
    At all times when the heat pump is turned off, the mass flow rate is reduced to 5% of its nominal value.

    Args:
        load_curve_orig: demand curve
        T_suf:
        T_gradient:
        x: borehole x position
        y: borehole y position
        H_bhe: borehole length;
        alpha:
        k_s:
        cap_bhe_h: Capacity for heating of BHE fields;
        cap_bhe_c: Capacity for cooling of BHE fields;
        q_peak_h:
        q_peak_c:
        delta_T_dhc: temperature difference between warm/cold pipe
        timestep: size of time step in h.
        ita_wshp: carnot efficiency of water source heat pump

    Returns:

    """
    # R_LT: long-term ground thermal resistence; B: Borehole spacing;
    # t2m: ambient temperature profile

    # -------------------------------------Scale the load curve---------------------------------
    # Scale to one bhe field with the same R_LT. This will reduce the computational time.
    # Applying scaling only introduced a relative change of less than 1E-5 in Q_bhe.
    # Without this line, 'load_curve_orig' will be changed.
    load_curve = load_curve_orig.copy(deep=True)
    load_curve = load_curve * 1000.0  # kw to w

    max_bhe_h = cap_bhe_h * 1000.0
    max_bhe_c = cap_bhe_c * 1000.0
    # Nominal heat exchange rate
    nom_bhe_hc = np.maximum(q_peak_h, np.absolute(q_peak_c)) * 1000

    # ------------------------------------Define parameters-------------------------------
    dt = 3600 * timestep  # size of time step in s
    tmax = dt * len(load_curve.index)  # simulation period in s
    Nt = len(load_curve.index)  # Number of time steps
    # time = dt * np.arange(1, Nt + 1)  # array of time
    timedelta = pd.Timedelta(timestep, "h")

    # Load aggregation scheme
    load_agg = gt.load_aggregation.ClaessonJaved(dt, tmax)

    # Define BHE field
    bore_field, network, cp_f, rho_f, _ = define_bhe(
        x, y, k_s, H_bhe, T_suf, T_gradient, fluid_type, fluid_percent
    )
    n_boreholes = len(bore_field)

    # Get time values needed for g-function evaluation
    time_req = load_agg.get_times_for_simulation()

    # g-Function calculation
    options = {"nSegments": 8, "disp": True}
    gFunc = gt.gfunction.gFunction(bore_field, alpha, time=time_req, options=options)
    # Initialize load aggregation scheme
    load_agg.initialize(gFunc.gFunc / (2 * np.pi * k_s))

    # Nominal mass flow rate
    nom_m_flow_bhe_hc = nom_bhe_hc / (cp_f * delta_T_dhc)
    # print('nom_m_flow_bhe_hc', nom_m_flow_bhe_hc)

    # Initialize
    time_range = pd.Index(load_curve.index)
    time_range_plus1 = time_range.append(pd.Index([time_range[-1] + timedelta]))
    T_b = pd.Series(data=np.zeros(Nt + 1), index=time_range_plus1)
    T_wp = pd.Series(data=np.zeros(Nt + 1), index=time_range_plus1)
    T_cp = pd.Series(data=np.zeros(Nt + 1), index=time_range_plus1)
    Q_dhc = pd.Series(data=np.zeros(Nt), index=time_range)
    Q_circu = pd.Series(data=np.zeros(Nt), index=time_range)
    Q_bhe = pd.Series(data=np.zeros(Nt), index=time_range)
    Q_remain = pd.Series(data=np.zeros(Nt), index=time_range)
    m_flow_bhe = pd.Series(data=np.zeros(Nt), index=time_range)
    E_decen_hp = pd.Series(data=np.zeros(Nt), index=time_range)
    E_decen_chiller = pd.Series(data=np.zeros(Nt), index=time_range)

    T_g = T_suf + T_gradient * H_bhe / 2
    T_b.iloc[0] = T_g
    T_wp.iloc[0] = T_g
    T_cp.iloc[0] = T_g - delta_T_dhc
    # load_curve['p_bld'] = load_curve['p_ht'] * np.nan

    # ------------------------------------------------
    # Solve operational parameters for each time step
    i_time = dt
    i_mode = 1.0  # 1 indicates flow direction for heating, -1 indicates cooling.
    for timestamp in time_range:
        # print the current time step
        if (
            ((timestamp.day == 1) or (timestamp.day == 11) or (timestamp.day == 21))
            and (timestamp.hour == 0)
            and (timestamp.minute == 0)
            and (timestep < 24)
        ):
            # Mark every 10-day
            print(timestamp)
        elif (timestamp.month == 1) & (timestamp.day < 6) & (timestep >= 24):
            print(timestamp)

        # Increment time step by (1)
        load_agg.next_time_step(i_time)
        i_time = i_time + dt

        n_ite = 0

        # Initialize T_wp to make sure T_wp doesn't converge in the begining
        T_wp_new = T_wp[timestamp] + 1

        # load_curve_row = load_curve.loc[load_curve.index == timestamp]
        load_curve_row = load_curve.loc[timestamp]

        while np.absolute(T_wp[timestamp] - T_wp_new) >= 0.005:
            # print('No. iteration:', n_ite)
            if n_ite > 0:  # updating
                T_wp[timestamp] = T_wp_new
                T_cp[timestamp] = T_wp[timestamp] - delta_T_dhc
            # print('T_wp_old', T_wp[i])
            # Calculat COP and electricity used by BHPs
            COP_ht = calculate_cop(ita_wshp, T_load_ht, T_wp[timestamp])
            COP_ht = np.minimum(7.0, COP_ht)
            COP_lt = calculate_cop(ita_wshp, T_load_lt, T_wp[timestamp])
            COP_lt = np.minimum(7.0, COP_lt)
            # if T_cp[i] <= T_load_c, free cooling; otherwise, use chiller(HP) to cool water to T_load_c.
            if T_cp[timestamp] <= T_load_c:
                COP_chiller = np.inf
            else:
                COP_chiller = calculate_cop_chiller(ita_wshp, T_cp[timestamp], T_load_c)
                COP_chiller = np.minimum(6.0, COP_chiller)

            E_decen_hp[timestamp] = load_curve_row.bld_h_ht / COP_ht + load_curve_row.bld_h_lt / COP_lt
            E_decen_chiller[timestamp] = load_curve_row.bld_c / COP_chiller

            Q_dhc[timestamp] = (
                load_curve_row.bld_h_ht * (COP_ht - 1) / COP_ht
                + load_curve_row.bld_h_lt * (COP_lt - 1) / COP_lt
                + load_curve_row.bld_c * (1 + 1 / COP_chiller)
                + load_curve_row.cen_c
            )
            Q_circu[timestamp] = np.absolute(Q_dhc[timestamp]) * 0.03
            Q_dhc[timestamp] = Q_dhc[timestamp] - Q_circu[timestamp]
            # print('Q_dhc', Q_dhc[i], 'Threshold', nom_bhe_hc * 0.05)

            # Find the flow direction
            if (Q_dhc[timestamp] <= nom_bhe_hc * 0.01) & (
                Q_dhc[timestamp] >= -nom_bhe_hc * 0.01
            ):  # No demand mode
                # print('No demand mode')
                # Not change i_mode, remain the same as last time step
                Q_remain[timestamp] = 0.0
                m_flow_bhe[timestamp] = nom_m_flow_bhe_hc * 0.05 * i_mode
                Q_b = Q_dhc[timestamp] / n_boreholes
            elif (Q_dhc[timestamp] <= nom_bhe_hc * 0.05) & (
                Q_dhc[timestamp] > nom_bhe_hc * 0.01
            ):  # Low heating mode
                # print('Low heating mode')
                i_mode = 1.0
                Q_remain[timestamp] = 0.0
                m_flow_bhe[timestamp] = nom_m_flow_bhe_hc * 0.05 * i_mode
                Q_b = Q_dhc[timestamp] / n_boreholes
            elif (Q_dhc[timestamp] >= -nom_bhe_hc * 0.05) & (
                Q_dhc[timestamp] < -nom_bhe_hc * 0.01
            ):  # Low cooling mode
                # print('Low cooling mode')
                i_mode = -1.0
                Q_remain[timestamp] = 0.0
                m_flow_bhe[timestamp] = nom_m_flow_bhe_hc * 0.05 * i_mode
                Q_b = Q_dhc[timestamp] / n_boreholes
            elif Q_dhc[timestamp] > nom_bhe_hc * 0.05:  # Normal heating mode
                # print('Normal heating mode')
                i_mode = 1.0
                Q_remain[timestamp] = np.maximum(Q_dhc[timestamp] - max_bhe_h, 0.0)
                m_flow_bhe[timestamp] = np.minimum(Q_dhc[timestamp], max_bhe_h) / cp_f / delta_T_dhc
                Q_b = np.minimum(Q_dhc[timestamp], max_bhe_h) / n_boreholes
            elif Q_dhc[timestamp] < -nom_bhe_hc * 0.05:  # Normal cooling mode
                # print('Normal cooling mode')
                i_mode = -1.0
                Q_remain[timestamp] = np.minimum(Q_dhc[timestamp] - (-max_bhe_c), 0.0)
                m_flow_bhe[timestamp] = np.maximum(Q_dhc[timestamp], -max_bhe_c) / cp_f / delta_T_dhc
                Q_b = np.maximum(Q_dhc[timestamp], -max_bhe_c) / n_boreholes
            # print('i_mode',  i_mode)
            # print('m_flow_bhe',  m_flow_bhe[i])
            # print('Q_bhe_planned', Q_b*nBoreholes)

            # Solve Q_bhe
            # NOTE get_network_heat_extraction_rate consumes 90% of the time of running this fn.
            if i_mode == 1.0:  # Flow direction is for heating
                load_agg.set_current_load(Q_b / H_bhe)  # Evaluate borehole wall temperature
                deltaT_b = load_agg.temporal_superposition()
                T_b[timestamp] = T_g - deltaT_b
                T_cp[timestamp] = T_wp[timestamp] - delta_T_dhc
                Q_bhe[timestamp] = network.get_network_heat_extraction_rate(
                    T_cp[timestamp],
                    T_b[timestamp],
                    m_flow_bhe[timestamp],
                    cp_f,
                    nSegments=1,
                    segment_ratios=None,
                )
                T_wp_new = (
                    T_cp[timestamp] + Q_bhe[timestamp] / cp_f / m_flow_bhe[timestamp]
                )  # Evaluate outlet fluid temperature
            else:  # Flow direction is for cooling
                load_agg.set_current_load(Q_b / H_bhe)  # Evaluate borehole wall temperature
                deltaT_b = load_agg.temporal_superposition()
                T_b[timestamp] = T_g - deltaT_b
                # Evaluate borehole heat exchange
                Q_bhe[timestamp] = network.get_network_heat_extraction_rate(
                    T_wp[timestamp],
                    T_b[timestamp],
                    (-m_flow_bhe[timestamp]),
                    cp_f,
                    nSegments=1,
                    segment_ratios=None,
                )
                T_cp[timestamp] = T_wp[timestamp] + Q_bhe[timestamp] / cp_f / (-m_flow_bhe[timestamp])
                T_wp_new = T_cp[timestamp] + delta_T_dhc  # Evaluate outlet fluid temperature
            # print('T_cp',T_cp[i])
            # print('T_b',T_b[i])
            # print('Q_bhe',Q_bhe[i])
            # print('T_wp_new',T_wp_new)

            n_ite = n_ite + 1  # Count number of iteration
            if n_ite == 5:
                # print('timestep', i, 'n_ite=5', 'T_wp_old', T_wp[i], 'T_wp_new', T_wp_new)
                break

        # After T_wp converge or n_ite > set value
        # print('Converge T_wp', T_wp_new)
        T_wp[timestamp] = T_wp_new
        T_cp[timestamp] = T_wp_new - delta_T_dhc
        # T_wp[i+timedelta] = T_wp_new # Get wrong when the delta is not constant (e.g. month).
        T_wp.iloc[T_wp.index.get_loc(timestamp) + 1] = T_wp_new
        # T_cp[i+timedelta] = T_wp_new - delta_T_dhc
        T_cp.iloc[T_cp.index.get_loc(timestamp) + 1] = T_wp_new - delta_T_dhc

    # Save results
    results = xr.Dataset(
        data_vars=dict(
            p_h=(["time"], np.zeros(len(load_curve.index))),
        ),
        coords=dict(time=load_curve.index.values),
    )
    results["p_bld_h_ht"] = ("time", load_curve.bld_h_ht.values / 1000)
    results["p_bld_h_lt"] = ("time", load_curve.bld_h_lt.values / 1000)
    results["p_bld_c"] = ("time", load_curve.bld_c.values / 1000)
    results["p_cen_c"] = ("time", load_curve.cen_c.values / 1000)
    results["Q_dhc"] = ("time", Q_dhc / 1000)
    results["Q_circu"] = ("time", Q_circu / 1000)
    results["Q_bhe"] = ("time", Q_bhe / 1000)
    results["Q_remain"] = ("time", Q_remain / 1000)
    results["E_decen_hp"] = ("time", E_decen_hp / 1000)
    results["E_decen_chiller"] = ("time", E_decen_chiller / 1000)
    results["m_flow_bhe"] = ("time", m_flow_bhe)
    results["T_b"] = ("time", T_b[:-1])
    results["T_wp"] = ("time", T_wp[:-1])
    results["T_cp"] = ("time", T_cp[:-1])
    results["H_bhe"] = ([], H_bhe)
    results["Cap_decen_hp"] = ([], ((load_curve.bld_h_ht + load_curve.bld_h_lt).max()))
    results["Time_step_h"] = ([], dt / 3600)

    return results
