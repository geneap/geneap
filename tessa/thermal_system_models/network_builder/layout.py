import logging
import uuid
from dataclasses import dataclass
from typing import TYPE_CHECKING

import geopandas as gpd
import numpy as np
import pandas as pd
import shapely
import sparse
import structlog
import xarray as xr
from _operator import itemgetter
from pandera.typing.geopandas import GeoDataFrame, GeoSeries
from xarray import DataArray

from tessa import sparse_graph
from tessa.road_network_routing import BuildingRouting
from tessa.thermal_system_models import DistrictNetwork

from ..models import BuildingConnections, Pipes
from .common import _ApplyAllMixin, select_district_network

if TYPE_CHECKING:
    from tessa.district import District

log = structlog.get_logger()


def _filter_graph_components(labels, n_components):
    components_to_keep = []
    for component_idx in range(n_components):
        sel = labels == component_idx
        size = sum(sel)
        # Skip if this for 'non-networks'
        if size <= 2:
            continue
        else:
            components_to_keep.append((component_idx, size))
    # Sort largest last so largest net overrides.
    components_to_keep = sorted(components_to_keep, key=itemgetter(1), reverse=True)
    return components_to_keep


def update_building_heat_source(building_ids, buildings):
    buildings.loc[building_ids, "thermal_generator_sh"] = "heat_exchanger"
    buildings.loc[building_ids, "thermal_generator_dhw"] = "heat_exchanger"
    buildings.loc[building_ids, "fuel_sh"] = None
    buildings.loc[building_ids, "fuel_dhw"] = None
    buildings.loc[building_ids, "heat_sh_efficiency"] = 0.99
    buildings.loc[building_ids, "heat_dhw_efficiency"] = 0.99
    buildings.loc[building_ids, "q_h_final_kwh"] = (
        buildings.loc[building_ids, "q_h_kwh"] / buildings.loc[building_ids, "heat_sh_efficiency"]
    )
    buildings.loc[building_ids, "q_ww_final_kwh"] = (
        buildings.loc[building_ids, "q_ww_kwh"] / buildings.loc[building_ids, "heat_dhw_efficiency"]
    )
    buildings.loc[building_ids, "q_hww_final_kwh"] = (
        buildings.loc[building_ids, "q_h_final_kwh"] + buildings.loc[building_ids, "q_ww_final_kwh"]
    )
    # Set the CO2 direct emissions to 0
    # Note this will be different for 5G with HP.
    buildings.loc[building_ids, "co2eq_h_kg"] = 0.0
    buildings.loc[building_ids, "co2eq_ww_kg"] = 0.0
    buildings.loc[building_ids, "co2eq_hww_kg"] = 0.0

    return buildings


@dataclass
class DnMstLayout(_ApplyAllMixin):
    """
    Builder for generating thermal network pipe layouts using Minimum Spanning Tree (MST)
    algorithm.

    Has methods to create a new district network object or to update an existing
    one adding the layout graph.

    """

    routing_service: BuildingRouting
    max_distance: float = 1000
    name: str = "district_heating"

    # TODO: extra ways should really be supplied to the routing service as an overlay
    ways: list | None = None

    # Usually we want to load pipe routes but keep this option exceptional cases
    #  one day might remove because we almost never use it.
    _load_pipes: bool = True

    def create(
        self,
        district: "District",
        *,
        name="district_heating",
        **kwargs,
    ) -> "District":
        """
        Can return several networks in the case the layout system
        con't connect every building in one, although in most cases
        expect a single result.

        Args:
            district:
            ways:
            name:

        Returns:

        """
        buildings = district.buildings
        points = buildings.geometry.copy()  # use native CRS
        ways = self.ways

        distances: DataArray = self.routing_service.load_distance_matrix(points, ways=ways)
        routes: DataArray = sparse_graph.fast_minimum_spanning_tree(
            distances, points=None, max_distance=self.max_distance
        )
        routes.attrs["units"] = "meters"

        # resulting route network might not be fully connected
        n_components, labels = sparse_graph.connected_components(routes, directed=False)
        components_to_keep = _filter_graph_components(labels, n_components)

        networks = []
        if name is None:
            name = self.name

        for i, (component_idx, component_size) in enumerate(components_to_keep):
            if n_components > 1:
                subgraph_name = f"{name}_{i}"
            else:
                subgraph_name = name
            sel = labels == component_idx
            # Use a two-step selection because of sparse package limitations
            subgraph_routes = routes[sel, :][:, sel].to_dataset(name="routes")
            building_ids = routes.id_from.to_numpy()
            if self._load_pipes:
                buildings = district.buildings_table(building_ids)
                ways_map = None
                if ways is not None:
                    ways_map = self.routing_service.find_points_on_ways(buildings.geometry, ways)
                pipes = self.load_network_pipes(buildings.geometry, subgraph_routes, extra_ways=ways_map)
                building_connections = self.load_building_connections(buildings.geometry, extra_ways=ways_map)
            else:
                pipes = None
                building_connections = None
            building_ids = subgraph_routes.id_from.to_numpy().tolist()

            network = DistrictNetwork(
                building_ids=building_ids,
                graph=subgraph_routes,
                pipes=pipes,
                building_connections=building_connections,
                name=subgraph_name,
                power_demand_curve=None,
                **kwargs,
            )
            networks.append(network)

            # UPDATE BUILDINGS
            update_building_heat_source(building_ids, buildings)

        # Update the district object with the new buildings and return a new DISTRICT
        utility_networks = district.utility_networks + networks
        district_new = district.model_copy(
            update=dict(buildings=buildings, utility_networks=utility_networks), deep=True
        )
        return district_new

    def update(
        self,
        district: "District",
        district_network: str | uuid.UUID | None = None,
        # *,
        # ways=None,
    ) -> "District":
        """
        Update a district network or all the district networks within a district with
        and MST layout thermal grid

        Args:
            district:
            district_network:
            ways:

        Returns:

        """
        dn = select_district_network(district, district_network)
        if dn is None:
            return self._apply_to_all_district_networks(district)

        buildings = district.buildings
        points = buildings.geometry.copy()  # use native CRS

        graph = dn.graph
        if graph is None or "routes" not in graph.data_vars or self.ways:
            # Need to force an update if ways have been set and potentially changed, this means
            # also invalidating the rest of the calculated parameters
            # TODO smarter layout engine that can detect if ways have changed, provides a distance matrix
            #  as the "service" taking into account all factors like custom draws, obstacles, etc.
            distances: DataArray = self.routing_service.load_distance_matrix(points, ways=self.ways)
            routes: DataArray = sparse_graph.fast_minimum_spanning_tree(
                distances, points=None, max_distance=self.max_distance
            )
            graph = routes.to_dataset(name="routes")

        elif set(graph["routes"].id_from.data) != set(buildings.index.values):
            # TODO: if generated net doesn't cover all previous buildings then running update
            #   will always end up here even if the district hasn't changed
            # If there are new points in the buildings, extend the routes with new distances
            routes = graph["routes"]
            distances = self.extend_routes_with_distance(routes, points, extra_ways=self.ways)
            # distances: DataArray = self.routing_service.load_distance_matrix(points, ways=ways)

            routes = sparse_graph.fast_minimum_spanning_tree(
                distances, points=None, max_distance=self.max_distance
            )
            # Invalidate the previous graph with new routes
            # graph["routes"] = routes
            graph = routes.to_dataset(name="routes")

        else:
            # Reuse the existing routes when updating an existing network with new parameters
            routes = graph["routes"]
        routes.attrs["units"] = "meters"

        # resulting route network might not be fully connected, BUT IT SHOULD BE
        n_components, labels = sparse_graph.connected_components(routes, directed=False)
        if n_components > 1:
            log.warning(
                f"Unexpected subgraphs found in update, keeping only largest component of {n_components}"
            )
            components_to_keep = _filter_graph_components(labels, n_components)
            component_idx = components_to_keep[0][0]
            sel = labels == component_idx
            routes = routes[sel, :][:, sel]
            graph["routes"] = routes

        building_ids = routes.id_from.to_numpy()
        if self._load_pipes:
            # This will force a reload of pipes .
            buildings = district.buildings_table(building_ids)
            ways_map = None
            if self.ways is not None:
                ways_map = self.routing_service.find_points_on_ways(buildings.geometry, self.ways)
            pipes = self.load_network_pipes(buildings.geometry, routes, extra_ways=ways_map)
            building_connections = self.load_building_connections(buildings.geometry, extra_ways=ways_map)
        else:
            pipes = None
            building_connections = None
        building_ids = routes.id_from.to_numpy().tolist()

        network = dn.model_copy(
            update=dict(
                building_ids=building_ids,
                graph=graph,
                pipes=pipes,
                building_connections=building_connections,
            ),
            deep=True,
        )

        # UPDATE BUILDINGS
        buildings = update_building_heat_source(building_ids, buildings)

        # Don't use update network function because we also update buildings in this case.
        utility_networks = [u for u in district.utility_networks if u.uid != network.uid]
        utility_networks += [network]
        return district.model_copy(
            update=dict(buildings=buildings, utility_networks=utility_networks), deep=True
        )

    def extend_routes_with_distance(self, routes, points: pd.Series, *, extra_ways=None):
        """
         - Load all points in buildings (this gets too much extra data but fine for now)
         - Delete point PAIRS that are in exsiting routes from loaded matrix
         (since want to keep distances to new points can't just delete existing points)
         - Merge with the previously loaded routes graph
        Args:
            routes:
            points:
            extra_ways:

        Returns:

        """
        orig_ids = routes.id_from.data

        new_ids = points.drop(orig_ids, errors="ignore").index.values

        distances = self.routing_service.load_distance_matrix(points, ways=extra_ways)

        # select subset of data only going to or from new ids.
        a = sparse_graph.sparse_2d_dataarray_to_dict(distances.sel(id_from=new_ids))
        b = sparse_graph.sparse_2d_dataarray_to_dict(distances.sel(id_to=new_ids))
        o = a | b

        # Add the original routes
        c = sparse_graph.sparse_2d_dataarray_to_dict(routes)
        o = o | c

        # Convert into sparse array
        indexes = np.array(list(o.keys()))
        data = np.fromiter(o.values(), float, count=len(o))

        coords = np.vstack(
            [
                distances.coords.indexes["id_from"].get_indexer_for(np.ravel(indexes.T[0])),
                distances.coords.indexes["id_to"].get_indexer_for(np.ravel(indexes.T[1])),
            ]
        )

        merged_distances = xr.DataArray(
            sparse.COO(coords, data=data, shape=distances.shape), coords=distances.coords, dims=distances.dims
        )
        return merged_distances

    def load_network_pipes(self, points, routes, *, extra_ways=None) -> GeoDataFrame[Pipes]:
        """
        Load the network geometry geojson for the given thermal network object.
        This is not done automatically because it takes time, so we return numeric information first
        then return graphic repr.
        """
        return GeoDataFrame[Pipes](
            self.routing_service.load_routing_geometry(points, routes, extra_ways=extra_ways)
        )

    def load_building_connections(
        self, building_points: GeoSeries | GeoDataFrame, *, extra_ways=None
    ) -> GeoDataFrame[BuildingConnections]:
        """
        Load the geometries for the connections between building centroids and the closest bit of road

        Args:
            building_points:
            extra_ways:

        Returns:
            GeoDataFrame with columns geom and index same as building points

        """
        points = building_points.geometry
        # columns point_id, routing_vertex_id, point
        connections = self.routing_service.routing_vertex_for_points(points)
        crs = connections.crs
        connections["building_point"] = points  # should join on index

        if extra_ways:
            if not isinstance(extra_ways, dict):
                extra_ways = self.routing_service.find_points_on_ways(points, extra_ways)
            for (bld_a, bld_b), line in extra_ways.items():
                first_point = shapely.geometry.Point(line.coords[0])
                last_point = shapely.geometry.Point(line.coords[-1])
                connections.loc[bld_a, "point"] = first_point
                connections.loc[bld_b, "point"] = last_point

        connections["geom"] = connections.apply(
            lambda row: shapely.LineString([row["building_point"], row["point"]]), axis=1
        )
        connections = connections.drop(columns=["point", "building_point"], axis=1).set_geometry("geom")
        connections = connections.set_crs(crs)
        return GeoDataFrame[BuildingConnections](connections)
