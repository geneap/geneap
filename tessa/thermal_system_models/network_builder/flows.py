import uuid
from dataclasses import dataclass, field
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import sparse
import structlog
import xarray as xr
from pandera.typing import DataFrame

from tessa import sparse_graph

from ..models import DistrictNetwork, FlowProperties
from ..pipes import (
    calc_flow_properties,
    calc_fluid_velocity,
    find_pipe_dn,
    find_pipe_dn_vectorised,
    load_steel_pipe_dn_table,
)
from .common import _ApplyAllMixin, select_district_network, update_district_with_network

if TYPE_CHECKING:
    from tessa.district import District

log = structlog.get_logger()


def _flow_data_for_buildings(p_max, *, fluid_density, fluid_heat_capacity, delta_t):
    """
    Base table for calculating flow properties at each building

    Args:
        p_max: maximum power demand of buildings (usually p_hww_max1h_kw, but could also use p_hww_max1d_kw)
        fluid_density: kg/m3
        fluid_heat_capacity: in J/(kg*K)
        delta_t:

    Returns:
    p_hww_max1h_kw, m_dot_kg_s
    """
    # Get average power per building and fill missing values
    # TODO: (n)ot sure if this should be part of flow calculation
    average_p_hww = p_max[p_max != 0].mean()

    # Replace zeros with the average value
    p_max = p_max.fillna(average_p_hww)
    p_max = p_max.copy()
    # p_hww_max1h_kw.loc[p_hww_max1h_kw == 0] = average_p_hww
    flow = pd.DataFrame(index=p_max.index)
    # Find the mass flow rate for each building in the grid [kg/s], note
    # conversion of the fluid heat capacity.
    flow["m_dot_kg_s"] = p_max / (fluid_heat_capacity * delta_t / 1000)
    # buildings.loc[:, 'm_dot_kg_s'] = buildings['p_hww_max1h_kw'] / (Cp * ΔT) # [kg/s]
    # Find the volumetric flow rate for each building in the grid [m3/s]
    flow["v_dot_m3_s"] = flow["m_dot_kg_s"] / fluid_density
    return flow


def _building_total_head(buildings, fluid_density, max_headloss=68947.6):
    """
    Calculate the total head loss for each building as the sum of the headloss
    from the connecting pipe and the head loss in the heat exchange unit.

    Allowable pressure drop in the heat exchangers is 10 [psi] or 69 [kPa];
    So, max headloss (h = P/ρg) in each building's heat exchangers would be:
    h_b_m = 0.5 * (max_headloss / (fluid_density * 9.8067)) [m]

    Args:
        buildings:
        fluid_density:
        max_headloss: Allowable pressure drop in the heat exchangers is 10 [psi] or 69 [kPa];

    Returns:

    """

    # TODO: (w)hy the factor 0.5?
    # Calculate the heat exchanger potential head loss.
    h_b_m = 0.5 * (max_headloss / (fluid_density * 9.8067))  # [m]

    # Modify buildings_data_table
    buildings["h_b_m"] = h_b_m
    # Total head loss as the sum of the supply pipe loss and the heat exchanger.
    buildings["total_h_b_m"] = buildings["h_f_dw_m"] + buildings["h_b_m"]
    buildings.loc[buildings["velocity_m_s"] == 0, "h_b_m"] = 0

    return buildings


class _PrepFlowMixin:
    def _prep_fluid_properties(self, district_network: DistrictNetwork):
        if district_network is not None and district_network.flow is not None:
            return district_network.flow.model_copy(deep=True)
        else:
            return self.fluid_properties.model_copy(deep=True)


@dataclass
class FlowSizing(_PrepFlowMixin, _ApplyAllMixin):
    """
    Size pipes by optimizing pipe inner diameter and calculate flow properties.
    """

    nominal_diameter_table: DataFrame = field(default_factory=load_steel_pipe_dn_table)
    fluid_properties: FlowProperties = field(default=FlowProperties())
    nominal_diameter_offset: int = 0

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        """
        Args:
            district:
            district_network:

        Returns:

        """
        network = select_district_network(district, district_network)
        if district_network is None:
            return self._apply_to_all_district_networks(district)

        if network is None or network.graph is None:
            log.warning("Attempting to calculate flows without graph data")
            return district

        fluid = self._prep_fluid_properties(network)
        graph = network.graph.copy()
        graph_table = network.graph_table
        # early exit if required params no in input data
        if "power_total" not in graph:
            log.warning("power_total variable not found, is required to calculate flow properties.")
            return district

        nnz = len(graph_table)

        n_tubes = network.pipe_spec.n_tubes
        if n_tubes == 1:
            # Try to pick a pipe where the max v < 4m/s,
            graph_table["m_dot_kg_s"] = graph_table["power_total"] / (
                fluid.heat_capacity * fluid.delta_t / 1000
            )
            graph_table["v_dot_m3_s"] = graph_table["m_dot_kg_s"] / fluid.density
            v_dot_m3_s = graph_table["v_dot_m3_s"].fillna(graph_table["v_dot_m3_s"].mean()).values

            nominal_diameter_lookup = self.nominal_diameter_table.sort_values("dn", ascending=True)
            # idx = nominal_diameter_lookup.index[-1]
            # Iterate in descending order while velocity is less than min_velocity
            for row in nominal_diameter_lookup.itertuples():
                dn = row.dn
                inner_diameter = row.id_mm
                # idx = row.Index
                velocity_m_s = calc_fluid_velocity(inner_diameter, np.abs(v_dot_m3_s))
                if np.all((velocity_m_s >= 1) & (velocity_m_s <= 4)):
                    break
            else:
                # If no smaller consensus pipe is found, just use the max
                dn, inner_diameter = find_pipe_dn(
                    np.nanmax(v_dot_m3_s), self.nominal_diameter_table, dn_offset=self.nominal_diameter_offset
                )
            dn = np.ones(nnz) * dn
            inner_diameter = np.ones(nnz) * inner_diameter
            graph_table["dn"] = dn
            graph_table["inner_diameter_mm"] = inner_diameter
            graph_table["fluid_volume_m3"] = graph_table["routes"] * np.pi * (graph_table["inner_diameter_mm"] / 1000) ** 2 / 4
            # Set peak velocity constant since it has to be same everywhere.
            graph_table["velocity_m_s"] = calc_fluid_velocity(inner_diameter, np.nanmax(v_dot_m3_s))

            # TODO: (t)his doesn't work for 1 pipe because flow has to be constant through
            #  loop, but could be a start for optimising pipe sections to reduce number of
            #  DN changes.
            # Calculate the dns for all the pipes as if it was a duel pipe system,
            # then find the dn such that velocities are between 1 and 4. If none is found
            # revert to the max dn required at any point.
            # dn, inner_diameter = find_pipe_dn_vectorised(
            #     v_dot_m3_s, self.nominal_diameter_table, dn_offset=self.nominal_diameter_offset
            # )
            # velocity_m_s = calc_fluid_velocity(inner_diameter, v_dot_m3_s)
            # lookup = pd.DataFrame({"dn": dn, "inner_diameter_mm": inner_diameter, "velocity_m_s": velocity_m_s})
            # search = lookup[(lookup.velocity_m_s >= 1) & (lookup.velocity_m_s <= 4)]
            # if len(search) == 0:
            #     dn = np.ones(nnz) * np.nanmax(dn)
            #     inner_diameter = np.ones(nnz) * np.nanmax(inner_diameter)
            # else:
            #     search = search.loc[search.velocity_m_s.idxmin()]
            #     dn = np.ones(nnz) * search.dn.item()
            #     inner_diameter = np.ones(nnz) * search.inner_diameter_mm.item()

        elif n_tubes == 2:
            graph_table["m_dot_kg_s"] = graph_table["power_total"] / (
                fluid.heat_capacity * fluid.delta_t / 1000
            )
            graph_table["v_dot_m3_s"] = graph_table["m_dot_kg_s"] / fluid.density

            v_dot_m3_s = graph_table["v_dot_m3_s"].fillna(graph_table["v_dot_m3_s"].mean()).values
            # Double tube system with out-and return-pipes, calculate diameters for each segment
            dn, inner_diameter = find_pipe_dn_vectorised(
                v_dot_m3_s, self.nominal_diameter_table, dn_offset=self.nominal_diameter_offset
            )
            graph_table["dn"] = dn
            graph_table["inner_diameter_mm"] = inner_diameter
            graph_table["fluid_volume_m3"] = graph_table["routes"] * np.pi * (graph_table["inner_diameter_mm"] / 1000) ** 2 / 4
            graph_table["velocity_m_s"] = calc_fluid_velocity(
                graph_table["inner_diameter_mm"], graph_table["v_dot_m3_s"]
            )

        else:
            raise ValueError("n_tubes must be 1 or 2")

        graph = sparse_graph.dataframe_to_sparse_with_coords(graph_table, graph.power_total)

        # n_components, labels = sparse_graph.connected_components(network.graph.routes, directed=False)
        # log.info(n_components, why="because FU thats why", where="Flows")

        # coords = graph["power_total"].data.coords
        # nnz = graph["power_total"].data.nnz
        # shape = graph["power_total"].data.shape
        #
        # graph["m_dot_kg_s"] = graph["power_total"] / (fluid.heat_capacity * fluid.delta_t / 1000)
        # graph["v_dot_m3_s"] = graph["m_dot_kg_s"] / fluid.density
        # v_dot_m3_s = graph["v_dot_m3_s"].data.data
        #
        # n_tubes = network.pipe_spec.n_tubes
        # if n_tubes == 1:
        #     # Single tube ring network, dimension according to the max flow required in any section
        #     dn, inner_diameter = find_pipe_dn(
        #         np.nanmax(v_dot_m3_s), self.nominal_diameter_table, dn_offset=self.nominal_diameter_offset
        #     )
        #     dn = np.ones(nnz) * dn
        #     inner_diameter = np.ones(nnz) * inner_diameter
        # elif n_tubes == 2:
        #     # Double tube system with out-and return-pipes, calculate diameters for each segment
        #     dn, inner_diameter = find_pipe_dn_vectorised(
        #         v_dot_m3_s, self.nominal_diameter_table, dn_offset=self.nominal_diameter_offset
        #     )
        #
        # else:
        #     raise ValueError("n_tubes must be 1 or 2")
        #
        # graph["dn"] = (("id_from", "id_to"), sparse.COO(coords=coords, data=dn, shape=shape))
        # graph["inner_diameter_mm"] = (
        #     ("id_from", "id_to"),
        #     sparse.COO(coords=coords, data=inner_diameter, shape=shape),
        # )

        for var in graph.data_vars:
            graph[var].attrs["is_directed"] = True

        if network.building_connections is not None:
            building_connections = network.building_connections.join(
                self._building_connection_size(
                    fluid, district.buildings_table(network.building_connections.index)
                )
            )
        else:
            building_connections = None

        network = network.model_copy(
            update=dict(graph=graph, building_connections=building_connections),
            deep=True,
        )

        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def _building_connection_size(self, fluid, buildings):
        """
        Mass flow rate and volumetric flow rate for each building in the grid
        """
        building_flows = _flow_data_for_buildings(
            buildings.p_hww_max1h_kw,
            fluid_density=fluid.density,
            fluid_heat_capacity=fluid.heat_capacity,
            delta_t=fluid.delta_t,
        )

        dn, id_mm = find_pipe_dn_vectorised(building_flows["v_dot_m3_s"], self.nominal_diameter_table)
        building_flows["id_mm"] = id_mm
        building_flows["dn"] = dn

        return building_flows


@dataclass
class FlowCharacteristics(_PrepFlowMixin, _ApplyAllMixin):
    """
    Calculate flow characteristics for network that has the pipe diameters already set.
    This can be used to get flow characteristics independently of the pipe dimension
    algorithm

    """

    fluid_properties: FlowProperties | None = field(default=FlowProperties())
    pipe_roughness: float = 0.0015  # [mm]

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        """
        Args:
            district:
            district_network:

        Returns:

        """
        network = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        fluid = self._prep_fluid_properties(network)
        # early exit if required params no in input data
        if (
            network.graph is None
            or "inner_diameter_mm" not in network.graph
            or "v_dot_m3_s" not in network.graph
        ):
            return district
        graph: xr.Dataset = network.graph.copy()

        pipe_spec = network.pipe_spec
        if pipe_spec is None:
            roughness_mm = self.pipe_roughness
        else:
            if pipe_spec.roughness_mm == 0:
                roughness_mm = self.pipe_roughness
            else:
                roughness_mm = pipe_spec.roughness_mm

        graph = self._network_flows(fluid, graph, roughness_mm)

        if network.building_connections is not None:
            building_connections = self._building_flows(fluid, network.building_connections)

        else:
            building_connections = None

        network = network.model_copy(
            update=dict(graph=graph, building_connections=building_connections),
            deep=True,
        )

        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def _network_flows(self, fluid, graph, roughness_mm):
        coords = graph["inner_diameter_mm"].data.coords
        shape = graph["inner_diameter_mm"].data.shape

      

        distances = graph["routes"].data.data
        v_dot_m3_s = graph["v_dot_m3_s"].data.data
        inner_diameter_mm = graph["inner_diameter_mm"].data.data
        velocity_m_s = graph["velocity_m_s"].data.data

        f_dw = darcy_weisbach_friction_f(
            velocity_m_s,
            inner_diameter_mm / 1000,
            fluid.density,
            fluid.dynamic_viscosity,
            roughness_mm / 1000,
        )

        r_dw = 8 * f_dw * distances / (9.8067 * (inner_diameter_mm / 1000) ** 5 * np.pi**2)
        h_f_dw_m = r_dw * v_dot_m3_s**2
        pressure_loss_pa_m = f_dw * 0.5 * fluid.density * ((velocity_m_s**2) / (inner_diameter_mm / 1000))

        graph["h_f_dw_m"] = (("id_from", "id_to"), sparse.COO(coords=coords, data=h_f_dw_m, shape=shape))
        graph["pressure_loss_pa_m"] = (
            ("id_from", "id_to"),
            sparse.COO(coords=coords, data=pressure_loss_pa_m, shape=shape),
        )
        graph["pressure_loss_pa"] = graph["pressure_loss_pa_m"] * graph["routes"]
        for var in graph.data_vars:
            graph[var].attrs["is_directed"] = True
        return graph

    def _building_flows(self, fluid, building_connections):
        if "v_dot_m3_s" not in building_connections.columns or "id_mm" not in building_connections.columns:
            return building_connections
        l_on_the_grid = building_connections.to_crs(3395).geom.length
        l_on_the_grid = l_on_the_grid.fillna(20)
        # TODO: (w)hat is the correct effective length on the grid?
        #  do we just ignore the connection pipe?
        # l_on_the_grid = 1

        building_connections = building_connections.join(
            pd.DataFrame(
                calc_flow_properties(
                    building_connections["v_dot_m3_s"],
                    l_on_the_grid,
                    building_connections["id_mm"],
                    fluid_density=fluid.density,
                    fluid_viscosity=fluid.dynamic_viscosity,
                )
            )
        )

        building_connections = _building_total_head(building_connections, fluid.density)

        return building_connections
