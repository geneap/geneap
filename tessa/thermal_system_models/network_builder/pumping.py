import uuid
from dataclasses import dataclass
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import scipy
import scipy.sparse.linalg
import structlog
import xarray as xr

from tessa.sparse_graph import dataframe_to_sparse_with_coords

from ..models import DistrictNetwork, Pump
from ..pipes import calc_pump_power
from .common import (
    _ApplyAllMixin,
    create_loop_matrix,
    create_sparse_row,
    select_district_network,
    substation_connection_ids,
    update_district_with_network,
)

log = structlog.get_logger()

if TYPE_CHECKING:
    from tessa.district import District


@dataclass
class PumpingPower(_ApplyAllMixin):
    """
    TODO add altitude change losses.

    TODO possibility to model more network elements in detail (branchings,
      diameter changes, etc)

    """

    default_building_heat_exchanger_pressure_drop: float = 30000
    """TODO: the max head loss is an arbitrary number estimated based on
     various spec sheets for domestic heat exchangers, e.g.
     https://shop.alfalaval.com/fr-fr/chauffage-radiateurs--c003
     See numbers as low as 10kPa and as high as 40kPa, which are still
     much lower than the 10PSI/70kPa sometimes quoted in process and HVAC
     engineering -> these higher numbers are more credible for the large
     central heat exchangers (although might apply for very large buildings)
    """
    # default_pump_efficiency = 0.85
    default_pump_efficiency = 0.75
    """Swiss Handbook of District Heating states:
    Pump efficiency is highly dependent on the pump capacity. design the pump so it reaches its capacity
    and the delivery head at the outlet in the most frequently operating point (usually at partial load)
    has a high efficiency. The highest efficiency for district heating pumps usually ranges between 65% and 85%.
    For an estimation, an efficiency (hydraulic system and electrical) of 75% can be applied.
    """

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | DistrictNetwork | None = None,
    ) -> "District":
        network: DistrictNetwork = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        if network.flow is None:
            return district
        if network.graph is None or "h_f_dw_m" not in network.graph:
            log.warning("h_f_dw_m not found in graph, is required to calculate network head loss")
            return district

        graph: xr.Dataset = network.graph

        if network.pipe_spec.n_tubes == 2:
            # TODO: need a system to decide when the pumps are central or distributed
            substations, building_connections, network_table = self.pump_dual_pipe_classic(network)

        elif network.pipe_spec.n_tubes == 1:
            substations, building_connections, network_table = self._pump_single_pipe(network)
        else:
            raise ValueError(f"N pipes must be 1 or 2, not {network.pipe_spec.n_tubes}")

        graph = dataframe_to_sparse_with_coords(network_table, graph=graph[list(graph.data_vars)[0]])
        network = network.model_copy(
            update=dict(substations=substations, graph=graph),
            deep=True,
        )
        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def _pump_single_pipe(self, network: DistrictNetwork):
        """
        For a single pipe system we have one loop. The length of the
        loop depends on the system design (there may or may not need to be double
        lengths of pipe along network branches depending on the exact layout)

        The buildings act as "side loops" that draw from the network.

        TODO: the actual length should be set using the loop route calculated with
         travelling salesman algorithm. There should also be an ordering of the heat sources
         so that we can for example set the pump requirement to just relate to the set of buildings
         downstream from this heat source. For now we split in proportion to the substation power.
         Unlike for the dual pipe this doesn't cause hydraulic issues.
        Args:
            fluid:
            graph:
            substations:

        Returns:

        """
        fluid = network.flow
        graph = network.graph
        substations = network.substations
        building_connections = network.building_connections
        network_table = network.graph_table
        if graph is None or fluid is None or building_connections is None:
            return substations, building_connections, network_table

        h_pump_m = graph["h_f_dw_m"].sum().item()
        # network_table["delta_h_pump_m"] = network_table["h_f_dw_m"]
        # TODO: how do the buildings need to be added?
        #  In the one pipe case perhaps they don't because they have their own pump to
        #  balance own head loss only (neutral loop on the network)
        # Calculate the head loss of the building heat exchanger
        self._set_h_b_m(building_connections, fluid)

        total_power = sum(sub.rated_power_useful for sub in substations)
        substation_heads = [h_pump_m * sub.rated_power_useful / total_power for sub in substations]
        # The required flow is the largest flow value, in the loop network all flow goes through
        # all substations.
        v_dot_m3_s = graph["v_dot_m3_s"].max().item()
        substations_new = []
        for sub, head in zip(substations, substation_heads, strict=True):
            sub = self._set_or_add_pump_head(sub, fluid, head, v_dot_m3_s)
            substations_new.append(sub)

        return substations, building_connections, network_table

    def pump_dual_pipe_classic(self, network):
        """
        In a dual pipe system with pumps at substations, we just need to calculate the total
        head losses.

        Following DHC handbook.
        In classic networks they put one circulation pump that needs to meet the total
        head loss in the circuit

        Since the buildings are in parallel relative to the out/return loop, we know that the pressure
        drop across every building must be the same (otherwise the system is not stable). We assume
        that the pressure drop between the out and return must be equal to the largest pressure drop
        of the set of buildings (analogous to voltage across parallel resistances). The varying energy loss
        is accounted for by different flow rates in each building.

        We dimension the system thinking that we need to reach the furthest away building (with the longest
        length of pipe to reach it) with the largest pressure drop.

        Ideally (according to the engineering hydraulics) we would like to put a pump at each building to
        control the total pressure drops and mass flow rates - see :meth:`dual_pipe_distributed`. However
        to install pumps and controls would be expensive and complicated, so traditionally we just install
        flow restrictors/pressure control valves and provide extra main pump head to compensate.


        Args:
            network:

        Returns:

        """
        fluid = network.flow
        graph = network.graph
        network_table = network.graph_table
        substations = network.substations
        building_connections = network.building_connections

        if "h_f_dw_m" not in graph:
            log.warning("h_f_dw_m not found in graph, is required to calculate network head loss")
            return substations

        # Calculate the head loss of the building heat exchanger
        self._set_h_b_m(building_connections, fluid)

        sub_head_m = pd.Series(0.0, index=list(sub.connection_point.node_id for sub in substations))

        h_b_m_from = building_connections["total_h_b_m"].reindex(sub_head_m.index).fillna(0.0)
        h_b_m_to = building_connections["total_h_b_m"].reindex(sub_head_m.index).fillna(0.0)

        # TODO: here assume that substations have no own head loss, should be added later.
        sub_head_m[:] = h_b_m_from + h_b_m_to

        friction_head_loss = graph["h_f_dw_m"].sum().item()
        friction_head_loss = network.pipe_spec.n_tubes * friction_head_loss

        # Estimate the building pump head loss from the worst performing building.
        bld_heat_exchange_heat_loss = building_connections["total_h_b_m"].max()

        # friction_head_loss += network.building_connections["h_f_dw_m"].sum()
        # bld_heat_exchange_heat_loss = network.building_connections["h_b_m"].sum()
        head_loss_m = friction_head_loss + bld_heat_exchange_heat_loss

        substations = self._set_single_pump(substations, fluid, head_loss_m)
        # No per-building pumps, this could be more flexible.
        building_connections["h_pump_m"] = 0

        return substations, building_connections, network_table

    def pump_dual_pipe_distributed(self, network):
        """
        In a dual pipe system, this is a method to optimise the network assuming each building
        has its own circulation pump then the heat sources need an extra pump to balance the system.

        The optimal distribution of heat sources is given by a minimization of the total head, with condition
        that the individual building heads are positive (always flow from hot to cold pipe)

        Solve system of linear equations:
         E.h = ∆P

        where matrix E setups up the equations

        The second linear equation system comes from head losses in each grid loop:

        $\Sigma H_{loop} = \Sigma (R \cdot Q^n + h_{building} - h_{pump}) = 0$

        From Darcy–Weisbach equation, $R = (f L) / (12.1 D^5) $ and $n = 2$. If we write head losses in each loop, we would have:


        $2 \cdot R_{12} \cdot Q_{12}^2 +  (R_{2} \cdot Q_{2}^2 + h_{2}) - (R_{1} \cdot Q_{1}^2 + h_{1}) = h_{P2} - h_{P1}$

        $2 \cdot R_{23} \cdot Q_{23}^2 +  (R_{3} \cdot Q_{3}^2 + h_{3}) - (R_{2} \cdot Q_{2}^2 + h_{2}) = h_{P3} - h_{P2}$

        $2 \cdot R_{24} \cdot Q_{24}^2 +  (R_{4} \cdot Q_{4}^2 + h_{4}) - (R_{2} \cdot Q_{2}^2 + h_{2}) = h_{P4} - h_{P2}$

        $2 \cdot R_{45} \cdot Q_{45}^2 +  (R_{5} \cdot Q_{5}^2 + h_{5}) - (R_{4} \cdot Q_{4}^2 + h_{4}) = h_{P5} - h_{P4}$

        $2 \cdot R_{A1} \cdot Q_{A1}^2 +  (R_{A} \cdot Q_{A}^2 + h_{A}) + (R_{1} \cdot Q_{1}^2 + h_{1}) = h_{PA} + h_{P1}$

        $2 \cdot R_{B3} \cdot Q_{B3}^2 +  (R_{3} \cdot Q_{3}^2 + h_{3}) + (R_{B} \cdot Q_{B}^2 + h_{B}) = h_{P3} + h_{PB}$


        We already knew all the $Q_i$, $Q_{ij}$, $R_i$, $R_{ij}$, and $h_i$. Therefore, we have 7 unknowns ($h_{P1}$, $h_{P2}$, $h_{P3}$, $h_{P4}$, $h_{P5}$, $h_{PA}$, and $h_{PB}$) and 6 equations. So, we need an extra equation to be able to solve it:

        $h_{PB} = constant$

        Args:
            network:

        Returns:

        """
        fluid = network.flow
        network_table = network.graph_table
        substations = network.substations
        building_connections = network.building_connections

        # Calculate the head loss of the building heat exchanger
        self._set_h_b_m(building_connections, fluid)

        h_b_m_from = (
            building_connections["total_h_b_m"]
            .reindex(network_table.index.get_level_values("id_from"))
            .fillna(0)
        )
        h_b_m_to = (
            building_connections["total_h_b_m"]
            .reindex(network_table.index.get_level_values("id_to"))
            .fillna(0)
        )

        # Each edge is a loop where the out and return pipes are connected at each end by the building
        # cross feed.
        # At each edge the delta head is the sum of head loss of the pipes out and return
        # plus the head loss of the target node building
        # minus the head loss of the source node building
        delta_h_pump_m = 2 * network_table["h_f_dw_m"] + h_b_m_to.values - h_b_m_from.values
        # NOTE: here provide a max required flow that is only used in the case there are no pumps
        #  so we assign all the pump power and flow need to a single pump. Otherwise we derive a flow
        #  for each substation based on its previously calculate power output and the flow properties.
        # required_flow = network_table["v_dot_m3_s"].max()

        sub_head_m = pd.Series(0, index=list(sub.connection_point.node_id for sub in substations))
        np.array(list(sub.connection_point.node_id for sub in substations))
        h_b_m_from = building_connections["total_h_b_m"].reindex(sub_head_m.index).fillna(0)
        h_b_m_to = building_connections["total_h_b_m"].reindex(sub_head_m.index).fillna(0)

        # TODO: here assume that substations have no own head loss, should be added later.
        sub_head_m.values = h_b_m_from + h_b_m_to

        # We need to perform an optimisation
        # to decide the pump sizing for each substation.
        def _opt(blnc, delta_h_pump_m, building_connections, sub_head_m):
            loop_heads, substation_heads = self._solve(
                delta_h_pump_m, building_connections, sub_head_m, balancing_pump_head=blnc
            )

            # Ensure that all loop heads are positive by applying a large penalty in case any are negative.
            if np.any(loop_heads < 0):
                return 1e20

            return loop_heads.sum()

        res = scipy.optimize.minimize(
            _opt,
            np.array([0]),
            args=(delta_h_pump_m, building_connections, sub_head_m),
            bounds=[(0, None)],
        )

        bld_heads, substation_heads = self._solve(
            delta_h_pump_m, building_connections, sub_head_m, balancing_pump_head=res.x[0]
        )

        # Set the building pump heads that are needed.
        building_connections["h_pump_m"] = bld_heads

        substations = self._set_substation_pumps(fluid, substations, substation_heads)

        return substations, building_connections, network_table

    def _solve(
        self,
        network_heads,
        building_connections,
        substation_connections=None,
        balancing_pump_head=0,
    ):
        r"""




        Args:
            network_heads:
            building_connections:
            substation_connections:
            balancing_pump_head:

        Returns:

        """
        if substation_connections is None:
            substation_connections = pd.Series([])
            substation_heads = np.zeros_like(substation_connections)
        else:
            substation_heads = np.asarray(substation_connections.values)

        design_matrix = create_loop_matrix(
            network_heads.index, building_connections.index, substation_connections.index
        )

        node_head = network_heads.fillna(0).to_numpy()
        node_head = np.append(node_head, substation_heads)

        # Need to set the pump head of one substation to be able to solve (can be zero).
        node_head = np.append(node_head, [balancing_pump_head])

        # Add a row to allow to solve the system of equations
        extra_row = create_sparse_row(design_matrix)
        design_matrix = scipy.sparse.vstack((design_matrix, extra_row))

        heads = scipy.sparse.linalg.spsolve(design_matrix, node_head)
        # edge_flows, istop, *x = scipy.sparse.linalg.lsqr(design_matrix, node_head)

        buildings_heads = heads[: len(building_connections)]
        substation_soln = heads[len(building_connections) :]
        return buildings_heads, substation_soln

    def _set_h_b_m(self, building_connections, fluid):
        h_b_m = self.default_building_heat_exchanger_pressure_drop / (fluid.density * 9.8067)  # [m]
        if "h_b_m" not in building_connections:
            building_connections["h_b_m"] = h_b_m
        else:
            building_connections.update(
                pd.Series(
                    h_b_m * np.ones_like(building_connections.index),
                    index=building_connections.index,
                    name="h_b_m",
                )
            )
        building_connections.loc[building_connections["velocity_m_s"] == 0, "h_b_m"] = 0

        # Calculate the head loss at the building as the sum of the loss in
        # the building heat exchanger and friction loss in the connecting pipe.
        building_connections["total_h_b_m"] = building_connections["h_f_dw_m"] + building_connections["h_b_m"]
        if np.any(building_connections["total_h_b_m"].abs() > 10):
            log.warning(
                f"Clipping total building head loss {building_connections['total_h_b_m'].abs().max()}. Needs better handling."
            )
            building_connections["total_h_b_m"] = building_connections["total_h_b_m"].clip(
                lower=-4 * h_b_m, upper=4 * h_b_m
            )

    def _set_single_pump(self, substations, fluid, head_loss_m):
        # unlike for heat injection, pumping gets complex if we split the main pump, so we just
        # assume circulation pump goes to the largest substation.
        sub_powers = [sub.rated_power_useful for sub in substations]
        if len(sub_powers) == 0:
            return substations
        largest = sub_powers.index(max(sub_powers))
        substations_new = []
        for i, sub in enumerate(substations):
            if i == largest:
                supply_power = sub.supply_curve.p_supply_kw.max()
                m_dot_kg_s = supply_power / (fluid.heat_capacity * fluid.delta_t / 1000)
                v_dot_m3_s = m_dot_kg_s / fluid.density
                sub = self._set_or_add_pump_head(sub, fluid, head_loss_m, v_dot_m3_s)
            substations_new.append(sub)
        return substations_new

    def _set_substation_pumps(self, fluid, substations, heads):
        """
        Pump assignment strategy

        Check existing pumps. If existing pump power > calculated pump power, do nothing

        TODO: later set a consumed power curve per pump in function of the volume flow curve of the substation.
          Volume flow can be calculated from the total substation power curve.


        If pump power < calculate and/or pumps have None rated power, divide the powers among pumps in relation
         to the power of the substation, or equally if no production powers.

        If there are no pumps, the default strategy is to add one to each substation
        with powers split evenly

        Args:
            substations
            fluid
            required_flow
            grid_total_head

        Returns:

        """

        substations_new = []
        # assume grid head already solved per substation
        for sub, head in zip(substations, heads, strict=True):
            supply_power = sub.supply_curve.p_supply_kw.max()
            m_dot_kg_s = supply_power / (fluid.heat_capacity * fluid.delta_t / 1000)
            v_dot_m3_s = m_dot_kg_s / fluid.density

            sub = self._set_or_add_pump_head(sub, fluid, head, v_dot_m3_s)

            substations_new.append(sub)
        return substations_new

    def _set_or_add_pump_head(self, substation, fluid, head_loss_m, v_dot_m3_s):
        if substation.pump_head < head_loss_m:
            head_loss_m = head_loss_m - substation.pump_head
            # Add a 0.5% extra to avoid rounding issues
            head_loss_m += head_loss_m * 0.005
            power_useful_main_pump_kw = calc_pump_power(
                v_dot_m3_s,
                head_loss_m,
                fluid_density=fluid.density,
                efficiency=self.default_pump_efficiency,
            )
            substation.pumps.append(
                Pump(
                    name="Autogenerated",
                    rated_power=round(power_useful_main_pump_kw / self.default_pump_efficiency, 4),
                    efficiency=self.default_pump_efficiency,
                    head=round(head_loss_m, 4),
                )
            )
        return substation

    #
    # def update_substations_jank(self, substations, fluid, grid_total_head, required_flow):
    #     """
    #     Pump assignment strategy
    #
    #     Check existing pumps. If existing pump power > calculated pump power, do nothing
    #
    #     TODO: later set a consumed power curve per pump in function of the volume flow curve of the substation.
    #       Volume flow can be calculated from the total substation power curve.
    #
    #
    #     If pump power < calculate and/or pumps have None rated power, divide the powers among pumps in relation
    #      to the power of the substation, or equally if no production powers.
    #
    #     If there are no pumps, the default strategy is to add one to each substation
    #     with powers split evenly
    #
    #     Args:
    #         substations
    #         fluid
    #         required_flow
    #         grid_total_head
    #
    #     Returns:
    #
    #     """
    #
    #     substations_new = []
    #     n_sub_pumps = [len(sub.pumps) for sub in substations]
    #     if not np.isscalar(grid_total_head):
    #         # assume grid head already solved per substation
    #         for sub, head in zip(substations, grid_total_head, strict=True):
    #             supply_power = sub.supply_curve.p_supply_kw.max()
    #             m_dot_kg_s = supply_power / (fluid.heat_capacity * fluid.delta_t / 1000)
    #             v_dot_m3_s = m_dot_kg_s / fluid.density
    #
    #             if sub.pump_head < head:
    #                 head = head - sub.pump_head
    #
    #             power_useful_main_pump_kw = calc_pump_power(
    #                 v_dot_m3_s,
    #                 head,
    #                 fluid_density=fluid.density,
    #                 efficiency=self.default_pump_efficiency,
    #             )
    #
    #             sub.pumps.append(
    #                 Pump(
    #                     name="Autogenerated",
    #                     rated_power=round(power_useful_main_pump_kw / self.default_pump_efficiency, 4),
    #                     efficiency=self.default_pump_efficiency,
    #                     head=round(head, 4),
    #                 )
    #             )
    #             substations_new.append(sub)
    #         return substations_new
    #
    #     if all(n == 0 for n in n_sub_pumps) and len(substations) > 0:
    #         if np.isscalar(grid_total_head):
    #             # decide if we just add a pump? do so for now. can be a setting later...
    #             idx = 0
    #             substation = substations[0]
    #
    #             for i, sub in enumerate(substations):
    #                 if sub.rated_power_useful > substation.rated_power_useful:
    #                     idx = i
    #                     substation = sub
    #                 substations_new.append(sub)
    #
    #             power_useful_main_pump_kw = calc_pump_power(
    #                 required_flow,
    #                 grid_total_head,
    #                 fluid_density=fluid.density,
    #                 efficiency=self.default_pump_efficiency,
    #             )
    #
    #             substations[idx].pumps.append(
    #                 Pump(
    #                     name="Autogenerated",
    #                     rated_power=round(power_useful_main_pump_kw / self.default_pump_efficiency, 4),
    #                     efficiency=self.default_pump_efficiency,
    #                     head=round(grid_total_head, 4),
    #                 )
    #             )
    #
    #     elif any(n > 0 for n in n_sub_pumps):
    #         # Split pump power by required head
    #         total_rated_power = sum(sub.rated_power_useful for sub in substations)
    #         head_existing = sum(sub.pump_head for sub in substations)
    #
    #         if grid_total_head > head_existing:
    #             head_remain = grid_total_head - head_existing
    #             # Need to add the missing power somewhere.
    #             # If there are pumps with no power set, we set it and DON'T ADD PUMPS.
    #             # Only if there are no pumps do we add new ones.
    #             pumps_need_setting = 0
    #             for sub in substations:
    #                 for pump in sub.pumps:
    #                     if pump.rated_power:
    #                         pumps_need_setting += pump.rated_power
    #
    #             for sub in substations:
    #                 supply_power = sub.supply_curve.p_supply_kw.max()
    #                 m_dot_kg_s = supply_power / (fluid.heat_capacity * fluid.delta_t / 1000)
    #                 v_dot_m3_s = m_dot_kg_s / fluid.density
    #
    #                 if len(sub.sources) == 0 and len(sub.building_ids) > 0:
    #                     # For now skip substations that are just connected to buildings
    #                     continue
    #                 elif pumps_need_setting:
    #                     # This allows to skip substations without pumps in the case we know another
    #                     # substation has a pump set with no power, that will be sized by the algorithm
    #                     for pump in sub.pumps:
    #                         if not pump.rated_power:
    #                             if not pump.efficiency:
    #                                 pump.efficiency = self.default_pump_efficiency
    #                             weight = sub.rated_power_useful / pumps_need_setting
    #                             pump_head = head_remain * weight
    #
    #                             pump_power = calc_pump_power(
    #                                 v_dot_m3_s,
    #                                 pump_head,
    #                                 fluid_density=fluid.density,
    #                                 efficiency=pump.efficiency,
    #                             )
    #
    #                             pump.rated_power = round(pump_power / pump.efficiency, 4)
    #                             pump.head = pump_head
    #                 else:
    #                     # TODO: (m)ight be simpler to require that you add pumps
    #                     weight = sub.rated_power_useful / total_rated_power
    #                     pump_head = head_remain * weight
    #                     pump_power = calc_pump_power(
    #                         v_dot_m3_s,
    #                         pump_head,
    #                         fluid_density=fluid.density,
    #                         efficiency=self.default_pump_efficiency,
    #                     )
    #                     rated_power = round(pump_power / self.default_pump_efficiency, 3)
    #                     sub.pumps.append(
    #                         Pump(
    #                             name="Autogenerated",
    #                             rated_power=rated_power,
    #                             efficiency=self.default_pump_efficiency,
    #                             head=pump_head,
    #                         )
    #                     )
    #
    #                 substations_new.append(sub)
    #         else:
    #             substations_new = substations
    #     return substations_new


def calc_pump_purchase_cost(pump_power_kw, r_cepci):
    return (
        10 ** (3.3892 + 0.0536 * np.log10(pump_power_kw) + 0.1538 * (np.log10(pump_power_kw)) ** 2)
        * r_cepci
        * 0.9548
    )


def calc_bare_module_cost(purchased_cost_chf, h_pump_m, r_cepci, fluid_density):
    specific_pump_force = fluid_density * 9.8067 * h_pump_m  # Newton/m2

    return (
        purchased_cost_chf
        * (
            1.89
            + 1.35
            * 1.6
            * (
                10
                ** (
                    (-1 * 0.3935)
                    + 0.3957 * np.log10(specific_pump_force / 100000)
                    - 0.00226 * (np.log10(specific_pump_force / 100000) ** 2)
                )
            )
        )
        * r_cepci
        * 0.9548
    )


def calculate_costs(v_dot_m3_s, h_pump_m, fluid_density):
    CEPCI_2001 = 394.3
    CEPCI_2022 = 816.0
    R_CEPCI = CEPCI_2022 / CEPCI_2001

    np.seterr(divide="ignore")

    # Power of the Pumps
    pump_power_kw = calc_pump_power(v_dot_m3_s, h_pump_m, fluid_density)

    # Purchased Cost
    purchased_cost_chf = calc_pump_purchase_cost(pump_power_kw, R_CEPCI)

    # Bare Module Cost
    bare_module_cost_chf = calc_bare_module_cost(purchased_cost_chf, h_pump_m, R_CEPCI, fluid_density)

    return pd.Series(
        {
            "Power_of_Pump_kW": pump_power_kw,
            "Purchased_Cost_of_Pump_CHF": purchased_cost_chf,
            "Bare_Module_Cost_of_Pump_CHF": bare_module_cost_chf,
        }
    )


@dataclass
class PumpCosts(_ApplyAllMixin):
    """
    The bare module cost and purchase cost of the pumps

    The bare module cost and purchase cost of the pumps can be calculated from the following equations [1]

    Operating pressure factor: F_P = 10**(-0.3935 + 0.3957 * log10(rho * g * h_p) - 0.00226 * (log10(rho * g * h_p))**2)
    The unit of pressure here should be bar gauge (100 kPa)

    Bare module factors: F_BM = 1.89 + 1.35 * 1.6 * F_P

    Bare Module Cost, which is the sum of the direct and indirect costs: C_BM = C_P * F_BM

    Average exchange rate in 2022: 1 USD = 0.9548 CHF

    Chemical Engineering Plant Cost Index (CEPCI) [2]

    [1]: Turton, R., Bailie, R.C., Whiting, W.B. and Shaeiwitz, J.A., 2008.
    Analysis, synthesis and design of chemical processes. Pearson Education.
    [2]: https://chemengonline.com
    """

    CEPCI_2001: float = 394.3
    CEPCI_2022: float = 816.0

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        network = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        r_cepci = self.CEPCI_2022 / self.CEPCI_2001

        fluid = network.flow

        # buildings = network.building_connections
        # results_table = pd.DataFrame(index=buildings.index)
        # The unit of Power should be kW
        # pump_power = buildings["pump_power_kw"]
        # # Purchased Cost
        # results_table["purchased_pump_cost_chf"] = calc_pump_purchase_cost(pump_power, r_cepci)
        # # Bare Module Cost
        # h_pump_m = buildings["h_Pump_m"]
        # results_table["Bare_Module_Cost_of_Pump_CHF"] = calc_bare_module_cost(
        #     results_table["purchased_pump_cost_chf"], h_pump_m, r_cepci, density
        # )
        #
        # # Total Purchased Cost of the Pumps in the Buildings
        # buildings["purchased_pump_cost_chf"].sum() / 1000000  # [Million CHF]
        #
        # # Total Bare Module Cost of the Pumps in the Buildings
        # buildings["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000  # [Million CHF]

        # The main Pump's Costs
        substations = []
        for sub in network.substations:
            # Only handle main circulation pumps here
            if len(sub.sources) != 0:
                # Update the pumps inside this substation
                for pump in sub.pumps:
                    # The unit of Power should be kW
                    if pump.rated_power and pump.pump_cost is None:
                        # Purchased Cost
                        pump.pump_cost = calc_pump_purchase_cost(pump.rated_power, r_cepci)

                    # Bare Module Cost
                    if (
                        pump.head
                        and pump.bare_module_cost is None
                        and fluid is not None
                        and pump.pump_cost is not None
                    ):
                        pump.bare_module_cost = calc_bare_module_cost(
                            pump.pump_cost, pump.head, r_cepci, fluid.density
                        )

            substations.append(sub)

        network = network.model_copy(update=dict(substations=substations), deep=True)
        return update_district_with_network(district, network)


# def create_coefficient_matrices(building_ids, network):
#     """
#     Coefficient Matrix for Solving the system of linear equations
#
#     Args:
#         buildings:
#         network:
#
#     Returns:
#
#     """
#     # Create a list of column names
#     # column_names = [f"Column_{i}" for i in range(len(network))]
#
#     # Initialize a list to store the new columns (as a list of lists)
#     new_columns = []
#
#     for index, row in network.iterrows():
#         new_column = []
#
#         for bid in building_ids:
#             if bid == row["id_from"]:
#                 new_column.append(-1)
#             elif bid == row["id_to"]:
#                 new_column.append(1)
#             else:
#                 new_column.append(0)
#
#         new_columns.append(new_column)
#
#     # Convert the list of lists into a matrix (2D list)
#     coeff_mat_v_dot = [list(row) for row in zip(*new_columns)]
#     coeff_mat_v_dot = coeff_mat_v_dot[:-1]
#     coeff_mat_v_dot = np.array(coeff_mat_v_dot)
#     coeff_mat_head = np.transpose(coeff_mat_v_dot)
#
#     # Add a column with all zeros except the last one which is 1
#     new_column = np.zeros((coeff_mat_head.shape[0], 1))
#     new_column[-1] = 1
#
#     # Concatenate the new column to the original matrix
#     coeff_mat_head = np.concatenate((coeff_mat_head, new_column), axis=1)
#
#     # Add a row with all zeros except the last one which is 1
#     new_row = np.zeros((1, coeff_mat_head.shape[1]))
#     new_row[0, -1] = 1
#
#     # Concatenate the new row to the original matrix
#     coeff_mat_head = np.concatenate((coeff_mat_head, new_row), axis=0)
#
#     return coeff_mat_v_dot, coeff_mat_head


# from tessa.sparse_graph import sparse_dataset_with_common_coords_to_dataframe


# def _solve_network_flow_matrix(buildings, network, L=30):
#     # TODO: (n)eeds to solve sparse matrix.
#
#     network_matrix = network.graph
#     network_table = sparse_dataset_with_common_coords_to_dataframe(network_matrix[["routes"]])
#
#     network_table.reset_index(inplace=True)
#
#     # Find the egid of the heat source
#     node_ids = [thermal_source.injection_point.node_id for thermal_source in network.heat_sources.sources]
#     unique_node_ids = list(set(node_ids))
#     # Adding Heat Source
#     data = {"id_from": ["heat source"], "id_to": unique_node_ids, "routes": L}
#
#     new_row = pd.DataFrame(data)
#     network_table = pd.concat([network_table, new_row], ignore_index=True)
#
#     # Solve the system of linear equations for finding the volumetric flow rate for each route
#     coeff_mat_v_dot, coeff_mat_head = create_coefficient_matrices(buildings.index, network_table)
#
#     rhs_v_dot = np.array(buildings["v_dot_m3_s"])
#     x_v_dot = np.linalg.solve(coeff_mat_v_dot, rhs_v_dot[:-1])
#
#     network_table["v_dot_m3_s"] = x_v_dot  # [m3/s]
#     network_table["direction"] = network_table["v_dot_m3_s"].apply(lambda x: 1 if x > 0 else -1)
#     network_table["v_dot_m3_s"] = abs(network_table["v_dot_m3_s"])
#     return network_table
#
#
# def calc_pump_head(h_main_pump_m, buildings, network):
#     """
#     Solving system of linear equations to find the head of the pumps
#     distributed through the system (pump at each building).
#     Need to construct system of linear equations with
#
#     Args:
#         h_main_pump_m:
#         buildings:
#         network:
#
#     Returns:
#
#     """
#
#     network_table = sparse_dataset_with_common_coords_to_dataframe(network.graph[["routes"]])
#
#     n_buildings = len(buildings)
#     n_substations = len(network.substations)
#     n_edges = network.graph.routes.data.nnz
#
#     # Construct sparse matrix using DOK
#     # keys are (edge index, building index)
#     # values are 1 for inward edge from the building (from id_from)
#     # values are -1 for outward edge from the building (from id_to)
#
#     n_equations = n_buildings + n_substations
#     head_coeffs = scipy.sparse.dok_array((n_equations, n_equations))
#
#     for index, row in network_table.reset_index().iterrows():
#         head_coeffs[index, buildings.index.get_loc(row.id_from)] = -1
#         head_coeffs[index, buildings.index.get_loc(row.id_to)] = 1
#
#     # For each heat source add an outgoing route to the target building
#
#     for i, sub in enumerate(network.substations):
#         egid = sub.connection_point.node_id
#         head_coeffs[n_edges - 1 + i, buildings.index.get_loc(egid)] = -1
#
#     # Get the target head values (flow per node)
#     rhs_head = network_table["delta_h_Pump_m"].to_numpy()
#
#     rhs_head = np.concatenate((rhs_head, h_main_pump_m))
#
#     res = scipy.sparse.linalg.lsmr(head_coeffs, rhs_head)
#     # TODO: (a)ssert successful solve.
#     return res[0]
