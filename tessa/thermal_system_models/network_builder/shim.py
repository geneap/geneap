from collections.abc import Iterable
from functools import partial

import pandas as pd

from tessa.road_network_routing import BuildingRouting

from ..models import (
    DistrictNetwork,
    Substation,
    ThermalSource,
    ThermalSources,
)
from ..pipes import load_hourly_heat_loss_percentage, load_monthly_heat_loss_percentage
from . import SubstationLocator
from .cost import DNCosts
from .heat_distribution import DNHeatFlowClassic, get_heat_source_options
from .layout import DnMstLayout


class DistrictNetworkBuilder:
    database = None
    routing_service: BuildingRouting

    max_distance: float = 1000

    hourly_network_loss: pd.Series
    monthly_network_loss: pd.Series

    heat_source_options: list = [
        {
            "code": "ashp",
            "cost_chf_kwh": 0.1746,
            "kg_co2_kwh": 0.063,
            "name": "ASHP (SPF 2.8)",
            "efficiency": 2.8,
            "fuel": "electricity",
            "investment_per_kw": 1430,
            "variable_o_and_m": 0.00269,
            "fixed_o_and_m": 2,
        }
    ]

    def __init__(
        self,
        database,
        routing_service: BuildingRouting,
        hourly_network_loss,
        monthly_network_loss,
        heat_source_options=None,
    ):
        self.database = database
        self.routing_service: BuildingRouting = routing_service
        self.hourly_network_loss = hourly_network_loss
        self.monthly_network_loss = monthly_network_loss
        if heat_source_options is not None:
            self.heat_source_options = heat_source_options

        # TODO: (4)% heat loss by default, needs to change in function of temperature levels
        #  For a simple model can set about 4% for HighT and 2% for low T. Later do the real loss.
        self.loss_fraction_km = 0.04

    def create_single(
        self,
        *,
        district=None,
        substations: list[Substation] | None = None,
        heat_sources: ThermalSources | Iterable[ThermalSource] | None = None,
        flow=None,
        costs=None,
        ways=None,
        name="district_heating",
    ) -> DistrictNetwork:
        heat_sources = heat_sources or []
        substations = substations or [Substation(sources=[hs]) for hs in heat_sources]
        layout_service = DnMstLayout(
            routing_service=self.routing_service, max_distance=self.max_distance, ways=ways, name=name
        )
        subs_locator_service = SubstationLocator(routing_service=self.routing_service)

        district_new = (
            district.apply(layout_service.create)
            .apply(partial(subs_locator_service, substations=substations))
            .apply(
                DNHeatFlowClassic(
                    fluid_properties=flow,
                    hourly_network_loss=self.hourly_network_loss,
                )
            )
            .apply(DNCosts(pipe_costs=costs))
        )
        utility_networks = sorted(
            district_new.utility_networks, key=lambda d: len(d.building_ids), reverse=True
        )
        district_networks = list(u for u in utility_networks if isinstance(u, DistrictNetwork))
        network = district_networks[0]
        return network


class SwissDistrictNetworkBuilder(DistrictNetworkBuilder):
    def __init__(self, database, routing_service: BuildingRouting):
        super().__init__(
            database=database,
            routing_service=routing_service,
            hourly_network_loss=load_hourly_heat_loss_percentage(),
            monthly_network_loss=load_monthly_heat_loss_percentage(),
            heat_source_options=get_heat_source_options(),
        )
