import uuid

import numpy as np
import pandas as pd
import scipy
import scipy.sparse
import structlog

from ..models import DistrictNetwork, Substation

log = structlog.get_logger()


def select_district_network(
    district, district_network: DistrictNetwork | uuid.UUID | str | None
) -> DistrictNetwork | None:
    """
    Find a district network within this district
    Args:
        district:
        district_network: a district network UUID, string of UUID, or network name

    Returns:

    """
    if district_network is None:
        return None
    elif isinstance(district_network, DistrictNetwork):
        district_network_id = district_network.uid
    elif isinstance(district_network, uuid.UUID):
        district_network_id = district_network
    elif isinstance(district_network, str):
        try:
            # could be the id of a network.
            district_network_id = uuid.UUID(district_network)
            ids = [d.uid for d in district.utility_networks]
            if district_network_id not in ids:
                raise RuntimeError(f"No network object with id {district_network_id} in this district")
        except ValueError:
            # If its a string but not a valid UUID, try it as the network name.
            sel = [d.uid for d in district.utility_networks if d.name == district_network]
            if len(sel) == 1:
                district_network_id = sel[0]
            else:
                raise RuntimeError(f"No exact match for name {district_network}")
    else:
        raise RuntimeError("Cannot find district network to update in this district")

    for d in district.utility_networks:
        if d.uid == district_network_id and isinstance(d, DistrictNetwork):
            return d
    else:
        raise RuntimeError(f"No network found for {district_network_id}")


def update_district_with_network(district, network):
    """
    Update the district object with the new network and return a new District object

    Args:
        district:
        network:

    Returns:

    """
    utility_networks = []
    for u in district.utility_networks:
        if u.uid == network.uid:
            utility_networks.append(network)
        else:
            utility_networks.append(u)
    return district.copy(update=dict(utility_networks=utility_networks), deep=True)


class _ApplyAllMixin:
    def _apply_to_all_district_networks(self, district, **kwargs):
        """Iterate over all networks within and apply callable self."""
        for d in district.utility_networks:
            if isinstance(d, DistrictNetwork) and d.uid is not None:
                district = self(district, district_network=d.uid, **kwargs)
        return district


def substation_connection_ids(substations: list[Substation]):
    return np.array(
        list(sub.connection_point.node_id for sub in substations if sub.connection_point is not None)
    )


def create_node_matrix(
    network_index: pd.MultiIndex, buildings_index: pd.Index, substations_index: list | pd.Index
):
    r"""
    Create a design matrix for the network and buildings, to solve
    nodal continuity (at each node: {math}`\Sigma Q = 0`)

    The matrix has <N Network Edges> columns and <N buildings + N substations> rows.

    Args:
        network_index: Multi index with levels id_from, id_to  representing building ids in the building_index var
        buildings_index: single level index of building ids
        substations_index: list or single level index of building ids where the substations are connected to.

    Returns:
        scipy csr matrix to use with sparse linear algebra routines

    """
    n_buildings = len(buildings_index)
    total_nodes = n_buildings
    n_building_edges = len(network_index)

    total_edges = n_building_edges
    edge_index = np.arange(n_building_edges)
    data_init = np.ones(n_building_edges)

    edge_index1 = network_index.get_level_values("id_from")
    bld_row_from = buildings_index.get_indexer_for(edge_index1)
    edge_index2 = network_index.get_level_values("id_to")
    bld_row_to = buildings_index.get_indexer_for(edge_index2)

    # Row for each building, column for each edge
    row = np.hstack((bld_row_from, bld_row_to))
    col = np.hstack((edge_index, edge_index))
    data = np.hstack((-1 * data_init, data_init))

    if len(substations_index) > 0:
        # Add rows for each substation
        n_sub_edges = len(substations_index)
        total_nodes = total_nodes + n_sub_edges
        total_edges = n_sub_edges + total_edges
        sub_data_init = np.ones(n_sub_edges)
        # from bld row -> substation i
        # from sub row -> edge of corresponding bld from
        bld_row_sub_from = buildings_index.get_indexer_for(substations_index)
        # bld_row_sub_to = network_index.get_level_values("id_from").get_indexer_for(sub_coords)
        sub_edge_index = np.arange(n_building_edges, n_building_edges + n_sub_edges)

        # Combine the substation data
        row = np.hstack((row, bld_row_sub_from, sub_edge_index + 1))
        col = np.hstack((col, sub_edge_index, sub_edge_index))
        data = np.hstack((data, -1 * sub_data_init, sub_data_init))

    design_matrix = scipy.sparse.csr_array((data, (row, col)), shape=(total_nodes, total_edges))
    return design_matrix


def create_loop_matrix(
    network_index: pd.MultiIndex, buildings_index: pd.Index, substations_index: list | pd.Index
):
    r"""
    Create a design matrix for the network and buildings, to edge loop balance
    At each edge (a loop of edge out/edge return): {math}`\Sigma H = 0`)

    The matrix has <N Network Edges> rows and <N buildings + N substations> columns.

    Args:
        network_index: Multi index with levels id_from, id_to  representing building ids in the building_index var
        buildings_index: single level index of building ids
        substations_index: list or single level index of building ids where the substations are connected to.

    Returns:
        scipy csr matrix to use with sparse linear algebra routines

    """
    n_buildings = len(buildings_index)
    total_nodes = n_buildings
    n_building_edges = len(network_index)

    total_edges = n_building_edges
    edge_index = np.arange(n_building_edges)
    data_init = np.ones(n_building_edges)

    edge_index1 = network_index.get_level_values("id_from")
    bld_from = buildings_index.get_indexer_for(edge_index1)
    edge_index2 = network_index.get_level_values("id_to")
    bld_to = buildings_index.get_indexer_for(edge_index2)

    # Row for each building, column for each edge
    col = np.hstack((bld_from, bld_to))
    row = np.hstack((edge_index, edge_index))
    data = np.hstack((-1 * data_init, data_init))

    if len(substations_index) > 0:
        # Add rows for each substation
        n_sub_edges = len(substations_index)
        total_nodes = total_nodes + n_sub_edges
        total_edges = n_sub_edges + total_edges
        sub_data_init = np.ones(n_sub_edges)
        # from bld row -> substation i
        # from sub row -> edge of corresponding bld from
        sub_edge_index = np.arange(n_building_edges, n_building_edges + n_sub_edges)
        bld_sub_from = buildings_index.get_indexer_for(substations_index)

        # Combine the substation data
        row = np.hstack((row, sub_edge_index, sub_edge_index))
        col = np.hstack((col, bld_sub_from, sub_edge_index + 1))

        # Don't add a negative value here because the substations only "add" head requirement
        # since they aren't in a parallel loop.
        data = np.hstack((data, sub_data_init, sub_data_init))

    design_matrix = scipy.sparse.csr_array((data, (row, col)), shape=(total_edges, total_nodes))
    return design_matrix


def create_sparse_row(design_matrix):
    extra_row = scipy.sparse.csr_matrix(
        ([1], ([0], [design_matrix.shape[1] - 1])), shape=(1, design_matrix.shape[1])
    )
    return extra_row
