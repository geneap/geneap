import uuid
from dataclasses import dataclass
from typing import TYPE_CHECKING

import numpy as np
import structlog

from tessa.sparse_graph import directed_multiply, ones_where_non_empty
from tessa.thermal_system_models import DistrictNetwork, NetworkCosts

from .common import _ApplyAllMixin, select_district_network, update_district_with_network

if TYPE_CHECKING:
    from tessa.district import District

log = structlog.get_logger()


def cost_per_segment(lengths, diameter_m, costs: NetworkCosts):
    """
    Cost per segment depending on lengths and diameters.
    TODO  can later depend on temperatures etc.

    Args:
        lengths:
        diameter_m:
        costs:

    Returns:

    """

    # apply ones_where_non_empty to make sure we don't generate garbage in the sparse multiply
    # TODO: (c)leanup sparse multiply and make more robust.
    piping_cost = np.nan_to_num(costs.c_pipe_var) * diameter_m + (
        np.nan_to_num(costs.c_pipe_fixed) * ones_where_non_empty(lengths, dtype=float)
    )
    trench_cost = np.nan_to_num(costs.c_trench_var) * diameter_m + (
        np.nan_to_num(costs.c_trench_fixed) * ones_where_non_empty(lengths, dtype=float)
    )
    cost_per_m = piping_cost + trench_cost
    return directed_multiply(lengths, cost_per_m)


@dataclass
class DNCosts(_ApplyAllMixin):
    pipe_costs: NetworkCosts | None = NetworkCosts()

    # TODO: (a)dd costs per connection

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        network = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        if network and network.costs is not None:
            costs = network.costs.model_copy(deep=True)
        elif self.pipe_costs is not None:
            costs = self.pipe_costs.model_copy(deep=True)
        else:
            costs = None

        graph = network.graph
        if graph is not None:
            graph = graph.copy()

            if costs is not None and "inner_diameter_mm" in graph:
                graph["cost"] = cost_per_segment(graph.routes, graph.inner_diameter_mm / 1000, costs)

        network = network.model_copy(update=dict(graph=graph, costs=costs), deep=True)

        return update_district_with_network(district, network)

    # def cost_per_meter(self, diameters, costs: NetworkCosts):
    #     piping_cost = np.nan_to_num(costs.c_pipe_var) * diameters + np.nan_to_num(costs.c_pipe_fixed)
    #     trench_cost = np.nan_to_num(costs.c_trench_var) * diameters + np.nan_to_num(costs.c_trench_fixed)
    #     cost_per_m = piping_cost + trench_cost
    #     return cost_per_m
