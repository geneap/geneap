import uuid
from collections.abc import Iterable, Sequence
from dataclasses import dataclass, field
from typing import TYPE_CHECKING

import numpy as np
import pandas as pd
import scipy.sparse
import scipy.sparse.linalg as sp_linalg
import sparse
import structlog
import xarray as xr
from geopandas import GeoSeries
from pandera.typing import DataFrame

from tessa import sparse_graph
from tessa.core.models import PowerLoad
from tessa.national_data import get_country_data
from tessa.sparse_graph import (
    directed_multiply,
    shortest_path,
    sparse_with_attr,
    undirect_full,
)
from tessa.thermal_system_models import (
    DistrictNetwork,
    FlowProperties,
    NetworkDemand,
    SupplyCurve,
    ThermalSource,
)
from tessa.thermal_system_models.pipes import (
    load_hourly_heat_loss_percentage,
    load_pipe_velocity_table,
    pipe_nominal_diameters_from_velocity_table,
)
from tessa.utils.pydantic_shapely import Point

from ..models import Substation
from .common import (
    _ApplyAllMixin,
    create_node_matrix,
    select_district_network,
    substation_connection_ids,
    update_district_with_network,
)
from .heat_sources import get_closest_building_to_point

if TYPE_CHECKING:
    from tessa.district import District

log = structlog.get_logger()


def get_heat_source_options(country="ch"):
    # From KBOB file, note co2 intensity related to useful heat delivered
    ch_data = get_country_data(country)

    fuels = {code: fdata | {"code": code} for code, fdata in ch_data["fuels"].items()}

    heat_source_options = [
        hs | {"code": k, "fuel": fuels[hs["fuel"]]}
        for i, (k, hs) in enumerate(ch_data["heat_sources"].items())
    ]
    return heat_source_options


def _update_source(source, power_demand_curve, unmet_demand):
    # Thermal sources can have existing supply curves
    if source.supply_curve is None:
        p_supply_potential_kw = (
            np.ones_like(unmet_demand.heat_demand_remainder.values) * source.rated_power_useful
        )
    else:
        p_supply_potential_kw = source.supply_curve.p_supply_potential_kw
    # stack the heat demand with the potential supply and use nanmin to get
    # the smallest of the two to define the actual supply
    pwr_demand = unmet_demand.heat_demand_remainder.values
    pwr_demand = np.vstack([pwr_demand, p_supply_potential_kw])
    p_supply_kw = np.nanmin(pwr_demand, axis=0)
    supply_curve = pd.DataFrame(
        {"p_supply_potential_kw": p_supply_potential_kw, "p_supply_kw": p_supply_kw},
        index=power_demand_curve.index,
    )
    supply_curve = DataFrame[SupplyCurve](supply_curve)
    energy_useful = supply_curve.p_supply_kw.mean() * 8760
    new_source = ThermalSource(
        **(
            source.model_dump()
            | {
                "supply_curve": DataFrame[SupplyCurve](supply_curve),
                "energy_useful": energy_useful,
            }
        )
    )
    return new_source


def _update_supply_curves(substations: Iterable[Substation], power_demand_curve):
    unmet_demand = pd.DataFrame(
        power_demand_curve.p_with_loss_kw.values,
        columns=["heat_demand_remainder"],
        index=power_demand_curve.index,
    )

    new_substations = []
    for sub in substations:
        new_sources = []
        for source in sub.sources:
            new_source = _update_source(source, power_demand_curve, unmet_demand)
            new_sources.append(new_source)
            unmet_demand.heat_demand_remainder = (
                unmet_demand.heat_demand_remainder - new_source.supply_curve.p_supply_kw
            )

        # new_substations.append(Substation(sources=new_sources))
        new_substations.append(sub.model_copy(update=dict(sources=new_sources), deep=True))
    return new_substations


@dataclass
class DNHeatFlowClassic(_ApplyAllMixin):
    """
    Builder class to apply heat flow calculations based on the total heat needed to
    reach each building in the network.

    Requires the thermal network already has a graph layout, does not generate one if
    it is missing.
    """

    hourly_network_loss: pd.Series
    # TODO: (t)hese monthly losses are not actually used!
    # monthly_network_loss: pd.Series
    fluid_properties: FlowProperties | None = None
    # TODO: (4)% heat loss by default, needs to change in function of temperature levels
    #  For a simple model can set about 4% for HighT and 2% for low T. Later do the real loss.
    loss_fraction_km: float = 0.04
    pipe_velocity_table: pd.DataFrame = field(default_factory=load_pipe_velocity_table)

    def _prep_flow(self, district_network: DistrictNetwork):
        if self.fluid_properties is None:
            if district_network and district_network.flow is not None:
                return district_network.flow.model_copy(deep=True)
        return self.fluid_properties

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        """
        Update an existing district network with the heat flow calculations

        Note: must first generate a layout, that will set the building_ids,
        that are the subset of district buildings that are connected to this network

        Args:
            district:
            district_network:

        Returns:

        """

        network: DistrictNetwork | None = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        if network.graph is None:
            log.warning("No graph data for DHN")
            return district

        fluid = self._prep_flow(network)
        load_curve = district.load_curve
        substations = network.substations
        graph: xr.Dataset = network.graph
        # Extract data subset for the subgraph
        # Use the previously defined building IDs, normally set by the
        # layout algorithm.
        routes = graph.routes
        buildings = district.buildings_table(network.building_ids)
        heat = buildings["q_hww_kwh"].astype(float)
        power = buildings["p_hww_max1d_kw"].astype(float)
        points = GeoSeries(buildings.geometry.copy())  # use native CRS

        # Skip flow calculations if there are no heat sources
        if len(substations) > 0 and len(points) > 0 and load_curve is not None:
            heat_flows = self.substations_heat_flow(points, heat, power, substations, routes)
            if heat_flows is None:
                return district
            # Change the direction of the routes to match the power flows, this makes things much easier later
            routes_doubled = undirect_full(routes)
            mask = heat_flows.heat_total.astype(bool).astype(int).astype(float)
            routes_directed = routes_doubled * mask
            network.graph["routes"] = routes_directed

            # Make sure to prune empty
            # dat = sparse_graph.ensure_xarray_sparse_coo(routes_directed)
            # dat.data = sparse.COO(dat.data.coords, shape=dat.data.shape, data=dat.data.data, prune=True)
            # network.graph["routes"].data = dat

            graph["heat_total"] = heat_flows["heat_total"]
            graph["power_total"] = heat_flows["power_total"]
            graph["heat_loss"] = heat_flows["heat_loss"]

            power_demand_curve = self._load_curve(graph, heat, load_curve.p_hww_kw)
            substations = _update_supply_curves(substations, power_demand_curve)
        else:
            power_demand_curve = None

        # Estimate diameters to match the power flow requirement
        if fluid is not None and "power_total" in graph:
            diameters = self._estimate_diameters(fluid, graph)
            graph["inner_diameter_mm"] = diameters

        network = network.model_copy(
            update=dict(
                graph=graph, flow=fluid, substations=substations, power_demand_curve=power_demand_curve
            ),
            deep=True,
        )

        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def substations_heat_flow(
        self, points, heat, power, substations: Sequence[Substation], routes: xr.DataArray
    ) -> xr.Dataset | None:
        """
        If we have any injection points or heat source positions, do each source individually.
        Any sources that are missing get assigned the same location as the biggest heat source
        that has a position defined.

        Args:
            heat:
            substations:
            points:
            power:
            routes:

        Returns: Graph as a dataset with variables heat_total, power_total, heat_loss

        """

        partial_graphs = []

        total_rated_power_useful = sum(
            sub.rated_power_useful for sub in substations if not pd.isna(sub.rated_power_useful)
        )
        for sub in substations:
            # Try to get the inject point info from the existing injection point,
            # if it fails revert to spatial search.
            if sub.connection_point is not None and sub.connection_point.node_id in heat.index:
                root_label = sub.connection_point.node_id
                root_idx = heat.index.get_loc(root_label)
            elif sub.position is not None:
                # Need to get the closest BUILDING point to the Heat source point -> this is the
                # injection point because in the simplfied graph only have distances
                # THEN get the routing vertex for the points to get the on-road position.
                root_idx, root_label = get_closest_building_to_point(points, sub.position)
            else:
                # default to a central-ish point
                sub.position = Point(points.unary_union.representative_point())
                root_idx, root_label = get_closest_building_to_point(points, sub.position)

            source_fraction = 1.0
            if sub.rated_power_useful is not None and total_rated_power_useful > 0:
                # Split the total power demand between thermal sources that might have different
                # positions based on the ratios of the different sources,
                source_fraction = sub.rated_power_useful / total_rated_power_useful

            # calculate the network flows.
            subgraph_partial_load = self._calc_network_energy_flow(
                heat * source_fraction, power * source_fraction, routes, root_idx
            )
            partial_graphs.append(subgraph_partial_load)

        if len(partial_graphs) == 0:
            log.warning(
                f"No partial graphs created for this heat flow calculation, N substations {len(substations)} were provided"
            )
            return None
        else:
            graph = partial_graphs[0]
            for partial_graph in partial_graphs[1:]:
                graph += partial_graph
        return graph

    def _calc_network_energy_flow(self, heat, power, routes, root_idx) -> xr.Dataset:
        """

        Args:
            heat:
            power:
            routes:
            root_idx:

        Returns: Graph as a dataset with variables heat_total, power_total, heat_loss

        """

        # pre-calculate preds shared across several calculations
        # TODO: (r)un checks on preds to make sure the root make sense
        # TODO: what happens to zero-length segments (buildings share a routing point?)
        _, preds = shortest_path(
            routes,
            directed=False,
            return_predecessors=True,
            indices=[root_idx],
        )

        # Since we asked for shortest paths for a single node, preds is 1D row
        # of indexes describing the path
        # NOTE: could also do a sparse matrix solve
        preds = preds.squeeze()
        heat_per_segment = sparse_graph.graph_flow_total_per_segment(routes, heat, root_idx, preds=preds)
        power_per_segment = sparse_graph.graph_flow_total_per_segment(routes, power, root_idx, preds=preds)

        # IMPORTANT: linear density != total/routes matrix because flow can be reversed w.r.t
        # the route -> this function takes care of doing non-directed division
        heat_loss = directed_multiply(self.loss_fraction_km * heat_per_segment, routes / 1000)

        # Disabled heat loss fraction because directed div was too fragile.
        # heat_loss_fraction = directed_div(heat_loss, heat_per_segment)
        # heat_loss_fraction = sparse_with_attr(heat_loss_fraction, fill_value=0.0)

        return xr.Dataset(
            {
                "heat_total": heat_per_segment,
                "power_total": power_per_segment,
                "heat_loss": heat_loss,
                # 'heat_loss_fraction': heat_loss_fraction,
            }
        )

    def _load_curve(self, graph, heat: pd.Series, load_curve: pd.Series) -> DataFrame[NetworkDemand]:
        """
        Calculate the annual loss, then sum and produce the time series
        for total loss to then get production

        NOTE: ideally need to re-calculate load curve for sub-components...
        For now just re-scale, but different sub-graphs could have different building mixes.
        One option would be to pass the heat calculation engine into the this class.

        Args:
            graph:
            heat: kWh heat demands of the buildings
            load_curve:

        Returns:

        """
        demand_curve = pd.DataFrame({"p_hww_kw": (heat.sum() / (load_curve.mean() * 8760)) * load_curve})

        # Important: don't want to end up with a hourly loss per segemnt (large data, also pointless!)
        # so get total heat energy loss as scalar
        if "heat_loss" in graph:
            annual_heat_loss = graph.heat_loss.sum().item()
            hourly_heat_loss = self.hourly_network_loss * annual_heat_loss
            # conform hourly heat loss to the index of the load curves
            idx: pd.DatetimeIndex = demand_curve.index
            hourly_heat_loss = pd.Series(
                hourly_heat_loss[(idx.dayofyear - 1) * 24 + idx.hour + 1].values, index=idx
            )
        else:
            hourly_heat_loss = pd.Series(np.zeros(len(demand_curve)), index=demand_curve.index)

        # hourly production is sum of demand and losses (final energy)
        # If no losses are calculated then we can still estimate the overall load
        demand_curve["p_loss_kw"] = hourly_heat_loss
        demand_curve["p_with_loss_kw"] = demand_curve["p_hww_kw"] + hourly_heat_loss
        return DataFrame[NetworkDemand](demand_curve)

    def _estimate_diameters(self, flow_properties, graph):
        # Need to monkey about to get the diameter data into the right xarray format
        power_total_data = graph.power_total.data.data
        diameters = pipe_nominal_diameters_from_velocity_table(
            power_total_data,
            temp_going=flow_properties.temp_going,
            temp_return=flow_properties.temp_return,
            fluid_cp=flow_properties.heat_capacity,
            fluid_density=flow_properties.density,
            pipe_velocity_table=self.pipe_velocity_table,
        )
        diameters = sparse_with_attr(graph.power_total, data=diameters, name="nominal_diameter_m")
        diameters.attrs["units"] = "meters"
        return diameters


class SwissDnHeatFlowClassic(DNHeatFlowClassic):
    def __init__(self, fluid_properties: FlowProperties | None = None):
        fluid_properties = fluid_properties or FlowProperties()
        super().__init__(
            fluid_properties=fluid_properties,
            hourly_network_loss=load_hourly_heat_loss_percentage(),
            # monthly_network_loss=load_monthly_heat_loss_percentage(),
            # heat_source_options=get_heat_source_options(),
        )


@dataclass
class ThermalDistribution(_ApplyAllMixin):
    """
    Builder class to apply heat flow calculations based on the total heat needed to
    reach each building in the network.

    Compared to .class:`.DNHeatFlowClassic`, this only calculates the thermal flows
    needed to meet demand, but does not do any fluid calculation or set pipe diameters so
    that this can be done in a separate step.

    Requires the thermal network already has a graph layout, does not generate one if
    it is missing.

    Currently the thermal distribution is calculated using a numeric method based on superposing
    the graphs of the distribution of heat from multiple heat sources. Each graph is calcualted
    using shortest route between heat sources and each building.

    This is a bit unorthodox, the traditional method is to solve a set of linear equations using
    a matrix solver, that will calculate the thermal flows in and out of each node.

    """

    # TODO: (4)% heat loss by default, needs to change in function of temperature levels
    #  For a simple model can set about 4% for HighT and 2% for low T. Later do the real loss.
    loss_fraction_km: float = 0.04
    hourly_network_loss: pd.Series = field(default_factory=load_hourly_heat_loss_percentage)

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
    ) -> "District":
        """
        Update an existing district network with the heat flow calculations

        Note: must first generate a layout, that will set the building_ids,
        that are the subset of district buildings that are connected to this network

        Args:
            district:
            district_network:

        Returns:

        """

        network = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        if network.graph is None:
            log.warning("No graph data for DHN")
            return district

        graph: xr.Dataset = network.graph
        load_curve = district.load_curve
        substations = network.substations
        # Extract data subset for the subgraph
        # Use the previously defined building IDs, normally set by the
        # layout algorithm.
        routes = graph.routes
        buildings = district.buildings.reindex(network.building_ids)
        heat = buildings["q_hww_kwh"].astype(float)
        power = buildings["p_hww_max1d_kw"].astype(float)
        points = GeoSeries(buildings.geometry.copy())  # use native CRS
        # Skip flow calculations if there are no heat sources

        if len(substations) > 0 and len(points) > 0:
            # Now that we calculate the flow, force the routes to follow the same
            # direction as the flow for simiplicty.
            heat_flows = self.substations_heat_distribution(points, heat, power, substations, routes)
            if heat_flows is None:
                # Could not apply heat distribution
                return district

            graph = self._make_routes_directed(graph, heat_flows["heat_total"])

            graph["heat_total"] = heat_flows["heat_total"]
            graph["power_total"] = heat_flows["power_total"]
            graph["heat_loss"] = heat_flows["heat_loss"]

        if load_curve is not None:
            for var in graph.data_vars:
                graph[var].attrs["is_directed"] = True
            power_demand_curve = self._load_curve(graph, heat, load_curve.p_hww_kw)
            substations = _update_supply_curves(substations, power_demand_curve)
        else:
            power_demand_curve = None

        network = network.model_copy(
            update=dict(graph=graph, substations=substations, power_demand_curve=power_demand_curve),
            deep=True,
        )

        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def _make_routes_directed(self, graph, heat_flows):
        """
        To make life easier we force routes distances to adopt the same directions
        as the calculated heat flows. To work around several issues we used these tricks:

        - For routes of length zero, we add a very small value so that they don't get
        "eaten" by the sparse conversion that assumes 0 as fill value. Ideally we would use
        a non-0 fill value (e.g. np.nan) but other sparse functions (e.g. scipy.sparse) don't
        support non-0 fill values.
        - To "undirect" the routes we add the matrix to its transpose then mask by the non-zero
        elements of the directed matrix.

        """
        # Make sure to prune empty
        mask = heat_flows.astype(bool).astype(int)
        routes = sparse_graph.ensure_xarray_sparse_coo(graph.routes)
        routes.data.data[graph.routes.data.data == 0] += 1e-5
        routes_doubled = undirect_full(graph.routes)
        routes_directed = routes_doubled * mask
        routes_directed.data = sparse.COO(
            routes_directed.data.coords,
            shape=routes_directed.data.shape,
            data=routes_directed.data.data,
            prune=True,
        )
        graph["routes"] = routes_directed
        graph["routes"].attrs["is_directed"] = True
        return graph

    def substations_heat_distribution(
        self, points, heat, power, substations: Sequence[Substation], routes: xr.DataArray
    ) -> xr.Dataset | None:
        """
        If we have any injection points or heat source positions, do each source individually.
        Any sources that are missing get assigned the same location as the biggest heat source
        that has a position defined.

        Args:
            heat:
            substations:
            points:
            power:
            routes:

        Returns: Graph as a dataset with variables heat_total, power_total, heat_loss

        """

        partial_graphs = []

        total_rated_power_useful = sum(
            sub.rated_power_useful for sub in substations if not pd.isna(sub.rated_power_useful)
        )
        for sub in substations:
            # Try to get the inject point info from the existing injection point,
            # if it fails revert to spatial search.
            if sub.connection_point is not None and sub.connection_point.node_id in heat.index:
                root_label = sub.connection_point.node_id
                root_idx = heat.index.get_loc(root_label)
            elif sub.position is not None:
                # Need to get the closest BUILDING point to the Heat source point -> this is the
                # injection point because in the simplfied graph only have distances
                # THEN get the routing vertex for the points to get the on-road position.
                root_idx, root_label = get_closest_building_to_point(points, sub.position)
            else:
                # default to a central-ish point
                sub.position = Point(points.unary_union.representative_point())
                root_idx, root_label = get_closest_building_to_point(points, sub.position)

            source_fraction = 1.0
            if sub.rated_power_useful is not None and total_rated_power_useful > 0:
                # Split the total power demand between thermal sources that might have different
                # positions based on the ratios of the different sources,
                source_fraction = sub.rated_power_useful / total_rated_power_useful
            else:
                log.warning("No rated_power_useful, assuming 100% of demand supplied here")
            # calculate the network flows.
            subgraph_partial_load = self._calc_network_energy_flow(
                heat * source_fraction, power * source_fraction, routes, root_idx
            )

            partial_graphs.append(subgraph_partial_load)

        if len(partial_graphs) == 0:
            log.warning(
                f"No partial graphs created for this heat flow calculation, N substations {len(substations)} were provided"
            )
            return None
        else:
            graph = partial_graphs[0]
            for partial_graph in partial_graphs[1:]:
                graph += partial_graph

        return graph

    def _calc_network_energy_flow(self, heat, power, routes, root_idx, eps=1e-5) -> xr.Dataset:
        """

        Args:
            heat:
            power:
            routes:
            root_idx:
            eps: to stop values disappearing in sparse conversion, a very small value can be added to the power flows

        Returns: Graph as a dataset with variables heat_total, power_total, heat_loss

        """
        if eps != 0:
            heat[heat == 0] = eps
            power[power == 0] = eps

        # pre-calculate preds shared across several calculations
        # TODO: (r)un checks on preds to make sure the root make sense
        # TODO: what happens to zero-length segments (buildings share a routing point?)
        _, preds = shortest_path(
            routes,
            directed=False,
            return_predecessors=True,
            indices=[root_idx],
        )
        # Since we asked for shortest paths for a single node, preds is 1D row
        # of indexes describing the path
        preds = preds.squeeze()
        heat_per_segment = sparse_graph.graph_flow_total_per_segment(routes, heat, root_idx, preds=preds)
        power_per_segment = sparse_graph.graph_flow_total_per_segment(routes, power, root_idx, preds=preds)

        # IMPORTANT: linear density != total/routes matrix because flow can be reversed w.r.t
        # the route -> this function takes care of doing non-directed division
        heat_loss = directed_multiply(self.loss_fraction_km * heat_per_segment, routes / 1000)

        # Disabled heat loss fraction because directed div was too fragile.
        # heat_loss_fraction = directed_div(heat_loss, heat_per_segment)
        # heat_loss_fraction = sparse_with_attr(heat_loss_fraction, fill_value=0.0)

        return xr.Dataset(
            {
                "heat_total": heat_per_segment,
                "power_total": power_per_segment,
                "heat_loss": heat_loss,
                # 'heat_loss_fraction': heat_loss_fraction,
            }
        )

    def _load_curve(self, graph, heat: pd.Series, load_curve: pd.Series) -> DataFrame[NetworkDemand]:
        """
        Calculate the annual loss, then sum and produce the time series
        for total loss to then get production

        NOTE: ideally need to re-calculate load curve for sub-components...
        For now just re-scale, but different sub-graphs could have different building mixes.
        One option would be to pass the heat calculation engine into the this class.

        Args:
            graph:
            heat: kWh heat demands of the buildings
            load_curve:

        Returns:

        """
        demand_curve = pd.DataFrame({"p_hww_kw": (heat.sum() / (load_curve.mean() * 8760)) * load_curve})

        # Important: don't want to end up with a hourly loss per segemnt (large data, also pointless!)
        # so get total heat energy loss as scalar
        if "heat_loss" in graph:
            annual_heat_loss = graph.heat_loss.sum().item()
            hourly_heat_loss = self.hourly_network_loss * annual_heat_loss
            # conform hourly heat loss to the index of the load curves
            idx: pd.DatetimeIndex = demand_curve.index
            hourly_heat_loss = pd.Series(
                hourly_heat_loss[(idx.dayofyear - 1) * 24 + idx.hour + 1].values, index=idx
            )
        else:
            hourly_heat_loss = pd.Series(np.zeros(len(demand_curve)), index=demand_curve.index)

        # hourly production is sum of demand and losses (final energy)
        # If no losses are calculated then we can still estimate the overall load
        demand_curve["p_loss_kw"] = hourly_heat_loss
        demand_curve["p_with_loss_kw"] = demand_curve["p_hww_kw"] + hourly_heat_loss
        return DataFrame[NetworkDemand](demand_curve)


@dataclass
class ThermalDistributionTwoPipe(_ApplyAllMixin):
    loss_fraction_km: float = 0.04
    hourly_network_loss: pd.Series = field(default_factory=load_hourly_heat_loss_percentage)

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | DistrictNetwork | None = None,
    ) -> "District":
        """
        Args:
            district:
            district_network:

        Returns:

        """

        network = select_district_network(district, district_network)
        if network is None:
            return self._apply_to_all_district_networks(district)

        if network.graph is None or network.building_connections is None:
            log.warning("No graph data for DHN")
            return district

        network_table: pd.DataFrame = network.graph_table

        substations = network.substations
        buildings = district.buildings_table(network.building_connections.index)

        # Solve for the energy needs
        try:
            network_energy, substation_energy = self._solve(
                network_table, buildings, substations, "q_hww_kwh"
            )
        except RuntimeError as e:
            log.warning(f"Solve failed with error {e}")
            return district
        network_table["heat_total"] = network_energy

        if district.load_curve is not None:
            # Update substations and power demand curve.
            power_demand_curve = self._load_curve(
                network_table, buildings["q_hww_kwh"].sum(), district.load_curve.p_hww_kw
            )
            substations = self._spread_supply_curves(
                network.substations, substation_energy, power_demand_curve
            )
        else:
            power_demand_curve = None

        # solve for power needs
        network_pwr, substation_pwr = self._solve(network_table, buildings, substations, "p_hww_max1h_kw")
        network_table["power_total"] = network_pwr

        # swap directions so that negative flows become positive in opposite direction
        network_table = self._harmonise_directions(network_table)

        # Calculate heat loss
        network_table["heat_loss"] = (
            self.loss_fraction_km * network_table["heat_total"] * network_table["routes"] / 1000
        )

        graph = sparse_graph.dataframe_to_sparse_with_coords(network_table, network.graph.routes)

        network = network.model_copy(
            update=dict(graph=graph, substations=substations, power_demand_curve=power_demand_curve),
            deep=True,
        )

        # Update the district object with the new buildings and return a new DISTRICT
        return update_district_with_network(district, network)

    def _harmonise_directions(self, network_table):
        """Swap directions turning negative values in one direction into
        positive values in the opposite direction (swapping the to/from ids)"""
        neg = network_table[network_table["heat_total"] < 0]
        neg = neg.swaplevel()
        neg.index.names = reversed(neg.index.names)
        neg = np.abs(neg)
        network_table = pd.concat([network_table[network_table["heat_total"] >= 0], neg])
        return network_table

    def _solve(self, network_table, buildings, substations, var):
        """Solve the flow problem using linear algebra.
        Set up a system of equations describing the total heat flows per node
        such that the sum of inflows and outflows at each network node equals the
        net heat demanded by the building at that node or the heat injection from a
        substation at that node.

        Args:
            network_table:
            buildings:
            substations:
            var:

        Returns:

        """
        coeff_matrix = create_node_matrix(
            network_table.index, buildings.index, substation_connection_ids(substations)
        )

        node_vals = buildings[var].fillna(0).to_numpy()
        total_rated_power = sum(sub.rated_power_useful for sub in substations)

        if total_rated_power > 0:
            # Split required supply across substations according to size and
            # swap direction w.r.t. building power demand
            sub_supply = (
                -1
                * buildings[var].sum()
                * np.array(list(sub.rated_power_useful / total_rated_power for sub in substations))
            )
        elif len(substations) > 0:
            sub_supply = (
                -1
                * buildings[var].sum()
                * np.array(list(sub.rated_power_useful / len(substations) for sub in substations))
            )
        else:
            raise RuntimeError("Could not setup substations power for solve")

        node_vals = np.hstack((node_vals, sub_supply))

        # since we have n nodes -1 edges in the matrix, drop the value for one
        # node and calculate at the end with knowledge that last node should be sum of other nodes.
        node_vals = node_vals[:-1]

        # Drop the last row of the coeff matrix since it has N-nodes rows and N edges = N-nodes - 1 columns
        # Could drop any row since there is exactly one row of redundant information. CSR matrix allows fast
        # row drop.
        coeff_matrix = coeff_matrix[:-1, :]

        edge_flows = sp_linalg.spsolve(coeff_matrix, node_vals)
        network_soln = edge_flows[: len(network_table)]
        substation_soln = edge_flows[-len(substations) :]
        return network_soln, substation_soln

    def _load_curve(
        self, network_table, total_heat: float, load_curve: pd.Series
    ) -> DataFrame[NetworkDemand]:
        """
        Calculate the annual loss, then sum and produce the time series
        for total loss to then get production

        NOTE: ideally need to re-calculate load curve for sub-components...
        For now just re-scale, but different sub-graphs could have different building mixes.
        One option would be to pass the heat calculation engine into the this class.

        Args:
            graph:
            heat: kWh heat demands of the buildings
            load_curve:

        Returns:

        """
        demand_curve = pd.DataFrame({"p_hww_kw": (total_heat / (load_curve.mean() * 8760)) * load_curve})

        # Important: don't want to end up with a hourly loss per segemnt (large data, also pointless!)
        # so get total heat energy loss as scalar
        if "heat_loss" in network_table:
            annual_heat_loss = network_table.heat_loss.sum().item()
            hourly_heat_loss = self.hourly_network_loss * annual_heat_loss
            # conform hourly heat loss to the index of the load curves
            idx: pd.DatetimeIndex = demand_curve.index
            hourly_heat_loss = pd.Series(
                hourly_heat_loss[(idx.dayofyear - 1) * 24 + idx.hour + 1].values, index=idx
            )
        else:
            hourly_heat_loss = pd.Series(np.zeros(len(demand_curve)), index=demand_curve.index)

        # hourly production is sum of demand and losses (final energy)
        # If no losses are calculated then we can still estimate the overall load
        demand_curve["p_loss_kw"] = hourly_heat_loss
        demand_curve["p_with_loss_kw"] = demand_curve["p_hww_kw"] + hourly_heat_loss
        return DataFrame[NetworkDemand](demand_curve)

    def _spread_supply_curves(
        self, substations: Iterable[Substation], substations_energy, power_demand_curve
    ):
        """
        1. Divide the supply according to the share for each substation calculation from the flow solution
        2. Within each substation, stack the supply curves across the multiple sources.
        Args:
            substations:
            power_demand_curve:

        Returns:

        """
        unmet_demand = pd.DataFrame(
            power_demand_curve.p_with_loss_kw.values,
            columns=["heat_demand_remainder"],
            index=power_demand_curve.index,
        )
        substations_energy = np.abs(substations_energy)
        substations_total_energy = np.sum(substations_energy)

        new_substations = []
        for sub, sub_energy in zip(substations, substations_energy):
            # Split the total unmet demand in proportion to the energy supplied by this substation
            sub_unmet_demand = unmet_demand * sub_energy / substations_total_energy
            # tot_pwr = sub.rated_power_useful
            # Stack the power from sources in order.
            # TODO: (a)dd option for sharing by rated power rather than stacking
            # TODO: (a)dd option for sharing by other priorities e.g. baseload, cap, min rated power...
            new_sources = []
            for source in sub.sources:
                # pwr_share = source.rated_power_useful / tot_pwr
                # energy_useful = pwr_share * sub_energy

                # Thermal sources can have existing supply curves
                if source.supply_curve is None:
                    p_supply_potential_kw = (
                        np.ones_like(sub_unmet_demand.heat_demand_remainder.values)
                        * source.rated_power_useful
                    )
                else:
                    p_supply_potential_kw = source.supply_curve.p_supply_potential_kw

                # stack the heat demand with the potential supply and use nanmin to get
                # the smallest of the two to define the actual supply
                pwr_demand = sub_unmet_demand.heat_demand_remainder.values
                pwr_demand = np.vstack([pwr_demand, p_supply_potential_kw])
                p_supply_kw = np.nanmin(pwr_demand, axis=0)
                supply_curve = pd.DataFrame(
                    {"p_supply_potential_kw": p_supply_potential_kw, "p_supply_kw": p_supply_kw},
                    index=power_demand_curve.index,
                )
                energy_useful = supply_curve.p_supply_kw.mean() * 8760
                new_source = ThermalSource(
                    **(
                        source.model_dump()
                        | {
                            "supply_curve": DataFrame[SupplyCurve](supply_curve),
                            "energy_useful": energy_useful,
                        }
                    )
                )

                sub_unmet_demand.heat_demand_remainder = (
                    sub_unmet_demand.heat_demand_remainder - supply_curve.p_supply_kw
                )
                new_sources.append(new_source)

            new_substations.append(sub.model_copy(update=dict(sources=new_sources), deep=True))
        return new_substations


def create_heat_distribution_coeffs_slow(building_index, network_index, substations=[]):
    """
    Define a system of linear equations as a matrix problem Ax = b
    where x is the heat transfer on each network connection (edge) and b is
    the heat demand at each building.

    Construct matrix A such that

    A1.x1 + A2.x2 + A3.x3 + A4.x4 = b1

    A(n) is the edge going into (positive 1) or out of (negative 1) building node x(n)

    Args:
        buildings:
        district_network:

    Returns:

    """
    n_buildings = len(building_index)
    n_substations = len(substations)
    n_edges = len(network_index)
    n_equations = n_buildings + n_substations

    # Construct sparse matrix using DOK
    # keys are (edge index, building index)
    # values are 1 for inward edge from the building (from id_from)
    # values are -1 for outward edge from the building (from id_to)
    design_matrix = scipy.sparse.dok_array((n_equations, n_equations))
    for i, (id_from, id_to) in enumerate(network_index):
        design_matrix[i, building_index.get_loc(id_from)] = -1
        design_matrix[i, building_index.get_loc(id_to)] = 1

    # For each heat source add an outgoing route to the target building
    for i, sub in enumerate(substations):
        egid = sub.connection_point.node_id
        design_matrix[n_edges - 1 + i, building_index.get_loc(egid)] = -1
    return design_matrix
