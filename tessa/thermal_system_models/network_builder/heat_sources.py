import uuid
from collections.abc import Collection, Iterable
from dataclasses import dataclass
from typing import TYPE_CHECKING

import geopandas as gpd
import structlog
import xarray as xr
from pandera.typing.geopandas import GeoDataFrame
from pydantic import BaseModel, ConfigDict

from tessa import sparse_graph
from tessa.road_network_routing import BuildingRouting
from tessa.thermal_system_models import DistrictNetwork, Substation, ThermalInjectionPoint
from tessa.thermal_system_models.models import ConnectionPoint
from tessa.utils.pydantic_shapely import Point

from ..models import BuildingConnections, Pipes
from .common import _ApplyAllMixin, select_district_network, update_district_with_network

if TYPE_CHECKING:
    from tessa.district import District

log = structlog.get_logger()


def get_closest_building_to_point(points: gpd.GeoSeries, source_point: Point, max_distance_meters=1000):
    """
    NOTE: Using very rough equation to convert distance in meters to degrees for latlon geom
    https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm

    Args:
        points:
        source_point:
        max_distance_meters:

    Returns:

    """

    conversion = 0.000009009009  # 0.00001 / 1.11
    max_distance = max_distance_meters * conversion
    in_point_idx, search_geom_idx = points.sindex.nearest(
        source_point.geometry, max_distance=max_distance, return_all=False
    )
    search_geom_idx = search_geom_idx.item()
    injection_building = points.index[search_geom_idx].item()

    return search_geom_idx, injection_building


@dataclass
class SubstationLocator(_ApplyAllMixin):
    """
    Builder class to configure substations locations and the position of thir
    connection (injection) points to the thermal grid.

    """

    routing_service: BuildingRouting
    max_distance: float = 1000

    def __call__(
        self,
        district: "District",
        *,
        district_network: str | uuid.UUID | None = None,
        substations: Collection[Substation] | None = None,
    ) -> "District":
        """
        Update an existing district network substation objects setting
        their position in the grid if it is not set and setting the positions of
        their connections to the grid if they are not set.

        Args:
            district:
            district_network:
            substations:

        Returns:

        """

        dn = select_district_network(district, district_network)
        if dn is None:
            return self._apply_to_all_district_networks(district, substations=substations)
        substations_ = substations if substations is not None else list(dn.substations)

        buildings = district.buildings
        points = buildings.geometry.copy()

        # Extract data subset for the subgraph
        points = points.loc[dn.building_ids]
        power = buildings.loc[dn.building_ids, "p_hww_max1d_kw"].astype(float)

        if len(substations_) > 0 and (
            any(((s.position is not None) or (s.connection_point is not None)) for s in substations_)
        ):
            substations_ = self.set_connections(substations_, points)

        elif len(substations_) > 0 and all(s.position is None for s in substations_):
            graph = dn.graph

            if graph is not None and "distances" in graph:
                distances = graph.distances
            elif graph is not None and "routes" in graph.data_vars:
                distances = graph.routes.copy()
            else:
                # TODO: (o)r reload distance matrix...
                log.warning("Reloading distances from scratch")
                distances = self.routing_service.load_distance_matrix(points)

            substations_ = self.guess_location(substations_, points, power, distances)

        # FIXME: had to explicitly re-validate pandera dataframes because they "lost" their wrapper.
        network = dn.model_copy(
            update=dict(
                # graph=dn.graph,
                pipes=GeoDataFrame[Pipes](dn.pipes),
                substations=substations_,
            ),
            deep=True,
        )

        return update_district_with_network(district, network)

    def guess_location(self, substations: Iterable[Substation], points, power, distances: xr.DataArray):
        # There are sources but no source position given, generate automatically
        if distances is not None:
            _, root_label = self.get_most_central_building(distances, points)
        else:
            _, root_label = power.argmax(), power.idxmax()
        inject_point = self._get_injection_points(points, root_label)
        # Assign missing positions and calculate, don't need to divide powers
        # because they all come from same point
        for hs in substations:
            hs.position = Point(inject_point)
            hs.connection_point = ConnectionPoint(position=Point(inject_point), node_id=int(root_label))
        return substations

    def set_connections(self, substations: Iterable[Substation], points):
        """
        If we have any injection points or heat source positions, do each source individually.
        Any sources that are missing get assigned the same location as the biggest heat source
        that has a position defined.

        Args:
            substations
            points:

        Returns:

        """
        position = self._position_of_biggest_heat_source(substations)
        out = []
        for sub in substations:
            # Assign missing positions
            if sub.position is None:
                if sub.connection_point is not None:
                    sub.position = sub.connection_point.position
                else:
                    sub.position = position
            # Try to get the inject point info from the existing injection point,
            # if it fails revert to spatial search.

            # TODO: (a)llow to manually set the connection point, today we always auto snap
            # if sub.connection_point is not None and sub.connection_point.node_id in points.index:
            #     pass
            # else:
            # Need to get the closest BUILDING point to the Heat source point -> this is the
            # injection point because in the simplfied graph only have distances
            # THEN get the routing vertex for the points to get the on-road position.
            if sub.position is not None:
                root_idx, root_label = get_closest_building_to_point(points, sub.position)
            else:
                # default to a central-ish point
                sub.position = Point(points.unary_union.representative_point())
                root_idx, root_label = get_closest_building_to_point(points, sub.position)
            inject_point = self._get_injection_points(points, root_label)

            sub.connection_point = ConnectionPoint(position=Point(inject_point), node_id=int(root_label))
            out.append(sub)

        return out

    def get_most_central_building(self, distances, points: gpd.GeoDataFrame | gpd.GeoSeries):
        root_idx = sparse_graph.sparse_centrality(distances, penality=self.max_distance)
        root_label = points.index[root_idx]
        return root_idx, root_label

    def _position_of_biggest_heat_source(self, substations: Iterable[Substation]) -> Point | None:
        pos = None
        hs_with_pos = [hs for hs in substations if hs.position is not None]
        if len(hs_with_pos) > 0:
            biggest = hs_with_pos[0]
            for hs in hs_with_pos:
                if hs.rated_power_useful is not None and hs.rated_power_useful > biggest.rated_power_useful:
                    biggest = hs
            pos = biggest.position
        if pos is None:
            hs_with_inj = [hs for hs in substations if hs.connection_point is not None]
            if len(hs_with_inj) > 0:
                biggest = hs_with_inj[0]
                for hs in hs_with_pos:
                    if (
                        hs.rated_power_useful is not None
                        and hs.rated_power_useful > biggest.rated_power_useful
                    ):
                        biggest = hs
                pos = biggest.position
        return pos

    def _get_injection_points(self, points: gpd.GeoDataFrame | gpd.GeoSeries, root_labels):
        """
        Get the point on the road (routing point) closest to the given point

        Args:
            points:
            root_labels:

        Returns:

        """
        if not isinstance(root_labels, Iterable):
            labels = [root_labels]
        else:
            labels = root_labels
        source_points = self.routing_service.routing_vertex_for_points(points.loc[labels])
        source_points = source_points.geometry
        # If given a single label return the single point directly
        if not isinstance(root_labels, Iterable):
            source_points = source_points.item()
        return source_points
