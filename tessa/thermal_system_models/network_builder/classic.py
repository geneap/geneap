# import warnings
# from typing import Optional, Iterable, Tuple
# from operator import itemgetter
#
# import numpy as np
# import pandas as pd
# import shapely
# import sparse
# import xarray as xr
# from geopandas import GeoDataFrame, GeoSeries
# from pandera.typing import DataFrame, Series
# from xarray import Dataset, DataArray
#
# from tessa.district import District
# from tessa.national_data import get_country_data
# from tessa.road_network_routing import BuildingRouting, routing_vertex_for_points
# from tessa import sparse_graph
# from tessa.sparse_graph import (
#     connected_components,
#     shortest_path,
#     directed_multiply,
#     sparse_with_attr,
# )
# from ..models import (
#     ThermalSources,
#     DistrictNetwork,
#     ThermalSource,
#     ThermalInjectionPoint,
#     NetworkDemand,
#     SupplyCurve,
# )
# from ..thermal_network import load_hourly_heat_loss_percentage, load_monthly_heat_loss_percentage
# from tessa.utils.pydantic_shapely import LineString, Point
#
#
# def _get_closest_building_to_point(points: GeoSeries, source_point: Point, max_distance_meters=1000):
#     # NOTE: max_distance in DEGREES. Could do some GIS wizardry to use meters
#     # TODO: (h)ow to define max distance? Mostly working in lat/lon but useful to have meter distances.
#     # Using very rough equation to convert distance in meters to degrees for latlon geom
#     # https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm
#     conversion = 0.000009009009  # 0.00001 / 1.11
#     max_distance = max_distance_meters * conversion
#     in_point_idx, search_geom_idx = points.sindex.nearest(
#         source_point.geometry, max_distance=max_distance, return_all=False
#     )
#     search_geom_idx = search_geom_idx.item()
#     injection_building = points.index[search_geom_idx].item()
#
#     return search_geom_idx, injection_building
#
#
# def _estimate_diameters(flow, graph):
#     # Need to monkey about to get the diameter data into the right xarray format
#     power_total_data = graph.power_total.data.data
#     diameters = flow.estimate_diameter(power_total_data)
#     diameters = sparse_with_attr(graph.power_total, data=diameters, name='nominal_diameter_m')
#     diameters.attrs['units'] = 'meters'
#     return diameters
#
#
# class DistrictNetworkBuilder:
#     database = None
#     routing_service: BuildingRouting
#
#     max_distance: float = 1000
#
#     hourly_network_loss: pd.Series
#     monthly_network_loss: pd.Series
#
#     heat_source_options: list = [
#         {
#             'code': 'ashp',
#             'cost_chf_kwh': 0.1746,
#             'kg_co2_kwh': 0.063,
#             'name': 'ASHP (SPF 2.8)',
#             'efficiency': 2.8,
#             'fuel': 'electricity',
#             'investment_per_kw': 1430,
#             'variable_o_and_m': 0.00269,
#             'fixed_o_and_m': 2,
#         }
#     ]
#
#     def __init__(
#         self,
#         database,
#         distances: BuildingRouting,
#         hourly_network_loss,
#         monthly_network_loss,
#         heat_source_options=None,
#     ):
#         self.database = database
#         self.routing_service: BuildingRouting = distances
#         self.hourly_network_loss = hourly_network_loss
#         self.monthly_network_loss = monthly_network_loss
#         if heat_source_options is not None:
#             self.heat_source_options = heat_source_options
#
#         # TODO: (4)% heat loss by default, needs to change in function of temperature levels
#         #  For a simple model can set about 4% for HighT and 2% for low T. Later do the real loss.
#         self.loss_fraction_km = 0.04
#
#     def _prep_args(self, costs, district, district_network, flow, heat_sources):
#         if district is None and district_network is None:
#             raise ValueError('Must provide district and/or district_network')
#
#         if flow is None:
#             if district_network:
#                 flow = district_network.flow.copy(deep=True)
#
#         if costs is None:
#             if district_network:
#                 costs = district_network.costs.copy(deep=True)
#
#         if heat_sources is None:
#             if district_network:
#                 heat_sources = district_network.heat_sources.copy(deep=True)
#             else:
#                 heat_sources = ThermalSources(sources=[])
#         elif not isinstance(heat_sources, ThermalSources):
#             heat_sources = ThermalSources(sources=heat_sources)
#
#         # require load curves is not None
#         assert district.load_curve is not None
#         return costs, district, flow, heat_sources
#
#     def create(
#         self,
#         *,
#         district: Optional[District] = None,
#         district_network: Optional[DistrictNetwork] = None,
#         heat_sources: ThermalSources | Iterable[ThermalSource] | None = None,
#         flow=None,
#         costs=None,
#         load_pipes=False,
#         graph=None,
#         ways=None,
#         **kwargs,
#     ) -> list[DistrictNetwork]:
#         """
#         Create as many subnets as needed.
#
#         Args:
#             district
#             district_network
#             flow
#             costs
#             heat_sources
#             load_pipes
#             graph
#             ways
#             kwargs
#
#         Returns:
#
#         """
#         costs, district, flow, heat_sources = self._prep_args(
#             costs, district, district_network, flow, heat_sources
#         )
#
#         # If generating fro the DN model, can return several subnets
#         graphs_data, power_demand_curves, multi_heat_sources = self.generate_network_multi_graph(
#             district.buildings, district.load_curve, heat_sources, graph, ways=ways
#         )
#
#         networks = []
#         for graph, pdc, heat_source in zip(graphs_data, power_demand_curves, multi_heat_sources):
#             off_street_connections, pipes = self._process_pipes(
#                 district, graph, ways=ways, load_pipes=load_pipes
#             )
#
#             # Always update diameters if flows change, unless we didn't set a power flow
#             if flow is not None and 'power_total' in graph:
#                 diameters = _estimate_diameters(flow, graph)
#                 graph['diameters'] = diameters
#
#             if costs is not None and 'diameters' in graph:
#                 graph['cost'] = costs.cost_per_segment(graph.routes, graph.diameters)
#
#             building_ids = graph.id_from.to_numpy().tolist()
#             network = DistrictNetwork(
#                 # district=district,
#                 graph=graph,
#                 building_ids=building_ids,
#                 flow=flow,
#                 costs=costs,
#                 heat_sources=heat_source,
#                 power_demand_curve=pdc,
#                 pipes=pipes,
#                 off_street_connections=off_street_connections,
#                 **kwargs,
#             )
#
#             networks.append(network)
#         return networks
#
#     def create_single(
#         self,
#         *,
#         district: Optional[District] = None,
#         district_network: Optional[DistrictNetwork] = None,
#         heat_sources: ThermalSources | Iterable[ThermalSource] | None = None,
#         flow=None,
#         costs=None,
#         graph=None,
#         ways=None,
#         load_pipes=True,
#         name='district_heating',
#         **kwargs,
#     ) -> DistrictNetwork:
#         costs, district, flow, heat_sources = self._prep_args(
#             costs, district, district_network, flow, heat_sources
#         )
#
#         # If generating for the DN model, can return several subnets
#         graph_data, power_demand_curves, multi_heat_sources = self.generate_network_multi_graph(
#             district.buildings, district.load_curve, heat_sources, graph=graph, ways=ways
#         )
#         graph = sparse_graph.merge_sparse_datasets(graph_data)
#
#         if any(dc is None for dc in power_demand_curves):
#             power_demand_curves = None
#         else:
#             power_demand_curves = sum(power_demand_curves)
#
#         # generate can modify the input heat sources according to
#         # different heuristics so need to return them
#         collected_hs = []
#         for hs in multi_heat_sources:
#             if hs is not None:
#                 collected_hs += hs.sources
#
#         multi_heat_sources = ThermalSources(sources=collected_hs)
#
#         off_street_connections, pipes = self._process_pipes(
#             district, graph, ways=ways, load_pipes=load_pipes
#         )
#         building_ids = graph.id_from.to_numpy().tolist()
#
#         # Always update diameters if flows change, unless we didn't set a power flow
#         if flow is not None and 'power_total' in graph:
#             diameters = _estimate_diameters(flow, graph)
#             graph['diameters'] = diameters
#
#         if costs is not None and 'diameters' in graph:
#             graph['cost'] = costs.cost_per_segment(graph.routes, graph.diameters)
#
#         network = DistrictNetwork(
#             graph=graph,
#             building_ids=building_ids,
#             flow=flow,
#             costs=costs,
#             heat_sources=multi_heat_sources,
#             power_demand_curve=power_demand_curves,
#             pipes=pipes,
#             off_street_connections=off_street_connections,
#             name=name,
#             **kwargs,
#         )
#
#         return network
#
#     def generate_network_multi_graph(
#         self,
#         buildings: GeoDataFrame,
#         load_curve: DataFrame[PowerLoad],
#         heat_sources: ThermalSources,
#         graph=None,
#         ways=None,
#     ) -> tuple[list[Dataset], list[Optional[DataFrame]], list[Optional[ThermalSources]]]:
#         """
#         TODO much better existing graph handling.
#           If we give a graph that only partially covers the buildings, should instead extend
#           the existing graph, keeping the routes that exist and adding distance matrix routes
#           only for the new IDs.
#
#         TODO make it easy to use pre calculated distance matrix to allow fast repeat runs
#
#         Args:
#             buildings:
#             load_curve:
#             heat_sources:
#             graph:
#             ways:
#
#         Returns:
#
#         """
#
#         points = GeoSeries(buildings.geometry.copy())  # use native CRS
#
#         if graph is None or 'routes' not in graph:
#             distances = self.routing_service.load_distance_matrix(points, ways=ways)
#             routes = sparse_graph.fast_minimum_spanning_tree(
#                 distances, points=None, max_distance=self.max_distance
#             )
#         else:
#             routes = graph['routes']
#             if set(routes.id_from.data) != set(buildings.index.values):
#                 # TODO: (i)f generated net doesn't cover all previous buildings then running update
#                 #   will always end up here even if the district hasn't changed/
#                 # If there are new points in the buildings, extend the routes with new distances
#                 distances = self.extend_routes_with_distance(routes, points, extra_ways=ways)
#
#                 routes = sparse_graph.fast_minimum_spanning_tree(
#                     distances, points=None, max_distance=self.max_distance
#                 )
#
#             elif ways:
#                 distances = self.routing_service.load_distance_matrix(points, ways=ways)
#                 routes = sparse_graph.fast_minimum_spanning_tree(
#                     distances, points=None, max_distance=self.max_distance
#                 )
#             # elif ways or not heat_sources.any_has_position:
#             #     # don't need to get distances if routes/heat source pos are provided
#             #     #   unless if no positions then need all the distance to get central point
#             #     distances = self.routing_service.load_distance_matrix(points, ways=ways)
#             else:
#                 # Reuse the existing routes e.g. when updating an existing network with new parameters
#                 distances = routes.copy()
#
#         # resulting route network might not be fully connected
#         n_components, labels = connected_components(routes, directed=False)
#
#         components_to_keep = []
#         for component_idx in range(n_components):
#             sel = labels == component_idx
#             size = sum(sel)
#             # Skip if this for 'non-networks'
#             if size <= 2:
#                 continue
#             else:
#                 components_to_keep.append((component_idx, size))
#
#         # Sort largest last so largest net does override.
#
#         components_to_keep = sorted(components_to_keep, key=itemgetter(1), reverse=True)
#
#         graph_data = []
#         demand_curves = []
#         multi_heat_sources = []
#
#         for component_idx, component_size in components_to_keep:
#             sel = labels == component_idx
#             # Use a two-step selection because of sparse package limitations
#             # TODO: (c)onvert one one select
#             subgraph_distances = distances[sel, :][:, sel]
#             subgraph_routes = routes[sel, :][:, sel]
#             # Extract data subset for the subgraph
#             subgraph_points = points.loc[subgraph_routes.id_from]
#             # subgraph_points_latlon = points_latlon.loc[subgraph_routes.id_from]
#             subgraph_heat = buildings.loc[subgraph_routes.id_from, 'q_hww_kwh'].astype(float)
#             subgraph_power = buildings.loc[subgraph_routes.id_from, 'p_hww_max1d_kw'].astype(float)
#             subgraph_heat_sources = self._prep_heat_sources(heat_sources)
#
#             subgraph, subgraph_heat_sources = self._generate_subnet(
#                 subgraph_heat,
#                 subgraph_power,
#                 subgraph_points,
#                 subgraph_heat_sources,
#                 subgraph_routes,
#                 subgraph_distances,
#             )
#
#             if load_curve is not None:
#                 subgraph_load_curve = self._subgraph_load_curve(subgraph, subgraph_heat, load_curve.p_hww_kw)
#
#                 subgraph_heat_sources = self._update_supply_curves(subgraph_heat_sources, subgraph_load_curve)
#
#             else:
#                 subgraph_load_curve = None
#
#             graph_data.append(subgraph)
#             multi_heat_sources.append(subgraph_heat_sources)
#             demand_curves.append(subgraph_load_curve)
#
#         return graph_data, demand_curves, multi_heat_sources
#
#     def extend_routes_with_distance(self, routes, points, *, extra_ways=None):
#         """
#          - Load all points in buildings (this gets too much extra data but fine for now)
#          - Delete point PAIRS that are in exsiting routes from loaded matrix
#          (since want to keep distances to new points can't just delete existing points)
#          - Merge with the previously loaded routes graph
#         Args:
#             routes:
#             points:
#             extra_ways:
#
#         Returns:
#
#         """
#
#         orig_ids = routes.id_from.data
#
#         new_ids = points.drop(orig_ids).index.values
#
#         distances = self.routing_service.load_distance_matrix(points, ways=extra_ways)
#
#         # select subset of data only going to or from new ids.
#         a = sparse_graph.sparse_2d_dataarray_to_dict(distances.sel(id_from=new_ids))
#         b = sparse_graph.sparse_2d_dataarray_to_dict(distances.sel(id_to=new_ids))
#         o = a | b
#
#         # Add the original routes
#         c = sparse_graph.sparse_2d_dataarray_to_dict(routes)
#         o = o | c
#
#         # Convert into sparse array
#         indexes = np.array(list(o.keys()))
#         data = np.fromiter(o.values(), float, count=len(o))
#
#         coords = np.vstack(
#             [
#                 distances.coords.indexes['id_from'].get_indexer_for(indexes.T[0]),
#                 distances.coords.indexes['id_to'].get_indexer_for(indexes.T[1]),
#             ]
#         )
#
#         merged_distances = xr.DataArray(
#             sparse.COO(coords, data=data, shape=distances.shape), coords=distances.coords, dims=distances.dims
#         )
#         return merged_distances
#
#     def load_network_pipes(self, points, graph, *, extra_ways=None) -> GeoDataFrame:
#         """
#         Load the network geometry geojson for the given thermal network object.
#         This is not done automatically because it takes time, so we return numeric information first
#         then return graphic repr.
#         """
#         # TODO: (q)uick fix for simple shortcuts, later want a better linking for extra ways with geom
#         #  e.g. using the distance cache to also cache the rout geom ids
#         return self.routing_service.load_routing_geometry(points, graph, extra_ways=extra_ways)
#
#     def load_building_connections(
#         self, building_points: GeoSeries | GeoDataFrame, *, extra_ways=None
#     ) -> GeoDataFrame:
#         """
#         Load the geometries for the connecitons between building centroids and the closes bit of road
#
#         Args:
#             building_points:
#             extra_ways:
#
#         Returns:
#             GeoDataFrame with columns geom and index same as building points
#
#         """
#         points = building_points.geometry
#         connections = routing_vertex_for_points(self.database, points)
#         connections['building_point'] = points  # should join on index
#
#         if extra_ways:
#             if not isinstance(extra_ways, dict):
#                 extra_ways = self.routing_service.find_points_on_ways(points, extra_ways)
#             for (bld_a, bld_b), line in extra_ways.items():
#                 first_point = shapely.geometry.Point(line.coords[0])
#                 last_point = shapely.geometry.Point(line.coords[-1])
#                 connections.loc[bld_a, 'point'] = first_point
#                 connections.loc[bld_b, 'point'] = last_point
#
#         connections['geom'] = connections.apply(
#             lambda row: LineString([row['building_point'], row['point']]), axis=1
#         )
#         connections = connections.drop(columns=['point', 'building_point'], axis=1).set_geometry('geom')
#         return connections
#
#     def _process_pipes(self, district, graph, *, district_network=None, ways=None, load_pipes=False):
#         building_ids = graph.id_from.to_numpy()
#         if load_pipes:
#             buildings = district.buildings_table(building_ids)
#             if ways is not None:
#                 ways = self.routing_service.find_points_on_ways(buildings.geometry, ways)
#             pipes = self.load_network_pipes(buildings.geometry, graph, extra_ways=ways)
#             off_street_connections = self.load_building_connections(buildings.geometry, extra_ways=ways)
#         elif district_network:
#             pipes = district_network.pipes
#             off_street_connections = district_network.off_street_connections
#         else:
#             pipes = None
#             off_street_connections = None
#         return off_street_connections, pipes
#
#     def _prep_heat_sources(self, heat_sources: ThermalSources) -> ThermalSources:
#         """
#         Copy the input heat sources adjusting the parameters, if there are several subgraphs
#         then we will have a heat sources copy for each one.
#         """
#         if len(heat_sources) > 0:
#             # In case SOME heat sources have position, ASSUME that the ones WITHOUT position
#             # share the location of the LARGEST source. Could add strategies like central, first, etc...
#             position = self._position_of_biggest_heat_source(heat_sources)
#             # Assign missing positions
#             for hs in heat_sources:
#                 if hs.position is None:
#                     hs.position = position
#
#         return heat_sources
#
#     def _generate_subnet(
#             self,
#             heat: Series,
#             power: Series,
#             points: GeoSeries,
#             heat_sources: ThermalSources,
#             routes: DataArray,
#             distances=None,
#     ) -> Tuple[Dataset, Optional[ThermalSources]]:
#         if heat_sources is None or len(heat_sources) == 0:
#             # Skip flow calculations if there are no heat sources
#             return routes.to_dataset(name='routes'), heat_sources
#         elif len(heat_sources) > 0 and (heat_sources.any_has_position or heat_sources.any_has_inject_point):
#             # If we have any injection points or heat source positions, do each source individually.
#             # Any sources that are missing get assigned the same location as the biggest heat source
#             # that has a position defined.
#             partial_graphs = []
#
#             position = self._position_of_biggest_heat_source(heat_sources)
#
#             for hs in heat_sources:
#                 if hs.position is None:
#                     if hs.injection_point is not None:
#                         hs.position = hs.injection_point.position
#                     else:
#                         hs.position = position
#                 # Try to get the inject point info from the existing injection point,
#                 # if it fails revert to spatial search.
#                 if hs.injection_point is not None and hs.injection_point.node_id in heat.index:
#                     root_label = hs.injection_point.node_id
#                     root_idx = heat.index.get_loc(root_label)
#                 else:
#                     # Need to get the closest BUILDING point to the Heat source point -> this is the
#                     # injection point because in the simplfied graph only have distances
#                     # THEN get the routing vertex for the points to get the on-road position.
#                     root_idx, root_label = _get_closest_building_to_point(points, hs.position)
#                     inject_point = self._get_injection_points(points, root_label)
#
#                     hs.injection_point = ThermalInjectionPoint(
#                         position=Point(inject_point), node_id=int(root_label)
#                     )
#                 # Split the total power demand between thermal sources that might have different
#                 # positions based on the ratios of the different sources, then calculate the network
#                 # flows.
#                 if hs.rated_power_useful is not None and heat_sources.power_useful_total > 0:
#                     source_fraction = hs.rated_power_useful / heat_sources.power_useful_total
#                     subgraph_partial_load = self._gen_network_flow_dataset(
#                         heat * source_fraction, power * source_fraction, routes, root_idx
#                     )
#
#                     partial_graphs.append(subgraph_partial_load)
#             subgraph = sum(partial_graphs)
#         elif len(heat_sources) > 0 and not heat_sources.any_has_position:
#             # There are sources but no source position given, generate automatically
#             # TODO: (o)ptions for position select strategy
#             if distances is not None:
#                 root_idx, root_label = self._get_most_central_building(distances, points)
#             else:
#                 root_idx, root_label = power.argmax(), power.idxmax()
#
#             inject_point = self._get_injection_points(points, root_label)
#
#             # Assign missing positions and calculate, don't need to divide powers
#             # because they all come from same point
#             for hs in heat_sources:
#                 hs.position = Point(inject_point)
#                 hs.injection_point = ThermalInjectionPoint(
#                     position=Point(inject_point), node_id=int(root_label)
#                 )
#             subgraph = self._gen_network_flow_dataset(heat, power, routes, root_idx)
#         else:
#             warnings.warn('Heat sources given but could not calculate flows, should not happen')
#             return routes.to_dataset(name='routes'), None
#
#         # Add the routes at the end because we don't want to accidentally sum routes several times
#         subgraph['routes'] = routes
#         return subgraph, heat_sources
#
#     def _position_of_biggest_heat_source(self, heat_sources) -> Point:
#         pos = None
#         hs_with_pos = [hs for hs in heat_sources if hs.position is not None]
#         if len(hs_with_pos) > 0:
#             biggest = hs_with_pos[0]
#             for hs in hs_with_pos:
#                 if hs.rated_power_useful is not None and hs.rated_power_useful > biggest.rated_power_useful:
#                     biggest = hs
#             pos = biggest.position
#         if pos is None:
#             hs_with_inj = [hs for hs in heat_sources if hs.injection_point is not None]
#             if len(hs_with_inj) > 0:
#                 biggest = hs_with_inj[0]
#                 for hs in hs_with_pos:
#                     if (
#                         hs.rated_power_useful is not None
#                         and hs.rated_power_useful > biggest.rated_power_useful
#                     ):
#                         biggest = hs
#                 pos = biggest.position
#         return pos
#
#     def _get_injection_points(self, points: GeoDataFrame | GeoSeries, root_labels):
#         """
#         Get the point on the road (routing point) closest to the given point
#
#         Args:
#             points:
#             root_labels:
#
#         Returns:
#
#         """
#         if not isinstance(root_labels, Iterable):
#             labels = [root_labels]
#         else:
#             labels = root_labels
#         source_points = self.routing_service.routing_vertex_for_points(points.loc[labels])
#         source_points = source_points.geometry
#         # If given a single label return the single point directly
#         if not isinstance(root_labels, Iterable):
#             source_points = source_points.item()
#         return source_points
#
#     def _get_most_central_building(self, subgraph_distances, subgraph_points):
#         root_idx = sparse_graph.sparse_centrality(subgraph_distances, penality=self.max_distance)
#         root_label = subgraph_points.index[root_idx]
#         return root_idx, root_label
#
#     def _gen_network_flow_dataset(self, heat, power, routes, root_idx) -> xr.Dataset:
#         # Supply pre-calculated preds shared across several calculations
#         # TODO: (r)un checks on preds to make sure the root make sense
#         # TODO: what happens to zero-length segments (buildings share a routing point?)
#
#         _, preds = shortest_path(
#             routes,
#             directed=False,
#             return_predecessors=True,
#             indices=[root_idx],
#         )
#         # Since we asked for shortest paths for a single node, preds is 1D row
#         # of indexes describing the path
#         # NOTE: could also do a sparse matrix solve
#         preds = preds.squeeze()
#         heat_per_segment = sparse_graph.graph_flow_total_per_segment(routes, heat, root_idx, preds=preds)
#         power_per_segment = sparse_graph.graph_flow_total_per_segment(routes, power, root_idx, preds=preds)
#         # IMPORTANT: linear density != total/routes matrix because flow can be reversed w.r.t
#         # the route -> this function takes care of doing non-directed division
#         # heat_loss = heat_loss_by_length_from_percent(routes, heat)
#         heat_loss = directed_multiply(self.loss_fraction_km * heat_per_segment, routes / 1000)
#         # heat_loss_fraction = directed_div(heat_loss, heat_per_segment)
#         # heat_loss_fraction = sparse_with_attr(heat_loss_fraction, fill_value=0.0)
#
#         graph = xr.Dataset(
#             {
#                 'heat_total': heat_per_segment,
#                 'power_total': power_per_segment,
#                 'heat_loss': heat_loss,
#                 # 'heat_loss_fraction': heat_loss_fraction,
#             }
#         )
#         return graph
#
#     def _subgraph_load_curve(
#         self, graph, heat: pd.Series, load_curve: DataFrame[PowerLoad]
#     ) -> DataFrame[NetworkDemand]:
#         """
#         Calculate the annual loss, then sum and produce the time series
#         for total loss to then get production
#
#         Args:
#             graph:
#             heat: kWh heat demands of the buildings
#             load_curve:
#
#         Returns:
#
#         """
#
#         # TODO: ideally need to re-calculate load curve for sub-components...
#         #  Now just re-scale, but different sub-graphs could have different building mixes.
#         subgraph_load_curve = pd.DataFrame(
#             {'p_hww_kw': (heat.sum() / (load_curve.mean() * 8760)) * load_curve}
#         )
#
#         # Important: don't want to end up with a hourly loss per segemnt (large data, also pointless!)
#         # so get total heat energy loss as scalar
#         if 'heat_loss' in graph:
#             annual_heat_loss = graph.heat_loss.sum().item()
#             hourly_heat_loss = self.hourly_network_loss * annual_heat_loss
#             # conform hourly heat loss to the index of the load curves
#             idx = subgraph_load_curve.index
#             hourly_heat_loss = pd.Series(
#                 hourly_heat_loss[(idx.dayofyear - 1) * 24 + idx.hour + 1].values, index=idx
#             )
#         else:
#             hourly_heat_loss = pd.Series(np.zeros(len(subgraph_load_curve)), index=subgraph_load_curve.index)
#
#         # hourly production is sum of demand and losses (final energy)
#         # If no losses are calculated then we can still estimate the overall load
#         subgraph_load_curve['p_loss_kw'] = hourly_heat_loss
#         subgraph_load_curve['p_with_loss_kw'] = subgraph_load_curve['p_hww_kw'] + hourly_heat_loss
#         return DataFrame[NetworkDemand](subgraph_load_curve)
#
#     def _update_supply_curves(self, thermal_sources, power_demand_curve, sort=False):
#         unmet_demand = pd.DataFrame(
#             power_demand_curve.p_with_loss_kw.values,
#             columns=['heat_demand_remainder'],
#             index=power_demand_curve.index,
#         )
#         if sort:
#             thermal_sources = sorted(thermal_sources, key=lambda s: s.rated_power_useful, reverse=True)
#
#         new_sources = []
#         for source in thermal_sources:
#             # Thermal sources can have existing supply curves
#             if source.supply_curve is None:
#                 p_supply_potential_kw = (
#                     np.ones_like(unmet_demand.heat_demand_remainder.values) * source.rated_power_useful
#                 )
#             else:
#                 p_supply_potential_kw = source.supply_curve.p_supply_potential_kw
#
#             pwr_crv = unmet_demand.heat_demand_remainder.values
#             pwr_crv = np.vstack([pwr_crv, p_supply_potential_kw])
#             p_supply_kw = np.nanmin(pwr_crv, axis=0)
#
#             supply_curve = pd.DataFrame(
#                 {'p_supply_potential_kw': p_supply_potential_kw, 'p_supply_kw': p_supply_kw},
#                 index=power_demand_curve.index,
#             )
#
#             # source.supply_curve = DataFrame[SupplyCurve](supply_curve)
#             source = ThermalSource(**(source.dict() | {'supply_curve': DataFrame[SupplyCurve](supply_curve)}))
#             new_sources.append(source)
#             unmet_demand.heat_demand_remainder = (
#                 unmet_demand.heat_demand_remainder - source.supply_curve.p_supply_kw
#             )
#
#         return ThermalSources(sources=new_sources)
#
#
# def get_heat_source_options(country='ch'):
#     # From KBOB file, note co2 intensity related to useful heat delivered
#     ch_data = get_country_data(country)
#
#     fuels = {code: fdata | {'code': code} for code, fdata in ch_data['fuels'].items()}
#
#     heat_source_options = [
#         hs | {'code': k, 'fuel': fuels[hs['fuel']]}
#         for i, (k, hs) in enumerate(ch_data['heat_sources'].items())
#     ]
#     return heat_source_options
#
#
# class SwissDistrictNetworkBuilder(DistrictNetworkBuilder):
#     def __init__(self, database, distances: BuildingRouting):
#         heat_source_options = get_heat_source_options()
#         super().__init__(
#             database,
#             distances,
#             hourly_network_loss=load_hourly_heat_loss_percentage(),
#             monthly_network_loss=load_monthly_heat_loss_percentage(),
#             heat_source_options=heat_source_options,
#         )
