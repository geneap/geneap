#!/usr/bin/env python

# # Pumping Energy Consumption
#
# #### In this code, the pumping energy consumption and the purchased and bare module costs of the pumps of a thermal grid are calculated.
# #### The grid configuration here is designed based on the minimum spanning tree method.

import warnings

import CoolProp.CoolProp as CP
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy as sp

from tessa.thermal_system_models import DistrictNetwork
from tessa.thermal_system_models.pipes import load_steel_pipe_dn_table

pd.options.mode.chained_assignment = None


# #### Known parameters:
DN_table = load_steel_pipe_dn_table()


# Density of water
ρ = CP.PropsSI("D", "T", 273.15 + 14, "P", 101325, "water")  # [kg/m3]

# Viscosity of water
μ = CP.PropsSI("V", "T", 273.15 + 14, "P", 101325, "water")  # [Pa.s]

# Constant pressure specific heat capacity of water
Cp = CP.PropsSI("C", "T", 273.15 + 14, "P", 101325, "water") / 1000  # [kJ/(kg.K)]

# Temperature differance
ΔT = 4  # [K]

# Allowable pressure drop in the heat exchangers is 10 [psi] or 69 [kPa];
# So, max headloss (h = P/ρg) in each building's heat exchangers would be:
h_b_m = round(68947.6 / (ρ * 9.8067), 0) / 2  # [m]

# Roughness of PVC pipes
ϵ = 0.0015  # [mm]

# Length of pipe
L = 30  # [m]


# #### Load the saved example data from a Python 'pickle' file

# In[3]:


# Pilot project
# with open('example_district_data.pkl', 'rb') as file:
#     district = pickle.load(file)

# with open("grandvaux_with_simple_heat_sources.pkl", "rb") as file:
#     district = pickle.load(file)


# #### The data of the buildings, the grid, and the heat sources are gathered here:

# In[4]:


# Coefficient Matrix for Solving the system of linear equations
def create_coefficient_matrices(buildings_data, network_data):
    # Create a list of column names
    # column_names = [f"Column_{i}" for i in range(len(network_data))]

    # Initialize a list to store the new columns (as a list of lists)
    new_columns = []

    for index, row in network_data.iterrows():
        new_column = []

        for _, building_row in buildings_data.iterrows():
            if building_row["egid"] == row["id_from"]:
                new_column.append(-1)
            elif building_row["egid"] == row["id_to"]:
                new_column.append(1)
            else:
                new_column.append(0)

        new_columns.append(new_column)

    # Convert the list of lists into a matrix (2D list)
    Coeff_Mat_V_dot = [list(row) for row in zip(*new_columns)]
    Coeff_Mat_V_dot = Coeff_Mat_V_dot[:-1]

    Coeff_Mat_Head = np.transpose(Coeff_Mat_V_dot)

    # Add a column with all zeros except the last one which is 1
    new_column = np.zeros((Coeff_Mat_Head.shape[0], 1))
    new_column[-1] = 1

    # Concatenate the new column to the original matrix
    Coeff_Mat_Head = np.concatenate((Coeff_Mat_Head, new_column), axis=1)

    # Add a row with all zeros except the last one which is 1
    new_row = np.zeros((1, Coeff_Mat_Head.shape[1]))
    new_row[0, -1] = 1

    # Concatenate the new row to the original matrix
    Coeff_Mat_Head = np.concatenate((Coeff_Mat_Head, new_row), axis=0)

    return Coeff_Mat_V_dot, Coeff_Mat_Head


def district_data(district):
    # Extract buildings_data_table
    buildings_data = district.buildings[["p_hww_max1h_kw"]]
    buildings_data.reset_index(inplace=True)
    # Replace zeros with the average value
    average_p_hww = buildings_data[buildings_data["p_hww_max1h_kw"] != 0]["p_hww_max1h_kw"].mean()
    buildings_data.loc[buildings_data["p_hww_max1h_kw"] == 0, "p_hww_max1h_kw"] = average_p_hww
    # Find the mass flow rate for each building in the grid
    buildings_data["m_dot_kg_s"] = buildings_data["p_hww_max1h_kw"] / (Cp * ΔT)  # [kg/s]
    # buildings_data_table.loc[:, 'm_dot_kg_s'] = buildings_data_table['p_hww_max1h_kw'] / (Cp * ΔT) # [kg/s]
    m_dot_kg_s_tot = buildings_data["m_dot_kg_s"].sum()
    # Adding Heat Source
    data = {"egid": ["heat source"], "p_hww_max1h_kw": np.nan, "m_dot_kg_s": m_dot_kg_s_tot}
    new_row = pd.DataFrame(data)
    buildings_data = pd.concat([buildings_data, new_row], ignore_index=True)
    # Find the volumetric flow rate for each building in the grid
    buildings_data["routes"] = L  # [m]
    buildings_data["V_dot_m3_s"] = buildings_data["m_dot_kg_s"] / ρ  # [m3/s]

    # Extract network_data_table
    district_heat_network: DistrictNetwork = district.get_utility_network("district_heating")
    if district_heat_network is None:
        return None, None

    # network_data_matrix = district_heat_network.graph
    # for variable in network_data_matrix.data_vars:
    #     try:
    #         network_data_matrix[variable].data = network_data_matrix[variable].data.todense()
    #     except AttributeError:
    #         pass
    # network_data = network_data_matrix.to_dataframe()

    network_data = district_heat_network.graph_table
    network_data = network_data[network_data.routes > 0]
    network_data = network_data[["routes"]]
    network_data.reset_index(inplace=True)
    # Find the egid of the heat source
    # for substation in district_heat_network.substations:
    #     substation.connection_point.node_id
    node_ids = [substation.connection_point.node_id for substation in district_heat_network.substations]
    unique_node_ids = list(set(node_ids))
    # Adding Heat Source
    data = {"id_from": ["heat source"], "id_to": unique_node_ids, "routes": L}
    new_row = pd.DataFrame(data)
    network_data = pd.concat([network_data, new_row], ignore_index=True)
    # Solve the system of linear equations for finding the volumetric flow rate for each route
    Coeff_Mat_V_dot, _ = create_coefficient_matrices(buildings_data, network_data)
    RHS_V_dot = np.array(buildings_data["V_dot_m3_s"])
    x_V_dot = np.linalg.solve(Coeff_Mat_V_dot, RHS_V_dot[:-1])
    network_data["V_dot_m3_s"] = x_V_dot  # [m3/s]
    network_data["direction"] = network_data["V_dot_m3_s"].apply(lambda x: 1 if x > 0 else -1)
    network_data["V_dot_m3_s"] = abs(network_data["V_dot_m3_s"])

    return buildings_data, network_data


# TODO: What if we had more than one heat source?


def find_id(V_dot_m3_s, DN_table, j):
    """
        # #### Pipe Nominal Diameter (DN):
    #
    # [Ref]: ISO 6708
        Args:
            V_dot_m3_s:
            DN_table:
            j:

        Returns:

    """
    # Selecting the suitable pipes (with minimum diameter in which the flow velocity is above 1 [m/s])
    for i in DN_table.index:
        ID_mm = DN_table["id_mm"][i]
        velocity_m_s = 4 * V_dot_m3_s / (np.pi * (ID_mm / 1000) ** 2)
        if velocity_m_s < 1:
            if i > 0:
                i = i - 1
                ID_mm = DN_table["id_mm"][i]
                velocity_m_s = 4 * V_dot_m3_s / (np.pi * (ID_mm / 1000) ** 2)
            break
    return pd.Series([DN_table["dn"][max(0, i + j)], DN_table["id_mm"][max(0, i + j)]], index=["dn", "id_mm"])


# In[8]:


# Darcy Weisbach friction factor

f_g = 0.1  # Initial guess


def f_f(ϵ, d, Re, f_g):
    if Re == 0:
        f_n = 0
    elif 0 < Re < 2000:
        f_n = 64 / Re
    else:
        error = 1
        count = 0
        while error > 1.0e-5:
            count += 1
            f_n = (1 / (-2 * np.log10(((ϵ / d) / 3.7) + (2.51 / (Re * f_g**0.5))))) ** 2
            error = abs(f_n - f_g)
            f_g = f_n

    return f_n


def calculate_properties(row, j=0):
    # Calculaion of some of the properties that are the same

    # Calculate properties
    V_dot_m3_s = row["V_dot_m3_s"]
    DN, ID_mm = find_id(V_dot_m3_s, DN_table, j)
    velocity_m_s = V_dot_m3_s / ((np.pi * (ID_mm / 1000) ** 2) / 4)
    Reynolds = velocity_m_s * (ID_mm / 1000) * ρ / μ
    f_DW = f_f(ϵ, ID_mm / 1000, Reynolds, f_g)
    R_DW = 8 * f_DW * row["routes"] / (9.8067 * (ID_mm / 1000) ** 5 * np.pi**2)
    h_f_DW_m = R_DW * V_dot_m3_s**2
    Pressure_Loss_Pa_m = f_DW * (ρ / 2) * ((velocity_m_s**2) / (ID_mm / 1000))

    # Assign values directly
    row.at["DN"] = DN
    row.at["ID_mm"] = ID_mm
    row.at["velocity_m_s"] = velocity_m_s
    row.at["Reynolds"] = Reynolds
    row.at["f_DW"] = f_DW
    row.at["R_DW"] = R_DW
    row.at["h_f_DW_m"] = h_f_DW_m
    row.at["Pressure_Loss_Pa_m"] = Pressure_Loss_Pa_m

    return row


def more_properties(buildings_data_table, network_data_table):
    # Modify buildings_data_table
    buildings_data_table["h_b_m"] = h_b_m
    buildings_data_table.loc[buildings_data_table["velocity_m_s"] == 0, "h_b_m"] = 0
    buildings_data_table["total_h_b_m"] = buildings_data_table["h_f_DW_m"] + buildings_data_table["h_b_m"]

    # Modify network_data_table
    egid_to_h_f_DW = buildings_data_table.set_index("egid")["total_h_b_m"].to_dict()

    # Augmented Matrix (delta_h_Pump_m)
    network_data_table["delta_h_Pump_m"] = (
        2 * network_data_table["direction"] * network_data_table["h_f_DW_m"]
        + network_data_table["id_to"].map(egid_to_h_f_DW).fillna(0)
        - network_data_table["id_from"].map(egid_to_h_f_DW).fillna(0)
    )

    network_data_table.loc[
        (network_data_table["id_from"] == "heat source") | (network_data_table["id_to"] == "heat source"),
        "delta_h_Pump_m",
    ] = (
        2 * network_data_table["direction"] * network_data_table["h_f_DW_m"]
        + network_data_table["id_to"].map(egid_to_h_f_DW).fillna(0)
        + network_data_table["id_from"].map(egid_to_h_f_DW).fillna(0)
    )

    return buildings_data_table, network_data_table


#### Solving other system of linear equations to find the head of the pumps
def Pump_Head(h_Main_Pump_m, buildings_data_table, network_data_table):
    XXX, Coeff_Mat_Head = create_coefficient_matrices(buildings_data_table, network_data_table)
    RHS_Head = np.array(network_data_table["delta_h_Pump_m"])
    RHS_Head = np.append(RHS_Head, h_Main_Pump_m)
    x_Head = np.linalg.solve(Coeff_Mat_Head, RHS_Head)
    return x_Head


# #### Output power of the pumps
#
# P_out = V_dot_m3_s * ρ * g * h_p
#
# P_in = P_out / η
#
# η = 0.85

# #### The bare module cost and purchase cost of the pumps
#
#
# The bare module cost and purchase cost of the pumps can be calculated from the following equations [Ref]
#
# [Ref]: Turton, R., Bailie, R.C., Whiting, W.B. and Shaeiwitz, J.A., 2008. Analysis, synthesis and design of chemical processes. Pearson Education.
#
# Operating pressure factor: F_P = 10**(−0.3935 + 0.3957 * log10(ρ * g * h_p) − 0.00226 * (log10(ρ * g * h_p))**2) . The unit of pressure here should be bar gauge (100 kPa)
#
# Bare module factors: F_BM = 1.89 + 1.35 * 1.6 * F_P
#
# Bare Module Cost, which is the sum of the direct and indirect costs: C_BM = C_P * F_BM
#
# Average exchange rate in 2022: 1 USD = 0.9548 CHF
#
# Chemical Engineering Plant Cost Index (CEPCI) [Ref]: https://chemengonline.com
CEPCI_2001 = 394.3
CEPCI_2022 = 816.0
R_CEPCI = CEPCI_2022 / CEPCI_2001


def calculate_costs(row):
    np.seterr(divide="ignore")

    # Power of the Pumps
    power_of_pump_kW = (row["V_dot_m3_s"] * ρ * 9.8067 * row["h_Pump_m"] / 1000) / 0.85

    # Purchased Cost
    purchased_cost_CHF = (
        10 ** (3.3892 + 0.0536 * np.log10(power_of_pump_kW) + 0.1538 * (np.log10(power_of_pump_kW)) ** 2)
        * R_CEPCI
        * 0.9548
    )

    # Bare Module Cost
    bare_module_cost_CHF = (
        purchased_cost_CHF
        * (
            1.89
            + 1.35
            * 1.6
            * (
                10
                ** (
                    (-1 * 0.3935)
                    + 0.3957 * np.log10(ρ * 9.8067 * row["h_Pump_m"] / 100000)
                    - 0.00226 * (np.log10(ρ * 9.8067 * row["h_Pump_m"] / 100000)) ** 2
                )
            )
        )
        * R_CEPCI
        * 0.9548
    )

    return pd.Series(
        {
            "Power_of_Pump_kW": power_of_pump_kW,
            "Purchased_Cost_of_Pump_CHF": purchased_cost_CHF,
            "Bare_Module_Cost_of_Pump_CHF": bare_module_cost_CHF,
        }
    )


# #### Effect of different pipe DN

DNs = ["DN⁻⁴", "DN⁻³", "DN⁻²", "DN⁻¹", "DN", "DN⁺¹", "DN⁺²", "DN⁺³", "DN⁺⁴"]
Different_DN_Data = pd.DataFrame({"DN": DNs})
Different_DN_Data[
    [
        "Total_Head_m",
        "Total_Power_kW",
        "Total_Purchased_Cost_MCHF",
        "Total_Bare_Module_Cost_MCHF",
        "Total_Electricity_Cost_kCHF",
        "Total_Cost_MCHF",
        "Mean_Flow_Velocity_m_s",
    ]
] = np.nan


def pumping_for_district(district, main_head=None):
    # pump_data = pd.DataFrame({"DN": DNs})
    pump_data = {"DN": DNs}

    warnings.filterwarnings(
        "ignore", category=RuntimeWarning, message="invalid value encountered in double_scalars"
    )

    # Initialize the dataframes
    buildings_data, network_data = district_data(district)

    buildings_data, main_head0, network_data = calc_pumps(buildings_data, network_data, main_head)

    pump_data["min_pump_head_m"] = round(main_head0, 3)
    pump_data["Total_Head_m"] = round(buildings_data["h_Pump_m"].sum(), 3)
    pump_data["Total_Power_kW"] = round(buildings_data["Power_of_Pump_kW"].sum(), 3)
    pump_data["Total_Purchased_Cost_MCHF"] = round(
        buildings_data["Purchased_Cost_of_Pump_CHF"].sum() / 1000000, 3
    )
    pump_data["Total_Bare_Module_Cost_MCHF"] = round(
        buildings_data["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000, 3
    )
    pump_data["Total_Electricity_Cost_kCHF"] = round(
        buildings_data["Power_of_Pump_kW"].sum() * 8760 * 32.28 / 100000, 3
    )  # Electricity Prices. [Ref]" https://strompreis.elcom.admin.ch
    pump_data["Total_Cost_MCHF"] = round(
        (buildings_data["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000)
        + (buildings_data["Power_of_Pump_kW"].sum() * 8760 * 32.28 / 100000) / 1000,
        3,
    )
    pump_data["Mean_Flow_Velocity_m_s"] = round(
        (buildings_data["velocity_m_s"].mean() + network_data["velocity_m_s"].mean()) / 2, 3
    )
    pump_data["Mean_Pressure_Loss_Pa_m"] = round(
        (buildings_data["Pressure_Loss_Pa_m"].mean() + network_data["Pressure_Loss_Pa_m"].mean()) / 2,
        3,
    )

    return pump_data


def calc_pumps(buildings_data_table, network_data_table, the_main_head, j=0):
    buildings_data_table = buildings_data_table.apply(lambda row: calculate_properties(row, j), axis=1)
    network_data_table = network_data_table.apply(lambda row: calculate_properties(row, j), axis=1)
    buildings_data_table, network_data_table = more_properties(buildings_data_table, network_data_table)
    min_head0 = np.min(Pump_Head(0, buildings_data_table, network_data_table)[:-1])
    main_head0 = the_main_head if the_main_head is not None else min_head0
    buildings_data_table["h_Pump_m"] = np.round(
        Pump_Head(main_head0, buildings_data_table, network_data_table), 5
    )
    buildings_data_table[
        ["Power_of_Pump_kW", "Purchased_Cost_of_Pump_CHF", "Bare_Module_Cost_of_Pump_CHF"]
    ] = buildings_data_table.apply(calculate_costs, axis=1)
    return buildings_data_table, main_head0, network_data_table


# #### Results of normal pipe (DN)
def calculate_pumping_results(district):
    buildings_data, network_data = district_data(district)
    if buildings_data is None:
        return None

    buildings_data = buildings_data.apply(lambda row: calculate_properties(row), axis=1)
    network_data = network_data.apply(lambda row: calculate_properties(row), axis=1)
    buildings_data, network_data = more_properties(buildings_data, network_data)

    min_head = np.min(Pump_Head(0, buildings_data, network_data)[:-1])

    buildings_data["h_Pump_m"] = np.round(Pump_Head(min_head, buildings_data, network_data), 5)  # [m]

    buildings_data[
        ["Power_of_Pump_kW", "Purchased_Cost_of_Pump_CHF", "Bare_Module_Cost_of_Pump_CHF"]
    ] = buildings_data.apply(calculate_costs, axis=1)
    return buildings_data


# #### Effect of head of main pump on the total head


def plot_effect_total_head(buildings_data_table, network_data_table):
    h_Main_Pump_m_rang = range(40)
    total_heads = []
    for h_Main_Pump_m in h_Main_Pump_m_rang:
        total_head = abs(Pump_Head(h_Main_Pump_m, buildings_data_table, network_data_table)).sum()
        total_heads.append(total_head)

    plt.plot(h_Main_Pump_m_rang, total_heads)
    plt.xlabel("Main pump head [m]")
    plt.ylabel("Total pumps head [m]")
    plt.show()


# #### Total power and costs of the pumps
def print_results(buildings_data_table):
    print(
        buildings_data_table["Power_of_Pump_kW"].sum()  # [kW]
    )
    # Total Purchased Cost of the Pumps in the Buildings
    print(round(buildings_data_table["Purchased_Cost_of_Pump_CHF"].sum() / 1000000, 3))  # [Million CHF]

    # Total Bare Module Cost of the Pumps in the Buildings
    print(round(buildings_data_table["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000, 3))  # [Million CHF]

    print(
        "The total power consumption of the pumps of the thermal grid is",
        round(buildings_data_table["Power_of_Pump_kW"].sum(), 3),
        "[kW].",
    )
    print(
        "The total estimated purchased costs of the pumps of the thermal grid is",
        round((buildings_data_table["Purchased_Cost_of_Pump_CHF"].sum()) / 1000000, 3),
        "[Million CHF].",
    )
    print(
        "The total estimated Bare Module costs of the pumps of the thermal grid is",
        round((buildings_data_table["Bare_Module_Cost_of_Pump_CHF"].sum()) / 1000000, 3),
        "[Million CHF].",
    )


def plot_dn_variants_total_power():
    plt.plot(Different_DN_Data["Total_Power_kW"], "c")

    plt.title("Effect of different pipe DNs on Pumping Power")
    plt.xlabel("Pipe DN")
    plt.ylabel("Pumping Power [kW]")
    x_labels = DNs
    plt.xticks(range(len(x_labels)), x_labels)


def plot_dn_variants_total_elec_cost():
    plt.plot(Different_DN_Data["Total_Electricity_Cost_kCHF"], "r")

    plt.title("Effect of different pipe DNs on Electricity Cost")
    plt.xlabel("Pipe DN")
    plt.ylabel("Electricity Cost [kCHF]")
    x_labels = DNs
    plt.xticks(range(len(x_labels)), x_labels)
    plt.show()


# In[18]:
def plot_dn_variants_total_purchase_cost():
    plt.plot(Different_DN_Data["Total_Purchased_Cost_MCHF"] * 1000)

    plt.title("Effect of different pipe DNs on Purchased Cost")
    plt.xlabel("Pipe DN")
    plt.ylabel("Purchased Cost [kCHF]")
    x_labels = DNs
    plt.xticks(range(len(x_labels)), x_labels)
    plt.show()


def DN_effect(district, main_head=None):
    i = 0

    warnings.filterwarnings(
        "ignore", category=RuntimeWarning, message="invalid value encountered in double_scalars"
    )

    # Initialize the dataframes
    buildings_data, network_data = district_data(district)

    # for j in range(-4, 5, 1):
    for j in range(-4, 5, 1):
        buildings_data, main_head0, network_data = calc_pumps(buildings_data, network_data, main_head)

        Different_DN_Data.loc[i, "min_pump_head_m"] = round(main_head0, 3)
        Different_DN_Data.loc[i, "Total_Head_m"] = round(buildings_data["h_Pump_m"].sum(), 3)
        Different_DN_Data.loc[i, "Total_Power_kW"] = round(buildings_data["Power_of_Pump_kW"].sum(), 3)
        Different_DN_Data.loc[i, "Total_Purchased_Cost_MCHF"] = round(
            buildings_data["Purchased_Cost_of_Pump_CHF"].sum() / 1000000, 3
        )
        Different_DN_Data.loc[i, "Total_Bare_Module_Cost_MCHF"] = round(
            buildings_data["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000, 3
        )
        Different_DN_Data.loc[i, "Total_Electricity_Cost_kCHF"] = round(
            buildings_data["Power_of_Pump_kW"].sum() * 8760 * 32.28 / 100000, 3
        )  # Electricity Prices. [Ref]" https://strompreis.elcom.admin.ch
        Different_DN_Data.loc[i, "Total_Cost_MCHF"] = round(
            (buildings_data["Bare_Module_Cost_of_Pump_CHF"].sum() / 1000000)
            + (buildings_data["Power_of_Pump_kW"].sum() * 8760 * 32.28 / 100000) / 1000,
            3,
        )
        Different_DN_Data.loc[i, "Mean_Flow_Velocity_m_s"] = round(
            (buildings_data["velocity_m_s"].mean() + network_data["velocity_m_s"].mean()) / 2, 3
        )
        Different_DN_Data.loc[i, "Mean_Pressure_Loss_Pa_m"] = round(
            (buildings_data["Pressure_Loss_Pa_m"].mean() + network_data["Pressure_Loss_Pa_m"].mean()) / 2,
            3,
        )

        i += 1

    return Different_DN_Data


# In[19]:
#
#
# plt.plot(
#     Different_DN_Data["Total_Purchased_Cost_MCHF"] + Different_DN_Data["Total_Electricity_Cost_kCHF"] / 1000,
#     "m",
# )
#
# plt.title("Effect of different pipe DNs on Costs (Electricity + Purchased Cost)")
# plt.xlabel("Pipe DN")
# plt.ylabel("Purchased Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[20]:
#
#
# plt.plot(Different_DN_Data["Total_Cost_MCHF"], "g")
#
# plt.title("Effect of different pipe DNs on Costs (Electricity + Bare Module Cost)")
# plt.xlabel("Pipe DN")
# plt.ylabel("Purchased Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[21]:
#
#
# plt.plot(Different_DN_Data["Total_Purchased_Cost_MCHF"], label="Total Purchased Cost of the Pumps")
# plt.plot(Different_DN_Data["Total_Bare_Module_Cost_MCHF"], label="Total Bare Module Cost of the Pumps")
# plt.plot(
#     Different_DN_Data["Total_Cost_MCHF"], label="Total Cost (Electricity + Bare Module Cost of the Pumps)"
# )
#
# plt.title("Effect of different pipe DNs on Costs")
# plt.xlabel("Pipe DN")
# plt.ylabel("Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.legend()
# plt.show()
#
#
#
#
# # In[23]:
#
#
# plt.plot(Different_DN_Data["Total_Power_kW"], "c")
#
# plt.title("Effect of different pipe DNs on Pumping Power")
# plt.xlabel("Pipe DN")
# plt.ylabel("Pumping Power [kW]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[24]:
#
#
# plt.plot(Different_DN_Data["Total_Electricity_Cost_kCHF"], "r")
#
# plt.title("Effect of different pipe DNs on Electricity Cost")
# plt.xlabel("Pipe DN")
# plt.ylabel("Electricity Cost [kCHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[25]:
#
#
# plt.plot(Different_DN_Data["Total_Purchased_Cost_MCHF"] * 1000)
#
# plt.title("Effect of different pipe DNs on Purchased Cost")
# plt.xlabel("Pipe DN")
# plt.ylabel("Purchased Cost [kCHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[26]:
#
#
# plt.plot(
#     Different_DN_Data["Total_Purchased_Cost_MCHF"] + Different_DN_Data["Total_Electricity_Cost_kCHF"] / 1000,
#     "m",
# )
#
# plt.title("Effect of different pipe DNs on Costs (Electricity + Purchased Cost)")
# plt.xlabel("Pipe DN")
# plt.ylabel("Purchased Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[27]:
#
#
# plt.plot(Different_DN_Data["Total_Cost_MCHF"], "g")
#
# plt.title("Effect of different pipe DNs on Costs (Electricity + Bare Module Cost)")
# plt.xlabel("Pipe DN")
# plt.ylabel("Purchased Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.show()
#
#
# # In[28]:
#
#
# plt.plot(Different_DN_Data["Total_Purchased_Cost_MCHF"], label="Total Purchased Cost of the Pumps")
# plt.plot(Different_DN_Data["Total_Bare_Module_Cost_MCHF"], label="Total Bare Module Cost of the Pumps")
# plt.plot(
#     Different_DN_Data["Total_Cost_MCHF"], label="Total Cost (Electricity + Bare Module Cost of the Pumps)"
# )
#
# plt.title("Effect of different pipe DNs on Costs")
# plt.xlabel("Pipe DN")
# plt.ylabel("Cost [Million CHF]")
# x_labels = DNs
# plt.xticks(range(len(x_labels)), x_labels)
# plt.legend()
# plt.show()
#


def effect_of_head_on_total_power(buildings_data_table, network_data_table):
    """
    Effect of head of main pump on total power and the total purchased costs of the pumps
    Args:
        buildings_data_table:
        network_data_table:

    Returns:

    """
    h_Main_Pump_m_rang = range(20)
    total_powers = []
    for h_Main_Pump_m in h_Main_Pump_m_rang:
        buildings_data_table["h_Pump_m"] = np.round(
            Pump_Head(h_Main_Pump_m, buildings_data_table, network_data_table), 5
        )  # [m]
        buildings_data_table["Power_of_Pump_kW"] = (
            buildings_data_table["V_dot_m3_s"] * ρ * 9.8067 * buildings_data_table["h_Pump_m"] / 1000
        ) / 0.85  # [kW]
        total_power = abs(buildings_data_table["Power_of_Pump_kW"]).sum()
        total_powers.append(total_power)

    plt.plot(h_Main_Pump_m_rang, total_powers)
    plt.xlabel("Main pump head [m]")
    plt.ylabel("Total power [kW]")
    plt.show()


def Pump_Cost(h_Main_Pump_m, buildings_data, network_data):
    buildings_data["h_Pump_m"] = np.round(Pump_Head(h_Main_Pump_m, buildings_data, network_data), 5)
    buildings_data["Power_of_Pump_kW"] = (
        (buildings_data["V_dot_m3_s"] / 1) * ρ * 9.8067 * buildings_data["h_Pump_m"] / 1000
    ) / 0.85  # [kW]
    np.seterr(divide="ignore")
    buildings_data["Purchased_Cost_of_Pump_CHF"] = (
        10
        ** (
            3.3892
            + 0.0536 * np.log10(buildings_data["Power_of_Pump_kW"])
            + 0.1538 * (np.log10(buildings_data["Power_of_Pump_kW"])) ** 2
        )
        * R_CEPCI
        * 0.9548
    )  # The unit of Power should be kW
    Cost = 1 * round(buildings_data["Purchased_Cost_of_Pump_CHF"].sum() / 1000000, 3)
    return Cost


def study_pump_head():
    h_Main_Pump_m_rang = range(20)
    total_costs = []
    for h_Main_Pump_m in h_Main_Pump_m_rang:
        np.seterr(divide="ignore")
        total_cost = Pump_Cost(h_Main_Pump_m)
        total_costs.append(total_cost)

    plt.plot(h_Main_Pump_m_rang, total_costs)
    plt.xlabel("Main pump head [m]")
    plt.ylabel("Total purchased costs of the pumps [M CHF]")
    plt.show()
