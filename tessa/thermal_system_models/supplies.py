import numba
import numpy as np
import pandas as pd
from numba import float64

# TODO: (f)ix this
# def calc_power_heat_pump_water(t_resource):
#     """
#     """
#     t_going_cad = 80
#     cop = (0.45 * (t_going_cad + 273)) / ((t_going_cad + 273) - (t_resource + 273))
#     q_cond = cop * q_evap / (cop - 1)
#     q_elec = q_cond - q_evap
#
#     return q_cond


def hp_cost_per_kw(rated_thermal_power, a, b):
    """

    | HP type | graph min | graph max | extra. min | extra. max |
    |---------|-----------|-----------|------------|------------|
    | ASHP    | 4         | 52        | 5          | 65         |
    | GSHP    | 13.6      | 280       | 10         | 350        |
    | WSHP    | 17        | 250       | 10         | 350        |

    Args:
        rated_thermal_power
        a
        b

    Returns:

    """
    # TODO: (m)ake it possible to load data from reference table. Decide where this should be stored.

    return a * np.power(rated_thermal_power, b)


@numba.vectorize([float64(float64, float64, float64, float64, float64)])
def hp_constrained_cost_per_kw(rated_thermal_power, a, b, range_min, range_max):
    """Constrained function of HP costs
    Clips the inputs to the presumably valid range based on
    light manual extrapolation from the input data

    NOTE: the rater power is for the delivered heat, to get the
    required electricity would need a COP value.

    Parameters
    ----------
    rated_thermal_power
    a
    b
    range_min
    range_max

    Returns
    -------

    """
    pwr = rated_thermal_power
    if pwr == np.nan:
        return np.nan
    else:
        if pwr < range_min:
            pwr = range_min
        elif pwr > range_max:
            pwr = range_max
        return a * np.power(pwr, b)


def high_t_geothermal_flow(t_resource, power, t_return=60):
    """
    Give the flow ressource in l/s needed to have the wanted power
    """
    flow_resource = power / ((t_resource - t_return) * 4.18)
    return flow_resource


def high_t_geothermal_power(t_resource, flow_resource, t_return=60):
    """
    Give the theoretical power in kW of the geothermal resource.
    This is the geothermal valorization made in direct, that is to say that the
     temperature of the resource is higher than 80° (T°C go of the CAD).
    This valorization is made until the return temperature of the CAD (60°)
    The flow ressource is in l/s and t_ressource is in °C
    """
    power = (t_resource - t_return) * flow_resource * 4.18
    return power


def geothermal_with_heat_pump_power(t_resource, flow_resource, t_geo_min_reinjection=25, t_required=80):
    """
    Give the theoretical power in kW of the geothermal resource.
    This is the geothermal valorization made with an heat pump,
    that is to say that the temperature of the resource is lower than 80° (T°C go of the CAD).

    The flow ressource is in l/s and t_ressource is in °C
    """
    # TODO: (w)hich to use??
    # cop_fix = 4
    cop = (0.45 * (t_required + 273)) / ((t_required + 273) - (t_resource + 273))
    q_evap = (t_resource - t_geo_min_reinjection) * flow_resource * 4.18
    q_heat = cop * q_evap / (cop - 1)
    # q_elec = q_heat - q_evap
    return q_heat


def combine_heat_sources(heat_demand, heat_sources, source_order=None):
    # TODO: (w)hat format should heat sources come in ? Really need a name and value, so could be mapping
    #  Otherwise can have source powers and source names. Important that input is ordered. Could have
    #  optional ordering param as name list.
    result = pd.DataFrame(heat_demand.values, columns=["heat_demand"], index=heat_demand.index)
    result["heat_demand_remainder"] = result["heat_demand"]
    if source_order is None:
        source_order = list(heat_sources.keys())

    for source_name in source_order:
        source_power = heat_sources[source_name]

        result["heat_source"] = source_power
        result[source_name] = result[["heat_demand_remainder", "heat_source"]].min(axis=1)
        result["heat_demand_remainder"] = result["heat_demand_remainder"] - result[source_name]

    # NOTE: because the column list gets turned to np array in the pandas internals,
    # and that the heat source names are tuples, need to explicity conver to object array
    # to prevent numpy trying to treat the list of tuples as a 2D array
    return result[np.asarray(source_order + ["heat_demand_remainder"], dtype="object")]
