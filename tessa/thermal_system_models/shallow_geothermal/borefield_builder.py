from dataclasses import dataclass, field

import geopandas as gpd
import shapely
import structlog
from pydantic import BaseModel, ConfigDict

from tessa.building_data.swisstopo import BuildingPolygonDataSource
from tessa.thermal_system_models.models import FlowProperties
from tessa.utils.pydantic_shapely import MultiPolygon, Polygon, wrap_shape

from .borefield import BoreField, Borehole, DoubleUBorehole
from .borehole_locator import locator_given_area_and_spacing
from .ground import GroundDataSource, GroundShallow, SwissGroundDataSource
from .models import sizing_bore_length_given_config

log = structlog.get_logger()


@dataclass
class BoreholeSetup:
    borehole_length: float | None = 250.0
    borehole_radius: float | None = 0.065

    def __call__(
        self,
        borefield: BoreField,
        *,
        borehole: Borehole | None = None,
    ) -> BoreField:
        borehole = self.setup_borehole(borehole)

        return borefield.model_copy(update=dict(borehole=borehole))

    def setup_borehole(self, borehole=None) -> Borehole:
        if borehole is None and self.borehole_length is not None and self.borehole_radius is not None:
            borehole = DoubleUBorehole(length=self.borehole_length, r_b=self.borehole_radius)
        return borehole


@dataclass
class BoreFieldBaseData:
    crs: int = 4326
    initial_spacing: float = 12
    obstruction_buffer: float = 4.0
    building_polygon_data: BuildingPolygonDataSource | None = None
    borehole_config: BoreholeSetup = field(default_factory=BoreholeSetup)
    ground_data_source: GroundDataSource = field(default_factory=SwissGroundDataSource)

    def _clip_obstructions(self, region):
        # Load obstruction data
        if self.building_polygon_data is None:
            raise ValueError("Clipping obstructions requires providing an obstruction data source")
        # Load building footprints and clip
        bld_footprint = self.building_polygon_data.load_polygons_in_region(region)
        # If buildings are found, subtract them from the drawn area.
        if len(bld_footprint) > 0:
            bld_buffer = self.obstruction_buffer
            bld_footprint = bld_footprint.buffer(bld_buffer).simplify(1.0).to_crs(4326)
            region = region.difference(bld_footprint.unary_union)

        # TODO: (l)oad other obstructions e.g. water bodies.

        return region

    def _get_ground(self, drill_area, borehole, ground=None):
        if ground is None and self.ground_data_source is not None:
            ground = self.ground_data_source.load_by_location(
                drill_area,
                drill_area,
                # to do: determine ground data by the centroid of drill_area
            )

        if ground is None:
            raise ValueError("Ground must be provided in the borehole field or in the GroundModel")

        if ground.T_g is None and borehole is not None:
            mean_depth = borehole.buried_depth + borehole.length / 2
            ground.T_g = ground.T_suf + ground.T_gradient * mean_depth

        return ground

    def from_configuration(
        self,
        configuration,
        *,
        boundary=None,
        crs=None,
        clip_obstructions=True,
        borehole=None,
        ground=None,
        **kwargs,
    ):
        """If we directly have borehole positions, we can generate boundary and drill areas

        Args:
            configuration: table of borehole locations
            boundary
            crs: CRS of the input dataframe
            **kwargs: additional arguments passed directly to the constructor.
        """
        if crs is None:
            crs = self.crs
        if configuration.crs is None:
            configuration.set_crs(crs)
        elif configuration.crs != crs:
            configuration = configuration.to_crs(crs)

        if boundary is None:
            boundary = shapely.concave_hull(configuration.geometry.unary_union, ratio=0.5)
        drill_area = shapely.MultiPolygon([boundary])

        if clip_obstructions:
            drill_area = self._clip_obstructions(drill_area)
        borehole = self.borehole_config.setup_borehole(borehole)
        ground = self._get_ground(drill_area, borehole, ground)
        return BoreField(
            configuration=configuration,
            boundary=wrap_shape(boundary),
            drill_area=wrap_shape(drill_area),
            crs=crs,
            borehole=borehole,
            ground=ground,
            **kwargs,
        )

    def from_drill_area(
        self,
        drill_area: Polygon | MultiPolygon,
        *,
        boundary=None,
        crs=None,
        clip_obstructions=True,
        borehole=None,
        ground=None,
        **kwargs,
    ) -> BoreField:
        if crs is None:
            crs = self.crs
        if clip_obstructions:
            drill_area = self._clip_obstructions(drill_area)
        if isinstance(drill_area, (gpd.GeoDataFrame, gpd.GeoSeries)):
            drill_area_df = drill_area.explode(ignore_index=True)
            drill_area_shape = drill_area.dissolve().geometry.values[0]
        else:
            drill_area_df = gpd.GeoSeries([drill_area], crs=crs).explode(ignore_index=True)
            drill_area_shape = drill_area

        # Make sure drill area is always a multipolygon
        if drill_area_shape.geom_type == "Polygon":
            drill_area_shape = shapely.MultiPolygon([drill_area_shape])

        if boundary is None:
            boundary = shapely.concave_hull(drill_area_df.unary_union, ratio=0.7)
        borehole = self.borehole_config.setup_borehole(borehole)
        ground = self._get_ground(drill_area, borehole, ground)

        return BoreField(
            boundary=wrap_shape(boundary),
            drill_area=wrap_shape(drill_area_shape),
            configuration=locator_given_area_and_spacing(drill_area_df, self.initial_spacing),
            spacing=self.initial_spacing,
            crs=crs,
            borehole=borehole,
            ground=ground,
            **kwargs,
        )

    def from_district(self, district, *, clip_obstructions=True, **kwargs) -> BoreField:
        drill_area = district.boundary.geometry
        if drill_area.geom_type == "Polygon":
            drill_area = shapely.MultiPolygon([drill_area])

        return self.from_drill_area(
            drill_area,
            boundary=district.boundary,
            crs=district.crs,
            clip_obstructions=clip_obstructions,
            **kwargs,
        )


@dataclass
class GroundModel:
    # Have to be run after running AddBorehole
    ground_data_source: GroundDataSource | None = None

    def __call__(
        self,
        borefield: BoreField,
        *,
        ground: GroundShallow | None = None,
    ) -> BoreField:
        if ground is None and self.ground_data_source is not None:
            ground = self.ground_data_source.load_by_location(
                borefield.drill_area,
                borefield.drill_area,
                # to do: determine ground data by the centroid of drill_area
            )

        if ground is None:
            raise ValueError("Ground must be provided in the borehole field or in the GroundModel")

        if ground.T_g is None and borefield.borehole is not None:
            mean_depth = borefield.borehole.buried_depth + borefield.borehole.length / 2
            ground.T_g = ground.T_suf + ground.T_gradient * mean_depth

        return borefield.model_copy(update=dict(ground=ground))


@dataclass
class AddFluid:
    # Have to be run after running AddBorehole and AddGround
    model_config = ConfigDict(arbitrary_types_allowed=True)

    fluid_type: str = "MPG"
    mix_percent: float | None = 25

    def __call__(
        self,
        borefield: BoreField,
        *,
        fluid: FlowProperties | None = None,
    ) -> BoreField:
        if fluid is None and self.fluid_type is not None and self.mix_percent is not None:
            fluid = FlowProperties(
                fluid_type=self.fluid_type,
                mix_percent=self.mix_percent,
                temp_going=borefield.ground.T_g,
                temp_return=borefield.ground.T_g - 5.0,
            )

        return borefield.model_copy(update=dict(fluid=fluid))


@dataclass
class BoreFieldOptimiser:
    # See network_builder.cost

    T_f_max: float = 50.0  # Unit: degC
    T_f_min: float = -1.5  # Unit: degC

    def from_borehole_load(
        self,
        borefield,
        borehole_load,
    ) -> BoreField:
        borefield = sizing_bore_length_given_config(
            borefield,
            borehole_load,
            t_f_max=self.T_f_max,
            t_f_min=self.T_f_min,
        )
        return borefield
