# load ground data, define ground properties
from abc import abstractmethod

from pydantic import BaseModel, ConfigDict, model_validator
from pydantic.dataclasses import dataclass


class GroundShallow(BaseModel):
    # See climate_data/models.py
    T_suf: float = 10.0  # Surface temperature: °C
    T_gradient: float = 0.03  # Temperature gradient: K/m
    # T_g should always be calculated from T_suf, T_gradient, median_depth.
    # If a uniform ground temperature is given, assign it to T_suf, and set T_gradient to zero.
    T_g: float = 8.0  # Undisturbed ground temperature: °C
    alpha_g: float = 1.25e-6  # Ground thermal diffusivity: m2/s
    k_g: float = 2.5  # Ground thermal conductivity (or lamda): W/(m*K)
    rho_g: float | None = None  # Density: kg/m3
    c_g: float | None = None  # Thermal capacity: J/(kg*K)
    # Volumetric thermal capacity (denisty * thermal capacity) = k_g / alpha: J/(m3*K)
    rhoc_g: float | None = None

    model_config = ConfigDict(from_attributes=True)

    @model_validator(mode="after")
    def _generate_alpha_g(self):
        if self.alpha_g is None:
            if self.rhoc_g is None:
                if (self.rho_g is None) or (self.c_g is None):
                    raise ValueError("Not able to get ground thermal diffusivity.")
                else:
                    rhoc_g = self.rho_g * self.c_g
            else:
                rhoc_g = self.rhoc_g

            self.alpha_g = self.k_g / rhoc_g
        return self


@dataclass(kw_only=True)
class GroundDataSource:
    # look for ground data (See BuildingDataSource)
    # TODO: (F)ind Swiss datasets for ground properties.
    name = "default"

    @abstractmethod
    def load_by_location(self, lat=None, lon=None) -> GroundShallow:
        pass


@dataclass(kw_only=True)
class SwissGroundDataSource(GroundDataSource):
    name = "Swiss default"

    def load_by_location(self, lat=None, lon=None) -> GroundShallow:
        return GroundShallow()
