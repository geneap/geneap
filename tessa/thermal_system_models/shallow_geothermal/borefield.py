import uuid
from collections.abc import Callable
from typing import Annotated, Optional

import numpy as np
import pandera as pa
import pygfunction as gt
import shapely
from geopandas import GeoDataFrame as gpdGeoDataFrame
from pandera.typing import DataFrame, DateTime, Index, Series
from pandera.typing.geopandas import GeoDataFrame, GeoSeries
from pydantic import BaseModel, BeforeValidator, ConfigDict, Field

from tessa.thermal_system_models.models import Substation
from tessa.utils.pydantic_shapely import MultiPolygon, Polygon, wrap_shape

from ..models import FlowProperties
from ..pipes import calc_fluid_velocity, darcy_weisbach_friction_f, darcy_weisbach_friction_f_fsolve
from .ground import GroundShallow


class Borehole(BaseModel):
    # contains borehole dimension
    name: str = "borehole"

    buried_depth: float = 2.0  # unit: m
    length: float = 200.0  # unit: m
    r_b: float = 0.065  # Borehole radius: m

    k_grout: float = 1.0  # Grout thermal conductivity: W/(m.K)
    n_loops: int = 1

    @property
    def total_pipe_length(self) -> float:
        return self.length * 2 * self.n_loops


class DoubleUBorehole(Borehole):
    # contains double U dimensions
    name: str = "double_u_borehole"

    # Pipe dimensions
    d_s: float = 0.0400  # Shank spacing: m
    r_out: float = 0.020  # Pipe outer radius: m
    r_in: float = 0.01775  # Pipe inner radius: m
    # Pipe properties
    k_pipe: float = 0.4  # Pipe thermal conductivity: W/(m.K)
    epsilon: float = 1.0e-6  # Pipe roughness: m
    n_loops: int = 2


class Configuration(pa.DataFrameModel):
    point: GeoSeries = pa.Field(nullable=True, coerce=True)
    length: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    # TODO: (a)dd here the bore_ranking column
    ranking: Optional[Series[int]] = pa.Field(coerce=True)


def _convert_layout_table(configuration):
    if configuration is not None and not isinstance(configuration, GeoDataFrame):
        return GeoDataFrame[Configuration](configuration)
    else:
        return configuration


class BoreOperation(pa.DataFrameModel):
    timestamp: Index[DateTime] = pa.Field(coerce=True)
    p_bhe_kw: Series[float] = pa.Field(nullable=True, coerce=True)
    T_b_degC: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    T_f_degC: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)


class BoreField(BaseModel):
    """
    Represents a borehole field consisting of a borehole type,
    BoreholeConfiguration giving the location of the boreholes
    and Ground and  Fluid properties

    Args:
      borehole: assume that all boreholes are identical. Therefore, only need to store the parameters of one borehole.

    """

    model_config = ConfigDict(arbitrary_types_allowed=True, validate_assignment=True, from_attributes=True)

    uid: uuid.UUID = Field(default_factory=uuid.uuid4)
    borehole: DoubleUBorehole
    configuration: Annotated[GeoDataFrame[Configuration], BeforeValidator(_convert_layout_table)]
    ground: GroundShallow
    boundary: Polygon
    drill_area: Polygon | MultiPolygon

    lifetime: float = 50.0  # Unit: year

    # TODO: add T_g_mean
    fluid: FlowProperties = Field(default=FlowProperties(fluid_type="water", temp_going=14, temp_return=7))

    spacing: float | None = None

    # costs: None
    substations: list[Substation] = []

    # Similar to supply_curve of ThermalSource
    operations: DataFrame[BoreOperation] | None = Field(None, repr=False)
    name: str = "bore_field"
    crs: int = 4326
    local_crs: int = 2056

    @classmethod
    def from_configuration(cls, configuration: GeoDataFrame[Configuration], **kwargs):
        """If we directly have borehole positions, we can generate boundary and drill areas

        Args:
            configuration: table of borehole locations
            **kwargs: additional arguments passed directly to the constructor.
        """
        bound = shapely.concave_hull(configuration.geometry.unary_union, ratio=0.5)
        drill_area = shapely.MultiPolygon([bound])
        return cls(
            configuration=configuration,
            boundary=wrap_shape(bound),
            drill_area=wrap_shape(drill_area),
            **kwargs,
        )

    @property
    def n_boreholes(self):
        if self.configuration is None:
            return 0

        n_bores = len(self.configuration)
        return n_bores

    @property
    def fluid_volume(self):
        # Return volume of carrier fluid in borehole pipes. Unit: m3
        # fluid volume of double U borehole = number of borehole * 4 * borehole length * pi * pipe inner radius ** 2
        fluid_volume = self.n_boreholes * 4 * self.borehole.length * np.pi * self.borehole.r_in ** 2
        return fluid_volume

    @property
    def antifreeze_fluid_mass(self):
        # Return mass of antifreezer in the carrier fluid. Unit: kg
        pure_antifreeze_fluid = FlowProperties(
            fluid_type=self.fluid.fluid_type,
            mix_percent=100,
            temp_going=25,
            temp_return=25,
        )
        mass = self.fluid_volume * self.fluid.mix_percent / 100 * pure_antifreeze_fluid.density
        return mass

    @property
    def R_pulse(self, t_pulse=4):
        """Calculate the thermal resistance R value for short term thermal pulse."""
        # t_pulse unit h
        if self.ground is None:
            return None
            # raise ValueError("Ground properties is not available.")
        if self.borehole is None:
            return None
            # raise ValueError("Borehole data is not available.")

        r_pulse = (
            np.log(4 * self.ground.alpha_g * t_pulse * 3600 / self.borehole.r_b / self.borehole.r_b) - 0.5772
        ) / (4 * np.pi * self.ground.k_g)
        return r_pulse

    @property
    def R_seasonal(self):
        if self.ground is None:
            return None
            # raise ValueError("Ground properties is not available.")
        if self.borehole is None:
            return None
            # raise ValueError("Borehole data is not available.")
        t = 8760 * 3600
        delta = np.sqrt(self.ground.alpha_g * t / np.pi)  # penetration depth
        r_pb = self.borehole.r_b * np.sqrt(2) / delta
        r_seasonal = np.sqrt(np.square(np.log(2 / r_pb) - 0.5772) + np.pi * np.pi / 16) / (
            2 * np.pi * self.ground.k_g
        )
        return r_seasonal

    @property
    def R_b(self):
        if self.ground is None:
            raise ValueError("Ground properties is not available.")
        if self.borehole is None:
            raise ValueError("Borehole data is not available.")
        if self.fluid is None:
            raise ValueError("Fluid data is not available.")

        m_flow_borehole = 0.28  # TODO: (d)ouble-check mass flow rate

        # Pipe thermal resistance
        R_p = gt.pipes.conduction_thermal_resistance_circular_pipe(
            self.borehole.r_in, self.borehole.r_out, self.borehole.k_pipe
        )
        # Fluid to inner pipe wall thermal resistance (Double U-tube in parallel)
        h_f = gt.pipes.convective_heat_transfer_coefficient_circular_pipe(
            m_flow_borehole / 2.0,
            self.borehole.r_in,
            self.fluid.dynamic_viscosity,
            self.fluid.density,
            self.fluid.thermal_conductivity,
            self.fluid.heat_capacity,
            self.borehole.epsilon,
        )
        R_f = 1.0 / (h_f * 2.0 * np.pi * self.borehole.r_in)

        gt_borehole = gt.boreholes.Borehole(
            self.borehole.length, self.borehole.buried_depth, self.borehole.r_b, x=0.0, y=0.0
        )

        D_s = self.borehole.d_s
        tube_pos = [(-D_s, 0.0), (0.0, -D_s), (D_s, 0.0), (0.0, D_s)]

        u_tube = gt.pipes.MultipleUTube(
            tube_pos,
            self.borehole.r_in,
            self.borehole.r_out,
            gt_borehole,
            self.ground.k_g,
            self.borehole.k_grout,
            R_f + R_p,
            nPipes=2,
            config="parallel",
        )

        R_b = u_tube.effective_borehole_thermal_resistance(m_flow_borehole, self.fluid.heat_capacity)
        return R_b

    def single_borehole_head_loss(self, v_dot_m3_s):
        """
        Calculate the borehole head loss. For dual/multi tube boreholes we assume
        the tubes are connected in parallel
        Args:
            v_dot_m3_s:

        Returns:

        """
        # TODO the supplied power for the boreholes should be derived from the Operation, but accouting
        #  for the distribution of power supply per borehole.
        # TODO total head loss in boreholes using the length, mass flow...
        roughness_mm = self.borehole.epsilon
        inner_radius_mm = self.borehole.r_in * 1000  # r_in stored in meters
        fluid_fraction_per_tube = 1 / self.borehole.n_loops
        v_dot_per_loop = fluid_fraction_per_tube * v_dot_m3_s
        velocity_m_s = calc_fluid_velocity(2 * inner_radius_mm, v_dot_per_loop)
        f_dw = darcy_weisbach_friction_f(
            velocity_m_s,
            2 * inner_radius_mm / 1000,
            self.fluid.density,
            self.fluid.dynamic_viscosity,
            roughness_mm / 1000,
        )

        r_dw = (
            8
            * f_dw
            * self.borehole.total_pipe_length
            / (9.8067 * (2 * inner_radius_mm / 1000) ** 5 * np.pi**2)
        )
        h_f_dw_m = r_dw * v_dot_per_loop**2
        return h_f_dw_m

    # def total_borehole_head_loss(self, v_dot_m3_s):
    #     # TODO this actually depends on whether boreholes are connected in series or parallel
    #     #   in reality it will be a mix with a couple of boreholes connected together and grouped.
    #     single_head = self.single_borehole_head_loss(v_dot_m3_s)
    #     return single_head

    def apply(self, operator: Callable[["BoreField"], "BoreField"]) -> "BoreField":
        return operator(self)
