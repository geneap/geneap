# Find the location of boreholes (x,y)
import geopandas as gpd
import numpy as np
from pydantic import BaseModel
from shapely import LineString, Point, Polygon
from shapely.prepared import prep

# from tessa.utils.pydantic_shapely import Polygon


# class BoreFieldBoundary(BaseModel):
#     # Can be user given multi polygon. Otherwise, use district boundary subtract building polygons.
#     # See district.boundary
#     crs: int = 4326
#     local_crs: int = 2056
#
#     boundary: Polygon | None = None
#
#     def from_district(self):
#         # BoreFieldBoundary = district boundary subtract building polygons
#         return self


# !!!! Does BoreholeLocator need to be a class? It can be functions.
# class BoreholeLocator:
#     # functions that find the bore field configuration (See buildings.geometry)
#     # See DnMstLayout as an example
#     area: None
#     configuration: None


def locator_given_area_and_spacing(
    drill_area: gpd.GeoDataFrame | gpd.GeoSeries, spacing=10, local_crs=2056
) -> gpd.GeoDataFrame:
    """
    Generate regularly spaced borehole field within the provided area

    Args:
        drill_area: Must be a Geopandas table containing a set of single geometries of allowed area.
        spacing:
        local_crs:

    Returns:

    """
    # Given the spacing between borehole
    # See 'district/models.py function from_region()' for defining shape
    input_crs = drill_area.crs
    # create prepared polygon of the total outline
    filter_polygon = prep(drill_area.to_crs(local_crs).unary_union)

    # make sure
    drill_area = drill_area.to_crs(local_crs).explode(ignore_index=True).geometry
    points: list[Point] = []

    # TODO: (B)UG If generate borehole mesh for each geometry, some boreholes are too close to each other.
    for geometry in drill_area:
        # iterate over each geometry part and apply different
        #  positioning depending on geom type.
        if geometry.geom_type == "Polygon":
            minx, miny, maxx, maxy = geometry.bounds
            # construct a rectangular mesh
            # TODO: (t)he grid doesn't have to follow lat, lon
            # TODO: (p)otential solution is to use shapely.oriented_envelope
            for y in np.arange(miny, maxy, spacing):
                for x in np.arange(minx, maxx, spacing):
                    points.append(Point((round(x, 2), round(y, 2))))
        elif geometry.geom_type == "LineString":
            # Fit drilling points in narrow roads - allow linestring input geometries,
            distances = np.arange(0, geometry.length, spacing)
            points += [geometry.interpolate(distance) for distance in distances] + [geometry.boundary[1]]

    # validate if each point falls inside shape using the prepared polygon
    valid_points = list(filter(filter_polygon.contains, points))

    configuration = gpd.GeoSeries(valid_points, crs=local_crs)
    configuration.name = "point"
    configuration = gpd.GeoDataFrame(data=configuration, geometry="point")

    return configuration.to_crs(input_crs)


def locator_given_geometry(geometry) -> gpd.GeoDataFrame:
    # User specified drilling points
    configuration = geometry
    return configuration
