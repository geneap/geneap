# functions for designing and simulation
# See network_builder
# See api.district_heating_network_for_district
import math

import geopandas as gpd
import numpy as np
import pandas as pd
import pygfunction as gt
import structlog
from pandera.typing import DataFrame
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist, squareform
from tqdm.notebook import tqdm

from tessa.core.models import ThermalGenerator, TSFrame

from .borefield import BoreField, BoreOperation, Configuration
from .borehole_locator import locator_given_area_and_spacing

log = structlog.get_logger()


class ShallowGeothermal(ThermalGenerator):
    # contains BoreField, what else? lifetime, temperature constraints, ...
    lifetime: float = 50
    borefield: None


def duplicate_lc_for_n_years(lc, n_year):
    n_year = int(n_year)
    first_year = lc.index[0].year

    index_ny = pd.DataFrame({"index": lc.index.values})
    for i in np.arange(first_year + 1, first_year + n_year):
        datetime_i = pd.DataFrame({"index": lc.index.values})
        datetime_i["index"] = datetime_i["index"].apply(lambda x: x.replace(year=i))
        index_ny = pd.concat([index_ny, datetime_i])

    lc_ny = pd.DataFrame(
        data={"": np.tile(lc.values, n_year)},
        index=index_ny["index"],
    )
    return lc_ny


def _get_coords(geo_df, local_crs):
    geo_df_conv = geo_df.to_crs(local_crs)
    return geo_df_conv.geometry.x, geo_df_conv.geometry.y


def _define_gt_borefield(borefield: BoreField, local_crs=2056):
    """
    Define the borefield using PyGfunction classes, including a
    "Network" of boreholes (not to be confused with the district network)

    Args:
        borefield:
        local_crs:

    Returns:

    """
    if borefield.borehole is None or borefield.fluid is None or borefield.ground is None:
        log.warning("No borehole, fluid, or ground defined for this borehole field")
        return None, None
    x, y = _get_coords(borefield.configuration, local_crs)
    d_s = borefield.borehole.d_s
    fluid = borefield.fluid
    cp_f = fluid.heat_capacity
    rho_f = fluid.density
    mu_f = fluid.dynamic_viscosity
    k_f = fluid.thermal_conductivity
    r_in = borefield.borehole.r_in

    tube_pos = [(-d_s, 0.0), (0.0, -d_s), (d_s, 0.0), (0.0, d_s)]

    # Total fluid mass flow rate per borehole (kg/s). Equivalent to around 1 m3/h
    m_flow_borehole = 0.28  # TODO: (d)ouble-check mass flow rate
    m_flow_network = m_flow_borehole * len(x)

    gt_borefield = [
        gt.boreholes.Borehole(
            borefield.borehole.length, borefield.borehole.buried_depth, borefield.borehole.r_b, ix, iy
        )
        for ix, iy in zip(x, y)
    ]

    # Pipe thermal resistance
    r_p = gt.pipes.conduction_thermal_resistance_circular_pipe(
        borefield.borehole.r_in, borefield.borehole.r_out, borefield.borehole.k_pipe
    )
    # Fluid to inner pipe wall thermal resistance (Double U-tube in parallel)
    h_f = gt.pipes.convective_heat_transfer_coefficient_circular_pipe(
        m_flow_borehole / 2.0, r_in, mu_f, rho_f, k_f, cp_f, borefield.borehole.epsilon
    )
    r_f = 1.0 / (h_f * 2.0 * np.pi * r_in)

    u_tubes = []
    for bore in gt_borefield:
        u_tube = gt.pipes.MultipleUTube(
            tube_pos,
            r_in,
            borefield.borehole.r_out,
            bore,
            borefield.ground.k_g,
            borefield.borehole.k_grout,
            r_f + r_p,
            nPipes=2,
            config="parallel",
        )
        u_tubes.append(u_tube)
    network = gt.networks.Network(gt_borefield, u_tubes, m_flow_network=m_flow_network, cp_f=cp_f)

    return gt_borefield, network


def calculate_R_long_term(
    borefield: BoreField,
    *,
    time=None,
):
    """
    Calculate the long term R value (ground thermal resistivity)

    Args:
        borefield:
        time:

    Returns:

    """
    bhefield, network = _define_gt_borefield(borefield)

    if time is None:
        time = borefield.lifetime * 8760 * 3600

    # TODO: (a)dd segments to gfunction options
    g_lt = gt.gfunction.gFunction(bhefield, borefield.ground.alpha_g, time)
    r_lt = g_lt.gFunc / (2 * np.pi * borefield.ground.k_g)
    return r_lt


def _cal_q_mean_month_peak(
    load_curve: DataFrame[TSFrame],
):
    # lt means lifetime / long-term
    demands_lt = load_curve.copy(deep=True)
    dt_index: pd.DatetimeIndex = demands_lt.index

    # Calculate q_mean, q_month, q_peak
    # Note that cooling demand values should be negative !
    q_mean_lt = demands_lt.groupby(dt_index.year).mean().values
    demands_lt_monthmean = demands_lt.resample("M").mean()
    q_month_h_lt = demands_lt_monthmean.groupby(demands_lt_monthmean.index.year).max().values
    q_peak_h_lt = demands_lt.groupby(dt_index.year).max().values
    q_month_c_lt = np.minimum(0.0, demands_lt_monthmean.groupby(demands_lt_monthmean.index.year).min().values)
    q_peak_c_lt = np.minimum(0.0, demands_lt.groupby(dt_index.year).min().values)
    # TODO: (S)ince R_pulse is calculated for four hours, q_peak should consider four-hour average.
    return q_mean_lt, q_month_h_lt, q_peak_h_lt, q_month_c_lt, q_peak_c_lt


def _find_two_farest_boreholes(
    bf_configuration: DataFrame[Configuration],
    *,
    local_crs=2056,
):
    # find the two points with max distance
    points = pd.DataFrame(
        data={
            "x": bf_configuration.to_crs(local_crs).geometry.x,
            "y": bf_configuration.to_crs(local_crs).geometry.y,
        },
        index=bf_configuration.index,
    )

    # Find the convexhull
    hull = ConvexHull(points[["x", "y"]])
    hullpoints = points.loc[hull.vertices]

    # Find the farest pair in hull vertices
    hdist = pdist(hullpoints)
    hdist = squareform(hdist)
    bestpair = np.unravel_index(hdist.argmax(), hdist.shape)
    farest_index = hullpoints.iloc[[bestpair[0], bestpair[1]]].index.values
    return farest_index


def optimise_boreholes_configuration(
    borefield: BoreField,
    farest_index,
):
    # step by step: adding one extra optimal borehole that result in the min R_LT
    borefield_temp = borefield.model_copy(deep=True)

    adding_rank = list(farest_index)
    j_avail_idx = [x for x in borefield.configuration.index.values if x not in adding_rank]
    # add_prio = np.array([])

    for j in tqdm(np.arange(len(borefield.configuration) - 2)):
        # print('j', j)
        j_R_lt = np.zeros(len(j_avail_idx))
        for i in np.arange(len(j_avail_idx)):
            i_sel_idx = adding_rank + [j_avail_idx[i]]
            # print(i_sel_idx)

            # Update the configuration in place because this should be
            # significantly faster than creating a new borefield object
            borefield_temp.configuration = borefield.configuration.loc[i_sel_idx]
            j_R_lt[i] = calculate_R_long_term(borefield_temp)
            # print(j_R_lt)

        j_to_add = j_avail_idx[j_R_lt.argmin()]
        j_avail_idx.remove(j_to_add)
        # add_prio = np.concatenate((add_prio, [j_to_add]))
        adding_rank.append(j_to_add)
    return adding_rank


def rank_boreholes_for_extraction(borefield):
    two_farest = _find_two_farest_boreholes(borefield.configuration, local_crs=borefield.local_crs)
    borefield.configuration["ranking"] = optimise_boreholes_configuration(borefield, two_farest)
    return borefield


def sizing_n_bore_given_length(
    borefield: BoreField,
    load_curve: DataFrame,
    *,
    T_f_max: float = 50,
    T_f_min: float = -1.5,
    error_converge: float = 0.005,
):
    """
    Given borehole length, find the minimum required number of boreholes.

    Args:
        borefield: The inital configuration of the boreholes
        load_curve: The load curve over the total borehole field lifetime.
        T_f_max:
        T_f_min:
        error_converge:

    Returns:

    """
    # Pre-optimise each configuration (for an arbitrary number of boreholes) has been .
    # NOTE: this part is slow but will be savined in the bore_ranking, so the next time it will be fast.
    if borefield.configuration is not None and "ranking" not in borefield.configuration:
        two_farest = _find_two_farest_boreholes(borefield.configuration, local_crs=borefield.local_crs)
        borefield.configuration["ranking"] = optimise_boreholes_configuration(borefield, two_farest)

    # fixme: seems ranking needs to be preserved across the optimisation.
    ranking = borefield.configuration["ranking"].values.copy()
    borefield_tmpl = borefield.model_copy(deep=True)

    n_year = int(borefield_tmpl.lifetime)

    q_mean_lt, q_month_h_lt, q_peak_h_lt, q_month_c_lt, q_peak_c_lt = _cal_q_mean_month_peak(load_curve)

    n_old = borefield_tmpl.n_boreholes
    n_new = 0
    h_new = 0
    n_ite = 0
    n_repeat = 0
    time_index = np.arange(start=1, stop=n_year + 1, step=1) * 8760 * 3600

    while (
        (h_new <= borefield_tmpl.borehole.length * (1 - error_converge))
        or (h_new >= borefield_tmpl.borehole.length)
    ) and n_repeat < 2:
        if n_ite > 0:
            # consider the case where H_new never falls in this range because n_borehole is discrete (integer).
            if n_old == n_new:
                n_repeat = n_repeat + 1
            else:
                n_old = n_new
        # calculate R_long_term
        r_lt_lifespan = calculate_R_long_term(
            borefield_tmpl,
            time=time_index,
        )

        t_g = borefield_tmpl.ground.T_g

        # Evaluate the extreme value of q_x*R_x for each year
        qx_r_h_lt = np.zeros(n_year)
        qx_r_c_lt = np.zeros(n_year)
        q_meanx_r_lt_lt = np.zeros(n_year)
        for j in np.arange(0, n_year):
            i_q_meanxRlt = q_mean_lt[0] * r_lt_lifespan[j - n_year]
            for i in np.arange(1, j + 1):
                i_q_meanxRlt = (
                    i_q_meanxRlt + (q_mean_lt[i] - q_mean_lt[i - 1]) * r_lt_lifespan[-i + j - n_year]
                )
            q_meanx_r_lt_lt[j] = i_q_meanxRlt
            qx_r_h_lt[j] = (
                np.maximum(i_q_meanxRlt, 0)
                + (q_month_h_lt[j] - np.maximum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_h_lt[j] - q_month_h_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_h_lt[j] * borefield_tmpl.R_b
            )
            qx_r_c_lt[j] = (
                np.minimum(i_q_meanxRlt, 0)
                + (q_month_c_lt[j] - np.minimum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_c_lt[j] - q_month_c_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_c_lt[j] * borefield_tmpl.R_b
            )

        # Evaluate H_ideal
        H_h_ideal = qx_r_h_lt.max() * 1000 / (t_g - T_f_min) / n_old
        H_c_ideal = qx_r_c_lt.min() * 1000 / (t_g - T_f_max) / n_old
        h_new = np.maximum(H_h_ideal, H_c_ideal)
        n_new = math.ceil(n_old * h_new / borefield_tmpl.borehole.length)
        # print('H_goal', borefield.borehole.length, 'H_new', H_new)
        # print('n_old', n_old, 'n_new', n_new)

        if n_new > borefield_tmpl.n_boreholes:
            raise RuntimeError("Current available areas for boreholes drilling are not sufficient.")
        else:
            # idxes = borefield_tmpl.configuration.ranking.values[:n_new]
            idxes = ranking[:n_new]
            borefield_tmpl.configuration = borefield_tmpl.configuration.loc[idxes]

        n_ite = n_ite + 1
        # print('iteration:', n_ite, '\n')
        if n_ite > 20:
            break  # Even though not converged, report the latest version of results
    #             sys.exit('Error! Not able to converge on N_bhe.')

    # borefield_tmpl = borefield_tmpl.model_copy(update=dict(bore_ranking=borefield_tmpl.bore_ranking[:n_new]))
    borefield_tmpl.configuration["ranking"] = ranking[:n_new]

    # log the heat exchange curve for the borefield
    operations = pd.DataFrame(index=load_curve.index, columns=["p_bhe_kw"], data=load_curve.values)
    operations.index.name = "timestamp"
    borefield_tmpl.operations = operations

    return borefield_tmpl


def sizing_bore_spacing_given_length(
    borefield: BoreField,
    load_curve: DataFrame,
    *,
    T_f_max: float = 50,
    T_f_min: float = -1.5,
    error_converge: float = 0.005,
):
    """
    Determine the spacing between boreholes that is necessary to meet the load curve. The length of boreholes is fixed.

    Parameters
    ----------
    borefield
    load_curve
    T_f_max
    T_f_min
    error_converge

    Returns
    -------

    """
    borefield_tmpl = borefield.model_copy(deep=True)

    n_year = int(borefield_tmpl.lifetime)

    q_mean_lt, q_month_h_lt, q_peak_h_lt, q_month_c_lt, q_peak_c_lt = _cal_q_mean_month_peak(load_curve)

    drill_area_df = gpd.GeoSeries([borefield_tmpl.drill_area], crs=borefield.crs).explode(
        ignore_index=True
    )  # convert to GeoSeries

    spacing_old = borefield_tmpl.spacing
    spacing_new = 0
    n_old = n_new = borefield_tmpl.n_boreholes
    h_new = 0.0
    n_ite = 0
    n_bore_hist: list[int] = []
    time_index = np.arange(start=1, stop=n_year + 1, step=1) * 8760 * 3600

    while (h_new <= borefield_tmpl.borehole.length * (1 - error_converge)) or (
        h_new >= borefield_tmpl.borehole.length
    ):
        if n_ite > 0:
            spacing_old = spacing_new
            n_old = n_new
        # calculate R_long_term
        r_lt_lifespan = calculate_R_long_term(
            borefield_tmpl,
            time=time_index,
        )

        T_g = borefield_tmpl.ground.T_g

        # Evaluate the extreme value of q_x*R_x for each year
        qx_r_h_lt = np.zeros(n_year)
        qx_r_c_lt = np.zeros(n_year)
        q_meanx_r_lt_lt = np.zeros(n_year)
        for j in np.arange(0, n_year):
            i_q_meanxRlt = q_mean_lt[0] * r_lt_lifespan[j - n_year]
            for i in np.arange(1, j + 1):
                i_q_meanxRlt = (
                    i_q_meanxRlt + (q_mean_lt[i] - q_mean_lt[i - 1]) * r_lt_lifespan[-i + j - n_year]
                )
            q_meanx_r_lt_lt[j] = i_q_meanxRlt
            qx_r_h_lt[j] = (
                np.maximum(i_q_meanxRlt, 0)
                + (q_month_h_lt[j] - np.maximum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_h_lt[j] - q_month_h_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_h_lt[j] * borefield_tmpl.R_b
            )
            qx_r_c_lt[j] = (
                np.minimum(i_q_meanxRlt, 0)
                + (q_month_c_lt[j] - np.minimum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_c_lt[j] - q_month_c_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_c_lt[j] * borefield_tmpl.R_b
            )

        # Evaluate H_ideal
        h_h_ideal = qx_r_h_lt.max() * 1000 / (T_g - T_f_min) / n_old
        h_c_ideal = qx_r_c_lt.min() * 1000 / (T_g - T_f_max) / n_old
        h_new = np.maximum(h_h_ideal, h_c_ideal)

        # Since n_borehole has to be integer, it might oscillate and H_new never meet the condition to  stop the while loop.
        if len(n_bore_hist) > 4:
            if (
                (n_bore_hist[-1] == n_bore_hist[-3])
                & (n_bore_hist[-2] == n_bore_hist[-4])
                & (h_new < borefield_tmpl.borehole.length)
            ):
                break

        # Although it seems spacing is proportional to n_borehole squared, smaller power is used to avoid overshooting.
        if h_new <= borefield_tmpl.borehole.length:
            power = 0.25
        else:
            power = 0.5
        spacing_new = spacing_old * ((borefield_tmpl.borehole.length / h_new) ** power)
        # print('H_goal', borefield.borehole.length, 'H_new', H_new)
        # print('spacing_old', spacing_old, 'spacing_new', spacing_new)

        if spacing_new < 5:  # The spacing between boreholes has to be larger than 5m.
            raise RuntimeError("Current available areas for boreholes drilling are not sufficient.")
        else:
            borefield_tmpl.spacing = spacing_new
            borefield_tmpl.configuration = locator_given_area_and_spacing(
                drill_area_df, spacing=borefield_tmpl.spacing
            )
            n_new = borefield_tmpl.n_boreholes
            n_bore_hist.append(n_new)
            # print('n_old', n_old, 'n_new', n_new)

        n_ite = n_ite + 1
        if n_ite > 20:
            break  # Even though not converged, report the latest version of results
    #             sys.exit('Error! Not able to converge on N_bhe.')

    # log the heat exchange curve for the borefield
    operations = pd.DataFrame(index=load_curve.index, columns=["p_bhe_kw"], data=load_curve.values)
    operations.index.name = "timestamp"
    borefield_tmpl.operations = operations

    return borefield_tmpl


#
# def sizing_bore_spacing_given_length(
#     borefield: BoreField,
#     load_curve: DataFrame,
#     *,
#     T_f_max: float = 50,
#     T_f_min: float = -1.5,
#     error_converge: float = 0.005,
# ):
#     """
#     Determine the spacing between boreholes that is necessary to meet the load curve. The length of boreholes is fixed.
#
#     Parameters
#     ----------
#     borefield
#     load_curve
#     T_f_max
#     T_f_min
#     error_converge
#
#     Returns
#     -------
#
#     """
#     borefield_tmpl = borefield.model_copy(deep=True)
#
#     n_year = int(borefield_tmpl.lifetime)
#
#     q_mean_lt, q_month_h_lt, q_peak_h_lt, q_month_c_lt, q_peak_c_lt = _cal_q_mean_month_peak(load_curve)
#
#     drill_area_df = gpd.GeoSeries([borefield_tmpl.drill_area], crs=borefield.crs).explode(
#         ignore_index=True
#     )  # convert to GeoSeries
#
#     spacing_old = borefield_tmpl.spacing
#     spacing_new = 0
#     n_old = borefield_tmpl.n_boreholes
#     H_new = 0
#     n_ite = 0
#     n_repeat = 0
#     time_index = np.arange(start=1, stop=n_year + 1, step=1) * 8760 * 3600
#
#     while (
#         (H_new <= borefield_tmpl.borehole.length * (1 - error_converge))
#         or (H_new >= borefield_tmpl.borehole.length)
#     ) and n_repeat < 2:
#         if n_ite > 0:
#             # consider the case where H_new never falls in this range because n_borehole is discrete (integer).
#             spacing_old = spacing_new
#             if n_old == n_new:
#                 n_repeat = n_repeat + 1
#             else:
#                 n_old = n_new
#         # calculate R_long_term
#         R_LT_lifespan = calculate_R_long_term(
#             borefield_tmpl,
#             time=time_index,
#         )
#
#         T_g = borefield_tmpl.ground.T_g
#
#         # Evaluate the extreme value of q_x*R_x for each year
#         qxR_h_lt = np.zeros(n_year)
#         qxR_c_lt = np.zeros(n_year)
#         q_meanxR_lt_lt = np.zeros(n_year)
#         for j in np.arange(0, n_year):
#             i_q_meanxRlt = q_mean_lt[0] * R_LT_lifespan[j - n_year]
#             for i in np.arange(1, j + 1):
#                 i_q_meanxRlt = (
#                     i_q_meanxRlt + (q_mean_lt[i] - q_mean_lt[i - 1]) * R_LT_lifespan[-i + j - n_year]
#                 )
#             q_meanxR_lt_lt[j] = i_q_meanxRlt
#             qxR_h_lt[j] = (
#                 np.maximum(i_q_meanxRlt, 0)
#                 + (q_month_h_lt[j] - np.maximum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
#                 + (q_peak_h_lt[j] - q_month_h_lt[j]) * borefield_tmpl.R_pulse
#                 + q_peak_h_lt[j] * borefield_tmpl.R_b
#             )
#             qxR_c_lt[j] = (
#                 np.minimum(i_q_meanxRlt, 0)
#                 + (q_month_c_lt[j] - np.minimum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
#                 + (q_peak_c_lt[j] - q_month_c_lt[j]) * borefield_tmpl.R_pulse
#                 + q_peak_c_lt[j] * borefield_tmpl.R_b
#             )
#
#         # Evaluate H_ideal
#         H_h_ideal = qxR_h_lt.max() * 1000 / (T_g - T_f_min) / n_old
#         H_c_ideal = qxR_c_lt.min() * 1000 / (T_g - T_f_max) / n_old
#         H_new = np.maximum(H_h_ideal, H_c_ideal)
#         # Although it seems spacing is proportional to n_borehole squared, 0.25 power is used to avoid overshooting.
#         spacing_new = spacing_old * ((borefield_tmpl.borehole.length / H_new) ** 0.25)
#         print("H_goal", borefield.borehole.length, "H_new", H_new)
#         print("spacing_old", spacing_old, "spacing_new", spacing_new)
#
#         if spacing_new < 5:  # The spacing between boreholes has to be larger than 5m.
#             raise RuntimeError("Current available areas for boreholes drilling are not sufficient.")
#         else:
#             borefield_tmpl.spacing = spacing_new
#             borefield_tmpl.configuration = locator_given_area_and_spacing(
#                 drill_area_df, spacing=borefield_tmpl.spacing
#             )
#             n_new = borefield_tmpl.n_boreholes
#             print("n_old", n_old, "n_new", n_new)
#
#         n_ite = n_ite + 1
#         # print('iteration:', n_ite, '\n')
#         if n_ite > 20:
#             break  # Even though not converged, report the latest version of results
#     #             sys.exit('Error! Not able to converge on N_bhe.')
#     return borefield_tmpl


def sizing_bore_length_given_config(
    load_curve: DataFrame,
    borefield: BoreField,
    *,
    t_f_max: float = 50,
    t_f_min: float = -1.5,
    error_converge: float = 0.005,
):
    # Given borehole field configuration, estimate the required depth for each borehole.
    if borefield.ground is None or borefield.borehole is None:
        return None

    borefield_tmpl = borefield.model_copy(deep=True)

    n_year = int(borefield_tmpl.lifetime)
    n_boreholes = borefield_tmpl.n_boreholes
    t_suf = borefield_tmpl.ground.T_suf  # TODO: (c)onsider the case if t_g is given
    t_gradient = borefield_tmpl.ground.T_gradient

    q_mean_lt, q_month_h_lt, q_peak_h_lt, q_month_c_lt, q_peak_c_lt = _cal_q_mean_month_peak(load_curve)

    h_old = borefield_tmpl.borehole.length
    h_new = 0.0
    n_ite = 0

    time_index = np.arange(start=1, stop=borefield_tmpl.lifetime + 1, step=1) * 8760 * 3600

    while (h_new <= h_old * (1 - error_converge)) or (h_new >= h_old * (1 + error_converge)):
        if n_ite > 0:
            h_old = h_new
        # calculate R_long_term
        r_lt_lifespan = calculate_R_long_term(
            borefield_tmpl,
            time=time_index,
        )

        t_g = borefield_tmpl.ground.T_g

        # Evaluate the extreme value of q_x*R_x for each year
        qx_r_h_lt = np.zeros(n_year)
        qx_r_c_lt = np.zeros(n_year)
        q_meanx_r_lt_lt = np.zeros(n_year)
        for j in np.arange(0, n_year):
            i_q_meanx_rlt = q_mean_lt[0] * r_lt_lifespan[j - n_year]
            for i in np.arange(1, j + 1):
                i_q_meanx_rlt = (
                    i_q_meanx_rlt + (q_mean_lt[i] - q_mean_lt[i - 1]) * r_lt_lifespan[-i + j - n_year]
                )
            q_meanx_r_lt_lt[j] = i_q_meanx_rlt
            qx_r_h_lt[j] = (
                np.maximum(i_q_meanx_rlt, 0)
                + (q_month_h_lt[j] - np.maximum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_h_lt[j] - q_month_h_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_h_lt[j] * borefield_tmpl.R_b
            )
            qx_r_c_lt[j] = (
                np.minimum(i_q_meanx_rlt, 0)
                + (q_month_c_lt[j] - np.minimum(q_mean_lt[j], 0)) * borefield_tmpl.R_seasonal
                + (q_peak_c_lt[j] - q_month_c_lt[j]) * borefield_tmpl.R_pulse
                + q_peak_c_lt[j] * borefield_tmpl.R_b
            )

        # Evaluate H_ideal
        h_h_ideal = qx_r_h_lt.max() * 1000 / (t_g - t_f_min) / n_boreholes
        h_c_ideal = qx_r_c_lt.min() * 1000 / (t_g - t_f_max) / n_boreholes
        h_new = np.maximum(h_h_ideal, h_c_ideal)
        # Update borehole length. Will impact R_LT, R_b
        borefield_tmpl.borehole.length = h_new
        borefield_tmpl.ground.T_g = t_suf + t_gradient * h_new / 2
        # print('h_old', h_old, 'h_new', h_new)

        # TODO: (c)ap_bhe are not giving the same idea as capacity of device like HP
        # Need to decide whether to report cap_bhe
        # cap_bhe_h = q_peak_h_lt.max() * h_new / h_h_ideal
        # cap_bhe_c = q_peak_c_lt.min() * h_new / h_c_ideal

        n_ite = n_ite + 1
        # print('iteration:', n_ite)
        if n_ite > 10:
            break  # Even though not converged, report the latest version of results
    #             sys.exit('Error! Not able to converge on N_bhe.')

    # log the heat exchange curve for the borefield
    operations = pd.DataFrame(index=load_curve.index, columns=["p_bhe_kw"], data=load_curve.values)
    operations.index.name = "timestamp"
    borefield_tmpl.operations = operations

    return borefield_tmpl, h_h_ideal, h_c_ideal


def simulate_borehole_wall_temperature(
    borefield: BoreField,
    *,
    load_curve: pd.Series | None = None,
):
    """
    Simulate borehole wall temperature.
    Given borehole load and the design of borehole field, simulate its operation.

    Args:
        load_curve
        borefield_

    Returns:

    """
    borefield_tmpl = borefield.model_copy(deep=True)

    if load_curve is not None:
        if borefield_tmpl.operations is None:
            op = pd.DataFrame({"p_bhe_kw": load_curve})
            op.index.name = "timestamp"
            borefield_tmpl.operations = op
        else:
            borefield_tmpl.operations.p_bhe_kw = load_curve.values
    elif borefield_tmpl.operations is not None:
        load_curve = borefield_tmpl.operations.p_bhe_kw
    else:
        raise ValueError("Borefield load curve is not available.")

    try:
        # Time step in s
        dt = (load_curve.index[1] - load_curve.index[0]).total_seconds()  # type: ignore
    except AttributeError:
        dt = 3600  # Default time step 1 hour

    tmax = len(load_curve) * dt  # Maximum time in s
    nt = int(np.ceil(tmax / dt))  # Number of time steps
    time = dt * np.arange(1, nt + 1)

    q_bhe = (
        load_curve * 1000 / borefield_tmpl.n_boreholes / borefield_tmpl.borehole.length
    )  # in watts per meter of borehole
    load_agg = gt.load_aggregation.ClaessonJaved(dt, tmax)  # Load aggregation scheme

    bhefield, network = _define_gt_borefield(borefield_tmpl)

    time_req = load_agg.get_times_for_simulation()  # Get time values
    gFunc = gt.gfunction.gFunction(bhefield, borefield_tmpl.ground.alpha_g, time=time_req)
    load_agg.initialize(
        gFunc.gFunc / (2 * np.pi * borefield_tmpl.ground.k_g)
    )  # Initialize load aggregation scheme

    # Evaluate borehole wall temperature
    T_b = np.zeros(nt)
    for i, (t, q_bhe_i) in enumerate(zip(time, q_bhe.values)):
        load_agg.next_time_step(t)
        load_agg.set_current_load(q_bhe_i)
        deltaT_b = load_agg.temporal_superposition()
        T_b[i] = borefield_tmpl.ground.T_g - deltaT_b

    # Evaluate mean fluid temperature. This approach considers borehole thermal resistance.
    # It is fast but lacks validation. It should be fine to model extreme temperatures.
    T_f = T_b - q_bhe.values.flatten() * borefield_tmpl.R_b

    borefield_tmpl.operations["T_b_degC"] = T_b
    borefield_tmpl.operations["T_f_degC"] = T_f

    return borefield_tmpl
