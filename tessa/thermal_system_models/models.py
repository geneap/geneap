import dataclasses
import uuid
from collections.abc import Iterable, Iterator, Sequence
from typing import Any, Literal, Optional, Type

import numpy as np
import pandas as pd
import pandera as pa
import structlog
import xarray as xr
from pandera.typing import DataFrame, DateTime, Index, Series
from pandera.typing.geopandas import GeoDataFrame, GeoSeries
from pydantic import (
    UUID4,
    BaseModel,
    ConfigDict,
    Field,
    ValidationError,
    field_validator,
    model_validator,
)
from pydantic_core.core_schema import ValidationInfo
from pygfunction.media import Fluid

from tessa import sparse_graph
from tessa.core.models import Fuel, ThermalGenerator, UtilityNetwork
from tessa.sparse_graph.common import flatten_directed_2d_graph
from tessa.utils.pydantic_shapely import Point

from .pipes import (
    NUSSBAUMER_COSTS,
)

log = structlog.get_logger()


class PipeSpec(BaseModel):
    """

    Attributes:
        n_tubes: Number of pipes. Can be 1: single pipe (ring), 2: double pipe (out, return)

    """

    n_tubes: Literal[1, 2] = 2
    material: str = "pvc"
    roughness_mm: float = 0.0015


class Pipes(pa.DataFrameModel):
    # Disable the multi index validation because there is a backend bug in pandera
    # that I think is caused by geopandas backend not picking up the pandas multiindex support.
    # Arguably we could move away from relying on indexes to be future compatible with polars.

    # id_from: Index[int] = pa.Field(check_name=True, coerce=True)
    # id_to: Index[int] = pa.Field(check_name=True, coerce=True)
    routes: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    heat_total: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    power_total: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    inner_diameter_mm: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    geom: Optional[GeoSeries] = pa.Field(nullable=True, coerce=True)

    class Config:
        strict = False
        # multiindex_strict = True
        multiindex_coerce = True


class BuildingConnections(pa.DataFrameModel):
    # TODO: (t)his might be superceded by substation -> building connections
    index: Index[int]
    geom: Optional[GeoSeries] = pa.Field(nullable=True, coerce=True)
    # TODO: (a)dd building heat exchanger or booster heat pump data

    class Config:
        strict = False


class NetworkDemand(pa.DataFrameModel):
    index: Index[DateTime]
    p_hww_kw: Series[float]
    p_loss_kw: Series[float]
    p_with_loss_kw: Series[float]


class ThermalInjectionPoint(BaseModel):
    model_config = ConfigDict(validate_assignment=True, arbitrary_types_allowed=True, from_attributes=True)

    # NOTE decide not to flatten this into the thermal generator b/c later
    #  can want things like injection heat exchange properties as subclass of injeciotn point
    position: Point | None
    node_id: int | None  # for now this is the building ID


class ConnectionPoint(BaseModel):
    model_config = ConfigDict(validate_assignment=True, arbitrary_types_allowed=True, from_attributes=True)

    # NOTE decide not to flatten this into the thermal generator b/c later
    #  can want things like injection heat exchange properties as subclass of injeciotn point
    position: Point | None
    node_id: int | None  # for now this is the building ID


class SupplyCurve(pa.DataFrameModel):
    timestamp: Index[DateTime] = pa.Field(coerce=True)
    p_supply_potential_kw: Series[float] = pa.Field(coerce=True)
    p_supply_kw: Series[float] = pa.Field(coerce=True)

    # Consuption column can be used to provide the energy consumption
    # time series of the generator, for example for a heat pump with variable
    # COP the electricity consumption will vary.
    p_consumption_kw: Optional[Series[float]] = pa.Field(coerce=True)


class ThermalSource(ThermalGenerator):
    model_config = ConfigDict(
        # enabling validate assignment breaks the model_validator
        # validate_assignment=True,
        arbitrary_types_allowed=True,
        from_attributes=True,
    )

    uid: UUID4 = Field(default_factory=uuid.uuid4)
    position: Point | None = None
    injection_point: ThermalInjectionPoint | None = None

    # TODO should we require there is a fuel object even if it is a "dummy"
    fuel: Fuel | None = None
    supply_curve: DataFrame[SupplyCurve] | None = Field(None, repr=False)
    rated_power_useful: float | None = None
    rated_power_final: float | None = None

    energy_useful: float | None = None
    energy_final: float | None = None

    lifetime: float = 20
    # Following naming in DK database
    # investment is per capacity
    investment_per_kw: float | None = 0
    # Variable O&M is per kWh supplied. DO NOT include the fuel costs.
    variable_o_and_m: float | None = 0
    # fixed O&M is per rated thermal capacity
    fixed_o_and_m: float | None = 0
    # kg_co2_kwh: float

    investment_cost: float | None = None
    o_and_m: float | None = None
    co2_eq_total: float | None = None
    fuel_cost: float | None = None

    @field_validator("code", mode="before")
    @classmethod
    def _ensure_code(cls, v, info: ValidationInfo):
        if v is None:
            if info.data["name"] is None:
                raise ValidationError("Both name and code cannot be None")
            else:
                return info.data["name"].lower().replace(" ", "_")
        return v

    @field_validator("supply_curve")
    @classmethod
    def _check_supply_curve(cls, v: DataFrame, info: ValidationInfo):
        if v is not None:
            return DataFrame[SupplyCurve](v)
        else:
            return None

    @property
    def supply_duration_curve(self):
        if self.supply_curve is not None:
            return (
                self.supply_curve.p_supply_kw.resample("1h")
                .mean()
                .interpolate()
                .sort_values(ascending=False, ignore_index=True)
            )

    @model_validator(mode="after")
    def _power_energy(self):
        """After parsing, fill missing values according to known ones."""
        eff = self.efficiency or float("nan")
        # Calculate power and energy from the supply curve if they are not provided.
        if self.supply_curve is not None:
            crv: DataFrame[SupplyCurve] = self.supply_curve
            if pd.isna(self.rated_power_useful):
                self.rated_power_useful = crv.p_supply_potential_kw.max()

                if pd.isna(self.rated_power_final) and not pd.isna(self.efficiency):
                    self.rated_power_final = self.rated_power_useful / eff
            # if pd.isna(self.energy_useful) and self.supply_curve is not None:
            #     self.energy_useful = float(self.supply_curve.p_supply_kw.sum())
            if (pd.isna(self.energy_useful) or self.energy_useful == 0) and not pd.isna(
                crv.p_supply_kw.mean()
            ):
                self.energy_useful = crv.p_supply_kw.mean() * 8760
                if (pd.isna(self.energy_final) or self.energy_final == 0) and not pd.isna(self.efficiency):
                    self.energy_final = self.energy_useful / eff

        # Calculate useful or final power if one of them is given and efficiency is known
        if pd.isna(self.rated_power_useful) or self.rated_power_useful == 0:
            pwr = self.rated_power_final or 0
            if pd.notna(pwr) and pd.notna(eff) and eff > 0:
                self.rated_power_useful = pwr * eff
            else:
                self.rated_power_final = None

        # Calculate useful or final power if one of them is given and efficiency is known
        if pd.isna(self.rated_power_final) or self.rated_power_final == 0:
            pwr = self.rated_power_useful or 0
            if pd.notna(pwr) and pd.notna(eff) and eff > 0:
                self.rated_power_final = pwr / eff
            else:
                self.rated_power_final = None

        # Calculate useful or final energy if one of them is given and efficiency is known
        if pd.isna(self.energy_useful) and not pd.isna(self.energy_final):
            e_final = self.energy_final or 0
            if pd.notna(e_final) and pd.notna(eff) and eff > 0:
                self.energy_useful = e_final * eff
        elif (pd.isna(self.energy_final) or self.energy_final == 0) and not pd.isna(self.energy_useful):
            e_use = self.energy_useful or 0
            if pd.notna(e_use) and pd.notna(eff) and eff > 0:
                self.energy_final = e_use / eff
            else:
                self.energy_final = None

        # Calculate CO2 emissions from fuel CO2 intensities
        if (pd.isna(self.co2_eq_total) or self.co2_eq_total == 0) and self.fuel is not None:
            fuel_co2_kwh = self.fuel.co2_eq_kwh or np.nan
            energy_final = self.energy_final or np.nan
            if pd.notna(fuel_co2_kwh) and pd.notna(energy_final):
                self.co2_eq_total = fuel_co2_kwh * energy_final

        # Calculate fuel costs from unit prices
        if pd.isna(self.fuel_cost) and self.fuel is not None:
            energy_final = self.energy_final or np.nan
            if pd.notna(energy_final) and pd.notna(self.fuel.price):
                self.fuel_cost = energy_final * self.fuel.price

        # Calculate total costs from cost per kw, or cost per kw from total cost
        investment_per_kw = self.investment_per_kw
        rated_power_useful = self.rated_power_useful
        if (
            pd.isna(self.investment_cost)
            or self.investment_cost == 0
            and not pd.isna(investment_per_kw)
            and not pd.isna(rated_power_useful)
        ):
            self.investment_cost = investment_per_kw * rated_power_useful
        elif (
            not pd.isna(self.investment_cost)
            and self.investment_cost > 0
            and pd.isna(investment_per_kw)
            or investment_per_kw == 0
            and not pd.isna(rated_power_useful)
        ):
            self.investment_per_kw = self.investment_cost / self.rated_power_useful

        #  Calculate total O&M costs from parameters.
        if pd.isna(self.o_and_m):
            # IF both parts ar not nan -> calculate. If either
            if pd.notna(self.fixed_o_and_m) and pd.notna(self.rated_power_useful):
                pwr_o_and_m = self.fixed_o_and_m * self.rated_power_useful
            else:
                pwr_o_and_m = np.nan
            if pd.notna(self.variable_o_and_m) and pd.notna(self.energy_useful):
                ener_o_and_m = self.variable_o_and_m * self.energy_useful
            else:
                ener_o_and_m = np.nan

            o_and_m = pwr_o_and_m + ener_o_and_m
            if pd.isna(o_and_m):
                self.o_and_m = None
            else:
                self.o_and_m = o_and_m

        return self


class ThermalSources(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True, validate_assignment=True, from_attributes=True)

    sources: list[ThermalSource] = Field([])

    @classmethod
    def model_validate(cls: type["ThermalSources"], obj: Any) -> "ThermalSources":
        if hasattr(obj, "sources"):
            srcs = obj.sources
        else:
            srcs = obj
        srcs = [ThermalSource.model_validate(o) for o in srcs]

        return ThermalSources(sources=srcs)

    @property
    def rated_power_useful(self) -> float:
        """Total useful thermal power output from heat sources"""
        return sum(
            s.rated_power_useful
            for s in self.sources
            if s.rated_power_useful is not None and not pd.isna(s.rated_power_useful)
        )

    @property
    def supply_curve(self) -> DataFrame[SupplyCurve]:
        """Total useful thermal power output from heat sources"""
        if len(self.sources) > 0:
            crv = sum(s.supply_curve for s in self.sources if s.supply_curve is not None)
        else:
            crv = pd.DataFrame(
                np.zeros((8760, 3)),
                columns=list(SupplyCurve.to_schema().columns.keys()),
                index=pd.date_range("2015-01-01", "2015-12-31 23:59:00", freq="h"),
            )
        return DataFrame[SupplyCurve](crv)

    @property
    def any_has_position(self):
        return any((s.position is not None) for s in self.sources)

    def __len__(self):
        return len(self.sources)

    def __iter__(self) -> Iterator[ThermalSource]:
        return iter(self.sources)

    def __repr__(self):
        s = f"ThermalSources(n_sources={len(self)}, sources=[\n"
        for src in self.sources:
            s += src.__repr__()
            s += "\n"
        s += "])"
        return s


class Pump(BaseModel):
    model_config = ConfigDict(validate_assignment=True, from_attributes=True)

    uid: UUID4 = Field(default_factory=uuid.uuid4)
    type: str | None = None
    name: str | None = None
    description: str | None = None

    rated_power: float | None = None
    efficiency: float | None = 0.85
    head: float | None = None

    pump_cost: float | None = None
    bare_module_cost: float | None = None


class Substation(BaseModel):
    """
    Represents a substation on the network that can house the heat source including the main
    heat source, circulation pump, or act as a substation connected to several buildings
    containing only a heat exchanger or more heat sources such as a booster heat pump.

    Provides methods to sumarise heat source and demands from the connected heat sources.

    The most common case we leave the connected buildings list empty and provide sources
    list for each heat source. Next can add a circulation pump.
    """
    model_config = ConfigDict(arbitrary_types_allowed=True, validate_assignment=True, from_attributes=True)

    uid: UUID4 = Field(default_factory=uuid.uuid4)
    position: Point | None = None
    connection_point: ConnectionPoint | None = None
    name: str | None = None
    building_ids: set[int] = set()

    sources: list[ThermalSource] = Field([])

    pumps: list[Pump] = Field([])

    @property
    def rated_power_useful(self) -> float:
        """Total useful thermal power output from heat sources"""
        return sum(
            s.rated_power_useful
            for s in self.sources
            if s.rated_power_useful is not None and not pd.isna(s.rated_power_useful)
        )

    @property
    def energy_useful(self) -> float:
        """Total useful thermal power output from heat sources"""
        return sum(
            s.energy_useful
            for s in self.sources
            if s.energy_useful is not None and not pd.isna(s.energy_useful)
        )

    @property
    def supply_curve(self) -> DataFrame[SupplyCurve]:
        """Total useful thermal power output from heat sources"""
        if len(self.sources) > 0:
            # start = self.sources[0].supply_curve
            return sum(s.supply_curve for s in self.sources if s.supply_curve is not None)
        else:
            return pd.DataFrame(
                np.zeros((8760, 3)),
                columns=list(SupplyCurve.to_schema().columns.keys()),
                index=pd.date_range("2015-01-01", "2015-12-31 23:59:00", freq="h"),
            )

    @property
    def rated_pump_power(self) -> float:
        return sum(
            p.rated_power for p in self.pumps if p.rated_power is not None and not pd.isna(p.rated_power)
        )

    @property
    def pump_power_useful(self) -> float:
        return sum(
            p.rated_power * p.efficiency
            for p in self.pumps
            if p.rated_power is not None and not pd.isna(p.rated_power)
        )

    @property
    def pump_head(self) -> float:
        return sum(p.head for p in self.pumps if p.head is not None and not pd.isna(p.head))


class FlowTemperatures(BaseModel):
    temp_going: float = Field(80.0, gt=0, description="Flow $T_{out}$ [C]")
    temp_return: float = Field(60.0, gt=0, description="Flow $T_{return}$ [C]")

    @property
    def delta_t(self):
        return self.temp_going - self.temp_return


class FluidProperties(BaseModel):
    """Only the essential information needed to create a SCP/Pygfunction fluid
    Attributes:
        fluid_type: The type of fluid (water, mix of water an antifreeze)
        mix_percent: the percent of antifreeze in the water
    """

    fluid_type: str = "water"
    mix_percent: float = 0

    @field_validator("fluid_type", mode="before")
    @classmethod
    def _set_fluid_type(cls, v):
        return v or "water"

    @field_validator("mix_percent", mode="before")
    @classmethod
    def _set_mix_percent(cls, v):
        return v or 0

    # fluid_heat_capacity: float = Field(
    #     4.196, gt=0, description='Carrier fluid heat capacity[kJ/kg*K]', unit='kJ/(kg*K)'
    # )
    # fluid_density: float = Field(971.81, gt=0, description='Carrier fluid density [kg/m3]', unit='kg/m3')


class FlowProperties(FluidProperties, FlowTemperatures):
    model_config = ConfigDict(from_attributes=True, validate_assignment=True)

    @property
    def heat_capacity(self):
        t_mean_c = (self.temp_going + self.temp_return) / 2
        fluid = Fluid(self.fluid_type, self.mix_percent, T=t_mean_c)
        return fluid.specific_heat_capacity()

    @property
    def density(self):
        # TODO: (u)se cached property
        t_mean_c = (self.temp_going + self.temp_return) / 2
        fluid = Fluid(self.fluid_type, self.mix_percent, T=t_mean_c)
        return fluid.density()

    @property
    def dynamic_viscosity(self):
        # TODO: (u)se cached property
        t_mean_c = (self.temp_going + self.temp_return) / 2
        fluid = Fluid(self.fluid_type, self.mix_percent, T=t_mean_c)
        return fluid.dynamic_viscosity()

    @property
    def thermal_conductivity(self):
        t_mean_c = (self.temp_going + self.temp_return) / 2
        fluid = Fluid(self.fluid_type, self.mix_percent, T=t_mean_c)
        return fluid.thermal_conductivity()


_DEFAULT_COSTS = NUSSBAUMER_COSTS["urban"]


class NetworkCosts(BaseModel):
    """
    Network costs properties

    Attributes:
        c_trench_var: chf/m/m trench variable costs (in function of pipe diameter)
        c_trench_fixed: trench fixed costs per meter
        c_pipe_var: chf/m/m pipe variable costs (in function of pipe diameter)
        c_pipe_fixed: pipe fixed costs per meter
        variable_o_and_m: operation and maintenance cost per kWh
        network_lifetime: lifetime in years
    """
    model_config = ConfigDict(from_attributes=True)

    c_trench_var: float = Field(_DEFAULT_COSTS["c_trench_var"], description="$CT_{{variable}}$ [CHF/m2]")
    c_trench_fixed: float = Field(_DEFAULT_COSTS["c_trench_fixed"], description="$CT_{{fixed}}$ [CHF/m]")
    c_pipe_var: float = Field(_DEFAULT_COSTS["c_pipe_var"], description="$CP_{{variable}}$ [CHF/m2]")
    c_pipe_fixed: float = Field(_DEFAULT_COSTS["c_pipe_fixed"], description="$CP_{{fixed}}$ [CHF/m]")
    # percentage O&M is per total network investment (e.g. 10% of total initial investment)
    # percentage_o_and_m: float = 0.1
    # Variable O&M is per kWh supplied.
    variable_o_and_m: float = Field(0.002295, description="O&M [CHF/kWh]")

    network_lifetime: float = Field(40.0, min=1.0, description="Network Life [years]")

    def cost_per_meter(self, diameters):
        piping_cost = np.nan_to_num(self.c_pipe_var) * diameters + np.nan_to_num(self.c_pipe_fixed)
        trench_cost = np.nan_to_num(self.c_trench_var) * diameters + np.nan_to_num(self.c_trench_fixed)
        cost_per_m = piping_cost + trench_cost

        return cost_per_m


class DistrictNetwork(UtilityNetwork):
    model_config = ConfigDict(arbitrary_types_allowed=True, from_attributes=True)

    kind: Literal["districtnetwork"] = "districtnetwork"
    name: str = "district_heating"
    # graph: DatasetType | None = Field(None, exclude=True, repr=False)
    graph: xr.Dataset | None = Field(None, exclude=True, repr=False)

    building_ids: list[int] = []
    pipes: GeoDataFrame[Pipes] | None = Field(None, repr=False)
    building_connections: GeoDataFrame[BuildingConnections] | None = Field(None, repr=False)

    pipe_spec: PipeSpec = PipeSpec()
    flow: FlowProperties = FlowProperties()
    costs: NetworkCosts = NetworkCosts()

    power_demand_curve: DataFrame[NetworkDemand] | None = Field(None, repr=False)

    substations: list[Substation] = []

    @classmethod
    def model_validate(
        cls: type[BaseModel],
        obj: Any,
        *,
        strict: bool | None = None,
        from_attributes: bool | None = None,
        context: dict[str, Any] | None = None,
    ) -> "DistrictNetwork":
        if dataclasses.is_dataclass(obj):
            # FIXME using asdict seems to unwrap the geodataframes so they no longer
            #  have pandera type wrappers, no idea why.
            return cls(**dataclasses.asdict(obj))
        else:
            return super().model_validate(obj, strict, from_attributes)

    @model_validator(mode="after")
    def _cross_validated(self):
        # If pipes are deleted clear the building connections and network graph
        if self.pipes is None and self.building_connections is not None:
            self.building_connections = None
            self.graph = None
        elif self.graph is None:
            self.building_connections = None
            self.pipes = None
        # TODO: (s)hould be also true for building connections but less clear when this could happen
        elif self.building_connections is None and self.pipes is not None:
            self.pipes = None
            # self.graph = None
        return self

    @field_validator("substations", mode="before")
    def _substations_never_none(cls, v):
        # This is to avoid validation error
        # when trying to set substations to None
        return v if v else []

    @field_validator("pipes", mode="before")
    def _check_pipes(cls, v):
        # Avoid validation error in some circumstances where
        # the geodataframe loses its pandera wrapper
        if v is not None and not isinstance(v, GeoDataFrame):
            return GeoDataFrame[Pipes](v)
        return v

    @field_validator("building_connections", mode="before")
    def _check_building_connections(cls, v):
        # Avoid validation error in some circumstances where
        # the geodataframe loses its pandera wrapper
        if v is not None and not isinstance(v, GeoDataFrame):
            return GeoDataFrame[BuildingConnections](v)
        return v

    @field_validator("pipe_spec", mode="before")
    def _enusre_pipe_spec(cls, v):
        # Avoid validation error in some circumstances where
        # the geodataframe loses its pandera wrapper
        if v is None:
            return PipeSpec()
        return v

    @property
    def annual_demand_kwh(self) -> float | None:
        if self.power_demand_curve is not None:
            return self.power_demand_curve.p_with_loss_kw.mean() * 8760
        else:
            return None

    @property
    def supply_curve(self):
        if len(self.substations) > 0:
            return sum(s.supply_curve for s in self.substations if s.supply_curve is not None)
        else:
            return pd.DataFrame(
                np.zeros((8760, 3)),
                columns=list(SupplyCurve.to_schema().columns.keys()),
                index=pd.date_range("2015-01-01", "2015-12-31 23:59:00", freq="h"),
            )

    @property
    def heat_sources(self) -> ThermalSources:
        log.warning("use of property heat_sources is deprecated")
        return ThermalSources(sources=sum((sub.sources for sub in self.substations), []))

    @heat_sources.setter
    def heat_sources(self, sources: Iterable[ThermalSource] | ThermalSources):
        sources = list(sources)
        sources_to_add = []
        for hs in sources:
            for sub in self.substations:
                if hs in sub.sources:
                    break
            else:
                sources_to_add.append(hs)

        for hs in sources_to_add:
            self.substations.append(
                Substation(sources=[hs], position=hs.position, connection_point=hs.injection_point)
            )

    @property
    def graph_table(self):
        if self.graph is None:
            return None
        # pick a directed var with priority
        # directed_var = None
        # d_vars = {"heat_total", "power_total"}
        # d_vars = d_vars.intersection(self.graph.data_vars)
        # if len(d_vars) > 0:
        #     directed_var = list(d_vars)[0]
        # else:
        #     for var in self.graph.data_vars:
        #         if hasattr(self.graph[var], "is_directed") and self.graph[var].is_directed:
        #             directed_var = var
        #             break
        #     else:
        #         log.warning("No directed variables found")

        return flatten_directed_2d_graph(self.graph, direct_var=None)
        # return flatten_directed_2d_graph(self.graph, direct_var=directed_var)

    @graph_table.setter
    def graph_table(self, graph_table):
        # Should respect shape and coord order if there is a graph already set.
        if graph_table is None:
            self.graph = None
        elif self.graph is not None:
            self.graph = sparse_graph.dataframe_to_sparse_with_coords(
                graph_table, self.graph[list(self.graph.data_vars)[0]]
            )
        else:
            row_index = graph_table.index.get_level_values("id_from")
            col_index = graph_table.index.get_level_values("id_to")
            n = len(graph_table)
            self.graph = sparse_graph.dataframe_to_sparse_with_coords(
                graph_table, shape=(n, n), row_index=row_index, col_index=col_index
            )

    def pipes_geojson(
        self,
        with_props: bool | Sequence = True,
        include_route_ids=True,
    ) -> dict | None:
        """
        Generate a geojson map from the table of pipes geometries, optionally
        applying a color style property based on a given data parameter.

        Args:
            with_props:


        Returns:

        """
        if self.pipes is None or self.graph is None:
            return None
        # aim to move this method out of the model eventually
        graph = self.graph
        pipes = self.pipes
        if not (with_props):
            features = []
            for feature in pipes.iterfeatures(na="null", show_bbox=False):
                if include_route_ids == "string":
                    feature["properties"]["route_id"] = str(
                        [
                            (int(route_id[0]), int(route_id[1]))
                            for route_id in feature["properties"]["route_id"]
                        ]
                    )
                elif include_route_ids:
                    feature["properties"]["route_id"] = [
                        (int(route_id[0]), int(route_id[1])) for route_id in feature["properties"]["route_id"]
                    ]
                features.append(feature)
            return {
                "type": "FeatureCollection",
                "features": features,
            }

        if isinstance(with_props, Sequence):
            props_list = list(with_props)
            existing_props = {v for v in graph.data_vars if v in props_list}
            if set(with_props) != existing_props:
                # raise an error if we asked for variables and didn't get them.
                raise ValueError(f"Requested props {with_props} but only {existing_props} were found.")
        else:
            props_list = list(graph.data_vars)

        val_cache = sparse_graph.sparse_dataset_with_common_coords_to_dataframe(graph[props_list])

        val_cache = val_cache.fillna(0).sort_index()

        default_zeros = np.zeros(len(props_list))
        feature_collection = []

        for feature in pipes.iterfeatures():
            route_ids = feature.get("properties", {}).get("route_id", [])
            # keep json serializer happy with plain python types
            route_ids = [(int(route_id[0]), int(route_id[1])) for route_id in route_ids]

            if route_ids is None:
                continue
            elif len(route_ids) == 1:
                route_id = route_ids[0]
                try:
                    data_vals = val_cache.loc[route_id].values
                except KeyError:
                    data_vals = default_zeros
                try:
                    data_vals = val_cache.loc[(route_id[1], route_id[0])].values + data_vals
                except KeyError:
                    pass

                props = dict(zip(val_cache.columns, data_vals))
                feature["properties"].update(props)

            else:
                # Performance note: selecting in a loop was 2x faster than
                # merging the index sets and doing a multi select for the test data

                # route_ids_invert = {(route_id[1], route_id[0]) for route_id in route_ids}
                # select = (val_cache.index.intersection(set(route_ids))
                #                    .union(val_cache.index.intersection(route_ids_invert)))
                # data_vals = val_cache.loc[select]
                # feature['properties'].update(data_vals.sum(axis='rows').to_dict())

                props = np.zeros((len(val_cache.columns), len(route_ids)))
                for i, route_id in enumerate(route_ids):
                    try:
                        data_vals = val_cache.loc[route_id]
                    except KeyError:
                        data_vals = default_zeros
                    try:
                        data_vals = val_cache.loc[(route_id[1], route_id[0])] + data_vals
                    except KeyError:
                        pass

                    props[:, i] = data_vals

                props = np.nansum(props, axis=1)
                feature["properties"].update(dict(zip(val_cache.columns, props)))

            if include_route_ids == "string":
                feature["properties"]["route_id"] = str(route_ids)
            elif include_route_ids:
                feature["properties"]["route_id"] = route_ids
            else:
                if "route_id" in feature["properties"]:
                    del feature["properties"]["route_id"]

            # Finally add the feature to the collection
            feature_collection.append(feature)

        return {"features": feature_collection, "type": "FeatureCollection"}
