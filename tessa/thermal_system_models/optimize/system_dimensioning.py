import numpy as np
from scipy.optimize import fsolve


def _cost_fn_solve_peak_power(x, load_curve, share_you_want):
    diff = load_curve.clip(0, x[0]).sum() - load_curve.sum() * share_you_want
    return diff


def power_to_meet_demand_share(load_curve, share):
    """
    Calculates the system power required to meet a given percent of the total energy
    supply for the given load curve.

    Args:
        load_curve:

    Returns:

    """
    starting_estimate = load_curve.max()
    soln = fsolve(_cost_fn_solve_peak_power, starting_estimate, args=(load_curve, share))
    peak_power = soln[0]
    return peak_power


def test_power_to_meet_demand_share():
    test_load_curve = np.array([1000, 2000, 1000, 1500])
    peak_power = power_to_meet_demand_share(test_load_curve, 0.6)
    assert peak_power is not None
