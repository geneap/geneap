from functools import cache
from importlib import resources
from pathlib import Path

import numpy as np
import pandas as pd
import pandera as pa
import scipy.optimize
import structlog
from numba import float64, vectorize
from numpy import pi
from numpy.typing import ArrayLike
from pandera.typing import DataFrame, Series

from tessa.utils.datapackage import read_datapackage_table

from . import data as thermal_data

log = structlog.get_logger()


class NominalDiameterTable(pa.DataFrameModel):
    dn: Series[int]
    od_mm: Series[float]
    thickness_mm: Series[float]
    id_mm: Series[float]


@cache
@pa.check_types
def load_steel_pipe_dn_table() -> DataFrame[NominalDiameterTable]:
    source = resources.files(thermal_data).joinpath("ise_6708_medium_inner_pipe_nominal_diameter.csv")
    with resources.as_file(source) as parameter_file:
        table = pd.read_csv(parameter_file)
        table.columns = [c.lower() for c in table.columns]
        return DataFrame[NominalDiameterTable](table)


@cache
def load_pipe_velocity_table():
    source = resources.files(thermal_data).joinpath("pipe_velocities/datapackage.json")
    pipe_velocity_table = read_datapackage_table(source)
    pipe_velocity_table.index = pipe_velocity_table["nominal_diameter__mm"]

    # source = resources.files(thermal_data).joinpath("pipe_velocities/pipe_velocities.csv")
    # pipe_velocity_table = pd.read_csv(source)
    # pipe_velocity_table.index = pipe_velocity_table['nominal_diameter__mm']

    return pipe_velocity_table


@cache
def load_pipe_materials_table():
    source = resources.files(thermal_data).joinpath("pipe_materials.csv")
    pipe_materials_table = pd.read_csv(source)
    pipe_materials_table = pipe_materials_table.set_index("material")
    return pipe_materials_table


@cache
def load_hourly_heat_loss_percentage() -> pd.Series:
    """
    Load the hourly heat loss percentage based
    on return of experience of the CADSIG-CADIOM connexion [1]

    Returns:
            With 8760 float values
            hourly percentage of heat losses on the district heating

    References:
    [1] QUIQUEREZ, Loic, FAESSLER, Jérôme, LACHAL, Bernard Marie & Services Industriels de Genève.
    Réseaux thermiques multi-ressources efficients et renouvelables: Etude de cas
    de la connexion des réseaux thermiques CADIOM (chaleur fatale) et CADSIG (gaz)
    à Genève et perspectives d’évolution. Genève : Services Industriels de Genève, 2015
    """
    from . import data as thermal_data

    source = resources.files(thermal_data).joinpath("dhn_hourly_heat_lost_fractions.csv")
    with resources.as_file(source) as parameter_file:
        with Path(parameter_file).open() as f:
            return pd.read_csv(f, sep=",", encoding="utf8").set_index("hour")["percent_loss"] / 100


@cache
def load_monthly_heat_loss_percentage() -> pd.Series:
    """
    Load the monthly heat loss percentage based
    on return of experience of the CADSIG-CADIOM connexion [1]

    Returns:
            With 12 float values
            monthly percentage of heat losses on the district heating

    References:
    [1] QUIQUEREZ, Loic, FAESSLER, Jérôme, LACHAL, Bernard Marie & Services Industriels de Genève.
    Réseaux thermiques multi-ressources efficients et renouvelables: Etude de cas
    de la connexion des réseaux thermiques CADIOM (chaleur fatale) et CADSIG (gaz)
    à Genève et perspectives d’évolution. Genève : Services Industriels de Genève, 2015
    """
    from . import data as thermal_data

    source = resources.files(thermal_data).joinpath("dhn_monthly_heat_lost_percentage.csv")
    with resources.as_file(source) as parameter_file:
        with Path(parameter_file).open() as f:
            return pd.read_csv(f, sep=",", encoding="utf8").set_index("month")["percent_loss"] / 100


def average_annual_heat_loss_percentage(average_linear_heat_density):
    """
    Give the annual heat loss of the district heating in percentage per year.
    Based on Thomas Nussbaumer & Stefan Thalmann report [1]

    ..note::
        This should be used for the *average* linear heat density to give
        an overall heat loss percentage, not to give it per pipe section.

    ..math::
        annual loss [%] = 0.17 * linear heat density^-0.5

    Args:
        linear_heat_density : kWh/m

    Returns:
        Estimated annual heat loss in % / year

    References:
    [1] Status Report on District Heating Systems in IEA Countries IEA Bioenergy Task 32,
    Swiss Federal Office of Energy, and Verenum, Zürich 2014 ISBN 3-908705-28-2
    """
    annual_heat_loss_percentage = 0.17 * np.power(average_linear_heat_density, -0.5)
    return annual_heat_loss_percentage


def heat_loss_by_length_from_percent(distance, heat, loss_fraction_km=0.04):
    """
    Estimate heat loss from pipes based on a percentage loss per km

    Args:
        distance: in meters. Scalar of section length or ndarray-like of lengths
        heat: heat flow (usually kWh) of same type/shape as distance
        loss_fraction_km: fraction loss per km, defaults to 4% (0.04)

    References:
    [1] Julien F. Marquant, Ralph Evins, L. Andrew Bollinger, Jan Carmeliet,
    A holarchic approach for multi-scale distributed energy system optimisation,
    Applied Energy, Volume 208, 2017, Pages 935-953, ISSN 0306-2619, https://doi.org/10.1016/j.apenergy.2017.09.057.

    [2] Eugenia D. Mehleri, Haralambos Sarimveis, Nikolaos C. Markatos, Lazaros G. Papageorgiou,
    A mathematical programming approach for optimal design of distributed energy systems at the neighbourhood level,
    Energy, Volume 44, Issue 1, 2012, Pages 96-104, ISSN 0360-5442, https://doi.org/10.1016/j.energy.2012.02.009.

    """
    return loss_fraction_km * heat * distance / 1000


def specific_heat_loss_per_delta_t(r_P, r_JP, lambda_im, lambda_gr, h_cov, a):
    r"""Calculate the pipe heat loss per unit length and per unit temperature difference
    between mean fluid temperature and mean ground temperature

    Implement section 7.1.3 of district heating handbook to calculate the specific heat loss
    from parallel pipes as the loss per unit length AND per unit temperature difference:

    .. math::
        \frac{q_{L}}{\Delta T_{ELP} }

    where

    .. math::
        $\Delta T_{ELP} = \frac{T_{SPLY} + T_{RTN}}{2} - T_{gr}$


    .. math::
        \frac{q_{L}}{\Delta T_{ELP} } = \frac{\displaystyle 4 \pi}{\displaystyle
        \frac{1}{\lambda_{IM}} ln(\frac{r_{JP}}{{r_P}}) + \frac{1}{\lambda_{gr}} ln(\frac{4 (h_{Cov} + r_{JP})}{r_{JP}})  + \frac{1}{\lambda_{gr}} ln( [ (\frac{2(h_{Cov} + r_{JP} )}{a + 2 r_{JP}})^2  +1 ] ^{0.5})}

    Args:
        r_P: pipe nominal radius (approx internal radius)
        r_JP: pipe jacket radius (radius of whole pipe including insulation)
        lambda_im: conductivity of value of pipe insulation material
        lambda_gr: conductivity of ground
        h_cov: soil cover of pipes (to top surface of pipe)
        a: ground spacing between pipe pairs
    """
    return (4 * pi) / (
        (1 / lambda_im) * log(r_JP / r_P)
        + (1 / lambda_gr) * log(4 * (h_cov + r_JP) / r_JP)
        + (1 / lambda_gr) * log((2 * (h_cov + r_JP) / (a + 2 * r_JP)) ** 2 + 1) ** 0.5
    )


def specific_heat_loss(loss_per_delta_t, *, delta_t=None, t_supply=None, t_return=None, t_ground=None):
    """Calculate the pipe heat loss per unit length for given temperature conditions"""
    if delta_t is None:
        if not (t_supply and t_return and t_ground):
            raise ValueError(
                "If delta T is not given, must provide all of supply, return, & ground temperatures"
            )
        delta_t = ((t_supply - t_return) / 2) - t_ground

    return loss_per_delta_t * delta_t


def heat_loss_by_length_from_thermal_model(t_supply, t_return, t_ground):
    raise NotImplementedError
    # # TODO: need to have all the data and/or default values. Also option to estimate numbers e.g. hcov estimate from r_jp
    # loss_per_delta_t = specific_heat_loss_per_delta_t(r_P, r_JP, lambda_im, lambda_gr, h_cov, a)
    # delta_t = ((t_supply - t_return) / 2) - t_ground
    #
    # return loss_per_delta_t * delta_t


def calculate_network_heat_loss(heat_loss_percent, annual_heat_loss_percentage):
    return heat_loss_percent * annual_heat_loss_percentage


def calculate_hourly_network_production(hourly_heat_loss_percentage, hourly_load_curve) -> pd.DataFrame:
    index = pd.date_range("2015-01-01", periods=8760, freq="H")

    hourly_network_production = pd.DataFrame(index=index)
    hourly_network_production["p_hww_kw_lost"] = hourly_heat_loss_percentage * hourly_load_curve
    hourly_network_production["p_hww_kw_production"] = (
        hourly_load_curve + hourly_network_production["p_hww_kw_lost"]
    )

    return hourly_network_production


def pipe_nominal_diameters_from_velocity_table(
    power_total: ArrayLike,
    *,
    temp_going=80.0,
    temp_return=60.0,
    fluid_density=971.81,
    fluid_cp=4.196,
    pipe_velocity_table=None,
):
    """
    Args:
        power_total: kW
        temp_going: degC
        temp_return: degC
        fluid_density: kg/m3
        fluid_cp: kJ/(kg*K)

    Returns:
        nominal dimaeters in mm

    """
    if pipe_velocity_table is None:
        pipe_velocity_table = load_pipe_velocity_table()

    nominal_diameter_table_m = pipe_velocity_table["nominal_diameter__mm"] / 1000
    allowed_velocity = pipe_velocity_table["maximum_allowed_velocity__m_s"]

    volumetric_flow = np.pi * np.power(nominal_diameter_table_m, 2) * allowed_velocity  # [m3/s]
    max_power = volumetric_flow * (fluid_density * fluid_cp * (temp_going - temp_return))  # kW

    # Important to iterate in descending order so that 'smallest wins'
    max_power = max_power.sort_values(ascending=False)

    # Create output and set value to maxiumum DN size
    dn = max_power.index[0]
    nominal_diameters = np.copy(power_total)
    nominal_diameters[np.isfinite(nominal_diameters)] = dn

    for dn, power in max_power.items():
        nominal_diameters[(power_total < power)] = dn / 1000  # default to meters

    # Allow to provide both numpy array and named series/dataarray
    if hasattr(nominal_diameters, "name"):
        nominal_diameters.name = "nominal_diameter__m"
    return nominal_diameters


def pipe_diameter_from_fixed_flow(
    max_power, *, flow_speed=1.0, temp_going=80.0, temp_return=60.0, fluid_density=971.81, fluid_cp=4.196
):
    maximum_flow = max_power / (fluid_density * fluid_cp * (temp_going - temp_return))
    diameter = np.sqrt((maximum_flow * 4) / (np.pi * flow_speed))
    return diameter


@vectorize([float64(float64, float64, float64, float64, float64)])
def darcy_weisbach_friction_f(velocity_m_s, d, density, dynamic_viscosity, roughness):
    """
    Darcy Weisbach friction factor

    Args:
        velocity_m_s: velocity in m/s
        d: Inner Diameter (m)
        reynolds: Reynolds number
        roughness: [m] Roughness of pipes
        f_g: Initial guess

    Returns:

    """
    reynolds = d * velocity_m_s * density / dynamic_viscosity
    if reynolds == 0:
        f_n = 0
    elif 0 < reynolds < 2000:
        f_n = 64 / reynolds
    else:
        error = 1
        f_n = 0
        f_g = 0.1
        while error > 1.0e-5:
            f_n = (1 / (-2 * np.log10(((roughness / d) / 3.7) + (2.51 / (reynolds * f_g**0.5))))) ** 2
            error = abs(f_n - f_g)
            f_g = f_n

    return f_n



def calc_fluid_velocity(id_mm, v_dot_m3_s):
    """
    Calculate the fluid velocity based on the nominal pipe diameter
    and volumetric flow rate

    Args:
        id_mm: pipe inner diameter in millimeters
        v_dot_m3_s: volumetric flow rate m3/s

    Returns:

    """
    return 4 * v_dot_m3_s / (np.pi * (id_mm / 1000) ** 2)


def find_pipe_dn(v_dot_m3_s, nominal_diameter_lookup, dn_offset=0, min_velocity=1.0):
    """
    Selecting the suitable pipes (with minimum diameter in which the flow velocity is above 1 [m/s])

    Pipe Nominal Diameter (DN)

    Args:
        v_dot_m3_s: volumetric flow rate
        nominal_diameter_lookup: lookup table for nominal diameters
        dn_offset: optionally pick a DN offset by this value relative to the optimal value.

    Returns:

    References:
        ISO 6708
    """
    nominal_diameter_lookup = nominal_diameter_lookup.sort_values("dn", ascending=False)
    idx = nominal_diameter_lookup.index[-1]
    # Iterate in descending order while velocity is less than min_velocity
    # As DN decreases, V increases until it reaches at least 1
    for row in nominal_diameter_lookup.itertuples():
        id_mm = row.id_mm
        idx = row.Index
        velocity_m_s = calc_fluid_velocity(id_mm, np.abs(v_dot_m3_s))
        if velocity_m_s > min_velocity:
            idx = idx
            break
        # otherwise go to smallest dn

    idx = max(0, idx + dn_offset)
    return (
        nominal_diameter_lookup["dn"].loc[idx],
        nominal_diameter_lookup["id_mm"].loc[idx],
    )


def find_pipe_dn_vectorised(v_dot_m3_s: ArrayLike, nominal_diameter_lookup, dn_offset=0, min_velocity=1.0):
    """
    Selecting the suitable pipes (with minimum diameter in which the flow velocity is above 1 [m/s])

    This version is designed to work on numpy arrays

    Pipe Nominal Diameter (DN)

    Args:
        v_dot_m3_s: volumetric flow rate
        nominal_diameter_lookup: lookup table for nominal diameters
        dn_offset: optionally pick a DN offset by this value relative to the optimal value.

    Returns:

    References:
        ISO 6708
    """
    # init output to be like input (might be sparse one day although support is limited)
    dns = np.copy(v_dot_m3_s)
    id_mms = np.copy(v_dot_m3_s)
    nominal_diameter_lookup = nominal_diameter_lookup.sort_values("dn", ascending=False)
    # Start with largest DN
    dns[:] = nominal_diameter_lookup["dn"].iloc[0]
    id_mms[:] = nominal_diameter_lookup["id_mm"].iloc[0]

    # Iterate in descending order while velocity is less than one
    for row in nominal_diameter_lookup.itertuples():
        id_mm = row.id_mm
        velocity_m_s = calc_fluid_velocity(id_mm, np.abs(v_dot_m3_s))

        # For the locs where the velocity for this DN are still under max_v, set
        # the DN of the previous loop,plus the offset.
        # This will continue until row velocity is > max_v, in
        # which case the previously set DN will be kept for those items
        idx = row.Index
        idx = max(0, idx + dn_offset - 1)
        idx = min(idx, len(nominal_diameter_lookup) - 1)
        dns[(velocity_m_s < min_velocity)] = nominal_diameter_lookup["dn"].loc[idx]
        id_mms[(velocity_m_s < min_velocity)] = nominal_diameter_lookup["id_mm"].loc[idx]

        # dns[(velocity_m_s >= 1) & (velocity_m_s <= 4 )] = nominal_diameter_lookup["dn"].loc[idx]
        # id_mms[(velocity_m_s >= 1) & (velocity_m_s <= 4 )] = nominal_diameter_lookup["id_mm"].loc[idx]

    return dns, id_mms


def calculate_volumetric_flow_rate(fluid, power_kw):
    m_dot_kg_s = power_kw / (fluid.heat_capacity * fluid.delta_t / 1000)
    return m_dot_kg_s / fluid.density


def calc_flow_properties(
    v_dot_m3_s, length, inner_diameter, *, fluid_density, fluid_viscosity, pipe_roughness=0.0015
):
    """
    Calculate flow properties for a pipe section based on the required volumetric flow rate,
    the properties of the fluid, and the pipe specification table.

    Args:
        v_dot_m3_s: required volumetric flow rate [m3/s]
        length: pipe length in meters
        inner_diameter
        fluid_density:
        fluid_viscosity:
        pipe_roughness: DW roughness factor for pipe section, defaults to value for PVC

    Returns:
        velocity_m_s
        f_dw
        r_dw
         h_f_dw_m
        pressure_loss_pa_m
    """
    velocity_m_s = calc_fluid_velocity(inner_diameter, v_dot_m3_s)
    f_dw = darcy_weisbach_friction_f(
        velocity_m_s, inner_diameter / 1000, fluid_density, fluid_viscosity, pipe_roughness
    )
    r_dw = 8 * f_dw * length / (9.8067 * (inner_diameter / 1000) ** 5 * np.pi**2)
    h_f_dw_m = r_dw * v_dot_m3_s**2
    pressure_loss_pa_m = f_dw * (fluid_density / 2) * ((velocity_m_s**2) / (inner_diameter / 1000))
    # reynolds = velocity_m_s * (ID_mm / 1000) * ρ / μ

    return {
        "velocity_m_s": velocity_m_s,
        # "reynolds": reynolds,
        "f_dw": f_dw,
        "r_dw": r_dw,
        "h_f_dw_m": h_f_dw_m,
        "pressure_loss_pa_m": pressure_loss_pa_m,
    }


def estimate_pipe_diameter_ZUBERI(heat_linear_density):
    """
    Estimate the thermal network pipe diameter as a function of the
    heat demand per unit length in kWh/m. This is in reality a function
    of the flow rate needed, climate, etc.

    The function gives an empirical approximation derived from case
    studies. Based on ZUBERI paper.[1]_

    Args:
        heat_linear_density : float or array of float
                            Heat demand per unit length in kWh/m

    Returns:
        float or array of float
            Estimated pipe diameter needed for the thermal capacity

    References:
    .. [1] Zuberi MJS, Narula K, Klinke S, Chambers J, Streicher KN, Patel MK.
    Potential and costs of decentralized heat pumps and thermal networks in Swiss residential areas.
    Int J Energy Res 2021:er.6801. doi:10.1002/er.6801.
    """
    # NOTE: accepts heat density in kWh/m, coefficients are defined for GJ
    return 0.0486 * np.log(heat_linear_density * 0.0036) + 0.0007


ZUBERI_COSTS = {
    "inner_city": {"c_1": 315, "c_2": 2225},
    "outer_city": {"c_1": 233, "c_2": 1879},
    "rural": {"c_1": 233, "c_2": 1879},
    "exchange_factor": 1.5,
    "currency": "CHF",
}


def pipe_cost_model_ZUBERI(c_1: float, c_2: float, diameter=None, heat_linear_density=None):
    """
    Pipe costs are estimated as a function of pipe diameter,
    which is calculated as a function of the heat demand per unit length
    Given the linear heat demand density, return the cost per unit length.

    Based on Jibran ZUBERI paper.[1]_

    Args:
        c_1 : Constant cost in CHF/m
        c_2 : Diameter dependant cost in CHF/m2
        diameter
        heat_linear_density : heat demand per unit length in kWh/m
    Returns:
        Estimated pipe cost permeter needed for the thermal capacity

    References:
    .. [1] Zuberi MJS, Narula K, Klinke S, Chambers J, Streicher KN, Patel MK.
    Potential and costs of decentralized heat pumps and thermal networks in Swiss residential areas.
    Int J Energy Res 2021:er.6801. doi:10.1002/er.6801.
    """
    if diameter is None:
        diameter = estimate_pipe_diameter_ZUBERI(heat_linear_density)
        diameter[diameter <= 0] = np.nan
    cost_per_m = c_1 + (c_2 * diameter)
    return cost_per_m


# UNITS ??
# TODO: use Pint units everywhere here
# TODO: load these costs from data files
# TODO: consistent currency
SVENSK_COSTS = {
    "inner_city": {"c_1": 2.4165, "c_2": 341.63},
    "outer_city": {"c_1": 2.0511, "c_2": 257.47},
    "rural": {"c_1": 1.6299, "c_2": 178.8},
    "new_construction": {"c_1": 1.5412, "c_2": 74.439},
    "exchange_factor": 1.1,
    "currency": "EUR",
}

PERSSON_COSTS = {
    "inner_city": {"c_1": 286, "c_2": 2022},
    "outer_city": {"c_1": 214, "c_2": 1725},
    "rural": {"c_1": 151, "c_2": 1378},
    "exchange_factor": 1.1,
    "currency": "EUR",
}

# units:
# _var for const per unit diameter in CHF/m2, for diameters in meters
# _fix for const per meter, in CHF/m
NUSSBAUMER_COSTS = {
    "urban": {"c_trench_var": 1053.6, "c_trench_fixed": 140.75, "c_pipe_var": 4788.2, "c_pipe_fixed": 66.981},
    "rural": {"c_trench_var": 575.6, "c_trench_fixed": 72.21, "c_pipe_var": 4788.2, "c_pipe_fixed": 66.981},
    "exchange_factor": 1.0,
    "currency": "CHF",  # currency after exchange factor applied
}


def pipe_cost_model_nussbaumer(
    diameter,
    *,
    c_trench_var=None,
    c_trench_fixed=None,
    c_pipe_var=None,
    c_pipe_fixed=None,
    location_type="inner_city",
    exchange_factor=1,
    params=None,
):
    """
    Cost in CHF/m calculated as

    linear formula : y = ax + b
    cost = e * (a * d + b)

    where e is an exchange factor (e.g. currency conversion between euros to chf),

    Based on T. Nussbaumer, S. Thalmann [1]

    Args:
        diameter:       Diameter in m
        c_pipe_fixed
        c_trench_fixed
        c_pipe_var
        c_trench_var

        location_type:   inner city / outer city
        params
        exchange_factor

    Returns:
        Costs

    References:
        [1] T. Nussbaumer, S. Thalmann (2016) Influence of system design on heat distribution costs
        in district heating
    """
    if params is None:
        params = NUSSBAUMER_COSTS
    exchange_factor = params.get("exchange_factor", exchange_factor)

    c_trench_var = params[location_type]["c_trench_var"] if c_trench_var is None else c_trench_var
    c_trench_fixed = params[location_type]["c_trench_fix"] if c_trench_fixed is None else c_trench_fixed
    c_pipe_var = params[location_type]["c_pipe_var"] if c_pipe_var is None else c_pipe_var
    c_pipe_fixed = params[location_type]["c_pipe_fix"] if c_pipe_fixed is None else c_pipe_fixed
    # c_pipe_var = 4.7882
    # c_pipe_fixed = 66.981
    piping_cost = c_pipe_var * diameter + c_pipe_fixed
    trench_cost = c_trench_var * diameter + c_trench_fixed
    cost_per_m = exchange_factor * (piping_cost + trench_cost)

    return cost_per_m


def pipe_cost_model_basic(diameter, c_1=None, c_2=None, location_type=None, params=None):
    """
        Cost in CHF/m calculated as

    cost = e * (c_1 + c_2 * d)

    where e is an exchange factor (e.g. currency conversion),
    c_1 is constant in CHF/m
    c_2is constant in CHF/m2 (cost part dependant of pipe sizing)

    Based on Urban Persson & Sven Werner paper [1]
    Args:
        diameter:  Diameter in m
        c_1:
        c_2:
        location_type: inner city / outer city / park areas
        params:

    Returns:

    References:
        [1] Persson U., Werner S. (2011) Heat distribution and the future competitiveness of
        district heating, Applied Energy, Vol. 88, pp 568-576
    """
    if location_type is not None and params is not None and location_type in params:
        c_1 = params[location_type]["c_1"]
        c_2 = params[location_type]["c_1"]
    if c_1 is None or c_2 is None:
        raise ValueError("Must supply c_1 and c_2 cost parameters, or location type and parameters dict")

    cost_per_m = c_1 + (c_2 * diameter)

    return cost_per_m


def pipe_cost_model_svensk(diameter, c_1=None, c_2=None, location_type=None, params=None):
    """
        Pipe costs are estimated as a function of pipe diameter,
    and localisation (inner city, outer city, park areas, construction site areas)
    return the cost per unit length.

    Cost contains: raccordement, matériau, tuyauterie, travaux de terrassement,
    projet et contrôle

    Based on Svensk Fjärrvärme AB │ Januari 2007 [1]
    The regressions were calculated graphically and can be found
    in the Excel analysis_modules/load_curves/cost_by_pipe_diameter_Svensk_Fjärrvärme.xlsx

    Args:
        diameter:
        c_1:
        c_2:
        location_type:
        params:

    Returns:

    References:
        [1] Svensk Fjärrvärme AB │ Januari 2007
    """
    if location_type is not None and params is not None:
        c_1 = params[location_type]["c_1"]
        c_2 = params[location_type]["c_2"]
    if c_1 is None or c_2 is None:
        raise ValueError("Must supply c_1 and c_2 cost parameters, or location type and parameters dict")

    cost_per_m = c_1 + (c_2 * diameter * 1000)
    return cost_per_m


def calc_pump_power(v_dot_m3_s, h_pump_m, fluid_density, efficiency=0.85):
    specific_pump_force = fluid_density * 9.8067 * h_pump_m  # Newton/m2

    # Power of the Pumps
    return (v_dot_m3_s * specific_pump_force / 1000) / efficiency
