from collections.abc import Iterable
from typing import overload

import geopandas as gpd
import numpy as np
import pandas as pd
import pyproj
import shapely
import shapely.geometry
from geopandas import GeoDataFrame, GeoSeries
from numpy.typing import ArrayLike
from shapely.geometry import Polygon
from shapely.ops import transform
from sklearn.cluster import HDBSCAN


def hdbscan_cluster(points: np.ndarray, clustering_distance: float):
    """

    Args:
        points
        clustering_distance

    Returns:

    """

    # setup cluster params
    clustering = HDBSCAN(
        cluster_selection_epsilon=clustering_distance,
        algorithm="boruvka_kdtree",
    )

    clustering.fit(points)

    return clustering.labels_


@overload
def groupby_clusters(points: GeoDataFrame, clustering_distance: float) -> Iterable[tuple[int, GeoDataFrame]]:
    ...


@overload
def groupby_clusters(points: GeoSeries, clustering_distance: float) -> Iterable[tuple[int, GeoSeries]]:
    ...


def groupby_clusters(
    points: GeoDataFrame | GeoSeries | ArrayLike, clustering_distance: float
) -> Iterable[tuple[int, GeoDataFrame | GeoSeries | ArrayLike]]:
    if isinstance(points, GeoDataFrame):
        _is_df = True
        points_xy = np.stack([points.geometry.x, points.geometry.y]).T
    elif isinstance(points, pd.DataFrame):
        _is_df = True
        points_xy = points.to_numpy()  # assume is a dataframe with columns corresponding to points
    elif isinstance(points, GeoSeries):
        _is_df = False
        points_xy = np.stack([points.x, points.y]).T
    else:
        _is_df = False
        points_xy = points

    model = HDBSCAN(cluster_selection_epsilon=clustering_distance).fit(points_xy)

    cluster_ids = np.unique(model.labels_)

    for cid in cluster_ids:
        if cid == -1:
            # skip non-assigned points
            continue
        cluster_selection = model.labels_ == cid
        if hasattr(points, "index"):
            cluster_points = points.loc[cluster_selection]
        else:
            cluster_points = points[cluster_selection, :]
        yield cid, cluster_points


def point_clusters_with_hulls(
    points: GeoDataFrame | GeoSeries, clustering_distance=100, distance_crs=None, hull_tightness=0.5
) -> tuple[list[GeoDataFrame | GeoSeries], list[Polygon]]:
    """

    Args:
        points:
        clustering_distance:
        distance_crs: CRS in which clustering distance is given, otherwise clustering_distance must be in the
          same CRS as the input points.
        hull_tightness: used as ratio argument for concave_hull, determines how approximate the boundary hull is.
         if this is too low, you get very irregular cluster hulls that are not very intuitive.

    Returns:

    """
    clusters_data = []
    cluster_hulls = []
    if distance_crs is not None:
        points_proj: GeoSeries = points.geometry.to_crs(distance_crs)
    else:
        points_proj = points.geometry

    for cid, cluster_points in groupby_clusters(points_proj, clustering_distance=clustering_distance):
        if len(cluster_points) >= 3:
            # simplest way to get results in same CRS as the input with same data.
            cluster_data = points.loc[cluster_points.index]
            # Ensure we preserve geo-ness
            cluster_data = type(points)(cluster_data)
            hull = generate_boundary(cluster_data.geometry, buffer=0.00005, ratio=hull_tightness)

            clusters_data.append(cluster_data)
            cluster_hulls.append(hull)

    return clusters_data, cluster_hulls


def generate_boundary(
    features: gpd.GeoDataFrame | gpd.GeoSeries,
    *,
    buffer=0.0001,
    crs: str | int | None = None,
    ratio=0.0,
    how="concave_hull",
) -> shapely.Polygon:
    """

    Args:
        features:
        crs: crs of input features
        buffer: in meters

    Returns:

    """

    if features.crs is not None:
        crs = features.crs

    if len(features) >= 3:
        if how == "concave_hull":
            bounding_polygon = (
                gpd.GeoSeries([features.geometry.unary_union])
                .concave_hull(ratio=ratio)
                .buffer(buffer)
                .simplify(buffer / 2)
                .iloc[0]
            )
        else:
            bounding_polygon = (
                gpd.GeoSeries([features.geometry.unary_union])
                .convex_hull()
                .buffer(buffer)
                .simplify(buffer / 2)
                .iloc[0]
            )
    else:
        bounding_polygon = features.geometry.to_crs(2056).unary_union.centroid.buffer(buffer)
    project = pyproj.Transformer.from_crs(crs, "EPSG:4326", always_xy=True).transform
    out = transform(project, bounding_polygon)
    out = shapely.geometry.polygon.orient(out)
    return out


def highest_consumption_sample(fraction, cluster_data, consumption_col="heat_total"):
    """
    Sort the consumptions in descending order and select the
    `fraction` of the `cluster_data` with the highest
    consumption.

    Args:
        fraction
        cluster_data
        consumption_col

    Returns:

    """
    cut = int(len(cluster_data.egid) * (1 - fraction))

    sel_egids = cluster_data[[consumption_col]].sortby(consumption_col).isel(egid=slice(cut, None)).egid

    cluster_sample = cluster_data.sel(egid=sel_egids.data)

    return cluster_sample

