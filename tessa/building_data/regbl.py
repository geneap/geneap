from collections.abc import Iterable
from dataclasses import dataclass, field
from importlib.abc import Traversable
from importlib.resources import files
from typing import Union

import geopandas as gpd
import pandas as pd
import pandera as pa
import shapely.geometry
from pandera.typing import DataFrame, Index, Series
from pandera.typing.geopandas import GeoDataFrame, GeoSeries
from psycopg.types.json import Json
from sqlalchemy import Connection, Engine, text

from tessa.utils.pandera_extras import ensure_columns_for_schema

from .clusters import load_building_points_in_polygon
from .common import BuildingDataSource
from .models import Building


class RegblData(pa.DataFrameModel):
    egid: Index[int] = pa.Field(check_name=True, coerce=True)
    canton: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)
    gdenr: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    const_class: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)
    cat_sia: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    const_period: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)
    is_heated: Series[bool] = pa.Field(nullable=True, coerce=True)
    total_surface: Series[float] = pa.Field(nullable=True, coerce=True)
    total_surface_main_residence: Series[float] = pa.Field(nullable=True, coerce=True)
    source_sh: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    source_dhw: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_sh: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_dhw: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    source_sh1: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    source_dhw1: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_sh1: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_dhw1: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    source_sh2: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    source_dhw2: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_sh2: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    gen_dhw2: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    stationid: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)
    point: GeoSeries = pa.Field(nullable=True, coerce=True)


class _HeatSource(pa.DataFrameModel):
    thermal_generator: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)
    fuel: Series[pd.StringDtype] = pa.Field(nullable=True, coerce=True)


class Genh(_HeatSource):
    genh: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    genh_fr: Series[pd.StringDtype] = pa.Field(coerce=True)


class Gwaerzh(_HeatSource):
    gwaerzh: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    gwaerzh_fr: Series[pd.StringDtype] = pa.Field(coerce=True)


class Gwaerzw(_HeatSource):
    gwaerzw: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    gwaerzw_fr: Series[pd.StringDtype] = pa.Field(coerce=True)


class GenhGwaerzh(_HeatSource):
    genh: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    genh_fr: Series[pd.StringDtype] = pa.Field(coerce=True)
    gwaerzh: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    gwaerzh_fr: Series[pd.StringDtype] = pa.Field(coerce=True)


class GenhGwaerzw(_HeatSource):
    genh: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    genh_fr: Series[pd.StringDtype] = pa.Field(coerce=True)
    gwaerzw: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    gwaerzw_fr: Series[pd.StringDtype] = pa.Field(coerce=True)


class GklassSia(pa.DataFrameModel):
    code: Series[pd.StringDtype] = pa.Field(coerce=True)
    merkmal: Series[pd.StringDtype] = pa.Field(coerce=True)
    name_it: Series[pd.StringDtype] = pa.Field(coerce=True)
    langtext_it: Series[pd.StringDtype] = pa.Field(coerce=True)
    name_fr: Series[pd.StringDtype] = pa.Field(coerce=True)
    langtext_fr: Series[pd.StringDtype] = pa.Field(coerce=True)
    kurztext_de: Series[pd.StringDtype] = pa.Field(coerce=True)
    langtext_de: Series[pd.StringDtype] = pa.Field(coerce=True)
    name_en: Series[pd.StringDtype] = pa.Field(coerce=True)
    cat_sia: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    heated: Series[pd.BooleanDtype] = pa.Field(coerce=True)
    notes: Series[pd.StringDtype] = pa.Field(coerce=True)


@dataclass(kw_only=True)
class RegblDataSource(BuildingDataSource):
    """
    Encapsulates regbl data source from DB
    Idea is to work towards a utils interface like load_building_data() that works with other
    data sources esp geoimpact and even abroad eventually. So we can instead require Instance(BuildingDataSource)
    and pass in a regbl data instance.
    """

    engine: Engine
    genh: DataFrame[Genh] = field(init=False)
    gwaerzh: DataFrame[Gwaerzh] = field(init=False)
    genh_gwaerzh: DataFrame[GenhGwaerzh] = field(init=False)
    gwaerzw: DataFrame[Gwaerzw] = field(init=False)
    genh_gwaerzw: DataFrame[GenhGwaerzw] = field(init=False)

    name_building_type = {
        -1: "unheated",
        1: "collective housing",
        2: "individual housing",
        3: "administration",
        4: "schools",
        5: "commerce",
        6: "restaurants",
        7: "gathering places",
        8: "hospitals",
        9: "industry",
        10: "warehouses",
        11: "sports facilities",
        12: "indoor swimming pools",
    }

    def __post_init__(self):
        (
            self.genh,
            self.gwaerzh,
            self.genh_gwaerzh,
            self.gwaerzw,
            self.genh_gwaerzw,
        ) = load_regbl_heat_source_matching_csv()

    def load(self, building_ids: Iterable[int]) -> GeoDataFrame[Building]:
        """
        NOTE: request is only guaranteed to return table with egids that exist in the provided list
        so not guaranteed that len(egid_list) == len(buildings)

        Args:
            building_ids: Iterable
                building ids to fetch (EGIDs for RegBL)

        Returns:
            building data table
        """
        with self.engine.connect() as con:
            buildings = request_regbl_2021_data(building_ids, con)

        return self.transform_regbl_data(buildings)

    def transform_regbl_data(self, buildings):
        buildings = set_generator_and_fuel(
            buildings, self.gwaerzh, self.genh, self.genh_gwaerzh, self.gwaerzw, self.genh_gwaerzw
        )
        drop_cols = [
            "source_sh",
            "source_dhw",
            "gen_sh",
            "gen_dhw",
            "source_sh1",
            "source_dhw1",
            "gen_sh1",
            "gen_dhw1",
            "source_sh2",
            "source_dhw2",
            "gen_sh2",
            "gen_dhw2",
        ]
        buildings = buildings.drop(columns=drop_cols)
        return GeoDataFrame[Building](buildings)

    def load_points_in_region(self, polygon):
        with self.engine.connect() as con:
            return load_building_points_in_polygon(polygon, con)


@dataclass(kw_only=True)
class RegBlTableSource(RegblDataSource):
    buildings: DataFrame | GeoDataFrame | None = None
    engine: Engine = None

    def __post_init__(self):
        buildings = self.buildings.copy()
        if not isinstance(buildings, gpd.GeoDataFrame) and "lon" in buildings and "lat" in buildings:
            buildings["point"] = gpd.points_from_xy(buildings.lon, buildings.lat, crs="EPSG:4326")
        (
            self.genh,
            self.gwaerzh,
            self.genh_gwaerzh,
            self.gwaerzw,
            self.genh_gwaerzw,
        ) = load_regbl_heat_source_matching_csv()
        buildings = ensure_columns_for_schema(RegblData, buildings)
        self.buildings = buildings

    def load(self, building_ids) -> GeoDataFrame[Building]:
        """
        NOTE: request is only guaranteed to return table with egids that exist in the provided list
        so not guaranteed that len(egid_list) == len(buildings)

        Args:
            building_ids: building ids to fetch (EGIDs for RegBL)

        Returns:

        """
        buildings = self.buildings.loc[building_ids]
        return self.transform_regbl_data(buildings)


def transform_regbl_format_table(buildings: GeoDataFrame[RegblData]):
    buildings.columns = [c.lower() for c in buildings.columns]
    return RegBlTableSource(buildings=buildings).load(buildings.index)


def request_regbl_2021_data(egid_list: Union[Iterable[int], int], con: Connection) -> GeoDataFrame[RegblData]:
    """
    Function to make a secure SQL query to the edleweiss server
    to get all the data needed to calculate the annual heat demand and CO2 emissions.

    Args:
        egid_list : list of egids to fetch data for
        con :  database connectable

    Returns:
        Data extracted from the database for the egids.
    """
    try:
        egid_list = tuple(egid_list)
    except TypeError:
        egid_list = tuple([egid_list])

    buildings = gpd.read_postgis(
        text(
            """Select
       regbl_2021.egid_summary.egid::INTEGER,
       buildings.gdekt as canton,
       buildings.ggdenr as gdenr,
       coalesce(reg_bl_period_to_dataren.datarenperiod, 'Inconnu') as const_period,
       bld_class_code_lookup.kurztext_fr as const_class,
       reg_bl_class_to_sia_cat.cat_sia::INTEGER AS cat_sia,
       reg_bl_class_to_sia_cat.heated::bool as is_heated,
       total_surface_estimate as total_surface,
       main_residence_area as total_surface_main_residence,
       era,
       coalesce(genh1, genh2) as source_sh, -- Source d’énergie / de chaleur du chauffage
       coalesce(genw1, genw2) as source_dhw,  -- Source d’énergie / de chaleur eau chaude
       genh1 as source_sh1, -- Source d’énergie / de chaleur du chauffage
       genw1 as source_dhw1,  -- Source d’énergie / de chaleur eau chaude
       genh2 as source_sh2, -- Source d’énergie / de chaleur du chauffage
       genw2 as source_dhw2,  -- Source d’énergie / de chaleur eau chaude
       coalesce(gwaerzh1, gwaerzh2) as gen_sh, -- Générateur de chaleur pour le chauffage
       coalesce(gwaerzw1, gwaerzw2) as gen_dhw,  -- Générateur de chaleur eau chaude
       gwaerzh1 as gen_sh1, -- Générateur de chaleur pour le chauffage
       GWAERZW1 as gen_dhw1,  -- Générateur de chaleur eau chaude
       gwaerzh2 as gen_sh2, -- Générateur de chaleur pour le chauffage
       GWAERZW2 as gen_dhw2,  -- Générateur de chaleur eau chaude
       stationid,
       st_setsrid(point_latlon, 4326) as point

       from regbl_2021.egid_summary
           left join regbl_2021.existing_egid_points using (egid)
           left join regbl_2021.buildings using (egid)
           left join regbl_2021.gwr_codes as bld_age_code_lookup on buildings.gbaup = bld_age_code_lookup.code
           left join regbl_2021.gwr_codes as bld_class_code_lookup on buildings.gklas = bld_class_code_lookup.code
           left join prj_geneap.reg_bl_period_to_dataren
--                code 969 is 'unknown' in the gwr codes
               on coalesce(buildings.gbaup, 969) = reg_bl_period_to_dataren.gwr_code
           left join prj_geneap.reg_bl_class_to_sia_cat
               on coalesce(buildings.gklas, 0) = reg_bl_class_to_sia_cat.buildingclass

            cross join lateral (
              SELECT stationid
              FROM climate.clim_sia_stations
              ORDER BY st_distance(clim_sia_stations.point, egid_summary.point)
              LIMIT 1
            ) climate_stations
        where regbl_2021.egid_summary.egid=ANY(:egid_list)
        """
        ),
        con,
        params={"egid_list": list(egid_list)},
        geom_col="point",
        crs=4326,
    )
    buildings["is_cooled"] = None

    buildings = buildings.set_index("egid")
    buildings.index = buildings.index.astype(int)
    return GeoDataFrame[RegblData](buildings)


def set_generator_and_fuel(
    buildings: GeoDataFrame[RegblData],
    gwaerzh: DataFrame[Gwaerzh],
    genh: DataFrame[Genh],
    genh_gwaerzh: DataFrame[GenhGwaerzh],
    gwaerzw: DataFrame[Gwaerzw],
    genh_gwaerzw: DataFrame[GenhGwaerzw],
):
    generator_and_fuel_sh = get_generator_and_fuel_sh(buildings, gwaerzh, genh, genh_gwaerzh)

    generator_and_fuel_dhw = get_generator_and_fuel_dhw(buildings, gwaerzw, genh, genh_gwaerzw)

    # fill missing DHW source with SH source (no overwrite existing)
    generator_and_fuel_dhw.update(generator_and_fuel_sh, overwrite=False)

    # fill missing SH source with DHW
    generator_and_fuel_sh.update(generator_and_fuel_dhw, overwrite=False)

    buildings = buildings.assign(
        thermal_generator_sh=generator_and_fuel_sh["thermal_generator"],
        fuel_sh=generator_and_fuel_sh["fuel"],
        thermal_generator_dhw=generator_and_fuel_dhw["thermal_generator"],
        fuel_dhw=generator_and_fuel_dhw["fuel"],
    )
    return buildings


@pa.check_types
def get_generator_and_fuel_sh(
    buildings, gwaerzh: DataFrame[Gwaerzh], genh: DataFrame[Genh], genh_gwaerzh: DataFrame[GenhGwaerzh]
) -> DataFrame[_HeatSource]:
    """
    Guess the thermal heat source and the fuel for RegBL buildings.

    RegBL defines columns genh and gwaerzh that don't map cleanly onto a boiler type
    and fuel source (gwaerzh has some boiler type info, genh mixes fuel infor and boiler info).
    Therefore we use the manual mapping tables coded on gwaerzh, genh individually and
    (gwaerzh, gen) together and use a heuristic to fill on
    1. genh
    2. gwaerzh
    3. (gwaerzh, gen) as most specific

    Args:
        buildings:
        gwaerzh:
        genh:
        genh_gwaerzh:

    Returns:

    """
    index = buildings.index
    match_on_generator: DataFrame = (
        buildings.reset_index()[["egid", "gen_sh"]]
        .merge(
            gwaerzh[["gwaerzh", "thermal_generator", "fuel"]],
            how="left",
            left_on=["gen_sh"],
            right_on=["gwaerzh"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )

    match_on_source: DataFrame = (
        buildings.reset_index()[["egid", "source_sh"]]
        .merge(
            genh[["genh", "thermal_generator", "fuel"]],
            how="left",
            left_on=["source_sh"],
            right_on=["genh"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )

    match_on_source_and_generator: DataFrame = (
        buildings.reset_index()[["egid", "source_sh", "gen_sh"]]
        .merge(
            genh_gwaerzh[["genh", "gwaerzh", "thermal_generator", "fuel"]],
            how="left",
            left_on=["source_sh", "gen_sh"],
            right_on=["genh", "gwaerzh"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )

    # match on generator gives worst results, so update with increasingly specific match
    # IMPORTANT: exception to this is if the match gives 'unknown', then keep the 'preferred' one
    generator_and_fuel_sh = match_on_generator.copy()
    generator_and_fuel_sh.update(match_on_source, overwrite=True)
    generator_and_fuel_sh.update(match_on_source_and_generator, overwrite=True)
    generator_and_fuel_sh.index = index
    return DataFrame[_HeatSource](generator_and_fuel_sh)


@pa.check_types
def get_generator_and_fuel_dhw(
    buildings, gwaerzw: DataFrame[Gwaerzw], genh: DataFrame[Genh], genh_gwaerzw: DataFrame[GenhGwaerzw]
) -> DataFrame[_HeatSource]:
    index = buildings.index
    match_on_generator = (
        buildings.reset_index()[["egid", "gen_dhw"]]
        .merge(
            gwaerzw[["gwaerzw", "thermal_generator", "fuel"]],
            how="left",
            left_on=["gen_dhw"],
            right_on=["gwaerzw"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )

    match_on_source = (
        buildings.reset_index()[["egid", "source_dhw"]]
        .merge(
            genh[["genh", "thermal_generator", "fuel"]],
            how="left",
            left_on=["source_dhw"],
            right_on=["genh"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )

    match_on_source_and_generator = (
        buildings.reset_index()[["egid", "source_dhw", "gen_dhw"]]
        .merge(
            genh_gwaerzw[["genh", "gwaerzw", "thermal_generator", "fuel"]],
            how="left",
            left_on=["source_dhw", "gen_dhw"],
            right_on=["genh", "gwaerzw"],
            validate="many_to_one",
        )[["egid", "thermal_generator", "fuel"]]
        .set_index("egid")
    )
    generator_and_fuel_dhw = match_on_generator.copy()
    generator_and_fuel_dhw.update(match_on_source, overwrite=True)
    generator_and_fuel_dhw.update(match_on_source_and_generator, overwrite=True)
    generator_and_fuel_dhw.index = index

    return DataFrame[_HeatSource](generator_and_fuel_dhw)


# def load_regbl_heat_source_matching():
#     """load manual match table for regbl data.
#     """
#     from . import data
#
#     with as_file(files(data).joinpath(Path('regbl_heating_systems.ods'))) as data_file:
#         table_file = pd.ExcelFile(data_file)
#         dfs = {}
#         for sheet in table_file.sheet_names:
#             # Parse data from each worksheet as a Pandas DataFrame
#             df = table_file.parse(sheet)
#
#             # And append it to the list
#             dfs[sheet] = df
#
#     genh = DataFrame[Genh](dfs['genh'])
#     gwaerzh = DataFrame[Gwaerzh](dfs['gwaerzh'])
#     genh_gwaerzh = DataFrame[GenhGwaerzh](dfs['genh-gwaerzh'])
#
#     gwaerzw = DataFrame[Gwaerzw](dfs['gwaerzw'])
#     genh_gwaerzw = DataFrame[GenhGwaerzw](dfs['genh-gwaerzw'])
#     return gwaerzh, genh, genh_gwaerzh, gwaerzw, genh_gwaerzw


def load_regbl_heat_source_matching_csv():
    """load manual match table for regbl data.
    TODO have this in the DB instead of spreadsheet file
    """
    from . import data

    based_dir: Traversable = files(data)
    genh = DataFrame[Genh](pd.read_csv(based_dir.joinpath("genh.csv")))
    gwaerzh = DataFrame[Gwaerzh](pd.read_csv(based_dir.joinpath("gwaerzh.csv")))
    genh_gwaerzh = DataFrame[GenhGwaerzh](pd.read_csv(based_dir.joinpath("genh-gwaerzh.csv")))
    gwaerzw = DataFrame[Gwaerzw](pd.read_csv(based_dir.joinpath("gwaerzw.csv")))
    genh_gwaerzw = DataFrame[GenhGwaerzw](pd.read_csv(based_dir.joinpath("genh-gwaerzw.csv")))

    return genh, gwaerzh, genh_gwaerzh, gwaerzw, genh_gwaerzw


def load_regbl_sia_category_matching():
    """load manual match table for regbl data.
    TODO have this in the DB instead of spreadsheet file
    """
    from . import data

    based_dir: Traversable = files(data)
    return DataFrame[GklassSia](pd.read_csv(based_dir.joinpath("regbl_gklass_to_sia_category.csv")))


class GeoimpactDataSource(RegblDataSource):
    """
    Geoimpact local data (in the database rather than remove fetch) that shares
    some parameters with the standard redbl data source (e.g. efficiencies and co2
    intensities)
    """

    def load(self, building_ids: Iterable):
        """
        NOTE: request is only guaranteed to return table with egids that exist in the provided list
        so not guaranteed that len(egid_list) == len(buildings)

        Parameters
        ----------
        building_ids
            building ids to fetch (EGIDs for RegBL)

        Returns
        -------
        DataFrame: buidling data table
        """
        with self.engine.connect() as con:
            return request_geoimpact_data(building_ids, con)

    def load_points_in_region(self, polygon):
        return load_geoimpact_building_points_in_polygon(polygon, self.engine.engine)


def request_geoimpact_data(egid_list: Iterable, con) -> gpd.GeoDataFrame:
    """
    Args:
        egid_list : list of egid to fetch data for
        con :  database connectable

    Returns:
    """

    buildings = gpd.read_postgis(
        text(
            """Select
    building_data_geoimpact.egid,
    canton,
    construction_period as const_period,
    bld_class_code_lookup.kurztext_fr as const_class,
    reg_bl_class_to_sia_cat.cat_sia::INTEGER AS cat_sia,
    reg_bl_class_to_sia_cat.heated as is_heated,

    CASE
        when residential_area_egid is null or building_area is null and volume_egid is not null then volume_egid / 3.0
        when residential_area_egid is not null and residential_area_egid > building_area then residential_area_egid
        when building_area is null and egid_geometries.geom is not null then st_area(egid_geometries.geom)
        else building_area
    END as total_surface,
    residential_area_egid as total_surface_main_residence,
    null era,
    heating1_energysource as source_sh,
    hotwater1_energysource as source_dhw,
    st_setsrid(st_point(long, lat), 4326) as point,
    st_x(st_transform(st_setsrid(st_point(long, lat), 4326), 2056)) as x,
    st_y(st_transform(st_setsrid(st_point(long, lat), 4326), 2056)) as y,
    long as lon,
    lat,
    st_asgeojson(st_transform(egid_geometries.geom, 4326))::json as footprint_geom

    from prj_geneap.building_data_geoimpact
        left join prj_geneap.egid_geometries using (egid)
        left join regbl_2021.gwr_codes as bld_class_code_lookup on building_class = bld_class_code_lookup.code
        left join prj_geneap.reg_bl_class_to_sia_cat
                   on coalesce(building_class, 0) = reg_bl_class_to_sia_cat.buildingclass
        where egid in :egid_list
        """
        ),
        con,
        params={"egid_list": tuple(egid_list)},
        geom_col="point",
        crs=4326,
    )
    buildings["is_cooled"] = None
    # fix some WTFs with dtypes
    check_dtypes = {
        "total_surface": float,
        "total_surface_main_residence": float,
        "era": float,
        "cat_sia": pd.Int64Dtype(),
    }
    for col, enforced_dtype in check_dtypes.items():
        buildings[col] = buildings[col].astype(enforced_dtype)

    return buildings.set_index("egid")


def load_geoimpact_building_points_in_polygon(shape, engine) -> gpd.GeoDataFrame:
    """
    geojson: dict with geojson compatible schema representing a (multi)boundary
    engine: sqlalchemy engine
    """
    cluster_data_sql = """select
    egid,
    st_setsrid(st_point(long, lat), 4326) as point,
    st_x(st_transform(st_setsrid(st_point(long, lat), 4326), 2056)) as x,
    st_y(st_transform(st_setsrid(st_point(long, lat), 4326), 2056)) as y,
    long as lon,
    lat
    from prj_geneap.building_data_geoimpact
        where st_contains(ST_GeomFromGeoJSON(:geojson), st_setsrid(st_point(long, lat), 4326))
    """
    if isinstance(shape, (shapely.geometry.Polygon, shapely.geometry.MultiPolygon)):
        shape = shapely.geometry.mapping(shape)
    with engine.begin() as connection:
        egid_points = gpd.read_postgis(
            text(cluster_data_sql),
            connection,
            params={"geojson": Json(shape["geometry"])},
            geom_col="point",
            crs=4326,
        )

    return egid_points.set_index("egid")
