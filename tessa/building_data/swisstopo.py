from dataclasses import dataclass

import geopandas as gpd
from sqlalchemy import Engine, text


def load_building_polygons_in_region(boundary, connection) -> gpd.GeoDataFrame:
    sql = """SELECT
        shape_area,
        st_force2d(geom) as geom
        FROM swisstopo.tlm_building_footprint
        WHERE st_intersects(st_force2d(geom),
                st_transform(st_setsrid(st_geomfromwkb(:select_region), 4326), 2056)
            )
    """

    with connection.begin():
        bld_footprint = gpd.read_postgis(
            text(sql),
            connection,
            params={"select_region": boundary.wkb},
            geom_col="geom",
            crs=2056,
            # TODO: (r)eplace the default crs by BuildingPolygonDataSource.crs
        )

    return bld_footprint


@dataclass(kw_only=True)
class BuildingPolygonDataSource:
    engine: Engine
    crs: int = 4326

    def load_polygons_in_region(self, region):
        with self.engine.connect() as con:
            return load_building_polygons_in_region(region, con)
