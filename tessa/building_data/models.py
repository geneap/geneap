from typing import Optional

import pandas as pd
import pandera as pa
from pandera.typing import Index, Series
from pandera.typing.geopandas import GeoSeries

from tessa.utils.pandera_extras import pandera_to_pydantic_model
from tessa.utils.pydantic_shapely import Feature, FeatureCollection, Point


class BuildingBase(pa.DataFrameModel):
    egid: Index[int] = pa.Field(check_name=True, description="the building national identifier number")
    is_heated: Series[bool] = pa.Field(
        nullable=True,
        description="whether the building has heating (buildings with no heating are for example garage)",
        coerce=True,
    )
    total_surface: Optional[Series[float]] = pa.Field(
        nullable=True, description="total floor area of the building", coerce=True
    )
    total_surface_main_residence: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="total floor area of the building that is used a a main residence [m2]",
        coerce=True,
    )
    era: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="""Energy Reference Area, the floor area of the building that is heated
        (not including e.g. basement or attic) [m2]""",
        coerce=True,
    )
    era_origin: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the data source for the era", coerce=True
    )
    qh_origin: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the data source for the space heat demand", coerce=True
    )
    qww_origin: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the data source for the hot water demand", coerce=True
    )
    q_c_kwh_m2: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    q_h_kwh_m2: Optional[Series[float]] = pa.Field(
        nullable=True, description="the heating demand per square meter [kWh/m2]", coerce=True
    )
    q_ww_kwh_m2: Optional[Series[float]] = pa.Field(
        nullable=True, description="the warm water demand per square meter [kWh/m2]", coerce=True
    )
    q_hww_kwh_m2: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="the total heat and hot water demand per square meter [kWh/m2]",
        coerce=True,
    )
    q_c_kwh: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    q_h_kwh: Optional[Series[float]] = pa.Field(
        nullable=True, description="the space heat demand total [kWh]", coerce=True
    )
    q_ww_kwh: Optional[Series[float]] = pa.Field(
        nullable=True, description="the warm water demand total [kWh]", coerce=True
    )
    q_hww_kwh: Optional[Series[float]] = pa.Field(
        nullable=True, description="total heat and warm water demand total [kWh]", coerce=True
    )
    heat_sh_efficiency: Optional[Series[float]] = pa.Field(
        nullable=True, description="the efficiency of the heating system [-]", coerce=True
    )
    heat_dhw_efficiency: Optional[Series[float]] = pa.Field(
        nullable=True, description="the efficiency of the warm water system [-]", coerce=True
    )
    q_h_final_kwh: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="the final energy consumption of the heating system (accounting for heat losses) [kWh]",
        coerce=True,
    )
    q_ww_final_kwh: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="the final energy consumption of the warm water system (accounting for heat losses) [kWh]",
        coerce=True,
    )
    q_hww_final_kwh: Optional[Series[float]] = pa.Field(
        nullable=True,
        description="the total final energy consumption for heating and hot water accounting for heat losses [kWh]",
        coerce=True,
    )
    co2eq_h_kg: Optional[Series[float]] = pa.Field(
        nullable=True, description="the co2 emissions for heating [kg]", coerce=True
    )
    co2eq_ww_kg: Optional[Series[float]] = pa.Field(
        nullable=True, description="the co2 emissions for warm water", coerce=True
    )
    co2eq_hww_kg: Optional[Series[float]] = pa.Field(
        nullable=True, description="the total co2 emissions for heating and warm water", coerce=True
    )
    p_c_max1h_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)

    p_h_max1h_kw: Optional[Series[float]] = pa.Field(
        nullable=True, description="the maximum hourly power for heating", coerce=True
    )
    p_ww_max1h_kw: Optional[Series[float]] = pa.Field(
        nullable=True, description="the maximum hourly power for warm water", coerce=True
    )
    p_hww_max1h_kw: Optional[Series[float]] = pa.Field(
        nullable=True, description="the maximum hourly power for heating and warm water", coerce=True
    )
    p_c_max1d_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_h_max1d_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_ww_max1d_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_hww_max1d_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    canton: Optional[Series[pd.StringDtype]] = pa.Field(nullable=True, coerce=True)
    gdenr: Optional[Series[pd.Int64Dtype]] = pa.Field(nullable=True, coerce=True)
    const_class: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="building type", coerce=True
    )
    cat_sia: Optional[Series[pd.Int64Dtype]] = pa.Field(
        nullable=True, description="building class according to Swiss SIA standards", coerce=True
    )
    const_period: Optional[Series[pd.StringDtype]] = pa.Field(nullable=True, coerce=True)

    # source_sh: Optional[Series[pd.Int64Dtype]] = pa.Field(nullable=True)
    # source_dhw: Optional[Series[pd.Int64Dtype]] = pa.Field(nullable=True)

    thermal_generator_sh: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the heating system for space heating", coerce=True
    )
    thermal_generator_dhw: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description=" the warm water system", coerce=True
    )
    fuel_sh: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the fuel or energy source for space heating", coerce=True
    )
    fuel_dhw: Optional[Series[pd.StringDtype]] = pa.Field(
        nullable=True, description="the fuel or energy source for warm water", coerce=True
    )


class Building(BuildingBase):
    point: GeoSeries = pa.Field(nullable=True, coerce=True)


BuildingPropsSchema = pandera_to_pydantic_model(BuildingBase, "BuildingPropsSchema")


class BuildingsGeoJson(FeatureCollection[Feature[Point, BuildingPropsSchema]]):
    pass


# class BuildingProps(BaseModel):
#     egid: int
#     is_heated: bool = 1
#     total_surface: Optional[float] = None
#     total_surface_main_residence: Optional[float] = None
#     source_sh: int | None = None
#     source_dhw: int | None = None
#     era: Optional[float] = None
#     era_origin: str | None = None
#     qh_origin: str | None = None
#     qww_origin: str | None = None
#     q_h_kwh_m2: Optional[float] = None
#     q_ww_kwh_m2: Optional[float] = None
#     q_hww_kwh_m2: Optional[float] = None
#     q_h_kwh: Optional[float] = None
#     q_ww_kwh: Optional[float] = None
#     q_hww_kwh: Optional[float] = None
#     heat_sh_efficiency: Optional[float] = None
#     heat_dhw_efficiency: Optional[float] = None
#     q_h_final_kwh: Optional[float] = None
#     q_ww_final_kwh: Optional[float] = None
#     q_hww_final_kwh: Optional[float] = None
#     co2eq_h_kg: Optional[float] = None
#     co2eq_ww_kg: Optional[float] = None
#     co2eq_hww_kg: Optional[float] = None
#     p_h_max1h_kw: Optional[float] = None
#     p_ww_max1h_kw: Optional[float] = None
#     p_hww_max1h_kw: Optional[float] = None
#     p_h_max1d_kw: Optional[float] = None
#     p_ww_max1d_kw: Optional[float] = None
#     p_hww_max1d_kw: Optional[float] = None
#     canton: str | None
#     gdenr: int | None
#     const_class: str | None = None
#     cat_sia: int | None = None
#     const_period: str | None = None


#
# class Building(BuildingBase):
#     egid: int
#     is_heated: bool = 1
#     total_surface: Optional[float] = None
#     total_surface_main_residence: Optional[float] = None
#     source_sh: int | None = None
#     source_dhw: int | None = None
#     era: Optional[float] = None
#     era_origin: str | None = None
#     qh_origin: str | None = None
#     qww_origin: str | None = None
#     q_h_kwh_m2: Optional[float] = None
#     q_ww_kwh_m2: Optional[float] = None
#     q_hww_kwh_m2: Optional[float] = None
#     q_h_kwh: Optional[float] = None
#     q_ww_kwh: Optional[float] = None
#     q_hww_kwh: Optional[float] = None
#     heat_sh_efficiency: Optional[float] = None
#     heat_dhw_efficiency: Optional[float] = None
#     q_h_final_kwh: Optional[float] = None
#     q_ww_final_kwh: Optional[float] = None
#     q_hww_final_kwh: Optional[float] = None
#     co2eq_h_kg: Optional[float] = None
#     co2eq_ww_kg: Optional[float] = None
#     co2eq_hww_kg: Optional[float] = None
#     p_h_max1h_kw: Optional[float] = None
#     p_ww_max1h_kw: Optional[float] = None
#     p_hww_max1h_kw: Optional[float] = None
#     p_h_max1d_kw: Optional[float] = None
#     p_ww_max1d_kw: Optional[float] = None
#     p_hww_max1d_kw: Optional[float] = None
#     canton: str | None
#     gdenr: int | None
#     const_class: str | None = None
#     cat_sia: int | None = None
#     const_period: str | None = None
#
#     @property
#     def __geo_interface__(self):
#         propdict = self.dict(exclude={'point'})
#         return {"type": "Feature", "geometry": self.__point_geojson__, "properties": propdict}
#
#     @property
#     def __point_geojson__(self):
#         return {'type': 'Point', 'coordinates': [self.point.x, self.point.y]}
#
#
# class BuildingsBaseTable(GeoDataFrameModel[BuildingBase]):
#     class Config(CollectionModelConfig):
#         extra = Extra.allow
#         validate_assignment_strict = False
#
#
# class BuildingsTable(GeoDataFrameModel[Building]):
#     class Config(CollectionModelConfig):
#         extra = Extra.allow
#         validate_assignment_strict = False
