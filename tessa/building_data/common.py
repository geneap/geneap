from collections.abc import Iterable
from dataclasses import dataclass

import geopandas as gpd
import numpy as np
import pandas as pd
from pandera.typing.geopandas import GeoDataFrame
from shapely import GEOSException


@dataclass(kw_only=True)
class BuildingDataSource:
    # carbon_intensity: Series
    # efficiency: Series
    crs: int = 4326

    def load(self, building_ids: Iterable[int]) -> GeoDataFrame:
        raise NotImplementedError

    def load_points_in_region(self, shape):
        raise NotImplementedError


def try_add_geometry(buildings, default_crs="EPSG:4326"):
    """
    Try to setup geometry columns on a table by using lat lon or x y if they exist.
    If the input is already a geodataframe just make sure the geom is called point.

    Args:
        buildings:

    Returns:

    """
    # If already a Geodataframe, just make sure the geometry col is point.
    if isinstance(buildings, gpd.GeoDataFrame):
        if buildings.geometry.name != "point":
            buildings.rename_geometry("point", inplace=True)
        return buildings

    points = None
    if "point" in buildings.columns:
        try:
            points = gpd.GeoSeries.from_wkt(buildings.point)
        except (GEOSException, TypeError):
            points = None

    if points is None:
        if {"lat", "lon"}.issubset(buildings.columns):
            lon = buildings.lon.replace("", np.nan).astype(float)
            lat = buildings.lat.replace("", np.nan).astype(float)
            points = gpd.points_from_xy(lon, lat, crs="EPSG:4326")
        elif {"x", "y"}.issubset(buildings.columns):
            # x y cols need a default CRS
            x = buildings.x.replace("", np.nan).astype(float)
            y = buildings.y.replace("", np.nan).astype(float)
            points = gpd.points_from_xy(x, y, crs=default_crs)

    if points is not None:
        buildings["point"] = points
        buildings = gpd.GeoDataFrame(buildings, geometry="point")

    return buildings


def extract_egid_list_from_table(buildings, index_col=None) -> list[int]:
    """
    Extract egid column from table.
    Parameters
    ----------
    buildings : Table
    index_col

    Returns
    -------
    List of egid
    """
    if hasattr(buildings, "__dataframe__"):
        if index_col is None or index_col not in buildings.columns:
            id_list = buildings.index.dropna().astype(int)
        else:
            id_list = buildings.loc[~buildings[index_col].isna()]
            id_list = id_list[index_col]

        id_list = np.unique(id_list.astype(int))
        id_list = id_list[id_list > 0]
        egid_list: list[int] = id_list.tolist()
    else:
        egid_list = list(buildings)

    if len(egid_list) != len(set(egid_list)):
        raise ValueError("Building IDs must be unique")
    return egid_list


# def fill_buildings_data(
#     building_init_data: pd.DataFrame | gpd.GeoDataFrame | Iterable[int],
#     building_data_from_db: pd.DataFrame | gpd.GeoDataFrame,
#     combine=True,
#     index_col=None,
# ):
#     """
#
#     Fill the initial buildings data from using the data source. Attempts to load required
#     data from the data source that was not provided in the init data and merge them.
#     Provided data always overrides loaded data.
#
#     IMPORTANT make sure the provided data doesn't contain values you want to override.
#
#     Args:
#         building_init_data:
#         building_data_from_db:
#         combine:
#         index_col:
#
#     Returns:
#
#     """
#     if not hasattr(building_init_data, "__dataframe__"):
#         # for compat, assume that that if not dataframe is just an iterable and we reidex the db data
#         return building_data_from_db.reindex(building_init_data)
#     if hasattr(building_init_data, "geometry"):
#         geom_col = building_init_data.geometry.name
#         crs = building_init_data.crs
#     else:
#         crs = 4326
#         geom_col = "point"
#
#     if len(egid_list) == 0:
#         # This should return an empty dataframe with the right columns.
#         return building_data_from_db
#
#     if hasattr(building_data_from_db, "geometry"):
#         if geom_col and building_data_from_db.geometry.name != geom_col:
#             raise RuntimeError("Loaded buildings data has different geometry column than init data")
#         else:
#             geom_col = building_data_from_db.geometry.name
#             crs = building_data_from_db.crs
#
#     # Make sure we don't lose any building rows
#     if hasattr(building_init_data, "index"):
#         building_db_data = building_data_from_db.reindex(building_init_data.index)
#
#     if (
#         hasattr(building_init_data, "index")
#         or hasattr(building_init_data, "__dataframe__")
#         and combine
#         and not isinstance(building_init_data, pd.Series)
#     ):
#         # add any columns that were in the original DF but not in the data fetch output
#         join_col = [
#             c for c in building_init_data.columns if c not in building_db_data.columns and (~building_init_data[c].isna()).any()
#         ]
#         building_db_data = building_db_data.join(building_init_data[join_col])
#     # Update the DB table with the init data, overwriting anything that was fetched from the DB
#     # with what was in the original DF, this allows to fill extra data
#     building_db_data.update(building_init_data, overwrite=True)
#
#     if geom_col:
#         building_db_data.set_geometry(geom_col, inplace=True)
#         building_db_data.crs = crs
#
#     building_db_data.index = building_db_data.index.astype(int)
#     building_db_data.index.name = "egid"
#
#     return building_db_data


def fill_table(
    base_data: pd.DataFrame, fill_data: pd.DataFrame, index_col=None, coerce_dtype=False
) -> gpd.GeoDataFrame:
    """Fill the table using additional data (usually drawn from the database).
    - The Base data values are preserved if they are not NA.
    - If cells or columns are missing from the base data and present in the fill data,
        the values from fill data are used.
    - If there are columns in the base data not present in the fill data, these are added.
    - If there are columns in the fill data not present in the base data, they are added.

    IMPORTANT make sure the provided data doesn't contain values you want to override.

    Args:
         base_data :
         fill_data :
         index_col:

     Returns:

    """
    if isinstance(base_data, (pd.DataFrame, pd.Series, gpd.GeoDataFrame, gpd.GeoSeries)):
        # Fill missing building IDs with negative sentinel values
        if index_col is not None:
            base_data = base_data.set_index(index_col)
        index = np.copy(base_data.index.to_numpy())
        na_index = np.copy(index[base_data.index.isna()])

        index[base_data.index.isna()] = -1 * np.arange(1, len(na_index) + 1)
        base_data.index = index
    else:
        # Assume this is list/array like
        index = base_data

    if len(index) != len(set(index)):
        raise ValueError("Building IDs must be unique")

    if hasattr(base_data, "geometry"):
        crs = base_data.crs
        geom_col = base_data.geometry.name
    elif hasattr(fill_data, "geometry"):
        crs = fill_data.crs
        geom_col = fill_data.geometry.name
    else:
        crs = 4326
        geom_col = "point"

    # Set the base of the table as the data loaded from the DB since it is expected to be
    # the most complete dataset. If the base data input is just a list of egids, we're done.
    buildings = fill_data.reindex(index)
    if isinstance(base_data, (pd.DataFrame, GeoDataFrame)):
        # Make sure the output data matches in the init data egids.
        buildings = buildings.reindex(index=base_data.index)
        # add any columns that were in the original DF but not in the data fetch output
        # join_col = [
        #     c for c in buildings.columns if c not in
        #     buildings.columns and (~buildings[c].isna()).any()
        # ]
        # buildings = buildings.join(buildings[join_col])

        # merge columns preserving the order of the init data.
        merged_columns = base_data.dtypes.to_dict()
        for col, dtype in buildings.dtypes.items():
            if col not in merged_columns:
                merged_columns[col] = dtype
        buildings = buildings.reindex(columns=list(merged_columns.keys()))
        if coerce_dtype:
            buildings = buildings.astype(merged_columns)
        # Update the DB table with the init data, overwriting anything that was fetched from the DB
        # with what was in the original DF, this allows to fill extra data
        buildings.update(base_data, overwrite=True)

    buildings.index = buildings.index.astype(int)

    if not isinstance(buildings, gpd.GeoDataFrame):
        buildings = gpd.GeoDataFrame(buildings, crs=crs, geometry=geom_col)
    else:
        buildings.set_geometry(geom_col, inplace=True)
        buildings.crs = crs

    return buildings


def merge_building_tables(buildings_a: pd.DataFrame, buildings_b: pd.DataFrame):
    """
    Help to merge different building data sources which may both be a bit shit.

    For example to merge two user-supplied dataframes, with some overlapping IDs etc.

    Args:
        buildings_a:
        buildings_b:

    Returns:

    """
    if buildings_a is None or len(buildings_a) == 0:
        return buildings_b
    elif buildings_b is None or len(buildings_b) == 0:
        return buildings_a
    buildings = buildings_a.copy()
    buildings.update(buildings_b, overwrite=False)
    buildings = buildings.join(buildings_a[[c for c in buildings_a.columns if c not in buildings.columns]])
    buildings = buildings.join(buildings_b[[c for c in buildings_b.columns if c not in buildings.columns]])
    return buildings


def setup_building_index(buildings: gpd.GeoDataFrame | pd.DataFrame, real_id_col="egid") -> gpd.GeoDataFrame:
    """
    Setup a unique ID for each building. If the buildings have an official ID
    (egid usually) use that first, then generate IDs for the rest. To make it easy to differentiate
    the 'real' ones, try setting the generates ids as negative numbers.
    Then set the ID as the index

    Args:
        buildings
        real_id_col

    Returns:

    """
    if buildings.index.name == real_id_col:
        buildings = buildings.reset_index()
    buildings["id"] = (
        buildings[real_id_col].replace("", pd.NA).astype(pd.Float64Dtype()).astype(pd.Int64Dtype())
    )

    n_to_generate = len(buildings.loc[buildings["id"].isna()])
    buildings.loc[buildings["id"].isna(), "id"] = -1 * np.arange(1, n_to_generate + 1)
    buildings = buildings.set_index("id")
    buildings.index = buildings.index.astype(int)

    buildings.index.name = real_id_col
    buildings = buildings.drop(columns=real_id_col)
    return buildings
