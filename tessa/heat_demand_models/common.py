from abc import abstractmethod
from typing import Optional

import pandas as pd
import pandera as pa
from pandera.typing import DataFrame, Series, String
from pydantic.dataclasses import dataclass

from tessa.climate_data import ClimateRecord
from tessa.core.models import PowerLoad


class EfficiencyEmissions(pa.DataFrameModel):
    thermal_generator_sh: Series[String] = pa.Field(nullable=True, coerce=True)
    thermal_generator_dhw: Series[String] = pa.Field(nullable=True, coerce=True)
    fuel_sh: Series[String] = pa.Field(nullable=True, coerce=True)
    fuel_dhw: Series[String] = pa.Field(nullable=True, coerce=True)

    heat_sh_efficiency: Series[float] = pa.Field(nullable=True, coerce=True)
    heat_dhw_efficiency: Series[float] = pa.Field(nullable=True, coerce=True)
    q_h_final_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_ww_final_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_hww_final_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    sh_co2_kg_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    dhw_co2_kg_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    co2eq_h_kg: Series[float] = pa.Field(nullable=True, coerce=True)
    co2eq_ww_kg: Series[float] = pa.Field(nullable=True, coerce=True)
    co2eq_hww_kg: Series[float] = pa.Field(nullable=True, coerce=True)
    co2eq_hww_kg_m2: Series[float] = pa.Field(nullable=True, coerce=True)


class BuildingSurface(pa.DataFrameModel):
    era: Series[float] = pa.Field(nullable=True, coerce=True)
    era_origin: Series[str] | None = pa.Field(nullable=True, coerce=True)

    total_surface: Series[float] | None = pa.Field(nullable=True, coerce=True)
    total_surface_main_residence: Series[float] | None = pa.Field(nullable=True, coerce=True)


class YearlyPowerSummary(pa.DataFrameModel):
    p_h_max1h_kw: Series[float] = pa.Field(nullable=True)
    p_ww_max1h_kw: Series[float] = pa.Field(nullable=True)
    p_hww_max1h_kw: Series[float] = pa.Field(nullable=True)
    p_h_max1d_kw: Series[float] = pa.Field(nullable=True)
    p_ww_max1d_kw: Series[float] = pa.Field(nullable=True)
    p_hww_max1d_kw: Series[float] = pa.Field(nullable=True)


class YearlyHeatEnergy(pa.DataFrameModel):
    is_heated: Series[bool] = pa.Field(nullable=True, coerce=True)
    q_h_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_ww_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_hww_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_h_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)
    q_ww_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)
    q_hww_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)


class YearlyModelResult(BuildingSurface, YearlyHeatEnergy, EfficiencyEmissions):
    era_origin: Series[str] = pa.Field(nullable=True, coerce=True)
    qh_origin: Series[str] = pa.Field(nullable=True, coerce=True)
    qww_origin: Series[str] = pa.Field(nullable=True, coerce=True)


class DemandModelResult(YearlyModelResult, YearlyPowerSummary):
    pass


@dataclass
class HeatDemandModel:
    @abstractmethod
    def calculate(
        self, buildings: DataFrame, temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[DemandModelResult], DataFrame[PowerLoad]]:
        ...

    @abstractmethod
    def calculate_load_curves(
        self, buildings: DataFrame, temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[YearlyPowerSummary], DataFrame[PowerLoad]]:
        ...

    @abstractmethod
    def calculate_yearly(
        self,
        buildings: DataFrame,
        *,
        degree_days: Optional[float] = None,
        temperatures: Optional[DataFrame[ClimateRecord]] = None,
    ) -> DataFrame[YearlyModelResult]:
        ...


class CoolingPowerLoad(PowerLoad):
    p_c_kw: Series[float] = pa.Field(coerce=True)


class CoolingEfficiencyEmissions(pa.DataFrameModel):
    thermal_generator_c: Series[String] = pa.Field(nullable=True, coerce=True)
    fuel_c: Series[String] = pa.Field(nullable=True, coerce=True)

    thermal_c_efficiency: Series[float] = pa.Field(nullable=True, coerce=True)
    q_c_final_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    c_co2_kg_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    co2eq_c_kg: Series[float] = pa.Field(nullable=True, coerce=True)


class YearlyCoolingPowerSummary(pa.DataFrameModel):
    p_c_max1h_kw: Series[float] = pa.Field(nullable=True, coerce=True)
    p_c_max1d_kw: Series[float] = pa.Field(nullable=True, coerce=True)


class YearlyCoolingEnergy(pa.DataFrameModel):
    is_cooled: Series[bool] = pa.Field(nullable=True, coerce=True)
    q_c_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_c_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)


class YearlyCoolingModelResult(BuildingSurface, YearlyCoolingEnergy, CoolingEfficiencyEmissions):
    qc_origin: Series[str] = pa.Field(nullable=True, coerce=True)
    archetype: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)

    temp_h_deg: Series[float] = pa.Field(nullable=True, coerce=True)
    temp_ww_deg: Series[float] = pa.Field(nullable=True, coerce=True)
    temp_c_deg: Series[float] = pa.Field(nullable=True, coerce=True)


class CoolingDemandModelResult(YearlyCoolingModelResult, YearlyCoolingPowerSummary):
    pass


@dataclass
class CoolingDemandModel:
    @abstractmethod
    def calculate_cooling(
        self, buildings: DataFrame, heat_load_curves: DataFrame
    ) -> tuple[DataFrame[CoolingDemandModelResult], DataFrame[CoolingPowerLoad]]:
        ...

    @abstractmethod
    def calculate_load_curves_cooling(
        self, buildings: DataFrame, heat_load_curves: DataFrame
    ) -> tuple[DataFrame[YearlyCoolingPowerSummary], DataFrame[CoolingPowerLoad]]:
        ...

    @abstractmethod
    def calculate_yearly_cooling(
        self,
        buildings: DataFrame,
    ) -> DataFrame[YearlyCoolingModelResult]:
        ...
