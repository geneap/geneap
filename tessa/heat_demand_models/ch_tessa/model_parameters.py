import json
from functools import cache
from importlib.resources import as_file, files
from pathlib import Path

import numpy as np
import pandas as pd
import pandera as pa
from pandera.engines.pandas_engine import PydanticModel
from pandera.typing import DataFrame, Index, Series

from tessa.core.models import Fuel, ThermalGenerator

from . import data


class _GeNormClimate(pa.DataFrameModel):
    day_of_year: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    t_air: Series[float] = pa.Field(coerce=True)


@cache
def load_ge_norm_climate() -> DataFrame[_GeNormClimate]:
    """
    Daily mean air temperature for Geneva from IDC calculation

    TODO these climate corrections are actually tied to the heat model that uses the
     geneva IDC norm as a basis -> not actually very general.

    Returns:

    """

    from importlib.resources import as_file, files

    from . import data

    with as_file(files(data).joinpath("climate_norm_GE.csv")) as climate_ref_file:
        reference_climate = pd.read_csv(climate_ref_file, sep=",")
    reference_climate.columns = [c.lower() for c in reference_climate.columns]
    ge_climate = reference_climate[reference_climate["stn"] == "GVA_Ref_Y365"]
    ge_climate = ge_climate[["day_of_year", "t_air"]]
    return DataFrame[_GeNormClimate](ge_climate)


class _ThermalGeneratorTable(pa.DataFrameModel):
    class Config:
        dtype = PydanticModel(ThermalGenerator)


@cache
def _load_thermal_generators_defaults() -> DataFrame[_ThermalGeneratorTable]:
    with as_file(files(data).joinpath("thermal_generator.csv")) as data_file:
        return pd.read_csv(data_file, sep=",")


class _FuelTable(pa.DataFrameModel):
    class Config:
        dtype = PydanticModel(Fuel)
        coerce = True


@cache
def _load_fuel_defaults() -> DataFrame[_FuelTable]:
    with as_file(files(data).joinpath("fuel.csv")) as data_file:
        return pd.read_csv(data_file, sep=",")


class _QhwwAgeSia(pa.DataFrameModel):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    const_period: Series[pd.StringDtype] = pa.Field(coerce=True)
    # nb_idc: Series[pd.Int64Dtype]
    avg_q_h_kwh_m2: Series[float] = pa.Field(coerce=True)
    avg_q_ww_kwh_m2: Series[float] = pa.Field(coerce=True)


@cache
def _load_avg_qhww_sia_age() -> DataFrame[_QhwwAgeSia]:
    """
    AVG consumption per SIA class and construction period

    Returns

    """
    with as_file(files(data).joinpath("qhww_by_sia_and_age.csv")) as data_file:
        qh_sia_age = pd.read_csv(data_file, sep=",")

    qh_sia_age.columns = [c.lower() for c in qh_sia_age.columns]

    return DataFrame[_QhwwAgeSia](qh_sia_age)


# class _EraQhwwSia(pa.DataFrameModel):
#     class Config:
#         coerce = True
#
#     cat_sia: Series[pd.Int64Dtype]
#     a_s: Series[float]
#     a_app: Series[float]
#     obs_era_factor: Series[float]
#     avg_q_h_kwh_m2: Series[float]
#     avg_q_ww_kwh_m2: Series[float]
#     max_ratio_qww_qhww: Series[float]


class _QhwwSia(pa.DataFrameModel):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    avg_q_h_kwh_m2: Series[float] = pa.Field(coerce=True)
    avg_q_ww_kwh_m2: Series[float] = pa.Field(coerce=True)
    max_ratio_qww_qhww: Series[float] = pa.Field(coerce=True)


class _EraSia(pa.DataFrameModel):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    a_s: Series[float] = pa.Field(coerce=True)
    a_app: Series[float] = pa.Field(coerce=True)
    obs_era_factor: Series[float] = pa.Field(coerce=True)


# @cache
# def _load_avg_era_qhww_sia() -> DataFrame[_EraQhwwSia]:
#     """
#     AVG consumption per SIA class and SRE factors
#
#     Returns:
#         ERA estimation parameters and energy intensity per SIA class
#     """
#     with as_file(files(data).joinpath(Path('qhww_and_era_calc_by_sia_category.csv'))) as data_file:
#         era_qhww_sia = pd.read_csv(data_file, sep=",")
#
#     era_qhww_sia.columns = [c.lower() for c in era_qhww_sia.columns]
#
#     return DataFrame[_EraQhwwSia](era_qhww_sia)


@cache
def _load_avg_era_sia() -> DataFrame[_EraSia]:
    """
    AVG consumption per SIA class and SRE factors

    Returns:
        ERA estimation parameters and energy intensity per SIA class
    """
    with as_file(files(data).joinpath("era_param_by_sia.csv")) as data_file:
        era_qhww_sia = pd.read_csv(data_file, sep=",")

    era_qhww_sia.columns = [c.lower() for c in era_qhww_sia.columns]

    return DataFrame[_EraSia](era_qhww_sia)


@cache
def _load_avg_qhww_sia() -> DataFrame[_QhwwSia]:
    """
    AVG consumption per SIA class

    Returns:
       energy intensity per SIA class
    """
    with as_file(files(data).joinpath("qhww_by_sia.csv")) as data_file:
        qhww_sia = pd.read_csv(data_file, sep=",")

    qhww_sia.columns = [c.lower() for c in qhww_sia.columns]

    return DataFrame[_QhwwSia](qhww_sia)


class _LcModelSummary(pa.DataFrameModel):
    class Config:
        multiindex_strict = True
        multiindex_coerce = True

    buildingtype: Index[pd.StringDtype] = pa.Field(coerce=True)
    modelnb: Index[int] = pa.Field(coerce=True)
    lc_model_id: Series[int] = pa.Field(coerce=True)
    type: Series[pd.StringDtype] = pa.Field(coerce=True)
    modeldeffile: Series[pd.StringDtype] = pa.Field(coerce=True)
    nbmodels: Series[int] = pa.Field(coerce=True)


@cache
def _load_lc_model_summary() -> DataFrame[_LcModelSummary]:
    """
    Load the Load Curve model summary file and set the index to the unique
    (building type, model number) key

    Returns:
    """
    with as_file(files(data).joinpath(Path("lc_model_summary.csv"))) as data_file:
        lc_model_summary = pd.read_csv(data_file, sep=",")
    lc_model_summary.columns = [c.lower() for c in lc_model_summary.columns]
    lc_model_summary = lc_model_summary.rename(columns={"id": "lc_model_id"})
    lc_model_summary = lc_model_summary.set_index(["buildingtype", "modelnb"])
    return DataFrame[_LcModelSummary](lc_model_summary)


@cache
def _load_dhw_timeseries():
    # AVG values per day of year
    with as_file(files(data).joinpath(Path("DHW_Model_AVG1D.csv"))) as data_file:
        avg_dhw_day = pd.read_csv(data_file, sep=",")

    avg_dhw_day.columns = [c.lower() for c in avg_dhw_day.columns]

    # This file is much faster to use, needs to be generated from the raw data using the prepare function
    # prepare_dhw_hourly_model(base_path)
    with as_file(files(data).joinpath(Path("DHW_Model_Dev1H_profiles.parquet"))) as daily_profiles_file:
        hourly_profiles = pd.read_parquet(daily_profiles_file)
    return avg_dhw_day, hourly_profiles


class _DhwAvg1Day(pa.DataFrameModel):
    day_of_year: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    month: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    day_of_month: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    qww_norm_1d: Series[float] = pa.Field(coerce=True)
    rexid: Series[pd.StringDtype] = pa.Field(coerce=True)


@cache
def _load_dhw_daily_profile() -> DataFrame[_DhwAvg1Day]:
    with as_file(files(data).joinpath("dhw_model_avg_1d.csv")) as daily_profiles_file:
        daily_profiles = pd.read_csv(daily_profiles_file, sep=",")

    daily_profiles.columns = [c.lower() for c in daily_profiles.columns]
    return DataFrame[_DhwAvg1Day](daily_profiles)


@cache
def _load_dhw_hourly_profile():
    """
    Load the reference hourly profiles for domestic hot water use

    This file is much faster to use, needs to be generated from the raw data using the prepare function
    `prepare_dhw_hourly_model(base_path)`

    Returns:
        24 columns by N rows of typical daily profiles for DHW consumption

    """

    with as_file(files(data).joinpath(Path("DHW_Model_Dev1H_profiles.parquet"))) as profiles_file:
        return pd.read_parquet(profiles_file)


def _load_space_heating_load_curve_model(base_path, file_model) -> dict:
    """
    Function that stores in a dictionary the parameters necessary
    to generate an hourly load curve for space heating.
    For example: non-heating temperature, slope of the daily energy signature.

    Args:
        base_path : Path of Edelweiss serveur until -
        file_model : File containing the definition of model.

    Returns:
        Dictionary
    """
    file_model = file_model.replace(".txt", "")
    path = (
        base_path
        / "load_curves"
        / "heat_parameters"
        / "LC_Models"
        / "space_heating_models"
        / (file_model + ".json")
    )
    with open(path) as f:
        model = json.load(f)

    perturbations, perturbations_by_t_bin = _load_perturbations_by_t_ex_bin(base_path, file_model)

    bin_count = perturbations[["Text_Bin"]].value_counts().to_frame(name="Nb_Bin").sort_index()

    return {
        "rex_id": model["case_study"],
        # Slope of daily energy signature
        "a_sign": float(model["model_parameters"]["signature_daily_avg"]),
        # Energy signature intercept
        "t0": float(model["model_parameters"]["T0"]),
        # Non-heating temperature (Qh = 0 if T_Ext >= T_nc)
        "t_nc": float(model["model_parameters"]["T_NC"]),
        "perturbations_by_t_bin": perturbations_by_t_bin,
        "bin_count": bin_count,
    }


def _load_perturbations_by_t_ex_bin(base_path: Path, file_model: str):
    path = (
        base_path
        / "load_curves"
        / "heat_parameters"
        / "LC_Models"
        / "space_heating_models"
        / (file_model + ".csv")
    )
    perturbations = pd.read_csv(path, sep=",")
    t_levels = set(perturbations["Text_Bin"].unique())
    if 99 in t_levels:
        t_levels.remove(99)
    if -99 in t_levels:
        t_levels.remove(-99)
    expected_values = set(np.arange(min(t_levels), max(t_levels) + 2, 2))
    missing_values = expected_values - t_levels
    for v in missing_values:
        # Take from higher T values first, then lower
        if v + 2 in t_levels:
            fill = perturbations[perturbations["Text_Bin"] == v + 2].copy()
            fill["Text_Bin"] = v
            perturbations = perturbations.append(fill, ignore_index=True)
        elif v - 2 in t_levels:
            fill = perturbations[perturbations["Text_Bin"] == v - 2].copy()
            fill["Text_Bin"] = v
            perturbations = perturbations.append(fill, ignore_index=True)
        else:
            fill = perturbations[perturbations["Text_Bin"] == -99].copy()
            fill["Text_Bin"] = v
            perturbations = perturbations.append(fill, ignore_index=True)
    # Add a zero value for high climate
    perturbations = perturbations.append({"Text_Bin": 99, "Sample_Nb": 1, "Dev_p_h_kw": 0}, ignore_index=True)
    perturbations = perturbations.sort_values("Text_Bin")
    perturbations_by_t_bin = dict(tuple(perturbations.groupby("Text_Bin")["Dev_p_h_kw"]))
    # just keep the numpy arrays
    perturbations_by_t_bin = {k: v.values for k, v in perturbations_by_t_bin.items()}
    return perturbations, perturbations_by_t_bin


def _load_presampled_heating_load_curve_model(filename):
    filename = Path(filename)

    with as_file(files(data).joinpath(Path("space_heating_models") / filename.with_suffix(".json"))) as path:
        with open(path) as f:
            model = json.load(f)

    with as_file(
        files(data).joinpath(Path("space_heating_models") / filename.with_suffix(".parquet"))
    ) as path:
        perturbations = pd.read_parquet(path)

    return {
        "rex_id": model["case_study"],
        # Slope of daily energy signature
        "a_sign": float(model["model_parameters"]["signature_daily_avg"]),
        # Energy signature intercept
        "t0": float(model["model_parameters"]["T0"]),
        # Non-heating temperature (Qh = 0 if T_Ext >= T_nc)
        "t_nc": float(model["model_parameters"]["T_NC"]),
        "perturbations_presampled": perturbations,
        "filename": filename,
    }


class _LcModelMapping(pa.DataFrameModel):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    sel_epoque_construction: Series[pd.StringDtype] = pa.Field(coerce=True)
    buildingtype_sh: Series[pd.StringDtype] = pa.Field(coerce=True)
    buildingtype_dhw: Series[pd.StringDtype] = pa.Field(coerce=True)


@cache
def _load_building_lc_model_mapping() -> DataFrame[_LcModelMapping]:
    # Mapping between building type and model to be used
    with as_file(files(data).joinpath(Path("building_type_model_map.csv"))) as data_file:
        building_type_model_map = pd.read_csv(data_file, sep=",")
    building_type_model_map.columns = [c.lower() for c in building_type_model_map.columns]

    return DataFrame[_LcModelMapping](building_type_model_map)


def _prepare_dhw_hourly_model(base_path):
    # Observed deviation between AVG value of the day and hour of day
    dhw_hour = pd.read_csv(
        base_path / "load_curves" / "heat_parameters" / "LC_Models" / "DHW_Model_Dev1H.csv", sep=","
    )
    dhw_hour.columns = [c.lower() for c in dhw_hour.columns]
    dhw_hour["timestamp"] = pd.to_datetime(dhw_hour["timestamp"])
    dhw_hour["data_ts_houryear"] = (dhw_hour["timestamp"].dt.dayofyear - 1) * 24 + dhw_hour[
        "timestamp"
    ].dt.hour
    dhw_hour["hour_of_day"] = dhw_hour["hour_of_day"].astype(int)
    dhw_hour["data_ts_date"] = dhw_hour["timestamp"].dt.date
    # Profile data is a mess, was probably not doing what we thought it was doing
    # What we want are daily profiles where we will then
    # select for each day one profile (row)
    return (
        pd.concat(
            [
                d.set_index([d.groupby("hour_of_day").cumcount(), "hour_of_day"])["delt_qww"].unstack()
                for _, d in dhw_hour[["data_ts_date", "hour_of_day", "delt_qww"]].groupby("data_ts_date")
            ]
        ).reset_index(drop=True)
    ).fillna(0)


class _ArchetypeInput(pa.DataFrameModel):
    const_class: Series[pd.StringDtype] = pa.Field(coerce=True, nullable=True)
    const_period: Series[pd.StringDtype] = pa.Field(coerce=True, nullable=True)
    canton: Series[pd.StringDtype] = pa.Field(coerce=True, nullable=True)
    total_surface: Series[float] = pa.Field(coerce=True, nullable=True)


class _ArchetypeDetail(pa.DataFrameModel):
    archetype: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    type: Series[pd.StringDtype] = pa.Field(coerce=True)
    region: Series[pd.StringDtype] = pa.Field(coerce=True)
    year_of_construction: Series[pd.StringDtype] = pa.Field(coerce=True)
    building_floor_area_m2: Series[float] = pa.Field(coerce=True)


class _QcArchetype(pa.DataFrameModel):
    archetype: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    retrofit_measure: Series[pd.StringDtype] = pa.Field(coerce=True)
    rcp: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    simulation_year: Series[pd.Int64Dtype] = pa.Field(coerce=True)
    avg_q_c_kwh_m2_floor_area: Series[float] = pa.Field(coerce=True)


@cache
def _load_qc_by_archetype() -> DataFrame[_QcArchetype]:
    """
    Read PACE EMPA model - annual simulation summary per archetype
    TODO select the right scenario (retro/sim_year/rcp)
    Returns
    -------

    """
    with as_file(files(data).joinpath(Path("pace_annual_simulation_summary_processed.csv"))) as data_file:
        qc_archetype = pd.read_csv(data_file, sep=",")

    qc_archetype["avg_q_c_kwh_m2_floor_area"] = (
        qc_archetype["cooling_demand_kwh"] / qc_archetype["building_floor_area_m2"]
    )

    return DataFrame[_QcArchetype](qc_archetype)


@cache
def _load_archetype_detail() -> DataFrame[_ArchetypeDetail]:
    """
    Load PACE EMPA model - archetype (centroid fid) details.
    Returns
    -------

    """
    with as_file(files(data).joinpath(Path("pace_centroid_details.csv"))) as data_file:
        archetype_detail = pd.read_csv(data_file, sep=",")

    return DataFrame[_ArchetypeDetail](archetype_detail)
