from dataclasses import dataclass, field
from typing import Optional, Union

import numpy as np
import pandas as pd
import pandera as pa
import structlog
from pandera.typing import DataFrame, Series
from pandera.typing.geopandas import GeoDataFrame

from tessa.building_data import Building
from tessa.climate_data import ClimateRecord, calculate_climate_correction, calculate_heating_degree_days
from tessa.core.models import PowerLoad
from tessa.heat_demand_models.common import (
    BuildingSurface,
    DemandModelResult,
    EfficiencyEmissions,
    HeatDemandModel,
    YearlyHeatEnergy,
    YearlyModelResult,
    YearlyPowerSummary,
)
from tessa.utils.pandera_extras import ensure_columns_for_schema

from .model_parameters import (
    _DhwAvg1Day,
    _EraSia,
    _FuelTable,
    _LcModelMapping,
    _LcModelSummary,
    _load_avg_era_sia,
    # _load_avg_era_qhww_sia,
    _load_avg_qhww_sia,
    _load_avg_qhww_sia_age,
    _load_building_lc_model_mapping,
    _load_dhw_daily_profile,
    _load_dhw_hourly_profile,
    _load_fuel_defaults,
    _load_lc_model_summary,
    _load_presampled_heating_load_curve_model,
    _load_thermal_generators_defaults,
    _QhwwAgeSia,
    # _EraQhwwSia,
    _QhwwSia,
    #    _load_cooling_generators_defaults,
    _ThermalGeneratorTable,
    load_ge_norm_climate,
)

log = structlog.get_logger()


class _HeatCalcInput(pa.DataFrameModel):
    era: Series[float] = pa.Field(nullable=True, coerce=True)
    is_heated: Series[bool] = pa.Field(nullable=True, coerce=True)
    q_h_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)
    q_ww_kwh_m2: Series[float] = pa.Field(nullable=True, coerce=True)


class YearlyModelInput(BuildingSurface, YearlyHeatEnergy, EfficiencyEmissions):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    const_period: Series[str] = pa.Field(nullable=True, coerce=True)


class LoadCurveModelInput(pa.DataFrameModel):
    cat_sia: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    const_period: Series[str] = pa.Field(nullable=True, coerce=True)
    q_h_kwh: Series[float] = pa.Field(nullable=True, coerce=True)
    q_ww_kwh: Series[float] = pa.Field(nullable=True, coerce=True)


class DemandModelInput(LoadCurveModelInput, YearlyModelInput):
    pass


@dataclass
class SwissHeatDemandModel(HeatDemandModel):
    """
    Holds the heat demand model parameters and reference climate (which is part of the model)

    For now uses file storage as params are stored as a bunch of CSVs in a data folder,
    but could be replaced various ways (e.g. database store for model)
    Params are loaded once as default to avoid repeated loads.

    NOTE: this is not a general model, only works for Switzlerand.
    """

    # GE ref climate is part of the demand model, not the external data.
    reference_heating_degree_days: float = field(repr=False, init=False)
    all_lc_sh_models: dict = field(repr=False, init=False)

    reference_climate: pd.DataFrame = field(default_factory=load_ge_norm_climate)
    reference_year: int = 2015

    # era_qhww_per_sia: DataFrame[_EraQhwwSia] = field(default_factory=_load_avg_era_qhww_sia)
    era_by_sia: DataFrame[_EraSia] = field(default_factory=_load_avg_era_sia)
    qhww_by_sia: DataFrame[_QhwwSia] = field(default_factory=_load_avg_qhww_sia)
    qhww_by_sia_age: DataFrame[_QhwwAgeSia] = field(default_factory=_load_avg_qhww_sia_age)
    dhw_model_avg1d: DataFrame[_DhwAvg1Day] = field(default_factory=_load_dhw_daily_profile)
    dhw_model_dev1h: pd.DataFrame = field(default_factory=_load_dhw_hourly_profile)
    building_type_model: DataFrame[_LcModelMapping] = field(default_factory=_load_building_lc_model_mapping)
    lc_model_summary: DataFrame[_LcModelSummary] = field(default_factory=_load_lc_model_summary)

    thermal_generator_definitions: DataFrame[_ThermalGeneratorTable] = field(
        default_factory=_load_thermal_generators_defaults
    )
    fuel_definitions: DataFrame[_FuelTable] = field(default_factory=_load_fuel_defaults)

    def __post_init__(self):
        self.reference_heating_degree_days = calculate_heating_degree_days(self.reference_climate["t_air"])
        sh_models = self.lc_model_summary[self.lc_model_summary["type"] == "SH"]
        self.all_lc_sh_models = {}

        for key, filename in sh_models["modeldeffile"].items():
            # all_lc_sh_models[key] = read_space_heating_load_curve_model(base_path, filename)
            self.all_lc_sh_models[key] = _load_presampled_heating_load_curve_model(filename)

    @property
    def fuel_carbon_intensities(self):
        return self.fuel_definitions.set_index("code").carbon_intensity

    @property
    def system_efficiencies(self):
        return self.thermal_generator_definitions.set_index("code").efficiency

    def calculate_yearly(
        self,
        buildings: DataFrame[YearlyModelInput],
        *,
        degree_days: Optional[float] = None,
        temperatures: Optional[DataFrame[ClimateRecord]] = None,
    ) -> DataFrame[YearlyModelResult]:
        """
        Calculate yearly heat demand for a set of buildings and given temperature data
        (or degree days).

        The provided building table will be returned with the heat demand (useful and final),
        efficiency and co2 estimtes columns added or upated. If the provided buildings data already
        includes some values (see the fields in schemas BuildingSurface, YearlyHeatEnergy,
        EfficiencyEmissions), the provided values will be kept and missing ones calculated on a
        'best effort basis'.

        For example, if you provide the total useful heat demand of the building and the heat system
        type and fuel source, we calculate the final energy and co2 emissions.

        TODO need to check every possible combination of known input data, e.g. known energy final etc.
         might need to re-check several times

        Args:
            buildings:
            degree_days:
            temperatures:

        Returns:

        """
        if degree_days is None and temperatures is not None:
            degree_days = calculate_heating_degree_days(temperatures["temp2m"])

        clim_correction = calculate_climate_correction(degree_days, self.reference_heating_degree_days)
        # Make sure optional columns exist
        buildings = ensure_columns_for_schema(YearlyModelInput, buildings.copy())

        return apply_yearly_heat_demand_model(
            buildings,
            era_per_sia=self.era_by_sia,
            qhww_per_sia_age=self.qhww_by_sia_age,
            clim_correction=clim_correction,
            carbon_intensities=self.fuel_carbon_intensities,
            efficiencies=self.system_efficiencies,
        )

    def calculate_load_curves(
        self, buildings: DataFrame[LoadCurveModelInput], temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[YearlyPowerSummary], DataFrame[PowerLoad]]:
        # NOTE: use the parallel version for better responsiveness

        return generate_dhw_sh_load_curves_parallel(
            buildings,
            temperatures,
            lc_model_summary=self.lc_model_summary,
            building_type_model_map=self.building_type_model,
            lc_sh_models=self.all_lc_sh_models,
            dhw_model_avg1d=self.dhw_model_avg1d,
            dhw_model_dev1h=self.dhw_model_dev1h,
        )

    def calculate(
        self, buildings: DataFrame[DemandModelInput], temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[DemandModelResult], DataFrame[PowerLoad]]:
        buildings = ensure_columns_for_schema(DemandModelResult, buildings)
        buildings = self.calculate_yearly(buildings.copy(), temperatures=temperatures)

        yearly_power, load_curve = self.calculate_load_curves(buildings, temperatures)

        buildings = buildings.combine_first(yearly_power)

        return DataFrame[DemandModelResult](buildings), load_curve


@dataclass
class SwissHeatDemandData:
    """
    Holds the heat demand model parameters and reference climate (which is part of the model)

    For now uses file storage as params are stored as a bunch of CSVs in a data folder,
    but could be replaced various ways (e.g. database store for model)
    Params are loaded once as default to avoid repeated loads.

    NOTE: this is not a general model, only works for Switzlerand.
    """

    # GE ref climate is part of the demand model, not the external data.
    reference_heating_degree_days: float = field(repr=False, init=False)
    reference_climate: pd.DataFrame = field(default_factory=load_ge_norm_climate)
    reference_year: int = 2015

    era_by_sia: DataFrame[_EraSia] = field(default_factory=_load_avg_era_sia)
    qhww_by_sia: DataFrame[_QhwwSia] = field(default_factory=_load_avg_qhww_sia)
    qhww_by_sia_age: DataFrame[_QhwwAgeSia] = field(default_factory=_load_avg_qhww_sia_age)

    def __post_init__(self):
        self.reference_heating_degree_days = calculate_heating_degree_days(self.reference_climate["t_air"])

    def __call__(self, district: "District") -> "District":
        buildings = self.fill_with_sia(district.buildings, temperatures=district.climate.timeseries)
        return district.model_copy(
            update=dict(
                buildings=buildings,
            )
        )

    def fill_with_sia(
        self,
        buildings: DataFrame[YearlyModelInput],
        *,
        degree_days: Optional[float] = None,
        temperatures: Optional[DataFrame[ClimateRecord]] = None,
    ) -> DataFrame[YearlyModelResult]:
        """
        Calculate yearly heat demand for a set of buildings and given temperature data
        (or degree days).

        The provided building table will be returned with the heat demand (useful and final),
        efficiency and co2 estimtes columns added or upated. If the provided buildings data already
        includes some values (see the fields in schemas BuildingSurface, YearlyHeatEnergy,
        EfficiencyEmissions), the provided values will be kept and missing ones calculated on a
        'best effort basis'.

        For example, if you provide the total useful heat demand of the building and the heat system
        type and fuel source, we calculate the final energy and co2 emissions.

        TODO need to check every possible combination of known input data, e.g. known energy final etc.
         might need to re-check several times

        Args:
            buildings:
            degree_days:
            temperatures:

        Returns:

        """
        if degree_days is None and temperatures is not None:
            degree_days = calculate_heating_degree_days(temperatures["temp2m"])

        clim_correction = calculate_climate_correction(degree_days, self.reference_heating_degree_days)
        # Make sure optional columns exist
        buildings = ensure_columns_for_schema(YearlyModelInput, buildings.copy())

        buildings = DataFrame[YearlyModelInput](buildings)

        buildings = _consolidate_era(buildings, self.era_by_sia)

        heat_demand_by_archetype = _get_heat_demand_by_archetype(buildings, self.qhww_by_sia_age)
        buildings = _consolidate_q_ww_m2(buildings, heat_demand_by_archetype)
        buildings = _consolidate_q_h_kwh_m2(buildings, clim_correction, heat_demand_by_archetype)
        buildings = _fill_missing_dhw_generator(buildings)

        # double check validation after input df transform
        buildings = DataFrame[YearlyModelResult](buildings)

        return buildings
        # return DataFrame[_HeatCalcInput](buildings.loc[:, list(_HeatCalcInput.to_schema().columns)])


@dataclass
class CalculateHeatAndLoadCurve:
    """
    Holds the heat demand model parameters and reference climate (which is part of the model)

    For now uses file storage as params are stored as a bunch of CSVs in a data folder,
    but could be replaced various ways (e.g. database store for model)
    Params are loaded once as default to avoid repeated loads.

    NOTE: this is not a general model, only works for Switzlerand.
    """

    all_lc_sh_models: dict = field(repr=False, init=False)
    dhw_model_avg1d: DataFrame[_DhwAvg1Day] = field(default_factory=_load_dhw_daily_profile)
    dhw_model_dev1h: pd.DataFrame = field(default_factory=_load_dhw_hourly_profile)
    building_type_model: DataFrame[_LcModelMapping] = field(default_factory=_load_building_lc_model_mapping)
    lc_model_summary: DataFrame[_LcModelSummary] = field(default_factory=_load_lc_model_summary)

    thermal_generator_definitions: DataFrame[_ThermalGeneratorTable] = field(
        default_factory=_load_thermal_generators_defaults
    )
    fuel_definitions: DataFrame[_FuelTable] = field(default_factory=_load_fuel_defaults)

    @property
    def fuel_carbon_intensities(self):
        return self.fuel_definitions.set_index("code").carbon_intensity

    @property
    def system_efficiencies(self):
        return self.thermal_generator_definitions.set_index("code").efficiency

    def __post_init__(self):
        sh_models = self.lc_model_summary[self.lc_model_summary["type"] == "SH"]
        self.all_lc_sh_models = {}

        for key, filename in sh_models["modeldeffile"].items():
            # all_lc_sh_models[key] = read_space_heating_load_curve_model(base_path, filename)
            self.all_lc_sh_models[key] = _load_presampled_heating_load_curve_model(filename)

    def __call__(self, district: "District") -> "District":
        buildings, load_curve = self.calculate(district.buildings, district.climate.timeseries)

        buildings = GeoDataFrame[Building](buildings).set_geometry("point")
        buildings.crs = district.crs

        return district.model_copy(
            update=dict(
                buildings=buildings,
                load_curve=load_curve,
            )
        )

    def calculate(
        self, buildings: DataFrame[DemandModelInput], temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[DemandModelResult], DataFrame[PowerLoad]]:
        buildings = DataFrame[YearlyModelResult](buildings)
        buildings = ensure_columns_for_schema(YearlyModelResult, buildings)
        index = buildings.index.copy()
        columns = buildings.columns.copy()
        heat_demands = _calculate_heat_demands(buildings)

        buildings = buildings.combine_first(heat_demands)

        buildings = _fill_efficiency_and_co2_emissions(
            buildings,
            self.system_efficiencies,
            self.fuel_carbon_intensities,
        )

        # Need to remove extra columns added by the era and heat demand estimators
        out_cols = list(set(YearlyModelResult.to_schema().columns).union(columns))
        result = buildings.loc[:, out_cols]
        result.index = index

        yearly_power, load_curve = self.calculate_load_curves(buildings, temperatures)

        buildings = buildings.combine_first(yearly_power)

        return DataFrame[DemandModelResult](buildings), load_curve

    def calculate_load_curves(
        self, buildings: DataFrame[LoadCurveModelInput], temperatures: DataFrame[ClimateRecord]
    ) -> tuple[DataFrame[YearlyPowerSummary], DataFrame[PowerLoad]]:
        # NOTE: use the parallel version for better responsiveness

        return generate_dhw_sh_load_curves_parallel(
            buildings,
            temperatures,
            lc_model_summary=self.lc_model_summary,
            building_type_model_map=self.building_type_model,
            lc_sh_models=self.all_lc_sh_models,
            dhw_model_avg1d=self.dhw_model_avg1d,
            dhw_model_dev1h=self.dhw_model_dev1h,
        )


@pa.check_types
def apply_yearly_heat_demand_model(
    buildings: DataFrame[YearlyModelInput],
    *,
    era_per_sia: DataFrame[_EraSia],
    qhww_per_sia_age: DataFrame[_QhwwAgeSia],
    clim_correction: float,
    carbon_intensities: pd.Series,
    efficiencies: pd.Series,
) -> DataFrame[YearlyModelResult]:
    """
    Apply the heat model, which means filling the fields with SIA assumptions where
    data is not provided, therefore the returned dataframe has non-energy related fields

    Args:
        buildings :  Building data
        era_per_sia:
        qhww_per_sia_age:
        clim_correction: Ratio between the heating degree days at the building location and the reference
        HDD for the building model
        carbon_intensities : Mapping (Series) from building energy system name/code to carbon
            intensity (per unit final energy)
        efficiencies : Mapping (Series) from building energy system name/code to system efficiency
            (useful /final energy)

    Returns:
        Yearly heat demand model results
    """
    # Make sure output index equals input index
    index = buildings.index.copy()
    columns = buildings.columns.copy()

    buildings = ensure_columns_for_schema(YearlyModelInput, buildings.copy())

    buildings = DataFrame[YearlyModelInput](buildings)

    buildings = _consolidate_era(buildings, era_per_sia)

    heat_demand_by_archetype = _get_heat_demand_by_archetype(buildings, qhww_per_sia_age)
    buildings = _consolidate_q_ww_m2(buildings, heat_demand_by_archetype)
    buildings = _consolidate_q_h_kwh_m2(buildings, clim_correction, heat_demand_by_archetype)
    buildings = _fill_missing_dhw_generator(buildings)

    # double check validation after input df transform
    buildings = DataFrame[YearlyModelResult](buildings)

    heat_demand_input = DataFrame[_HeatCalcInput](buildings.loc[:, list(_HeatCalcInput.to_schema().columns)])

    heat_demands = _calculate_heat_demands(heat_demand_input)

    buildings = buildings.combine_first(heat_demands)

    # heat_system = buildings[list(_EfficiencyCo2.to_schema().columns)]
    buildings = _fill_efficiency_and_co2_emissions(
        buildings,
        efficiencies,
        carbon_intensities,
    )

    # Need to remove extra columns added by the era and heat demand estimators
    out_cols = list(set(YearlyModelResult.to_schema().columns).union(columns))
    result = buildings.loc[:, out_cols]
    result.index = index

    return DataFrame[YearlyModelResult](result)


@pa.check_types
def _calculate_heat_demands(buildings: DataFrame[_HeatCalcInput]) -> DataFrame[YearlyHeatEnergy]:
    """
    Calculate total demands based on the per-m2 demands and the ERA and applying
    the is_cooled' correction

    Args:
        buildings : required columns  'q_h_kwh_m2', 'q_ww_kwh_m2', 'era', 'is_heated'

    Returns:
    """
    # Compute useful energy
    heat_demands = buildings.loc[:, ["q_h_kwh_m2", "q_ww_kwh_m2", "is_heated"]].copy()

    heat_demands["q_h_kwh"] = heat_demands["q_h_kwh_m2"] * buildings["era"]
    heat_demands["q_ww_kwh"] = heat_demands["q_ww_kwh_m2"] * buildings["era"]

    # Compute annual total heat demand in kWh
    # NOTE: DELIBERATELY don't fillna when adding up because we
    #  want to know if either value failed to compute
    heat_demands["q_hww_kwh_m2"] = heat_demands["q_h_kwh_m2"] + heat_demands["q_ww_kwh_m2"]
    heat_demands["q_hww_kwh"] = heat_demands["q_h_kwh"] + heat_demands["q_ww_kwh"]

    # Apply the 'is heated' condition. If no data, assume building is heated
    cols = ["q_h_kwh", "q_ww_kwh", "q_h_kwh_m2", "q_ww_kwh_m2", "q_hww_kwh"]
    heated = heat_demands["is_heated"].astype(float).fillna(1.0)
    heat_demands[cols] = heat_demands[cols].multiply(heated, axis="index")
    return heat_demands


@pa.check_types
def _fill_efficiency_and_co2_emissions(
    buildings: DataFrame[YearlyModelResult],
    efficiencies: pd.Series,
    carbon_intensities: pd.Series,
) -> DataFrame[YearlyModelResult]:
    """
    Update heating efficiency values and co2 emissions.
    Note: important to set each value sequentially so that at each step we can use data that was provided
    by the user to override our assumptions.

    Args:
        buildings: Buildings table possibly containing energy efficiency and co2 emissions information.
        carbon_intensities
        efficiencies

    Returns

    """
    # Lookup the efficiency values for the heating system codes.
    # Keep data that was already provided
    buildings.update(
        pd.DataFrame(
            {
                "heat_sh_efficiency": efficiencies.reindex(buildings["thermal_generator_sh"]).values,
                "heat_dhw_efficiency": efficiencies.reindex(buildings["thermal_generator_dhw"]).values,
            },
            index=buildings.index,
        ),
        overwrite=False,
    )

    # merge calculated data for heat and dhw,
    buildings.update(
        pd.DataFrame(
            {
                "q_h_final_kwh": buildings["q_h_kwh"] / buildings["heat_sh_efficiency"],
                "q_ww_final_kwh": buildings["q_ww_kwh"] / buildings["heat_dhw_efficiency"],
            }
        ),
        overwrite=False,
    )

    # update the total using the merged data, keeping values that might have been provided already
    buildings.update(
        pd.DataFrame({"q_hww_final_kwh": (buildings["q_h_final_kwh"] + buildings["q_ww_final_kwh"])}),
        overwrite=False,
    )

    buildings.update(
        pd.DataFrame(
            {
                "sh_co2_kg_kwh": carbon_intensities.reindex(buildings["fuel_sh"]).values,
                "dhw_co2_kg_kwh": carbon_intensities.reindex(buildings["fuel_dhw"]).values,
            },
            index=buildings.index,
        ),
        overwrite=False,
    )
    # merge co2 demand data for heat and dhw
    buildings.update(
        pd.DataFrame(
            {
                "co2eq_h_kg": buildings["q_h_final_kwh"] * buildings["sh_co2_kg_kwh"],
                "co2eq_ww_kg": buildings["q_ww_final_kwh"] * buildings["dhw_co2_kg_kwh"],
            }
        ),
        overwrite=False,
    )
    # merge sum of co2, keeping existing data
    buildings.update(
        pd.DataFrame(
            {"co2eq_hww_kg": (buildings["co2eq_h_kg"].fillna(0) + buildings["co2eq_ww_kg"].fillna(0))}
        ),
        overwrite=False,
    )
    # merge co2 per m2, keeping existing data
    buildings.update(
        pd.DataFrame(
            {"co2eq_hww_kg_m2": buildings["co2eq_hww_kg"].div(buildings["era"]).replace(np.inf, 0)}
        ),
        overwrite=False,
    )

    return buildings


def _fill_missing_dhw_generator(buildings):
    # Fill missing dhw with sh if needed, assuming that if no dhw source is given, is the same as sh
    missing_dhw_source = pd.isna(buildings["thermal_generator_dhw"])
    buildings.loc[missing_dhw_source, "thermal_generator_dhw"] = buildings.loc[
        missing_dhw_source, "thermal_generator_sh"
    ].copy()
    return buildings


def _consolidate_era(buildings: DataFrame[BuildingSurface], era_per_sia: DataFrame[_EraSia]) -> pd.DataFrame:
    """
    TODO document what is A APP and A_S and where they come from.

    Consolidate ERA from different sources in priority order:
    1: Input Table
    2: Use total_surface * A_S
    3: Use S_Resid Input File total_residential_surface * A_app
    4: Use total_surface
    """

    # Temporarily add the ERA estimation factors to the table
    index = buildings.index
    # 'obs_era_factor',
    buildings = buildings.merge(
        era_per_sia.loc[:, ["cat_sia", "a_s", "a_app"]], left_on="cat_sia", right_on="cat_sia", how="left"
    )
    buildings.index = index

    if "era_origin" not in buildings.columns:
        buildings["era_origin"] = None

    # priority 1: Input Table
    select = (buildings["era"] > 0) & (buildings["era_origin"].isnull())
    buildings.loc[select, "era_origin"] = "input ERA"

    # priority 2: Use total_surface * A_S
    if "total_surface" in buildings:
        total_surface_known = (~buildings["total_surface"].isna()) & (buildings["total_surface"] > 0)
        select = total_surface_known & buildings["era_origin"].isna() & ~buildings["a_s"].isna()
        buildings.loc[select, "era_origin"] = "total_surface * a_s"
        buildings.loc[select, "era"] = buildings.loc[select, "total_surface"] * buildings["a_s"]

    # priority 3: Use S_Resid Input File total_residential_surface * A_app
    if "total_surface_main_residence" in buildings:
        tot_surf_main_res_known = ~buildings["total_surface_main_residence"].isna() & (
            buildings["total_surface_main_residence"] > 0
        )
        select = tot_surf_main_res_known & buildings["era_origin"].isna() & ~buildings["a_app"].isna()

        buildings.loc[select, "era_origin"] = "residential_surface * a_app"
        buildings.loc[select, "era"] = (
            buildings["a_app"] * buildings.loc[select, "total_surface_main_residence"]
        )

    # priority 4: Use total_surface non adjusted
    if "total_surface" in buildings:
        total_surface_known = (~buildings["total_surface"].isna()) & (buildings["total_surface"] > 0)
        select = total_surface_known & buildings["era_origin"].isna()
        buildings.loc[select, "era_origin"] = "total_surface"
        buildings.loc[select, "era"] = buildings.loc[select, "total_surface"]

    buildings = buildings.drop(columns=["a_s", "a_app"])
    return buildings


class _HeatByArchetype(pa.DataFrameModel):
    avg_q_h_kwh_m2: Series[float] = pa.Field(ge=0, nullable=True)
    avg_q_ww_kwh_m2: Series[float] = pa.Field(ge=0, nullable=True)


def _get_heat_demand_by_archetype(
    buildings, qhww_sia_age: DataFrame[_QhwwAgeSia]
) -> DataFrame[_HeatByArchetype]:
    # merge first on cat and const year, then on cat only, use combine first to fill
    index = buildings.index
    match_cols = ["cat_sia", "const_period"]
    heat_demand_avg = buildings[match_cols].merge(
        qhww_sia_age[["cat_sia", "const_period", "avg_q_h_kwh_m2", "avg_q_ww_kwh_m2"]],
        left_on=["cat_sia", "const_period"],
        right_on=["cat_sia", "const_period"],
        how="left",
    )
    heat_demand_avg.index = index
    heat_demand_avg = heat_demand_avg.drop(columns=match_cols)

    qhww_sia = qhww_sia_age.groupby("cat_sia").mean(numeric_only=True).reset_index()
    match_cols = ["cat_sia"]

    heat_cols_cat_only = buildings[match_cols].merge(
        qhww_sia[["cat_sia", "avg_q_h_kwh_m2", "avg_q_ww_kwh_m2"]],
        left_on=["cat_sia"],
        right_on=["cat_sia"],
        how="left",
    )
    heat_cols_cat_only.index = index
    heat_cols_cat_only = heat_cols_cat_only.drop(columns=match_cols)

    # Fill still missing data with average by category
    heat_demand_avg = heat_demand_avg.combine_first(heat_cols_cat_only)

    heat_demand_avg.index = index
    return DataFrame[_HeatByArchetype](heat_demand_avg)


def _consolidate_q_ww_m2(
    buildings: pd.DataFrame, heat_demand_by_archetype: DataFrame[_HeatByArchetype]
) -> pd.DataFrame:
    # Consolidate Qww
    buildings["qww_origin"] = None
    buildings["qww_origin"] = buildings["qww_origin"].astype(pd.StringDtype())
    buildings["q_ww_kwh_m2"] = buildings["q_ww_kwh_m2"].astype(float)

    # priority 1: Input File
    buildings.loc[buildings["q_ww_kwh_m2"] > 0, "qww_origin"] = "input: Qww"

    #  priority 2: SIA value
    no_data = pd.isna(buildings["q_ww_kwh_m2"])
    buildings.loc[no_data, "qww_origin"] = "SIA Norm"
    buildings.loc[no_data, "q_ww_kwh_m2"] = heat_demand_by_archetype.loc[no_data, "avg_q_ww_kwh_m2"]

    return buildings


def _consolidate_q_h_kwh_m2(
    buildings: pd.DataFrame,
    clim_correction: Union[float, pd.Series],
    heat_demand_by_archetype: DataFrame[_HeatByArchetype],
) -> pd.DataFrame:
    """
    Consolidate Qh per m2 from different possible sources.
    IMPORTANT must run AFTER Energy Reference Area (ERA) column has been processed.

    Args:
        buildings:
        clim_correction:

    Returns:

    """

    # priority 1: Input table
    if "q_h_kwh_m2" not in buildings.columns:
        buildings["q_h_kwh_m2"] = np.nan

    buildings["qh_origin"] = None
    buildings["qh_origin"] = buildings["qh_origin"].astype(pd.StringDtype())

    buildings.loc[buildings["q_h_kwh_m2"] > 0, "qh_origin"] = "input: Qh/m2"

    # priority 2: total useful heat demand q_h_kwh provided
    q_h_kwh_known = (buildings["q_h_kwh"] > 0) & buildings["q_h_kwh_m2"].isna() & (buildings["era"] > 0)
    buildings.loc[q_h_kwh_known, "qh_origin"] = "input: Qh / ERA"
    buildings.loc[q_h_kwh_known, "q_h_kwh_m2"] = (
        buildings.loc[q_h_kwh_known, "q_h_kwh"] / buildings.loc[q_h_kwh_known, "era"]
    )
    # priority 3: Input table Qhww - Qww
    q_h_ww_known = (
        (buildings["q_hww_kwh_m2"] > 0) & (buildings["q_ww_kwh_m2"] > 0) & (buildings["q_h_kwh_m2"].isna())
    )

    buildings.loc[q_h_ww_known, "qh_origin"] = "input: Qhww - Qww"
    buildings.loc[q_h_ww_known, "q_h_kwh_m2"] = (
        buildings.loc[q_h_ww_known, "q_hww_kwh_m2"] - buildings.loc[q_h_ww_known, "q_ww_kwh_m2"]
    )

    # # priority 4: AVG Values per Cat_SIA and constr-Period

    no_input_data = pd.isna(buildings.q_h_kwh_m2)
    buildings.loc[no_input_data, "q_h_kwh_m2"] = (
        heat_demand_by_archetype.loc[no_input_data, "avg_q_h_kwh_m2"] * clim_correction
    )
    buildings.loc[no_input_data, "qh_origin"] = "AVG Value per SIA and Age"

    return buildings


@pa.check_types
def generate_dhw_sh_load_curves(
    buildings: DataFrame[LoadCurveModelInput],
    *,
    climate: DataFrame[ClimateRecord],
    lc_model_summary,
    building_type_model_map: DataFrame[_LcModelMapping],
    lc_sh_models,
    dhw_model_avg1d: DataFrame[_DhwAvg1Day],
    dhw_model_dev1h: pd.DataFrame,
    hours_resample=1,
) -> tuple[DataFrame[YearlyPowerSummary], DataFrame[PowerLoad]]:
    """
    Generate load curve time series
    Args:
        buildings:
        climate:
        lc_model_summary:
        building_type_model_map:
        lc_sh_models:
        dhw_model_avg1d:
        dhw_model_dev1h:
        hours_resample:

    Returns:

    """
    # Allow lower time resolution
    # NOTE: NOT actually much faster (at the moment)
    if hours_resample != 1:
        if 24 % hours_resample != 0:
            raise ValueError("hours_resample must be exact divisor of 24")
        else:
            climate = climate.resample(f"{hours_resample}h").agg(
                {"temp2m": np.mean, "t_ext_1d": np.mean, "avg_3d": np.mean, "t_ext_bin": np.min}
            )
            dhw_model_dev1h = dhw_model_dev1h.groupby(
                np.repeat(np.arange(24 // hours_resample), hours_resample), axis=1
            ).mean()

    # We need hourly values from the daily average, calculate this once for all buildings
    # Because the DHW model is shared for all buildings, init a utils hourly profile matching the weather
    # and pass to each building. This should also work with variable length climate data
    dhw_lc_common = pd.Series(
        (dhw_model_avg1d.set_index("day_of_year").loc[climate.index.dayofyear, "qww_norm_1d"].values),
        index=climate.index,
    ).fillna(0)

    buildings_model_select = _prepare_load_curve_model_select(
        buildings, building_type_model_map, lc_model_summary
    )

    # Prepare the DF with the yearly max load per building in kW
    yearly_summary = pd.DataFrame(
        columns=[
            "p_h_max1h_kw",
            "p_ww_max1h_kw",
            "p_hww_max1h_kw",
            "p_h_max1d_kw",
            "p_ww_max1d_kw",
            "p_hww_max1d_kw",
        ],
        index=buildings.index,
    )
    yearly_summary.loc[:, :] = 0  # init all values to zero

    # prepare the DF for the aggregated load curve
    aggregated_lc = pd.DataFrame(columns=["p_h_kw", "p_ww_kw", "p_hww_kw"], index=climate.index, dtype=float)
    aggregated_lc.loc[:, :] = 0.0

    # Iterate over buildings in blocks of the building model type, so that we generate LC
    # for big sets of buildings at a time instead of one row at a time: much faster.
    for model_key, buildings_group in buildings_model_select.groupby(["buildingtype_sh", "sel_model"]):
        if pd.isna(model_key[0]):
            continue
        # Get selected model for SH LC
        current_model = lc_sh_models[model_key]
        # Only model for buildings where there is heat data
        (
            building_ids,
            p_hww_kw_daily_max,
            p_hww_kw_max,
            p_hww_kw_sum,
            p_h_kw_daily_max,
            p_h_kw_max,
            p_h_kw_sum,
            p_ww_kw_daily_max,
            p_ww_kw_max,
            p_ww_kw_sum,
        ) = _generate_lc_summary(buildings_group, climate, current_model, dhw_lc_common, dhw_model_dev1h)

        # compute Max Daily and Hourly demands
        yearly_summary.loc[building_ids, "p_h_max1h_kw"] = p_h_kw_max
        yearly_summary.loc[building_ids, "p_ww_max1h_kw"] = p_ww_kw_max
        yearly_summary.loc[building_ids, "p_hww_max1h_kw"] = p_hww_kw_max

        yearly_summary.loc[building_ids, "p_h_max1d_kw"] = p_h_kw_daily_max
        yearly_summary.loc[building_ids, "p_ww_max1d_kw"] = p_ww_kw_daily_max
        yearly_summary.loc[building_ids, "p_hww_max1d_kw"] = p_hww_kw_daily_max

        # merge the total LC (Add current LC to total LC) (values is faster)
        aggregated_lc["p_h_kw"] = aggregated_lc["p_h_kw"].values + p_h_kw_sum
        aggregated_lc["p_ww_kw"] = aggregated_lc["p_ww_kw"].values + p_ww_kw_sum
        aggregated_lc["p_hww_kw"] = aggregated_lc["p_hww_kw"].values + p_hww_kw_sum

    return yearly_summary, aggregated_lc


@pa.check_types
def generate_dhw_sh_load_curves_parallel(
    buildings: DataFrame[LoadCurveModelInput],
    climate: DataFrame[ClimateRecord],
    lc_model_summary: DataFrame[_LcModelSummary],
    building_type_model_map: DataFrame[_LcModelMapping],
    lc_sh_models,
    dhw_model_avg1d: DataFrame[_DhwAvg1Day],
    dhw_model_dev1h: pd.DataFrame,
    hours_resample=1,
    n_jobs=-1,
) -> tuple[DataFrame[YearlyPowerSummary], DataFrame[PowerLoad]]:
    """

    Args:
        buildings:
        climate:
        lc_model_summary: model summary contains the list of models and to what building type they apply
        building_type_model_map: per SIA category and construction period, define the used model
        lc_sh_models: list of all LC SH models
        dhw_model_avg1d: Domestic Hot Water model
        dhw_model_dev1h: Domestic Hot Water model
        hours_resample:
        n_jobs:

    Returns:

    """

    # Allow lower time resolution
    # NOTE: NOT actually much faster (at the moment)
    if hours_resample != 1:
        if 24 % hours_resample != 0:
            raise ValueError("hours_resample must be exact divisor of 24")
        else:
            climate = climate.resample(f"{hours_resample}h").agg(
                {"temp2m": np.mean, "t_ext_1d": np.mean, "avg_3d": np.mean, "t_ext_bin": np.min}
            )
            dhw_model_dev1h = dhw_model_dev1h.groupby(
                np.repeat(np.arange(24 // hours_resample), hours_resample), axis=1
            ).mean()

    # We need hourly values from the daily average, calculate this once for all buildings
    # Because the DHW model is shared for all buildings, init a utils hourly profile matching the weather
    # and pass to each building. This should also work with variable length climate data
    dhw_lc_common = pd.Series(
        (dhw_model_avg1d.set_index("day_of_year").loc[climate.index.dayofyear, "qww_norm_1d"].values),
        index=climate.index,
    ).fillna(0)

    buildings_model_select = _prepare_load_curve_model_select(
        buildings, building_type_model_map, lc_model_summary
    )

    # Prepare the DF with the yearly max load per building in kW
    yearly_summary = pd.DataFrame(
        columns=list(YearlyPowerSummary.to_schema().columns),
        index=buildings.index,
        dtype=float,
    )
    yearly_summary.loc[:, :] = 0.0  # init all values to zero

    # prepare the DF for the aggregated load curve
    aggregated_lc = pd.DataFrame(columns=["p_h_kw", "p_ww_kw", "p_hww_kw"], index=climate.index, dtype=float)
    aggregated_lc.loc[:, :] = 0.0

    from joblib import Parallel, delayed

    # Iterate over buildings in blocks of the building model type, so that we generate LC
    # for big sets of buildings at a time instead of one row at a time: much faster.
    # Run it in parallel with threading for now
    results = Parallel(backend="threading", n_jobs=n_jobs)(
        delayed(_generate_lc_summary)(
            buildings_group, climate, lc_sh_models[model_key], dhw_lc_common, dhw_model_dev1h
        )
        for model_key, buildings_group in buildings_model_select.groupby(["buildingtype_sh", "sel_model"])
        if not pd.isna(model_key[0])
    )

    # Assign the results
    for (
        building_ids,
        p_hww_kw_daily_max,
        p_hww_kw_max,
        p_hww_kw_sum,
        p_h_kw_daily_max,
        p_h_kw_max,
        p_h_kw_sum,
        p_ww_kw_daily_max,
        p_ww_kw_max,
        p_ww_kw_sum,
    ) in results:
        yearly_summary.loc[building_ids, "p_h_max1h_kw"] = p_h_kw_max
        yearly_summary.loc[building_ids, "p_ww_max1h_kw"] = p_ww_kw_max
        yearly_summary.loc[building_ids, "p_hww_max1h_kw"] = p_hww_kw_max

        yearly_summary.loc[building_ids, "p_h_max1d_kw"] = p_h_kw_daily_max
        yearly_summary.loc[building_ids, "p_ww_max1d_kw"] = p_ww_kw_daily_max
        yearly_summary.loc[building_ids, "p_hww_max1d_kw"] = p_hww_kw_daily_max

        # merge the total LC (Add current LC to total LC) (values is faster)
        aggregated_lc["p_h_kw"] = aggregated_lc["p_h_kw"].values + p_h_kw_sum
        aggregated_lc["p_ww_kw"] = aggregated_lc["p_ww_kw"].values + p_ww_kw_sum
        aggregated_lc["p_hww_kw"] = aggregated_lc["p_hww_kw"].values + p_hww_kw_sum
    yearly_summary["p_h_max1h_kw"] = yearly_summary["p_h_max1h_kw"].astype(float)
    return yearly_summary, aggregated_lc
    # return DataFrame[_LoadCurveYearlySummary](yearly_summary), DataFrame[_AggregatedLoadCurve](aggregated_lc)


def _generate_lc_summary(buildings_group, climate, current_model, dhw_lc_common, dhw_model_dev1h):
    buildings_group = buildings_group[~pd.isna(buildings_group.q_h_kwh)]
    building_ids = buildings_group.index
    q_h_kwh = buildings_group["q_h_kwh"]
    q_ww_kwh = buildings_group["q_ww_kwh"]
    climate_hourly = climate
    perturbations_presampled = current_model["perturbations_presampled"]

    t0 = current_model["t0"]
    t_nc = current_model["t_nc"]
    a_sign = current_model["a_sign"]
    # generate SH LC
    p_h_kw = generate_space_heating_load_curve(
        climate_hourly, perturbations_presampled, t0, t_nc, a_sign, building_ids
    )
    # upscale unitary LC
    p_h_kw = p_h_kw * q_h_kwh
    # generate and upscale DHW LC
    p_ww_kw = generate_dhw_load_curve(dhw_lc_common, dhw_model_dev1h, building_ids)
    p_ww_kw = p_ww_kw * q_ww_kwh
    # compute total demand
    p_hww_kw = p_h_kw + p_ww_kw
    p_h_kw_max = p_h_kw.max()
    p_ww_kw_max = p_ww_kw.max()
    p_hww_kw_max = p_hww_kw.max()

    p_h_kw_sum = p_h_kw.sum(axis=1).values
    p_ww_kw_sum = p_ww_kw.sum(axis=1).values
    p_hww_kw_sum = p_hww_kw.sum(axis=1).values
    # Maximum of daily mean
    p_h_kw_daily_max = p_h_kw.resample("1D").mean().max()
    p_ww_kw_daily_max = p_ww_kw.resample("1D").mean().max()
    p_hww_kw_daily_max = p_hww_kw.resample("1D").mean().max()
    return (
        building_ids,
        p_hww_kw_daily_max,
        p_hww_kw_max,
        p_hww_kw_sum,
        p_h_kw_daily_max,
        p_h_kw_max,
        p_h_kw_sum,
        p_ww_kw_daily_max,
        p_ww_kw_max,
        p_ww_kw_sum,
    )


def generate_dhw_load_curve(base_dhw_load_curve, dhw_model_perturbations, building_ids=None):
    """
    The hourly normalizd DHW load curve is generated using a random daily deviation.

    IMPORTANT: input data MUST be sorted by date! Maybe should use date min/max?
    Parameters:
        base_dhw_load_curve :
        dhw_model_perturbations :
        building_ids

    Returns:
    DataFrame
        The DataFrame will contain the hourly fraction of the DHW demand
    """
    if building_ids is None:
        building_ids = [0]
    n_buildings = len(building_ids)
    # Note: doing index[i].date() is much faster than index.date[i]!
    # probably because index.date generates the whole date column first
    n_days = (base_dhw_load_curve.index[-1].date() - base_dhw_load_curve.index[0].date()).days + 1

    hourly_samples = (
        dhw_model_perturbations.sample(n_buildings * n_days, replace=True)
        .reset_index(drop=True)
        .stack()
        .values
    )
    hourly_samples = hourly_samples.reshape((n_buildings, n_days * 24)).T
    qww_load_curve = pd.DataFrame(hourly_samples, index=base_dhw_load_curve.index, columns=building_ids)
    qww_load_curve = (-qww_load_curve).add(base_dhw_load_curve, axis="rows")
    qww_load_curve = qww_load_curve / qww_load_curve.sum()
    return qww_load_curve


def generate_space_heating_load_curve(
    climate_hourly: DataFrame[ClimateRecord], perturbations_presampled, t0, t_nc, a_sign, building_ids=None
) -> pd.Series:
    """
    The hourly normalized space heating load curve is generated using the chosen climate.

    Args:
        climate_hourly
        perturbations_presampled
        t0
        t_nc: Temperature threshold to start heating
        a_sign
        building_ids
    Returns:
        Heat demand load curve
    """
    # Allow to generate for a single building by passing none as building id list
    if building_ids is None:
        building_ids = [0]
    n_buildings = len(building_ids)

    # Compute begin and end of heating season
    # heating ends when moving AVG of t_ext_1d >= T_nc  and heating starts again when t_ext_1d <= T_nc
    qh_daily = _qh_daily_signature(a_sign, t0, climate_hourly["temp2m"].values)
    is_heating = _get_heating_season(climate_hourly["avg_3d"], t_nc)

    # Only need to copy t ext bin, not whole DF.
    t_ext_bin = climate_hourly["t_ext_bin"].copy()

    ts = t_ext_bin
    ts = ts.astype(int)
    ts = ts.values
    ts = ts.clip(perturbations_presampled.index.min(), perturbations_presampled.index.max())

    sel = np.random.randint(0, perturbations_presampled.shape[1] - 1, (len(ts), n_buildings))
    row_sel = perturbations_presampled.index.get_indexer(ts)
    res = perturbations_presampled.values[row_sel[:, np.newaxis], sel]

    delt_qh = pd.DataFrame(res, index=climate_hourly.index, columns=building_ids)

    # force daily average demand being >= 0
    qh_daily = np.nan_to_num(qh_daily, copy=False)
    qh_daily[~is_heating] = 0
    qh_daily = np.maximum(qh_daily, 0)

    # hourly demand = daily average + selected random deviation
    # qh_load_curve = qh_daily + delt_qh
    qh_load_curve = delt_qh + np.expand_dims(qh_daily, 1)

    # force hourly demand being >= 0
    qh_load_curve = np.maximum(qh_load_curve, 0)
    qh_load_curve.loc[~is_heating] = 0

    # normlize to 1kWh
    qh_load_curve = qh_load_curve / (qh_load_curve.sum())
    return qh_load_curve


def generate_space_heating_load_curve_original(
    climate_hourly: pd.DataFrame, perturbations_by_t_bin, t0, t_nc, a_sign, building_ids=None
) -> pd.Series:
    """
    The hourly normalized space heating load curve is generated using the chosen climate.

    Args:
        climate_hourly
        perturbations_by_t_bin
        t0
        t_nc
        a_sign
        building_ids
    Returns:
        Heat demand load curve
    """
    # Allow to generate for a single building by passing none as building id list
    if building_ids is None:
        building_ids = [0]
    n_buildings = len(building_ids)

    # Compute begin and end of heating season
    # heating ends when moving AVG of t_ext_1d >= T_nc  and heating starts again when t_ext_1d <= T_nc
    qh_daily = _qh_daily_signature(a_sign, t0, climate_hourly["temp2m"].values)
    is_heating = _get_heating_season(climate_hourly["avg_3d"], t_nc)

    # Only need to copy t ext bin, not whole DF.
    t_ext_bin: pd.Series = climate_hourly.t_ext_bin.copy()

    # The perturbation model is a dict mapping the temperature bin to an array
    # of possible perturbation values for that bin.
    # Sometimes there is not data in the model for high climate, need to fill those
    pmax = max(set(perturbations_by_t_bin.keys()) - {-99, 99})
    if t_ext_bin.max() > pmax:
        # Note: using .values makes this much faster (2x) by skipping index matching
        t_ext_bin[t_ext_bin.values > pmax] = 99
    # Model gives random deviation options for each temperature bin
    # For performance, we generate an array ordered by temperature bin, with as many values
    # for each bin as there are in the input data. Then add this as a column to
    # the dataframe and re-sort by timestamp.
    mapper = t_ext_bin.value_counts()
    samples = np.concatenate(
        [
            np.random.choice(pert_data, (mapper.get(k, 0), n_buildings), replace=True)
            for k, pert_data in perturbations_by_t_bin.items()
        ]
    )

    if len(samples) != len(climate_hourly):
        raise ValueError("Problem in Load curve input data")

    # Assign the results
    sorted_index = t_ext_bin.sort_values().index
    # put 0 for the high climate where no bin available then reindex to match the input time series
    # delt_qh = pd.Series(np.nan_to_num(samples), index=sorted_index).reindex(climate_hourly.index)
    delt_qh = pd.DataFrame(np.nan_to_num(samples), columns=building_ids, index=sorted_index).reindex(
        climate_hourly.index
    )

    # force daily average demand being >= 0
    qh_daily = np.nan_to_num(qh_daily, copy=False)
    qh_daily[~is_heating] = 0
    qh_daily = np.maximum(qh_daily, 0)

    # hourly demand = daily average + selected random deviation
    # qh_load_curve = qh_daily + delt_qh
    qh_load_curve = delt_qh + np.expand_dims(qh_daily, 1)

    # force hourly demand being >= 0
    qh_load_curve = np.maximum(qh_load_curve, 0)
    # qh_load_curve = np.where(is_heating, qh_load_curve, 0)
    # qh_load_curve = np.where( np.expand_dims(is_heating , 1), qh_load_curve, 0)
    qh_load_curve.loc[~is_heating] = 0

    # normlize to 1kWh
    qh_load_curve = qh_load_curve / (qh_load_curve.sum())
    return qh_load_curve


def _get_heating_season(average_3day_temperature: pd.Series, heating_threshold_temperature) -> pd.Series:
    """
    For each building set the heating season

    Original model: heating season is on/off affair, starting as soon as
    temperature drops below temperature_of_non_cooling and finishing as soon as
    it increases above the value.

    TBH I'm less convinced: not super robust because given a single hot day in the
    cold season could prematurely end the heating season. Better just set per day and
    clear JJA summer season.

    Args:
        average_3day_temperature:
        heating_threshold_temperature:

    Returns:

    """
    is_heating = average_3day_temperature <= heating_threshold_temperature
    ## 1st June to 31st Aug
    is_heating = is_heating & (is_heating.index.day_of_year < 152) | (is_heating.index.day_of_year > 243)

    return is_heating
    # climate['is_heating'] = heating_season
    # heating_season = climate[(climate.avg_3d >= heating_threshold_temperature)]
    # heating_season.index.min()
    # heating_season.index.max()
    # climate['is_heating'] = False
    # climate.loc[:heating_season.index.min(), 'is_heating'] = True
    # climate.loc[heating_season.index.max():, 'is_heating'] = True
    # return climate


def _qh_daily_signature(a_sign, t0, t_ext):
    # apply daily signature to compute average daily load
    n_timesteps = len(t_ext)
    v0 = np.zeros(n_timesteps)
    vx2 = np.ones(n_timesteps) * t0
    c_delt = np.maximum(v0, (vx2 - t_ext))
    qh_daily = a_sign * c_delt
    return qh_daily


def _prepare_load_curve_model_select(
    buildings,
    building_type_model_map: DataFrame[_LcModelMapping],
    lc_model_summary: DataFrame[_LcModelSummary],
):
    # for each building JOIN LC model BuildingTYPE_SH
    # buildings = _ensure_required_load_curve_columns(buildings.copy())
    buildings = DataFrame[LoadCurveModelInput](buildings.copy())
    index = buildings.index.copy()
    buildings_model_select = (
        buildings[["cat_sia", "const_period", "q_h_kwh", "q_ww_kwh"]]
        .merge(
            building_type_model_map[["cat_sia", "sel_epoque_construction", "buildingtype_sh"]],
            left_on=["cat_sia", "const_period"],
            right_on=["cat_sia", "sel_epoque_construction"],
            how="left",
        )
        .drop(["sel_epoque_construction"], axis="columns")
    )
    # The merges reset the index of the input, ensure we have the right index again afterwards
    buildings_model_select.index = index

    # get number of available models for each row's building type so we can select one at random
    lc_model_count = lc_model_summary.reset_index()["buildingtype"].value_counts()
    nb_models = lc_model_count.reindex(buildings_model_select["buildingtype_sh"].values)
    nb_models.index = index
    # select a random model for each model type. Allow that model might be None
    rng = np.random.default_rng()
    to_gen = ~nb_models.values.isna()
    buildings_model_select.loc[to_gen, "sel_model"] = rng.integers(
        1, nb_models.loc[to_gen] + 1, nb_models.count()
    )

    return buildings_model_select
