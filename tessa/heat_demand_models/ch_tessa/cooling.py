from dataclasses import InitVar, dataclass, field

import numpy as np
import pandas as pd
import pandera as pa
import structlog
import xarray as xr
from pandera.typing import DataFrame, Series

from tessa.core import Settings
from tessa.core.models import PowerLoad
from tessa.heat_demand_models.ch_tessa.model_parameters import (
    _ArchetypeDetail,
    _ArchetypeInput,
    _FuelTable,
    _load_archetype_detail,
    _load_fuel_defaults,
    _load_qc_by_archetype,
    _load_thermal_generators_defaults,
    _QcArchetype,
    _ThermalGeneratorTable,
)
from tessa.heat_demand_models.ch_tessa.pace import open_pace_dataset_from_cloud
from tessa.heat_demand_models.common import (
    BuildingSurface,
    CoolingDemandModel,
    CoolingDemandModelResult,
    CoolingEfficiencyEmissions,
    CoolingPowerLoad,
    YearlyCoolingEnergy,
    YearlyCoolingModelResult,
    YearlyCoolingPowerSummary,
)
from tessa.utils.pandera_extras import ensure_columns_for_schema

log = structlog.get_logger()


class YearlyCoolingModelInput(BuildingSurface, YearlyCoolingEnergy, CoolingEfficiencyEmissions):
    const_class: Series[str] = pa.Field(nullable=True, coerce=True)
    const_period: Series[str] = pa.Field(nullable=True, coerce=True)
    canton: Series[str] = pa.Field(nullable=True, coerce=True)


class CoolingLoadCurveModelInput(pa.DataFrameModel):
    archetype: Series[pd.Int64Dtype] = pa.Field(nullable=True, coerce=True)
    q_c_kwh: Series[float] = pa.Field(nullable=True, coerce=True)


class CoolingDemandModelInput(CoolingLoadCurveModelInput, YearlyCoolingModelInput):
    pass


@dataclass
class SwissCoolingDemandModel(CoolingDemandModel):
    """
    Calculate space cooling demand based PACE EMPA database.

    TODO consider cooling degree days.
    """

    pace_dataset: xr.Dataset | str | None = None
    s3_id: InitVar[str | None] = None
    s3_secret: InitVar[str | None] = None
    s3_endpoint_override: InitVar[str] = ""

    reference_year: int = 2015
    is_retrofitted: bool = True

    qc_by_archetype: DataFrame[_QcArchetype] = field(default_factory=_load_qc_by_archetype)
    archetype_detail: DataFrame[_ArchetypeDetail] = field(default_factory=_load_archetype_detail)
    thermal_generator_definitions: DataFrame[_ThermalGeneratorTable] = field(
        default_factory=_load_thermal_generators_defaults
    )
    fuel_definitions: DataFrame[_FuelTable] = field(default_factory=_load_fuel_defaults)

    def __post_init__(self, s3_id, s3_secret, s3_endpoint_override):
        if isinstance(self.pace_dataset, str):
            self.pace_dataset = open_pace_dataset_from_cloud(
                path=self.pace_dataset, key=s3_id, secret=s3_secret, endpoint_url=s3_endpoint_override
            )

    @property
    def fuel_carbon_intensities(self):
        return self.fuel_definitions.set_index("code").carbon_intensity

    @property
    def system_efficiencies(self):
        return self.thermal_generator_definitions.set_index("code").efficiency

    def calculate_yearly_cooling(
        self,
        buildings: DataFrame[YearlyCoolingModelInput],
        *,
        is_retrofitted: bool | None = None,
        sim_year: int | None = None,
    ) -> DataFrame[YearlyCoolingModelResult]:
        """
        Calculate yearly space cooling demand for a set of buildings.

        The provided building table will be returned with the heat demand (useful and final),
        efficiency and co2 estimates columns added or updated. If the provided buildings data already
        includes some values (see the fields in schemas BuildingSurface, YearlyHeatEnergy,
        EfficiencyEmissions), the provided values will be kept and missing ones calculated on a
        'best effort basis'.

        For example, if you provide the total useful heat demand of the building and the heat system
        type and fuel source, we calculate the final energy and co2 emissions.
        """
        if is_retrofitted is None:
            is_retrofitted = self.is_retrofitted
        if sim_year is None:
            sim_year = self.reference_year

        # Make sure optional columns exist
        buildings = ensure_columns_for_schema(YearlyCoolingModelInput, buildings.copy())

        return apply_yearly_cooling_demand_model(
            buildings,
            is_retrofitted=is_retrofitted,
            sim_year=sim_year,
            qc_per_archetype=self.qc_by_archetype,
            archetype_detail=self.archetype_detail,
            carbon_intensities=self.fuel_carbon_intensities,
            efficiencies=self.system_efficiencies,
        )

    def calculate_load_curves_cooling(
        self,
        buildings: DataFrame[CoolingLoadCurveModelInput],
        heat_load_curves: DataFrame[PowerLoad],
        *,
        is_retrofitted: bool | None = None,
        sim_year: int | None = None,
    ) -> tuple[DataFrame[YearlyCoolingPowerSummary], DataFrame[CoolingPowerLoad]]:
        # NOTE: use the parallel version for better responsiveness

        if is_retrofitted is None:
            is_retrofitted = self.is_retrofitted
        if sim_year is None:
            sim_year = self.reference_year

        return generate_cooling_load_curves(
            buildings,
            heat_load_curves,
            is_retrofitted=is_retrofitted,
            sim_year=sim_year,
            pace_dataset=self.pace_dataset,
        )

    def calculate_cooling(
        self,
        buildings: DataFrame[CoolingDemandModelInput],
        heat_load_curves: DataFrame[PowerLoad],
        *,
        is_retrofitted: bool | None = None,
        sim_year: int | None = None,
    ) -> tuple[DataFrame[CoolingDemandModelResult], DataFrame[CoolingPowerLoad]]:
        """
        TODO not return 'archetype' in buildings
        Parameters
        ----------
        buildings
        heat_load_curves

        Returns
        -------

        """
        if is_retrofitted is None:
            is_retrofitted = self.is_retrofitted
        if sim_year is None:
            sim_year = self.reference_year

        # ensure_columns_for_schema turns None values into False in 'is_cooled'
        buildings = ensure_columns_for_schema(CoolingDemandModelResult, buildings)
        buildings = self.calculate_yearly_cooling(
            buildings.copy(),
            is_retrofitted=is_retrofitted,
            sim_year=sim_year,
        )

        cooling_yearly_power, cooling_load_curve = self.calculate_load_curves_cooling(
            buildings,
            heat_load_curves,
            is_retrofitted=is_retrofitted,
            sim_year=sim_year,
        )

        buildings = buildings.combine_first(cooling_yearly_power)

        return DataFrame[CoolingDemandModelResult](buildings), cooling_load_curve


@pa.check_types
def apply_yearly_cooling_demand_model(
    buildings: DataFrame[YearlyCoolingModelInput],
    *,
    is_retrofitted: bool,
    sim_year: int,
    qc_per_archetype: DataFrame[_QcArchetype],
    archetype_detail: DataFrame[_ArchetypeDetail],
    carbon_intensities: pd.Series,
    efficiencies: pd.Series,
) -> DataFrame[YearlyCoolingModelResult]:
    """
    Assign yearly cooling demand by matching the archetype in PACE EMPA data model.

    Parameters
    ----------
    buildings
    qc_per_archetype
    archetype_detail
    carbon_intensities
    efficiencies

    Returns
    -------

    """
    # Make sure output index equals input index
    index = buildings.index.copy()
    columns = buildings.columns.copy()

    buildings = ensure_columns_for_schema(YearlyCoolingModelInput, buildings.copy())

    buildings = DataFrame[YearlyCoolingModelInput](buildings)

    buildings = _consolidate_archetype(buildings, archetype_detail)

    cooling_demand_by_archetype = _get_cooling_demand_by_archetype(
        buildings,
        qc_per_archetype,
        is_retrofitted=is_retrofitted,
        sim_year=sim_year,
    )
    buildings = _consolidate_q_c_kwh_m2(buildings, cooling_demand_by_archetype)

    buildings = _fill_missing_cooling_generator(buildings)
    buildings = _fill_missing_cooling_fuel(buildings)

    # double check validation after input df transform
    buildings = DataFrame[YearlyCoolingModelResult](buildings)

    cooling_demand_input = DataFrame[_CoolingCalcInput](
        buildings.loc[:, list(_CoolingCalcInput.to_schema().columns)]
    )

    cooling_demands = _calculate_cooling_demands(cooling_demand_input)

    buildings = buildings.combine_first(cooling_demands)

    buildings = _fill_cooling_efficiency_and_co2_emissions(
        buildings,
        efficiencies,
        carbon_intensities,
    )

    buildings = _fill_temperature_requirement(buildings)

    # Need to remove extra columns added by the era and heat demand estimators
    out_cols = list(set(YearlyCoolingModelResult.to_schema().columns).union(columns))
    result = buildings.loc[:, out_cols]
    result.index = index

    return DataFrame[YearlyCoolingModelResult](result)


# @pa.check_types
def _consolidate_archetype(
    buildings: DataFrame[_ArchetypeInput],
    archetype_detail: DataFrame[_ArchetypeDetail],
) -> pd.DataFrame:
    """
    Assign archetype to each building by matching type, year of construction, region, and total area.
    Assign building type according to the categories used by MANGOret

    Args:
        buildings
        archetype_detail

    Returns:

    """

    # matching based on 'type, region, year_of_construction, building_floor_area_m'
    blds = DataFrame[_ArchetypeInput](buildings.copy())

    blds["type"] = blds.const_class
    blds.loc[blds.const_class.isin(["Maison à 3+ logements", "Maison à 2 logements"]), "type"] = "MFH"
    blds.loc[blds.const_class == "Maison individuelle", "type"] = "SFH"
    blds.loc[
        blds.const_class.isin(
            [
                "Édifice cult./religieux",
                "Bât. enseign./recherche",
                "Bât. à usage cult./récr.",
                "Musée / bibliothèque",
                "Salle de sport",
            ]
        ),
        "type",
    ] = "Education"
    blds.loc[
        blds.const_class.isin(["Autre hébergement", "Habitat communautaire", "Hôtel"]), "type"
    ] = "Hospitality"
    blds.loc[blds.const_class.isin(["Bâtiment industriel", "Exploitation agricole"]), "type"] = "Hospitality"
    blds.loc[blds.const_class == "Bâtiment commercial", "type"] = "Retail"
    blds.loc[blds.const_class == "Hôpital/établ. de santé", "type"] = "Health"
    blds.loc[blds.const_class == "Immeuble de bureaux", "type"] = "Offices"
    # Fill any missing const_class with the most common const_class
    blds.loc[blds["type"].isna(), "type"] = blds.type.dropna().mode().squeeze()

    # Assign region according to the categories used by MANGOret
    blds["region"] = blds.canton
    blds.loc[blds.canton.isin(["ZG", "OW", "SZ", "NW", "UR", "LU"]), "region"] = "Central"
    blds.loc[blds.canton.isin(["VD", "VS", "GE"]), "region"] = "Lake-Geneva"
    blds.loc[blds.canton.isin(["SG", "SH", "AI", "GR", "AR", "GL", "TG"]), "region"] = "NE"
    blds.loc[blds.canton.isin(["BL", "BS", "AG"]), "region"] = "NW"
    blds.loc[blds.canton.isin(["JU", "FR", "SO", "NE", "BE"]), "region"] = "West"
    blds.loc[blds.canton == "TI", "region"] = "Ticino"
    blds.loc[blds.canton == "ZH", "region"] = "Zurich"
    # Fill any missing cantons with the most common region
    blds.loc[blds.region.isna(), "region"] = blds.region.mode()

    # Assign building construction period according to the categories used by MANGOret
    blds["year_of_construction"] = blds.const_period
    blds.loc[blds.const_period == "0-1918", "year_of_construction"] = "-1919"
    blds.loc[blds.const_period == "2011-2020", "year_of_construction"] = "2011-"

    # Matching egid and fid (archetype of MANGOret)
    # Firstly match region, type, year of construction. Secondly, select the archetype that have the closest floor area.
    blds["total_surface"] = blds.total_surface.fillna(0)

    buildings_sorted = blds.sort_values("total_surface").reset_index()
    archetype_detail_sorted = archetype_detail[
        ["archetype", "type", "region", "year_of_construction", "building_floor_area_m2"]
    ].sort_values("building_floor_area_m2")

    temp = pd.merge_asof(
        buildings_sorted,
        archetype_detail_sorted,
        left_by=["type", "region", "year_of_construction"],
        left_on="total_surface",
        right_by=["type", "region", "year_of_construction"],
        right_on="building_floor_area_m2",
        direction="nearest",
    ).set_index("egid")

    temp = temp.rename(columns={"archetype_y": "archetype"})
    buildings["archetype"] = pd.Series(index=buildings.index, dtype=pd.Int64Dtype())
    buildings.update(temp["archetype"])

    return buildings


def _get_cooling_demand_by_archetype(
    buildings,
    qc_per_archetype: DataFrame,
    *,
    is_retrofitted: bool = True,
    sim_year: int = 2030,
    rcp=45,
) -> pd.DataFrame:
    """
    Get the relationship between cooling demand and archetype
    Args:
        buildings
        qc_per_archetype

    Returns:

    """
    # select scenario. retrofit_measure: 'Full' / 'noretrofit';
    # rcp: 26 / 45 / 85; simulation_year: 2020 / 2030 / 2040 / 2050 / 2060.
    # default: 'noretrofit', 45, 2020
    sim_year = min([2020, 2030, 2040, 2050, 2060], key=lambda x: abs(x - sim_year))
    if is_retrofitted:
        retrofit_option = "Full"
    else:
        retrofit_option = "noretrofit"

    qc_per_archetype = qc_per_archetype.loc[
        (qc_per_archetype.retrofit_measure == retrofit_option)
        & (qc_per_archetype.rcp == rcp)
        & (qc_per_archetype.simulation_year == sim_year)
    ]

    # merge on archetype
    index = buildings.index
    match_cols = ["archetype"]
    cooling_demand_avg = buildings[match_cols].merge(
        qc_per_archetype[["archetype", "avg_q_c_kwh_m2_floor_area"]],
        left_on=["archetype"],
        right_on=["archetype"],
        how="left",
    )
    cooling_demand_avg.index = index
    cooling_demand_avg = cooling_demand_avg.drop(columns=match_cols)

    return cooling_demand_avg


class _CoolingCalcInput(pa.DataFrameModel):
    era: Series[float] = pa.Field(nullable=True)
    is_cooled: Series[bool] = pa.Field(nullable=True)
    q_c_kwh_m2: Series[float] = pa.Field(nullable=True)
    is_heated: Series[bool] = pa.Field(nullable=True)


def _consolidate_q_c_kwh_m2(
    buildings: pd.DataFrame,
    cooling_demand_by_archetype: pd.DataFrame,
) -> pd.DataFrame:
    """
    Consolidate q_c_kwh_m2 from various sources.

    Returns:

    """
    # priority 1: Input table
    if "q_c_kwh_m2" not in buildings.columns:
        buildings["q_c_kwh_m2"] = np.nan

    buildings["qc_origin"] = None
    buildings["qc_origin"] = buildings["qc_origin"].astype(pd.StringDtype())

    buildings.loc[buildings["q_c_kwh_m2"] > 0, "qc_origin"] = "input: Qc/m2"

    # priority 2: total useful cooling demand q_c_kwh provided
    q_c_kwh_known = (buildings["q_c_kwh"] > 0) & buildings["q_c_kwh_m2"].isna() & (buildings["era"] > 0)
    buildings.loc[q_c_kwh_known, "qc_origin"] = "input: Qc / ERA"
    buildings.loc[q_c_kwh_known, "q_c_kwh_m2"] = (
        buildings.loc[q_c_kwh_known, "q_c_kwh"] / buildings.loc[q_c_kwh_known, "era"]
    )

    # # priority c: AVG Values per Cat_SIA and constr-Period

    no_input_data = pd.isna(buildings.q_c_kwh_m2)
    buildings.loc[no_input_data, "q_c_kwh_m2"] = (
        cooling_demand_by_archetype.loc[no_input_data, "avg_q_c_kwh_m2_floor_area"]
        * buildings.loc[no_input_data, "total_surface"]
        / buildings.loc[no_input_data, "era"]
    )
    buildings.loc[no_input_data, "qc_origin"] = "PACE archetype"

    return buildings


def _fill_missing_cooling_generator(buildings):
    """
    If cooling generator is unknown, assume it's an air conditioner.
    Returns
    -------

    """
    missing_cooling_source = pd.isna(buildings["thermal_generator_c"]) & buildings["is_cooled"]
    buildings.loc[missing_cooling_source, "thermal_generator_c"] = "air_conditioner"

    return buildings


def _fill_missing_cooling_fuel(buildings):
    """
    If cooling fuel is unknown, assume it's electricity.
    Returns
    -------

    """
    missing_cooling_fuel = pd.isna(buildings["fuel_c"]) & buildings["is_cooled"]
    buildings.loc[missing_cooling_fuel, "fuel_c"] = "electricity"

    return buildings


@pa.check_types
def _calculate_cooling_demands(buildings) -> pd.DataFrame:
    """
    Calculate total demands based on the per-m2 demands and the ERA and applying
    the 'is_cooled' correction

    Args:
        buildings : required columns  'q_c_kwh_m2', 'era', 'is_cooled', 'is_heated'.

    Returns:
    """
    # The next two lines of code will never work, since 'is_cooled' is ensured to be 'bool' type.
    # See the comment in SwissCoolingDemandModel.calculate_cooling
    # Apply the 'is cooled' condition. If no data, assume building is not cooled.
    missing_cooled = pd.isna(buildings["is_cooled"])
    buildings.loc[missing_cooled, "is_cooled"] = False

    # Compute useful energy
    cooling_demands = buildings.loc[:, ["q_c_kwh_m2", "is_cooled"]].copy()

    cooling_demands["q_c_kwh"] = cooling_demands["q_c_kwh_m2"] * buildings["era"]

    cols = ["q_c_kwh", "q_c_kwh_m2"]
    cooled = cooling_demands["is_cooled"].astype(float)
    cooling_demands[cols] = cooling_demands[cols].multiply(cooled, axis="rows")
    return cooling_demands


@pa.check_types
def _fill_cooling_efficiency_and_co2_emissions(
    buildings: DataFrame,
    efficiencies: pd.Series,
    carbon_intensities: pd.Series,
) -> DataFrame[YearlyCoolingModelResult]:
    # Set the efficiency for the corresponding heating system.

    # Lookup the efficiency values for the cooling system codes.
    # Keep data that was already provided e.g., from the user.
    cooling_eff = efficiencies.reindex(buildings["thermal_generator_c"])
    cooling_eff.index = buildings.index
    cooling_eff.name = "thermal_c_efficiency"
    buildings.update(cooling_eff, overwrite=False)

    buildings["q_c_final_kwh"].update(buildings["q_c_kwh"] / buildings["thermal_c_efficiency"])

    cooling_co2 = carbon_intensities.reindex(buildings["fuel_c"])
    cooling_co2.index = buildings.index
    buildings["c_co2_kg_kwh"].update(cooling_co2)
    buildings["co2eq_c_kg"].update(buildings["q_c_final_kwh"] * buildings["c_co2_kg_kwh"])

    return buildings


def _fill_temperature_requirement(buildings):
    # Fill temperature requirement for space heating, domestic hot water, space cooling.
    # TODO:make it possible to consider user input.

    # space heating
    buildings.loc[(~buildings.is_cooled) & (buildings["q_h_kwh_m2"] >= 60), "temp_h_deg"] = 60
    buildings.loc[buildings.is_cooled & (buildings["q_h_kwh_m2"] >= 60), "temp_h_deg"] = 45
    buildings.loc[buildings["q_h_kwh_m2"] < 60, "temp_h_deg"] = 35

    # DHW
    buildings["temp_ww_deg"] = 60  # default 60°C

    # space cooling
    # buildings already have the columns 'q_c_kwh', 'q_c_kwh_m2', 'thermal_generator_c', 'const_class', 'const_period' blabla
    # Assume that new building constructed after 2010 have floor cooling /
    buildings["temp_c_deg"] = 7  # default option: fan coil requires 7 °C.
    buildings.loc[buildings.const_period == "2011-2020", "temp_c_deg"] = 17  # ceiling cooling / floor cooling

    return buildings


@pa.check_types
def generate_cooling_load_curves(
    buildings: DataFrame[CoolingLoadCurveModelInput],
    heat_load_curves: DataFrame[PowerLoad],
    *,
    is_retrofitted: bool,
    sim_year: int,
    pace_dataset=None,
) -> tuple[DataFrame[YearlyCoolingPowerSummary], DataFrame[CoolingPowerLoad]]:
    # Prepare the DF with the yearly max load per building in kW
    yearly_summary = pd.DataFrame(
        columns=[
            "p_c_max1h_kw",
            "p_c_max1d_kw",
        ],
        index=buildings.index,
    )
    yearly_summary.loc[:, :] = 0.0  # init all values to zero

    # prepare the DF for the aggregated load curve
    aggregated_lc = heat_load_curves.copy(deep=True)
    aggregated_lc["p_c_kw"] = 0.0

    # Iterate over buildings in blocks of the building archetype, so that we generate LC
    # for big sets of buildings at a time instead of one row at a time: much faster.
    for model_key, buildings_group in buildings.groupby(["archetype"]):
        if pd.isna(model_key[0]):
            continue
        # Only model for buildings where there is cooling data
        (
            building_ids,
            p_c_kw_daily_max,
            p_c_kw_max,
            p_c_kw_sum,
        ) = _generate_cooling_lc_summary(
            buildings_group,
            heat_load_curves,
            is_retrofitted=is_retrofitted,
            sim_year=sim_year,
            pace_dataset=pace_dataset,
        )

        # compute Max Daily and Hourly demands
        yearly_summary.loc[building_ids, "p_c_max1h_kw"] = p_c_kw_max
        yearly_summary.loc[building_ids, "p_c_max1d_kw"] = p_c_kw_daily_max

        # merge the total LC (Add current LC to total LC) (values is faster)
        aggregated_lc["p_c_kw"] = aggregated_lc["p_c_kw"].values + p_c_kw_sum

    return yearly_summary, aggregated_lc


def _generate_cooling_lc_summary(
    buildings_group,
    heat_load_curves,
    is_retrofitted,
    sim_year,
    pace_dataset=None,
):
    buildings_group = buildings_group[~pd.isna(buildings_group.q_c_kwh)]
    if len(buildings_group) == 0:
        return (
            pd.Series([], dtype=pd.Int64Dtype()),
            pd.Series([], dtype=pd.Float64Dtype()),
            pd.Series([], dtype=pd.Float64Dtype()),
            np.zeros(8760),
        )

    building_ids = buildings_group.index
    building_archetype = buildings_group.archetype.values[0]
    q_c_kwh = buildings_group["q_c_kwh"]

    # generate cooling load curve
    p_c_kw = pace_cooling_load_curve_per_archetype(
        heat_load_curves,
        building_ids,
        building_archetype,
        pace_dataset=pace_dataset,
        is_retrofitted=is_retrofitted,
        sim_year=sim_year,
    )
    # upscale unitary LC
    p_c_kw = p_c_kw * q_c_kwh

    p_c_kw_max = p_c_kw.max()

    p_c_kw_sum = p_c_kw.sum(axis=1).values

    # Maximum of daily mean
    p_c_kw_daily_max = p_c_kw.resample("1D").mean().max()

    return (
        building_ids,
        p_c_kw_daily_max,
        p_c_kw_max,
        p_c_kw_sum,
    )


def pace_cooling_load_curve_per_archetype(
    heat_load_curves,
    building_ids=None,
    building_archetype=None,
    pace_dataset=None,
    *,
    rcp="45",
    is_retrofitted=True,
    sim_year=2030,
) -> pd.Series:
    """
    The hourly normalized space cooling load curve is generated based on EMPA PACE model.

    Can use a provided PACE dataset to avoid repeated loads

    Args:
        heat_load_curves:
        building_ids:
        building_archetype:
        pace_dataset:

    Returns:

    """

    # Allow to generate for a single building by passing none as building id list
    if building_ids is None:
        building_ids = [0]
    n_buildings = len(building_ids)

    # Load PACE data
    if pace_dataset is None:
        # Fallback to default dataset load, this is better avoided.
        log.warning("No pace_dataset provided, falling back to loading default.")
        config = Settings()
        pace_dataset = open_pace_dataset_from_cloud(
            path="s3://tessa/pace_empa_hourly.zarr",
            key=config.s3_id,
            secret=config.s3_secret,
            endpoint_url=config.s3_endpoint_override,
        )
    # select scenario. retrofit_measure: 'Full' / 'noretrofit';
    # rcp: 26 / 45 / 85; simulation_year: 2020 / 2030 / 2040 / 2050 / 2060.
    # Note: Need to match function _get_cooling_demand_by_archetype
    # The available data on hourly load curves is only for rcp45, Full & noretrofit,  2030 & 2050.
    sim_year = min([2030, 2050], key=lambda x: abs(x - sim_year))
    if is_retrofitted:
        retrofit_option = "Full"
    else:
        retrofit_option = "noretrofit"

    qc_lc_pace = (
        pace_dataset["cooling_demand_kwh"]
        .sel(centroid_fid=building_archetype, rcp=rcp, retrofit_strategy=retrofit_option, sim_year=sim_year)
        .values
    )

    # Buildings assigned to the same archetype have the same normalized load curve
    qc_load_curve = pd.DataFrame(
        np.tile(qc_lc_pace, (n_buildings, 1)).T, index=heat_load_curves.index, columns=building_ids
    )

    # TODO:introduce perturbations
    # force hourly demand being >= 0
    qc_load_curve = np.maximum(qc_load_curve, 0)

    # normalize to 1kWh
    qc_load_curve = qc_load_curve / (qc_load_curve.sum())

    return qc_load_curve
