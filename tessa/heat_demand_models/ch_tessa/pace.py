"""
Access to the EMPA PACE Refit data
"""
import xarray as xr
from fsspec.implementations.local import LocalFileSystem
from zarr.storage import FSStore, LRUStoreCache

from tessa.core import Settings


def open_pace_dataset_from_cloud(
    path="s3://tessa/pace_empa_daily.zarr", key=None, secret=None, endpoint_url=""
):
    """
    Open the PACE dataset as a Zarr from a cloud storage and return an xarray dataset.

    TODO document file contents.

    Args:
        path:

    Returns:

    """
    if not path.startswith("s3://"):
        # Assume a local file system path, don't try to provide s3 connection params.
        store_nocache = FSStore(path)
    else:
        store_nocache = FSStore(path, key=key, secret=secret, endpoint_url=endpoint_url)
    store_cached = LRUStoreCache(store_nocache, max_size=int(1e8))
    return xr.open_zarr(store_cached)
    # ds = xr.open_dataset(
    #     path,
    #     backend_kwargs={
    #         "storage_options": dict(
    #             key=config.s3_id, secret=config.s3_secret, endpoint_url=config.s3_endpoint_override
    #         )
    #     },
    #     engine="zarr",
    # )
