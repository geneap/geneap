# """
# @author: ewilczynski
# """
# import pandas as pd
# import datetime
# import xarray as xr
# import time
# import pvlib
# import math
# import numpy as np
#
#
# def main():
#     fake_global_dict = {}
#     # Latitude / longitude
#     # GE
#     user_lat = 46.2044
#     user_long = 6.1432
#     # Conditioned gross floor area of medium (based on external dimensions) (m2)
#     gfa_medium = 192
#     # Max comfort level (cooling)
#     t_set_max = 26
#     # Min comfort level (heating)
#     t_set_min = 20
#     # Unrestricted heating power (W/m2)
#     heating_power = 10
#     # Building type ('MFH','SFH')
#     building_type = "SFH"
#     # 0: <1920
#     # 1: 1921-'45
#     # 2: 1946-'60
#     # 3: 1961-'70
#     # 4: 1971-'80
#     # 5: 1981-'90
#     # 6: 1991-'00
#     # 7: 2001-'10
#     # 8: 2011-'20
#     ch_age_select = 0
#     ch_age = [
#         "<1920",
#         "1921-'45",
#         "1946-'60",
#         "1961-'70",
#         "1971-'80",
#         "1981-'90",
#         "1991-'00",
#         "2001-'10",
#         "2011-'20",
#     ]
#
#     # Load CH data
#     df_ch = pd.read_csv("Input/results_eric_2.csv")
#     df_ch_row = df_ch[
#         (df_ch["buildingtype"] == str(building_type))
#         & (df_ch["canton"] == "GE")
#         & (df_ch["hvacrtype"] == "HP")
#         & (df_ch["agedesc"] == ch_age[ch_age_select])
#         & (df_ch["typology"] == "Urban")
#     ]
#     weather = load_climate_data(user_lat, user_long)
#
#     # Parameters
#     pm = {
#         # Parameter for calculating effective mass area, A_m (Table 12)
#         "A_m_parameter": 2.5,
#         # Parameter for internal heat capacity, C_m (Table 12)
#         "C_m_parameter": 45 * 3600,
#         # Reduction factor external shading, horizontal orientation (from Tabula)
#         "F_sh_hor": 0.8,
#         # Reduction factor external shading, vertical orientations (from Tabula)
#         "F_sh_vert": 0.6,
#         # Form factor for radiation between the element and the sky for unshaded horizontal roof
#         "F_r_hor": 1.0,
#         # Form factor for radiation between the element and the sky for unshaded vertical roof
#         "F_r_vert": 0.5,
#         # U value of wall 1
#         "u_wall_1": df_ch_row["u_wall"],
#         "u_wall_north": df_ch_row["u_wall"],
#         "u_wall_east": df_ch_row["u_wall"],
#         "u_wall_south": df_ch_row["u_wall"],
#         "u_wall_west": df_ch_row["u_wall"],
#         # U value of roof 1
#         "u_roof_1": df_ch_row["u_roof"],
#         "u_roof_2": df_ch_row["u_roof"],
#         # U value of Door
#         "u_door_1": df_ch_row["u_window"],
#         # U value of window 1
#         "u_window_1": df_ch_row["u_window"],
#         # Standard room height (m) (from Tabula)
#         "h_room": 2.5,
#         # Heat transfer coefficient between the air node and surface node (W/(m²K))
#         "h_is": 3.45,
#         # Dimensionless ratio between the internal surfaces area and the floor area
#         "lambda_at": 4.5,
#         # Heat transfer coefficient between nodes m and s
#         "h_ms": 9.1,
#         # Heat capacity of air per volume (J/(m^3 K))
#         "rho_a_c_a": 1200,
#         # Average heat flow per person (W/person)
#         "Q_P": 70,
#         # Frame area fraction
#         "F_F": 0.3,
#         # Frame area fraction
#         "F_w": 0.9,
#         # Total solar energy transmittance for radiation perpendicular to the glazing (from Tabula)
#         "g_gl_n": 0.6,
#         # Air change rate from infiltration (per hour) (Tabula value for "medium")
#         "n_air_inf": 0.2,
#         # Air change rate from use (per hour) (from Tabula)
#         "n_air_use": 0.4,
#         # Dimensionless absorption coefficient for solar radiation of the opaque part
#         "alpha_S_c": 0.6,
#         # Average difference between the external air temperature and the apparent sky temperature (degC)
#         "delta_theta_er": 11,
#         # Arithmetic average of the surface temperature and the sky temperature (degC)
#         "theta_ss": 11,
#         # Stefan-Boltzmann constant (W/(m^2 * K^4))
#         "SB_constant": 5.67 * (10**-8),
#         # Emissivity of external opaque elements (roof, wall)
#         "epsilon_op": 0.9,
#         # Emissivity of external glazed elements (window, door)
#         "epsilon_gl": 0.9,
#         # External surface heat resistance of the opaque part (ISO 6946)
#         "R_S_e_op": 0.04,
#         # External surface heat resistance of the glazed part (ISO 6946)
#         "R_S_e_gl": 0.04,
#         # Parameter for calculation of conditioned floor area (from Tabula calculations)
#         "gfa_parameter": 0.85,
#     }
#
#     # User defined:
#     # Number of stories
#     n_stories = 1
#     # Interior gross floor area
#     # / Create logic to match with tabula structure due to use of other values (e.g., roof area)
#     gfa_interior = gfa_medium * pm["gfa_parameter"] * n_stories
#
#     # Configure external elements
#
#     # Set roof type and orientation
#     # 0: gable roof; north/south orientation
#     # 1: gable roof; east/west orientation
#     # 2: skillion roof; north orientation
#     # 3: skillion roof; east orientation
#     # 4: skillion roof; south orientation
#     # 5: skillion roof; west orientation
#     # 6: flat roof; horizontal orientation
#     roof_type_orientation = 6
#     # Orientation values
#     orientation_lib = ["north", "east", "south", "west"]
#
#     # Set wall to floor ratio
#     w_f_r = 1.30
#     # Calculate external wall area
#     a_wall_ext = w_f_r * gfa_medium * n_stories
#     # Set building shape (L:W) NOTE: L is length of building (sides), W is width of building (front/back)
#     L = 1
#     W = 1
#     # Calculate building external wall areas based on shape (NOTE: wall side_1 is left wall when facing facade)
#     a_external_front = ((W / (L + W)) * a_wall_ext) / 2
#     a_external_back = ((W / (L + W)) * a_wall_ext) / 2
#     a_external_side_1 = ((L / (L + W)) * a_wall_ext) / 2
#     a_external_side_2 = ((L / (L + W)) * a_wall_ext) / 2
#
#     # Set building shape and facade orientation
#     # Select facade orientation (0: north, 1: east, 2: south, 3: west)
#     facade_orientation = orientation_lib[0]
#
#     # Set area of windows (as % of respective wall or roof)
#     window_front_proportion = 10
#     window_back_proportion = 25
#     window_side_1_proportion = 25
#     window_side_2_proportion = 25
#     window_roof_proportion = 0
#
#     # Set area of door (m2)
#     a_door_1 = 2
#
#     # Set roof pitch (angle in degrees)
#     user_roof_pitch = 0
#
#     # Ensure flat roof has 0 degree pitch
#     if roof_type_orientation == 6:
#         roof_pitch = 0
#     else:
#         roof_pitch = user_roof_pitch
#     # Calculate roof area based on external structure area and pitch
#     a_roof_total = gfa_medium / np.cos(roof_pitch)
#
#     # Calculate area of window
#     a_window_front = (window_front_proportion / 100) * a_external_front
#     a_window_back = (window_back_proportion / 100) * a_external_back
#     a_window_side_1 = (window_side_1_proportion / 100) * a_external_side_1
#     a_window_side_2 = (window_side_2_proportion / 100) * a_external_side_2
#
#     # Calculate area of external walls
#     a_wall_front = a_external_front - a_window_front - a_door_1
#     a_wall_back = a_external_back - a_window_back
#     a_wall_side_1 = a_external_side_1 - a_window_side_1
#     a_wall_side_2 = a_external_side_2 - a_window_side_2
#
#     if facade_orientation == "north":
#         door_1_azimuth = 0.0
#         a_window_north = a_window_front
#         a_window_south = a_window_back
#         a_window_west = a_window_side_1
#         a_window_east = a_window_side_2
#         a_wall_north = a_wall_front
#         a_wall_south = a_wall_back
#         a_wall_west = a_wall_side_1
#         a_wall_east = a_wall_side_2
#     elif facade_orientation == "south":
#         door_1_azimuth = 180.0
#         a_window_south = a_window_front
#         a_window_north = a_window_back
#         a_window_east = a_window_side_1
#         a_window_west = a_window_side_2
#         a_wall_south = a_wall_front
#         a_wall_north = a_wall_back
#         a_wall_east = a_wall_side_1
#         a_wall_west = a_wall_side_2
#     elif facade_orientation == "east":
#         door_1_azimuth = 90.0
#         a_window_east = a_window_front
#         a_window_west = a_window_back
#         a_window_north = a_window_side_1
#         a_window_south = a_window_side_2
#         a_wall_east = a_wall_front
#         a_wall_west = a_wall_back
#         a_wall_north = a_wall_side_1
#         a_wall_south = a_wall_side_2
#     else:
#         door_1_azimuth = 270.0
#         a_window_west = a_window_front
#         a_window_east = a_window_back
#         a_window_south = a_window_side_1
#         a_window_north = a_window_side_2
#         a_wall_west = a_wall_front
#         a_wall_east = a_wall_back
#         a_wall_south = a_wall_side_1
#         a_wall_north = a_wall_side_2
#
#     # Roof type, tilt, and orientation
#     if roof_type_orientation == 0:
#         roof_1_azimuth = 0.0
#         roof_2_azimuth = 180.0
#         a_roof_1 = a_roof_total / 2
#         a_roof_2 = a_roof_total / 2
#     elif roof_type_orientation == 1:
#         roof_1_azimuth = 90.0
#         roof_2_azimuth = 270.0
#         a_roof_1 = a_roof_total / 2
#         a_roof_2 = a_roof_total / 2
#     elif roof_type_orientation == 2:
#         roof_1_azimuth = 0.0
#         roof_2_azimuth = 0.0
#         a_roof_1 = a_roof_total
#         a_roof_2 = 0.0
#     elif roof_type_orientation == 3:
#         roof_1_azimuth = 90.0
#         roof_2_azimuth = 90.0
#         a_roof_1 = a_roof_total
#         a_roof_2 = 0.0
#     elif roof_type_orientation == 4:
#         roof_1_azimuth = 180.0
#         roof_2_azimuth = 180.0
#         a_roof_1 = a_roof_total
#         a_roof_2 = 0.0
#     elif roof_type_orientation == 5:
#         roof_1_azimuth = 270.0
#         roof_2_azimuth = 270.0
#         a_roof_1 = a_roof_total
#         a_roof_2 = 0.0
#     elif roof_type_orientation == 6:
#         roof_1_azimuth = 0.0
#         roof_2_azimuth = 0.0
#         a_roof_1 = a_roof_total
#         a_roof_2 = 0.0
#     else:
#         print("Invalid Roof Type/Orientation code")
#
#     element = [
#         "window_north",
#         "window_east",
#         "window_south",
#         "window_west",
#         "door_1",
#         "roof_1",
#         "roof_2",
#         "wall_north",
#         "wall_east",
#         "wall_south",
#         "wall_west",
#     ]
#     azimuth_lib = {
#         "north": 0.0,
#         "east": 90.0,
#         "south": 180.0,
#         "west": 270.0,
#         "door_1": door_1_azimuth,
#         "roof_1": roof_1_azimuth,
#         "roof_2": roof_2_azimuth,
#     }
#     tilt_lib = {
#         "north": 90.0,
#         "east": 90.0,
#         "south": 90.0,
#         "west": 90.0,
#         "door_1": 90.0,
#         "roof_1": roof_pitch,
#         "roof_2": roof_pitch,
#     }
#
#     # Set temperatures for heating and cooling
#     theta_int_C_set = t_set_max
#     theta_int_H_set = t_set_min
#     # Maximum heating and cooling power
#     phi_H_max = "inf"
#     phi_C_max = -("inf")
#     # phi_H_max = 100
#     # phi_C_max = -100
#
#     # Conditioned floor area (m2)
#     A_f = gfa_interior
#     # Effective mass area (m2)
#     A_m = pm["A_m_parameter"] * A_f
#     # Thermal capacitance of medium (J/K)
#     C_m = pm["C_m_parameter"] * A_f
#     # Area of all surfaces facing the building zone (m2)
#     A_tot = pm["lambda_at"] * A_f
#     A_t = A_tot
#     # Coupling conductance between air and surface nodes (W/K)
#     H_tr_is = pm["h_is"] * A_tot
#
#     # Ventilation heat transfer coefficients
#     # Total air changes (1/h)
#     n_air_total = pm["n_air_inf"] + pm["n_air_use"]
#     # Air volume of room (m3)
#     air_volume = pm["h_room"] * A_f
#     # Temperature adjustment factor for the air flow element
#     b_ve = 1
#     # Time fraction of operation of the air flow element (f_ve_t = 1 assumes element is always operating)
#     # E.g. If only operating from 8.00 to 18.00, then f_ve_t = 10 hours/24 hours = 0.417
#     f_ve_t = 1
#     # Airflow rate of the air flow element (m3/s)
#     q_ve = n_air_total / 3600 * air_volume
#     # Time-average airflow rate of the air flow element (m3/s)
#     q_ve_mn = f_ve_t * q_ve
#     # Ventilation heat transfer coefficient (W/K)
#     H_ve_adj = pm["rho_a_c_a"] * b_ve * q_ve_mn
#     # Combined heat conductance (W/K)
#     H_tr_1 = 1 / (1 / H_ve_adj + 1 / H_tr_is)
#
#     # Heat transfer opaque elements (walls and roof) (W/K)
#     H_tr_op = (a_wall_front + a_wall_back + a_wall_side_1 + a_wall_side_2) * pm[
#         "u_wall_1"
#     ] + a_roof_total * pm["u_roof_1"]
#     # Thermal transmission coefficient of glazed elements (windows and doors) (W/K)
#     H_tr_w = (
#         a_window_front * pm["u_window_1"]
#         + a_window_back * pm["u_window_1"]
#         + a_window_side_1 * pm["u_window_1"]
#         + a_window_side_2 * pm["u_window_1"]
#         + a_door_1 * pm["u_door_1"]
#     )
#     # Split H_tr_op into coupling conductance between nodes m and s (W/K)
#     H_op = H_tr_op
#     H_ms = pm["h_ms"] * A_m
#     H_em = 1 / (1 / H_op - 1 / H_ms)
#     H_tr_ms = H_ms
#     H_tr_em = H_em
#
#     # External radiative heat transfer coefficient for opaque and glazed surfaces (W/(m²K))
#     h_r_op = 4 * pm["epsilon_op"] * pm["SB_constant"] * ((pm["theta_ss"] + 273) ** 3)
#     h_r_gl = 4 * pm["epsilon_gl"] * pm["SB_constant"] * ((pm["theta_ss"] + 273) ** 3)
#
#     # Set up date/time and list of inputs/outputs for calculaton
#     start_date = datetime.datetime(2017, 1, 1, 0)
#     end_date = datetime.datetime(2017, 1, 31, 23)
#     delta = datetime.timedelta(hours=1)
#     time_length = end_date - start_date
#     cols_g = [
#         "hour_of_day",
#         "day_of_week",
#         "t_outside_t2m",
#         "t_outside_d2m",
#         "humidity",
#         "surface_pressure",
#         "ghi",
#         "dni",
#         "dhi",
#         "solar_position",
#         "relative_airmass",
#         "extra_radiation",
#         "solar_altitude",
#         "solar_altitude_radians",
#         "solar_azimuth",
#         "solar_azimuth_radians",
#         "solar_zenith",
#         "solar_zenith_radians",
#         "total_irradiance",
#         "phi_int",
#         "phi_sol",
#         "Q_HC_nd",
#         "Q_H_nd",
#         "Q_C_nd",
#         "theta_air",
#         "t_outside_t2m_degC",
#         "theta_air_ac",
#         "i_sol",
#         "theta_air_0",
#         "theta_air_10",
#         "theta_m_tp",
#         "theta_m_t",
#         "phi_ia",
#         "phi_st",
#         "phi_m",
#     ]
#     n_timesteps = int(time_length.total_seconds() / delta.total_seconds())
#     year = start_date.year
#     timestamp_list = [(start_date + x * delta) for x in range(0, n_timesteps + 1)]
#     solar_gains = pd.DataFrame(columns=cols_g, index=timestamp_list)
#
#     # Build DF with solar radiation data
#     for current_index, current_timestamp in enumerate(timestamp_list):
#         # Set weather variables
#         # Outside dry bulb temperature (K)
#         solar_gains.t_outside_t2m[current_index] = weather.t2m[current_index]
#         # Outside dry bulb temperature (deg C)
#         solar_gains.t_outside_t2m_degC[current_index] = solar_gains.t_outside_t2m[current_index] - 273.15
#         # Outside dew point temperature (K)
#         solar_gains.t_outside_d2m[current_index] = weather.d2m[current_index]
#         # Relative humidity
#         solar_gains.humidity[current_index] = weather.r[current_index]
#         # Surface pressure
#         solar_gains.surface_pressure[current_index] = weather.sp[current_index]
#         # Surface Solar Radiation Downwards (SSRD): Global Horizontal Irradiance (GHI)
#         solar_gains.ghi[current_index] = weather.ssrd[current_index]
#         # Direct solar radiation at the surface (FDIR): Direct Normal Irradiance (DNI)
#         solar_gains.dni[current_index] = weather.fdir[current_index]
#         solar_gains.dhi[current_index] = weather.ssrd[current_index] - weather.fdir[current_index]
#
#         t_outside_t2m = solar_gains.t_outside_t2m[current_index]
#         t_outside_d2m = solar_gains.t_outside_d2m[current_index]
#
#         # Outside dew point temperature (deg C)
#         t_outside_d2m_degC = t_outside_d2m - 273.15
#         ghi = solar_gains.ghi[current_index] / 3600  # Convert from J/m2 to W/m2
#         dni = solar_gains.dni[current_index] / 3600  # Convert from J/m2 to W/m2
#         # (SSRD - FDIR): Diffuse Horizontal Irradiance (DHI)
#         dhi = solar_gains.dhi[current_index] / 3600  # Convert from J/m2 to W/m2
#
#         # Convert lat/long to radians for pvlib
#         latitude_radians = math.radians(user_lat)
#         longitude_radians = math.radians(user_long)
#         # Solar position
#         solar_gains.solar_position[current_index] = pvlib.solarposition.get_solarposition(
#             current_timestamp,
#             user_lat,
#             user_long,
#             pressure=solar_gains.surface_pressure[current_index],
#             temperature=solar_gains.t_outside_t2m_degC[current_index],
#         )
#         solar_position = solar_gains.solar_position[current_index]
#         # Solar altitude
#         solar_gains.solar_altitude[current_index] = solar_position["apparent_elevation"]
#         solar_altitude = solar_gains.solar_altitude[current_index]
#         # Solar azimuth
#         solar_gains.solar_azimuth[current_index] = solar_position["azimuth"]
#         solar_azimuth = solar_gains.solar_azimuth[current_index]
#         # Solar zenith
#         solar_gains.solar_zenith[current_index] = solar_position["apparent_zenith"]
#         solar_zenith = solar_gains.solar_zenith[current_index]
#         # Airmass
#         solar_gains.relative_airmass[current_index] = pvlib.atmosphere.get_relative_airmass(solar_zenith)
#         relative_airmass = solar_gains.relative_airmass[current_index]
#         # Extra DNI
#         solar_gains.extra_radiation[current_index] = pvlib.irradiance.get_extra_radiation(
#             current_timestamp.timetuple().tm_yday, epoch_year=current_timestamp.year
#         )
#         dni_extra = solar_gains.extra_radiation[current_index]
#         # Finished getting values from pvlib'
#
#         # for current_index, current_timestamp in enumerate(timestamp_list):
#
#         # Calculating solar gains'
#
#         # Calculates values for the form factor for radiation between the unshaded roof and the sky
#         F_r_roof = 1 - roof_pitch / 180
#         # Solar radiation and gains results storage
#         phi_sol_results = []
#         i_sol_results = []
#         for i in azimuth_lib:
#             surface_azimuth = azimuth_lib[i]
#             surface_tilt = tilt_lib[i]
#             I_sol_element = pvlib.irradiance.get_total_irradiance(
#                 surface_tilt,
#                 surface_azimuth,
#                 solar_zenith,
#                 solar_azimuth,
#                 dni=dni,
#                 ghi=ghi,
#                 dhi=dhi,
#                 dni_extra=dni_extra,
#                 airmass=relative_airmass,
#                 model="perez",
#                 surface_type="urban",
#             )
#             # Irradiance on element (W/m2)
#             fake_global_dict[f"I_sol_{i}"] = I_sol_element["poa_global"].fillna(0)
#             i_sol_results.append(fake_global_dict[f"I_sol_{i}"])
#
#         i_sol_window_north = i_sol_results[0]
#         i_sol_window_east = i_sol_results[1]
#         i_sol_window_south = i_sol_results[2]
#         i_sol_window_west = i_sol_results[3]
#         i_sol_vert = (i_sol_results[0] + i_sol_results[1] + i_sol_results[2] + i_sol_results[3]) / 4
#         i_sol_door_1 = i_sol_results[4]
#         i_sol_roof_1 = i_sol_results[5]
#         i_sol_roof_2 = i_sol_results[6]
#         i_sol_wall_north = i_sol_results[0]
#         i_sol_wall_east = i_sol_results[1]
#         i_sol_wall_south = i_sol_results[2]
#         i_sol_wall_west = i_sol_results[3]
#         azimuth_lib = {
#             "north": 0.0,
#             "east": 90.0,
#             "south": 180.0,
#             "west": 270.0,
#             "door_1": door_1_azimuth,
#             "roof_1": roof_1_azimuth,
#             "roof_2": roof_2_azimuth,
#         }
#
#         for i in element:
#             element_name_a = "a_" + i
#             element_area = fake_global_dict[f"a_{i}"]
#             g_gl = pm["F_w"] * pm["g_gl_n"]
#             if (i == "window_north") | (i == "window_east") | (i == "window_south") | (i == "window_west"):
#                 element_name_i_sol = "i_sol_" + i
#                 i_sol_window = element_name_i_sol
#                 # Shading reduction factor for movable shading provisions
#                 F_sh_gl = pm["F_sh_vert"]
#                 element_u = pm["u_window_1"]
#                 element_r = pm["R_S_e_gl"]
#                 # Effective solar collecting area of element
#                 fake_global_dict[f"A_sol_{i}"] = F_sh_gl * g_gl * (1 - pm["F_F"]) * element_area
#                 # Thermal radiation heat flow to the sky (W)
#                 fake_global_dict[f"phi_r_{i}"] = (
#                     element_r * element_u * element_area * h_r_gl * pm["delta_theta_er"]
#                 )
#                 # Heat flow by solar gains through building element
#                 fake_global_dict[f"phi_sol_{i}"] = (
#                     pm["F_sh_vert"] * fake_global_dict[f"A_sol_{i}"] * fake_global_dict[f"i_sol_{i}"]
#                     - pm["F_r_vert"] * fake_global_dict[f"phi_r_{i}"]
#                 )
#                 if (fake_global_dict[f"phi_sol_{i}"]) < 0:
#                     fake_global_dict[f"phi_sol_{i}"] = 0
#                 phi_sol_results.append((fake_global_dict[f"phi_sol_{i}"]))
#             elif i == "door_1":
#                 # Shading reduction factor for movable shading provisions
#                 F_sh_gl = pm["F_sh_vert"]
#                 element_name_u = "u_" + i
#                 element_u = pm[element_name_u]
#                 element_r = pm["R_S_e_gl"]
#                 # Effective solar collecting area of element
#                 fake_global_dict[f"A_sol_{i}"] = F_sh_gl * g_gl * (1 - pm["F_F"]) * element_area
#                 # Thermal radiation heat flow to the sky (W)
#                 fake_global_dict[f"phi_r_{i}"] = (
#                     element_r * element_u * element_area * h_r_gl * pm["delta_theta_er"]
#                 )
#                 # Heat flow by solar gains through building element
#                 fake_global_dict[f"phi_sol_{i}"] = (
#                     pm["F_sh_vert"] * fake_global_dict[f"A_sol_{i}"] * i_sol_door_1
#                     - pm["F_r_vert"] * fake_global_dict[f"phi_r_{i}"]
#                 )
#                 if (fake_global_dict[f"phi_sol_{i}"]) < 0:
#                     fake_global_dict[f"phi_sol_{i}"] = 0
#                 phi_sol_results.append((fake_global_dict[f"phi_sol_{i}"]))
#             elif (i == "roof_1") | (i == "roof_2"):
#                 element_name_i_sol = "i_sol_" + i
#                 i_sol_roof = element_name_i_sol
#                 element_name_u = "u_" + i
#                 element_u = pm[element_name_u]
#                 element_r = pm["R_S_e_op"]
#                 # Effective solar collecting area of element
#                 fake_global_dict[f"A_sol_{i}"] = pm["alpha_S_c"] * element_r * element_u * element_area
#                 # Thermal radiation heat flow to the sky (W)
#                 fake_global_dict[f"phi_r_{i}"] = (
#                     element_r * element_u * element_area * h_r_op * pm["delta_theta_er"]
#                 )
#                 # Heat flow by solar gains through building element
#                 fake_global_dict[f"phi_sol_{i}"] = (
#                     pm["F_sh_hor"] * fake_global_dict[f"A_sol_{i}"] * fake_global_dict[f"I_sol_{i}"]
#                     - F_r_roof * fake_global_dict[f"phi_r_{i}"]
#                 )
#                 if (fake_global_dict[f"phi_sol_{i}"]) < 0:
#                     fake_global_dict[f"phi_sol_{i}"] = 0
#                 phi_sol_results.append((fake_global_dict[f"phi_sol_{i}"]))
#             elif (i == "wall_north") | (i == "wall_east") | (i == "wall_south") | (i == "wall_west"):
#                 element_name_u = "u_" + i
#                 element_u = pm[element_name_u]
#                 element_r = pm["R_S_e_op"]
#                 # Effective solar collecting area of element
#                 fake_global_dict[f"A_sol_{i}"] = pm["alpha_S_c"] * element_r * element_u * element_area
#                 # Thermal radiation heat flow to the sky (W)
#                 fake_global_dict[f"phi_r_{i}"] = (
#                     element_r * element_u * element_area * h_r_op * pm["delta_theta_er"]
#                 )
#                 # Heat flow by solar gains through building element
#                 fake_global_dict[f"phi_sol_{i}"] = (
#                     pm["F_sh_vert"] * fake_global_dict[f"A_sol_{i}"] * fake_global_dict[f"i_sol_{i}"]
#                     - pm["F_r_vert"] * fake_global_dict[f"phi_r_{i}"]
#                 )
#                 if (fake_global_dict[f"phi_sol_{i}"]) < 0:
#                     fake_global_dict[f"phi_sol_{i}"] = 0
#                 phi_sol_results.append((fake_global_dict[f"phi_sol_{i}"]))
#
#         solar_gains.i_sol[current_index] = sum(i_sol_results)
#         solar_gains.phi_sol[current_index] = sum(phi_sol_results)
#         phi_sol = solar_gains.phi_sol[current_index]
#
#         # Finished calculating solar gains
#
#         # Calculating internal gains
#         # INTERNAL GAINS
#         # From Table G.8
#         # Check if weekday or weekend (Monday-Friday = 0-4, Saturday-Sunday = 5-6)
#         # Adjust heat flow rate from occupants and appliances in living room and kitchen (hf_Oc_A_LRK)
#         # Adjust heat flow rate from occupants and appliances in other rooms (e.g. bedrooms) (hf_Oc_A_Oth)
#         solar_gains.day_of_week[current_index] = solar_gains.index[current_index].weekday()
#         solar_gains.hour_of_day[current_index] = solar_gains.index[current_index].hour
#         day_of_week = solar_gains.index[current_index].weekday()
#         hour_of_day = solar_gains.index[current_index].hour
#         phi_int_Oc_A = metabolic_gains(A_f, day_of_week, hour_of_day)
#         # Heat flow rate from internal sources (W)
#
#         solar_gains.phi_int[current_index] = phi_int_Oc_A
#         phi_int = solar_gains.phi_int[current_index]
#         # Heat flow rate to air (W)
#         solar_gains.phi_ia[current_index] = 0.5 * phi_int
#         # Heat flow rate to internal surface (W)
#         solar_gains.phi_st[current_index] = (1 - (A_m / A_t) - (H_tr_w / (9.1 * A_t))) * (
#             0.5 * phi_int + phi_sol
#         )
#         # Heat flow rate to medium (W)
#         solar_gains.phi_m[current_index] = (A_m / A_t) * (0.5 * phi_int + phi_sol)
#         print("    Finished calculating internal gains")
#
#         current_timestamp += delta
#         current_index += 1
#
#     cols = [
#         "hour_of_day",
#         "day_of_week",
#         "Q_HC_nd",
#         "Q_H_nd",
#         "Q_C_nd",
#         "theta_air",
#         "theta_air_ac",
#         "theta_air_0",
#         "theta_air_10",
#         "theta_m_tp",
#         "theta_m_t",
#         "phi_int",
#         "phi_sol",
#     ]
#     hourly_result = pd.DataFrame(columns=cols, index=timestamp_list)
#     # Initial theta_m_t value
#     theta_m_t = 12
#     # Initial theta_air
#     theta_air_ac = 12
#     theta_air = theta_air_ac
#     theta_m_tp_list = [theta_m_t]
#     # RC simulation
#     for current_index, current_timestamp in enumerate(timestamp_list):
#         # Set supply temperature equal to outside temperature
#         theta_sup = solar_gains.t_outside_t2m_degC[current_index]
#
#         hourly_result.phi_int[current_index] = solar_gains.phi_int[current_index]
#         phi_int = hourly_result.phi_int[current_index]
#         hourly_result.phi_sol[current_index] = solar_gains.phi_sol[current_index]
#         phi_sol = hourly_result.phi_sol[current_index]
#         phi_ia = solar_gains.phi_ia[current_index]
#         phi_st = solar_gains.phi_st[current_index]
#         phi_m = solar_gains.phi_m[current_index]
#
#         # STEP 1
#         # ------
#         # Check if heating or cooling is needed:
#         phi_HC_nd_0 = 0
#         H_tr_2_0 = H_tr_1 + H_tr_w
#         H_tr_3_0 = 1 / (1 / H_tr_2_0 + 1 / H_tr_ms)
#         if current_index == 0:
#             theta_m_tp_0 = 0
#             theta_m_t_0 = 0
#         else:
#             theta_m_tp_0 = theta_m_t
#         phi_mtot_0 = (
#             phi_m
#             + H_tr_em * theta_sup
#             + H_tr_3_0
#             * (phi_st + H_tr_w * theta_sup + H_tr_1 * (((phi_ia + phi_HC_nd_0) / H_ve_adj) + theta_sup))
#             / H_tr_2_0
#         )
#         theta_m_tp_0 = theta_m_tp_list[0]  # EJW mtp
#         theta_m_t_0 = (theta_m_tp_0 * ((C_m / 3600) - 0.5 * (H_tr_3_0 + H_tr_em)) + phi_mtot_0) / (
#             (C_m / 3600) + 0.5 * (H_tr_3_0 + H_tr_em)
#         )
#         theta_m_0 = (theta_m_t_0 + theta_m_tp_0) / 2
#         theta_s_0 = (
#             H_tr_ms * theta_m_0
#             + phi_st
#             + H_tr_w * theta_sup
#             + H_tr_1 * (theta_sup + (phi_ia + phi_HC_nd_0) / H_ve_adj)
#         ) / (H_tr_ms + H_tr_w + H_tr_1)
#         hourly_result.theta_air_0[current_index] = (
#             H_tr_is * theta_s_0 + H_ve_adj * theta_sup + phi_ia + phi_HC_nd_0
#         ) / (H_tr_is + H_ve_adj)
#         theta_air_0 = hourly_result.theta_air_0[current_index]
#         theta_op_0 = 0.3 * theta_air_0 + 0.7 * theta_s_0
#
#         if ((theta_air_0) >= theta_int_H_set) & ((theta_air_0) <= theta_int_C_set):
#             phi_HC_nd_ac = 0
#             hourly_result.theta_air_ac[current_index] = theta_air_0
#             hourly_result.theta_air[current_index] = hourly_result.theta_air_ac[current_index]
#             hourly_result.Q_HC_nd[current_index] = 0
#             hourly_result.Q_H_nd[current_index] = 0
#             hourly_result.Q_C_nd[current_index] = 0
#             Q_int = phi_int * 0.036
#             Q_sol = phi_sol * 0.036
#             hourly_result.theta_m_t[current_index] = theta_m_t_0
#             theta_m_t = theta_m_t_0
#             theta_m_tp_list.clear()  # EJW mtp
#             theta_m_tp_list.append(theta_m_t)  # EJW mtp
#             current_timestamp += delta
#             current_index += 1
#             print("mark1")
#             print("<<<End calculation for timestep, Case 3>>>")
#             continue
#         else:
#             pass
#             print("mark2")
#
#         # STEP 2
#         # ------
#         if (theta_air_0) > theta_int_C_set:
#             theta_air_set = theta_int_C_set
#             print("mark3")
#         elif (theta_air_0) < theta_int_H_set:
#             theta_air_set = theta_int_H_set
#             print("mark4")
#
#         # Apply heating factor of 10 W/m2:
#         phi_HC_nd_10 = A_f * heating_power
#         H_tr_2_10 = H_tr_1 + H_tr_w
#         H_tr_3_10 = 1 / (1 / H_tr_2_10 + 1 / H_tr_ms)
#         if current_index == 0:
#             theta_m_tp_10 = 0
#             theta_m_t_10 = 0
#         else:
#             theta_m_tp_10 = theta_m_t
#         phi_mtot_10 = (
#             phi_m
#             + H_tr_em * theta_sup
#             + H_tr_3_10
#             * (phi_st + H_tr_w * theta_sup + H_tr_1 * (((phi_ia + phi_HC_nd_10) / H_ve_adj) + theta_sup))
#             / H_tr_2_10
#         )
#         theta_m_tp_10 = theta_m_tp_list[0]  # EJW mtp
#         theta_m_t_10 = (theta_m_tp_10 * ((C_m / 3600) - 0.5 * (H_tr_3_10 + H_tr_em)) + phi_mtot_10) / (
#             (C_m / 3600) + 0.5 * (H_tr_3_10 + H_tr_em)
#         )
#         theta_m_10 = (theta_m_t_10 + theta_m_tp_10) / 2
#         theta_s_10 = (
#             H_tr_ms * theta_m_10
#             + phi_st
#             + H_tr_w * theta_sup
#             + H_tr_1 * (theta_sup + (phi_ia + phi_HC_nd_10) / H_ve_adj)
#         ) / (H_tr_ms + H_tr_w + H_tr_1)
#         hourly_result.theta_air_10[current_index] = (
#             H_tr_is * theta_s_10 + H_ve_adj * theta_sup + phi_ia + phi_HC_nd_10
#         ) / (H_tr_is + H_ve_adj)
#         theta_air_10 = hourly_result.theta_air_10[current_index]
#         theta_op_10 = 0.3 * theta_air_10 + 0.7 * theta_s_10
#         # Unrestricted heating/cooling, phi_HC_nd_un, is positive for heating and negative for cooling
#         phi_HC_nd_un = (phi_HC_nd_10 * (theta_air_set - theta_air_0)) / (theta_air_10 - theta_air_0)
#
#         # STEP 3
#         # ------
#         if ((phi_HC_nd_un) > phi_C_max) & ((phi_HC_nd_un) < phi_H_max):
#             phi_HC_nd_ac = phi_HC_nd_un
#             hourly_result.theta_air_ac[current_index] = theta_air_set
#             hourly_result.theta_air[current_index] = hourly_result.theta_air_ac[current_index]
#             # The energy need (MJ) for heating or cooling for a given hour, Q_HC_nd, is positive in the case of heating need and negative in the case of cooling need
#             hourly_result.Q_HC_nd[current_index] = phi_HC_nd_ac * 0.036
#             hourly_result.Q_H_nd[current_index] = max(0, (hourly_result.Q_HC_nd[current_index]))
#             hourly_result.Q_C_nd[current_index] = abs(min(0, (hourly_result.Q_HC_nd[current_index])))
#             # To kWh
#             hourly_result.Q_H_nd[current_index] = hourly_result.Q_H_nd[current_index] * 0.2777777778
#             hourly_result.Q_C_nd[current_index] = hourly_result.Q_C_nd[current_index] * 0.2777777778
#             Q_int = phi_int * 0.036
#             Q_sol = phi_sol * 0.036
#             hourly_result.theta_m_t[current_index] = theta_m_t_10
#             theta_m_t = theta_m_t_10
#             theta_m_tp_list.clear()  # EJW mtp
#             theta_m_tp_list.append(theta_m_t)  # EJW mtp
#             current_timestamp += delta
#             current_index += 1
#             continue
#
#         # STEP 4
#         # ------
#         else:
#             if phi_HC_nd_un > 0:
#                 phi_HC_nd_ac = phi_H_max
#             elif phi_HC_nd_un < 0:
#                 phi_HC_nd_ac = phi_C_max
#         # Other combined heat conductances
#         H_tr_2 = H_tr_1 + H_tr_w
#         H_tr_3 = 1 / (1 / H_tr_2 + 1 / H_tr_ms)
#         # Set theta_m_tp as theta_m_t from previous step
#         if current_index == 0:
#             theta_m_tp = 0
#             theta_m_t = 0
#         else:
#             theta_m_tp = theta_m_t
#         phi_HC_nd = phi_HC_nd_ac
#         phi_mtot = (
#             phi_m
#             + H_tr_em * theta_sup
#             + H_tr_3
#             * (phi_st + H_tr_w * theta_sup + H_tr_1 * (((phi_ia + phi_HC_nd) / H_ve_adj) + theta_sup))
#             / H_tr_2
#         )
#         theta_m_tp = theta_m_tp_list[0]  # EJW mtp
#         hourly_result.theta_m_t[current_index] = (
#             theta_m_tp * ((C_m / 3600) - 0.5 * (H_tr_3 + H_tr_em)) + phi_mtot
#         ) / ((C_m / 3600) + 0.5 * (H_tr_3 + H_tr_em))
#         theta_m_t = hourly_result.theta_m_t[current_index]
#         theta_m_tp_list.clear()  # EJW mtp
#         theta_m_tp_list.append(theta_m_t)  # EJW mtp
#         theta_m = (theta_m_t + theta_m_tp) / 2
#         theta_s = (
#             H_tr_ms * theta_m
#             + phi_st
#             + H_tr_w * theta_sup
#             + H_tr_1 * (theta_sup + (phi_ia + phi_HC_nd) / H_ve_adj)
#         ) / (H_tr_ms + H_tr_w + H_tr_1)
#         hourly_result.theta_air_ac[current_index] = (
#             H_tr_is * theta_s + H_ve_adj * theta_sup + phi_ia + phi_HC_nd
#         ) / (H_tr_is + H_ve_adj)
#         theta_air_ac = hourly_result.theta_air_ac[current_index]
#         hourly_result.theta_air[current_index] = hourly_result.theta_air_ac[current_index]
#         theta_op = 0.3 * theta_air_ac + 0.7 * theta_s
#         # The energy need (MJ) for heating or cooling for a given hour, Q_HC_nd, is positive in the case of heating need and negative in the case of cooling need
#         hourly_result.Q_HC_nd[current_index] = phi_HC_nd_ac * 0.036
#         # Heating
#         hourly_result.Q_H_nd[current_index] = max(0, (hourly_result.Q_HC_nd[current_index]))
#         # Cooling
#         hourly_result.Q_C_nd[current_index] = abs(min(0, (hourly_result.Q_HC_nd[current_index])))
#         # To kWh
#         hourly_result.Q_H_nd[current_index] = hourly_result.Q_H_nd[current_index] * 0.2777777778
#         hourly_result.Q_C_nd[current_index] = hourly_result.Q_C_nd[current_index] * 0.2777777778
#
#         # Other (EJW: also add to unrestricted if statement)
#         # Internal and solar heat gains (MJ)
#         Q_int = phi_int * 0.036
#         Q_sol = phi_sol * 0.036
#
#         print("<<<End calculation for timestep, Case 2 or 4>>>")
#
#         current_timestamp += delta
#         current_index += 1
#
#     print("LOADING RESULTS")
#     # Results
#     results = {
#         "Hour of day": solar_gains.hour_of_day,
#         "Day of week": solar_gains.day_of_week,
#         "Outside temperature (DegC)": solar_gains.t_outside_t2m_degC,
#         "Actual internal temperature (DegC)": hourly_result.theta_air,
#         "Heating demand": hourly_result.Q_H_nd,
#         "Cooling demand": hourly_result.Q_C_nd,
#         "phi_int": hourly_result.phi_int,
#         "phi_sol": hourly_result.phi_sol,
#         "i_sol": solar_gains.i_sol,
#         "theta_air_0": hourly_result.theta_air_0,
#         "theta_air_10": hourly_result.theta_air_10,
#         "theta_m_tp": hourly_result.theta_m_tp,
#         "theta_m_t": hourly_result.theta_m_t,
#     }
#     df_results = pd.DataFrame(results)
#     df_results.to_csv("Output/output" + "_" + time.strftime("%Y%m%d-%H%M%S") + ".csv")
#
#
# def metabolic_gains(A_f, day_of_week, hour_of_day):
#     """
#     Heat flow rate for metabolic heat from occupants and dissipated heat from appliances (W)
#     Args:
#         A_f:
#         day_of_week:
#         hour_of_day:
#
#     Returns:
#
#     """
#     if (
#         (day_of_week == 0) | (day_of_week == 1) | (day_of_week == 2) | (day_of_week == 3) | (day_of_week == 4)
#     ) & (7 <= hour_of_day <= 17):
#         hf_Oc_A_LRK = 8
#         hf_Oc_A_Oth = 1
#     elif (
#         (day_of_week == 0) | (day_of_week == 1) | (day_of_week == 2) | (day_of_week == 3) | (day_of_week == 4)
#     ) & (17 < hour_of_day <= 23):
#         hf_Oc_A_LRK = 20
#         hf_Oc_A_Oth = 1
#     elif (
#         (day_of_week == 0) | (day_of_week == 1) | (day_of_week == 2) | (day_of_week == 3) | (day_of_week == 4)
#     ) & (23 < hour_of_day < 7):
#         hf_Oc_A_LRK = 2
#         hf_Oc_A_Oth = 6
#     elif ((day_of_week == 5) | (day_of_week == 6)) & (7 <= hour_of_day <= 17):
#         hf_Oc_A_LRK = 8
#         hf_Oc_A_Oth = 2
#     elif ((day_of_week == 5) | (day_of_week == 6)) & (17 < hour_of_day <= 23):
#         hf_Oc_A_LRK = 20
#         hf_Oc_A_Oth = 4
#     else:
#         hf_Oc_A_LRK = 2
#         hf_Oc_A_Oth = 6
#
#     phi_int_Oc_A = (hf_Oc_A_LRK + hf_Oc_A_Oth) * A_f
#     return phi_int_Oc_A
#
#
# def load_climate_data(user_lat, user_long):
#     # Load weather data
#     climate_data = xr.open_dataset("Input/copernicus_weather.nc")
#     ds_weather_loc = climate_data.sel(latitude=user_lat, longitude=user_long, method="nearest")
#     df_weather_loc = ds_weather_loc.to_dataframe()
#     humidity_data = xr.open_dataset("Input/copernicus_pressurelevels.nc")
#     ds_humidity_loc = humidity_data.sel(latitude=user_lat, longitude=user_long, method="nearest")
#     df_humidity_loc = ds_humidity_loc.to_dataframe()
#     humidity_join = df_humidity_loc["r"]
#     df_weather = df_weather_loc.join(humidity_join)
#     return df_weather
