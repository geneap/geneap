import uuid
from datetime import datetime
from typing import Annotated, Optional

import shapely
from pandera.typing import DataFrame
from pandera.typing.geopandas import GeoDataFrame
from sqlalchemy import ARRAY, Column, ForeignKey, Identity, Integer, PickleType, Table, func
from sqlalchemy.orm import Mapped, mapped_column, relationship

import tessa.core.models as core_models
import tessa.district
import tessa.thermal_system_models.models as thermal_models
from tessa import building_data, climate_data, thermal_system_models
from tessa.administration.orm import Project, User
from tessa.core.models import DatasetType
from tessa.core.orm import (
    Base,
    BaseDataclass,
    DateTimeDefault,
    IdentityPk,
    LineStringGeom,
    MultiLineStringGeom,
    MultiPolygonGeom,
    ParquetDataframe,
    ParquetGeoDataFrame,
    PointGeom,
    PolygonGeom,
    PydanticType,
    ShapelyGeometry,
    UuidIndex,
    UuidPk,
)
from tessa.thermal_system_models.shallow_geothermal import Borehole, DoubleUBorehole, GroundShallow, borefield
from tessa.utils.pydantic_shapely import Polygon

# Declare the projects schema to be used in sql queries and table definitions
PROJECTS_SCHEMA = "projects"


class _ProjectBaseDataclass(BaseDataclass):
    __abstract__ = True
    __table_args__ = {"schema": PROJECTS_SCHEMA}


# This the helper table that handles a many-to-many relation
# Between SaveInfo and SaveInfoCompare. More details in the
# according model definitions.
# This need to be defined first, so that we can refer to this
# Table in the SaveInfo and SaveInfoCompare tables
saveinfo_saveinfocompare_table = Table(
    "info_info_compare",
    Base.metadata,
    Column("info_id", ForeignKey("projects.info.id", ondelete="CASCADE"), primary_key=True),
    Column("info_compare_id", ForeignKey("projects.info_compare.id", ondelete="CASCADE"), primary_key=True),
    schema=PROJECTS_SCHEMA,
)


class SaveInfo(_ProjectBaseDataclass):
    """SaveInfo representes file-like metadata for generic saved objects in our app.

    We can attach any object to a saveinfo to give standard metadata like created
    and modified times, project, etc.

    Ownership is handled through projects, all objects are shared within a project between all
    users with access to that project.

    """

    __tablename__ = "info"

    name: Mapped[str]

    # For now, don't include a relationship to Project because usually we only need save infos for
    # projects, not projects for save info. Could add just to use back_populates on Projects.
    project_id: Mapped[uuid.UUID] = mapped_column(ForeignKey(Project.id, ondelete="CASCADE"))

    description: Mapped[str | None] = mapped_column(default=None)
    kind: Mapped[str | None] = mapped_column(default=None)
    created_by: Mapped[str | None] = mapped_column(default=None)
    modified_by: Mapped[str | None] = mapped_column(default=None)
    created_on: Mapped[datetime] = mapped_column(insert_default=func.now, default=None)
    modified: Mapped[datetime] = mapped_column(insert_default=func.now, default=None)

    # A field to handle the many-to-many relationship with SaveInfoCompare
    saveinfo_compares: Mapped[list["SaveInfoCompare"]] = relationship(
        secondary=saveinfo_saveinfocompare_table, back_populates="saveinfos", default_factory=list
    )

    # Allow to create save history as a linked list
    previous_saveinfo_id: Mapped[uuid.UUID | None] = mapped_column(
        ForeignKey(f"{PROJECTS_SCHEMA}.info.id"), default=None
    )
    previous_saveinfo: Mapped[Optional["SaveInfo"]] = relationship("SaveInfo", default=None)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    def __repr__(self):
        return f"SaveInfo({self.id}, {self.name}, {self.kind}, created_on={self.created_on}, modified={self.modified})"


SaveInfoFk = Annotated[uuid.UUID | None, mapped_column(ForeignKey(SaveInfo.id, ondelete="CASCADE"))]


class SaveInfoCompare(_ProjectBaseDataclass):
    """
    SaveInfoCompare (SIC) represents an object that we use to compare multiple SaveInfos (SI)
    within a single project.

    The same SI can be compared in different SIC and each SIC can have multiple SIs.
    This is ensured using a many-to-many relationship with SaveInfo.
    """

    __tablename__ = "info_compare"

    project_id: Mapped[uuid.UUID] = mapped_column(ForeignKey(Project.id, ondelete="CASCADE"))

    description: Mapped[str | None] = mapped_column(default=None)

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    # A field to handle the many-to-many relationship with SaveInfo
    saveinfos: Mapped[list["SaveInfo"]] = relationship(
        secondary=saveinfo_saveinfocompare_table, back_populates="saveinfo_compares", default_factory=list
    )

    def __repr__(self):
        return f"SaveInfoCompare(id={self.id}, description={self.description})"


class SavedPickle(_ProjectBaseDataclass):
    __tablename__ = "saved_pickle"

    data = Column(PickleType)
    info_id: Mapped[SaveInfoFk]
    info = relationship(SaveInfo, passive_deletes=True)
    uid: Mapped[uuid.UUID] = mapped_column(
        primary_key=True, index=True, unique=True, default_factory=uuid.uuid4
    )
    py_type: Mapped[str | None] = mapped_column(default=None)
    object_version: Mapped[int] = mapped_column(nullable=False, default=1)

    def __init__(self, data, info=None):
        if hasattr(data, "uid"):
            uid = data.uid
        else:
            uid = uuid.uuid4()
        py_type = type(data)
        super().__init__(uid=uid, py_type=py_type, data=data, info=info)


class Lock(_ProjectBaseDataclass):
    """
    Analogue to linux /proc/locks table, holds information on locking of save info objects
    to avoid conflicts when several users try to edit the same file.
    """

    saveinfo_id: Mapped[SaveInfoFk] = mapped_column(default=None)
    saveinfo: Mapped[SaveInfo] = relationship(init=False)

    # Allow user to be none because we might apply locks for e.g. automated processing where we
    # don't want to create a fake user.
    by_user_id: Mapped[uuid.UUID] = mapped_column(ForeignKey(User.id, ondelete="CASCADE"), default=None)
    by_user: Mapped[User] = relationship(init=False)
    # when using dataclass, need to make sure attrs with defaults come after non default ones
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)
    type: Mapped[str] = mapped_column(default="exclusive")
    # use server default so don't need to supply a value for created
    created: Mapped[DateTimeDefault] = mapped_column(default=None)


class Workspace(_ProjectBaseDataclass):
    saveinfo_id: Mapped[SaveInfoFk] = mapped_column(default=None)
    saveinfo: Mapped[SaveInfo] = relationship(init=False)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


ShapelyLineStringGeom = Annotated[
    shapely.LineString, mapped_column(ShapelyGeometry(geometry_type="LINESTRING", spatial_index=True))
]


class RoadsAndWays(_ProjectBaseDataclass):
    saveinfo_id: Mapped[SaveInfoFk] = mapped_column(default=None)
    saveinfo: Mapped[SaveInfo] = relationship(init=False)

    name: Mapped[str | None] = mapped_column(default=None)
    source: Mapped[str | None] = mapped_column(default=None)

    geometry: Mapped[ShapelyLineStringGeom | None] = mapped_column(default=None)

    # For drawn roads we don't consider the individual element id to be important, just
    # so no need for a UUID.
    id: Mapped[IdentityPk] = mapped_column(init=False)

    def equals(self, other):
        return self.geometry.equals(other)


BuildingsDataframe = Annotated[
    GeoDataFrame[core_models.PowerLoad], mapped_column(ParquetGeoDataFrame(building_data.Building))
]
PowerLoadDataframe = Annotated[
    DataFrame[core_models.PowerLoad], mapped_column(ParquetDataframe(core_models.PowerLoad))
]


class District(_ProjectBaseDataclass):
    """District ORM class"""

    name: Mapped[str]
    uid: Mapped[uuid.UUID] = mapped_column(index=True, default_factory=uuid.uuid4)

    info_id: Mapped[SaveInfoFk] = mapped_column(default=None)
    info: Mapped[SaveInfo] = relationship(passive_deletes=True, default=None)

    calculation_year: Mapped[int] = mapped_column(default=2015)
    buildings: Mapped[BuildingsDataframe | None] = mapped_column(default=None, repr=False)
    climate: Mapped[Optional["ClimateData"]] = relationship(
        passive_deletes=True, cascade="all, delete", default=None
    )

    load_curve: Mapped[PowerLoadDataframe | None] = mapped_column(default=None, repr=False)

    crs: Mapped[int] = mapped_column(default=4326)
    local_crs: Mapped[int] = mapped_column(default=2056)
    boundary: Mapped[PolygonGeom] = mapped_column(default=None)

    utility_networks: Mapped[list["UtilityNetwork"]] = relationship(
        passive_deletes=True, cascade="all, delete", default_factory=list
    )

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    @classmethod
    def from_object(cls, district: tessa.district.District, include_relations=True, save_info=None):
        utility_networks = []
        if include_relations:
            for un in district.utility_networks:
                if isinstance(un, thermal_system_models.DistrictNetwork):
                    t = DistrictNetwork.from_object(un)
                    utility_networks.append(t)
        if district.climate is not None:
            climate = ClimateData(
                schema_version=district.climate.schema_version,
                source=district.climate.source,
                timeseries=district.climate.timeseries,
            )
        else:
            climate = None
        d = cls(
            info=save_info,
            uid=district.uid,
            name=district.name,
            boundary=district.boundary,
            buildings=district.buildings,
            calculation_year=district.calculation_year,
            crs=district.crs,
            local_crs=district.local_crs,
            load_curve=district.load_curve,
            climate=climate,
            utility_networks=utility_networks,
        )
        return d


ClimateDataframe = Annotated[
    DataFrame[climate_data.ClimateRecord], mapped_column(ParquetDataframe(climate_data.ClimateRecord))
]


class ClimateData(_ProjectBaseDataclass):
    """
    NOTE for future: could copy a version of this class to save climate
    time series that are not tied to a district (e.g. custom climate files)
    """

    source: Mapped[str | None] = mapped_column(default=None)
    schema_version: Mapped[int] = mapped_column(default=1)
    timeseries: Mapped[ClimateDataframe | None] = mapped_column(default=None, repr=False)
    # CDD&HDD get regenerated so no need to store
    # heating_degree_days = Column(Float)
    # cooling_degree_days = Column(Float)
    district_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey(District.id, ondelete="CASCADE"), index=True, nullable=False, default=None
    )

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


class UtilityNetwork(
    _ProjectBaseDataclass,
    # dataclass_callable=pydantic.dataclasses.dataclass,
):
    """UtilityNetwork save class

    Parent class for district networks (and eventually other types of utility networks e.g. gas).

    Uses sqlalchemy join table subclassing through polymorphic identity. The kind field is also
    used in the related Pydantic UtilityNetwork class to help distinguish different types of
    utility network. The model of utility networks being a list of objects attached to a district
    is based on the CityGML specification (although the spec isn't very useful for defining a lot
    of the details we need).

    """

    uid: Mapped[UuidIndex]
    kind: Mapped[str]
    name: Mapped[str | None]
    # district = relationship(District)
    district_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey(District.id, ondelete="CASCADE"),
        index=True,
        nullable=False,
        default=None,  # Default to none for the python side constructor, this gets set by the District relation
    )

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    __mapper_args__ = {
        "polymorphic_identity": "utilitynetwork",
        "polymorphic_on": "kind",
    }


NetworkDemandDataframe = Annotated[
    DataFrame[thermal_models.NetworkDemand],
    mapped_column(ParquetDataframe(thermal_models.NetworkDemand), nullable=True),
]
PipesDataFrame = Annotated[
    GeoDataFrame[thermal_models.Pipes], mapped_column(ParquetGeoDataFrame(thermal_models.Pipes))
]
BuildingConnectionsDataFrame = Annotated[
    GeoDataFrame[thermal_models.BuildingConnections],
    mapped_column(ParquetGeoDataFrame(thermal_models.BuildingConnections)),
]


class DistrictNetwork(
    UtilityNetwork,
    # dataclass_callable=pydantic.dataclasses.dataclass,
):
    """ORM mapping for district network"""

    __tablename__ = "districtnetwork"
    __mapper_args__ = {
        "polymorphic_identity": "districtnetwork",
    }

    graph: Mapped[DatasetType | None] = mapped_column(PickleType, default=None, repr=False)
    pipes: Mapped[PipesDataFrame | None] = mapped_column(default=None, repr=False)
    building_connections: Mapped[BuildingConnectionsDataFrame | None] = mapped_column(
        default=None, repr=False
    )

    building_ids: Mapped[list[int]] = mapped_column(ARRAY(Integer), default_factory=list)
    power_demand_curve: Mapped[NetworkDemandDataframe | None] = mapped_column(default=None, repr=False)

    flow: Mapped[Optional["FlowProperties"]] = relationship(passive_deletes=True, default=None)

    costs: Mapped[thermal_models.NetworkCosts | None] = mapped_column(
        PydanticType(thermal_models.NetworkCosts), default=None
    )
    pipe_spec: Mapped[Optional[thermal_models.PipeSpec]] = mapped_column(
        PydanticType(thermal_models.PipeSpec), default=None
    )

    substations: Mapped[list["Substation"]] = relationship(passive_deletes=True, default_factory=list)

    id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey(UtilityNetwork.id, ondelete="CASCADE"), primary_key=True, default_factory=uuid.uuid4
    )

    @classmethod
    def from_object(cls, district_network, include_relations=True):
        return cls(
            kind="districtnetwork",
            name=district_network.name or "district_heating",
            uid=district_network.uid,
            graph=district_network.graph,
            pipes=district_network.pipes,
            pipe_spec=district_network.pipe_spec,
            building_connections=district_network.building_connections,
            building_ids=district_network.building_ids,
            power_demand_curve=district_network.power_demand_curve,
            costs=district_network.costs,
            flow=FlowProperties.from_object(district_network.flow),
            substations=[Substation.from_object(s) for s in district_network.substations],
        )


DistrictNetworkUniqueFk = Annotated[
    uuid.UUID, mapped_column(ForeignKey(DistrictNetwork.id, ondelete="CASCADE"), unique=True)
]

DistrictNetworkFk = Annotated[
    uuid.UUID, mapped_column(ForeignKey(DistrictNetwork.id, ondelete="CASCADE"), unique=False)
]



class Substation(_ProjectBaseDataclass):
    """Substation orm mapping"""

    name: Mapped[str] = mapped_column(default="Substation", server_default="Substation")
    district_network_id: Mapped[DistrictNetworkFk] = mapped_column(default=None)

    sources: Mapped[list["ThermalSource"]] = relationship(passive_deletes=True, default_factory=list)
    pumps: Mapped[list["Pump"]] = relationship(passive_deletes=True, default_factory=list)

    position: Mapped[PointGeom | None] = mapped_column(default=None, nullable=True)
    connection_point: Mapped[Optional["ConnectionPoint"]] = relationship(passive_deletes=True, default=None)

    uid: Mapped[uuid.UUID] = mapped_column(index=True, default_factory=uuid.uuid4)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    @classmethod
    def from_object(cls, obj, include_relations=False):
        substation = super(Substation, cls).from_object(obj)
        if substation.position is not None:
            substation.position = substation.position.wkt  # maybe use custom type instead
        if obj.connection_point is not None:
            substation.connection_point = ConnectionPoint(
                position=obj.connection_point.position, node_id=obj.connection_point.node_id
            )
        else:
            substation.connection_point = None
        sources = []
        for s in obj.sources:
            ts = ThermalSource.from_object(s)
            ts.injection_point = None
            ts.position = None
            sources.append(ts)
        substation.sources = sources

        pumps = []
        for s in obj.pumps:
            p = Pump.from_object(s)
            pumps.append(p)
        substation.pumps = pumps

        return substation


SubstationFk = Annotated[uuid.UUID | None, mapped_column(ForeignKey(Substation.id, ondelete="CASCADE"))]

SupplyCurveDatafame = Annotated[
    DataFrame[thermal_models.SupplyCurve], mapped_column(ParquetDataframe(thermal_models.SupplyCurve))
]


class ThermalSource(_ProjectBaseDataclass):
    # uid: Mapped[UuidIndex] = mapped_column(default_factory=uuid.uuid4)
    uid: Mapped[UuidIndex]

    name: Mapped[str | None]
    code: Mapped[str | None]

    description: Mapped[str | None] = mapped_column(default=None)
    efficiency: Mapped[float | None] = mapped_column(default=None)

    rated_power_useful: Mapped[float | None] = mapped_column(default=None)
    rated_power_final: Mapped[float | None] = mapped_column(default=None)
    fuel: Mapped[Optional["Fuel"]] = relationship(passive_deletes=True, default=None)

    supply_curve: Mapped[SupplyCurveDatafame | None] = mapped_column(default=None, repr=False)
    energy_useful: Mapped[float | None] = mapped_column(default=None)
    energy_final: Mapped[float | None] = mapped_column(default=None)
    lifetime: Mapped[float | None] = mapped_column(default=None)
    # investment per capacity
    investment_per_kw: Mapped[float | None] = mapped_column(default=None)
    # Variable O&M is per kWh supplied. DO NOT include the fuel costs.
    variable_o_and_m: Mapped[float | None] = mapped_column(default=None)
    # fixed O&M is per rated thermal capacity
    fixed_o_and_m: Mapped[float | None] = mapped_column(default=None)

    investment_cost: Mapped[float | None] = mapped_column(default=None)
    o_and_m: Mapped[float | None] = mapped_column(default=None)
    co2_eq_total: Mapped[float | None] = mapped_column(default=None)
    fuel_cost: Mapped[float | None] = mapped_column(default=None)

    substation_id: Mapped[SubstationFk] = mapped_column(default=None)

    injection_point: Mapped[Optional["ThermalInjectionPoint"]] = relationship(
        passive_deletes=True, default=None
    )

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    @classmethod
    def from_object(cls, obj: thermal_system_models.ThermalSource, include_relations=False):
        source: ThermalSource = super(ThermalSource, cls).from_object(obj)
        # if source.position is not None:
        #     source.position = source.position.wkt
        if source.injection_point is not None:
            source.injection_point = ThermalInjectionPoint.from_object(obj.injection_point)
        source.fuel = Fuel.from_object(obj.fuel)
        return source


ThermalSourceFk = Annotated[
    uuid.UUID, mapped_column(ForeignKey(ThermalSource.id, ondelete="CASCADE"), unique=False)
]


class ThermalInjectionPoint(_ProjectBaseDataclass):
    position: Mapped[PointGeom]
    thermal_source_id: Mapped[ThermalSourceFk]

    node_id: Mapped[int | None]

    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


class ConnectionPoint(_ProjectBaseDataclass):
    position: Mapped[PointGeom]
    node_id: Mapped[int | None]

    substation_id: Mapped[SubstationFk] = mapped_column(init=False)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


class Fuel(_ProjectBaseDataclass):
    code: Mapped[str | None]
    name: Mapped[str | None]
    price: Mapped[float | None] = mapped_column(default=None)
    currency: Mapped[str | None] = mapped_column(default=None)
    co2_eq_kwh: Mapped[float | None] = mapped_column(default=None)
    description: Mapped[str | None] = mapped_column(default=None)
    price_unit: Mapped[str | None] = mapped_column(default=None)

    thermal_source_id: Mapped[ThermalSourceFk] = mapped_column(default=None)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


class FlowProperties(_ProjectBaseDataclass):
    """"""

    temp_going: Mapped[float | None]
    temp_return: Mapped[float | None]
    fluid_type: Mapped[str | None]
    mix_percent: Mapped[float | None]

    district_network_id: Mapped[DistrictNetworkUniqueFk | None] = mapped_column(default=None, nullable=True)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)


class Pump(_ProjectBaseDataclass):
    name: Mapped[str | None]
    type: Mapped[str | None]
    description: Mapped[str | None]

    rated_power: Mapped[float | None]
    efficiency: Mapped[float | None]
    head: Mapped[float | None]
    pump_cost: Mapped[float | None]
    bare_module_cost: Mapped[float | None]

    uid: Mapped[UuidIndex] = mapped_column(default_factory=uuid.uuid4)

    substation_id: Mapped[SubstationFk] = mapped_column(default=None)
    id: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)



ConfigurationDataframe = Annotated[
    GeoDataFrame[borefield.Configuration],
    mapped_column(ParquetGeoDataFrame(borefield.Configuration), nullable=True),
]


class BoreField(_ProjectBaseDataclass):
    """
    Represents a borehole field consisting of a borehole type,
    BoreholeConfiguration giving the location of the boreholes
    and Ground and  Fluid properties
    """

    fluid_id: Mapped[uuid.UUID | None] = mapped_column(
        ForeignKey(FlowProperties.id), default=None, nullable=True
    )
    fluid: Mapped[FlowProperties | None] = relationship(
        passive_deletes=True, cascade="all, delete-orphan", single_parent=True, default=None
    )
    # fluid: Mapped[FlowProperties | None] = mapped_column(PydanticType(FlowProperties), default=None)
    name: Mapped[str] = mapped_column(default="bore_field")
    lifetime: float | None = 50.0  # Unit: year
    ground: Mapped[GroundShallow | None] = mapped_column(PydanticType(GroundShallow), default=None)
    # assume that all boreholes are identical. Therefore, only need to store the parameters of one borehole.
    borehole: Mapped[Borehole | DoubleUBorehole | None] = mapped_column(
        PydanticType(DoubleUBorehole), default=None
    )

    crs: int = 4326
    local_crs: int = 2056
    drill_area: Mapped[MultiPolygonGeom | None] = mapped_column(default=None, nullable=True)
    configuration: Mapped[ConfigurationDataframe | None] = mapped_column(default=None)
    boundary: Mapped[Polygon] = mapped_column(
        ShapelyGeometry(result_type=Polygon, geometry_type="POLYGON"),
        default=None,
        nullable=True,
    )

    uid: Mapped[UuidPk] = mapped_column(default_factory=uuid.uuid4)

    info_id: Mapped[SaveInfoFk] = mapped_column(default=None)
    info: Mapped[SaveInfo] = relationship(init=False)

    @classmethod
    def from_object(cls, obj, include_relations=True):
        borefield = super().from_object(obj, include_relations=False)
        if obj.fluid is not None:
            borefield.fluid = FlowProperties.from_object(obj.fluid)
            assert borefield.fluid is not None

        return borefield

