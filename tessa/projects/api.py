import datetime
import uuid
from typing import Any

import structlog
from pydantic import BaseModel
from sqlalchemy import delete, inspect, select
from sqlalchemy.orm import Session, make_transient, selectinload

import tessa.core.orm
from tessa.administration import Project, User
from tessa.district.models import District, DistrictNetwork, UtilityNetwork
from tessa.log import log_inputs_and_outputs

from . import orm

log = structlog.get_logger()


@log_inputs_and_outputs
def create_lock(db: Session, save_info: orm.SaveInfo | str | uuid.UUID, user: User | None = None):
    if isinstance(save_info, orm.SaveInfo):
        saveinfo_id = save_info.id
    elif isinstance(save_info, str):
        saveinfo_id = uuid.UUID(save_info)
    else:
        saveinfo_id = save_info
    lock = orm.Lock(saveinfo_id=saveinfo_id)
    if user is not None:
        lock.by_user_id = user.id
    db.add(lock)
    db.commit()

    return lock


@log_inputs_and_outputs
def check_saveinfo_locks(
    db: Session,
    saveinfo_id,
    except_user: User | None = None,
    for_user: User | None = None,
) -> list[orm.Lock]:
    """
    Check if this save info is locked, optionally excluding a user - usually the
    current user, since it's fine if user asks for a lock they already hold.
    Args:
        db:
        saveinfo_id:
        except_user:
        for_user:
    Returns:

    """

    # Only one of the two User-related arguments can be specified
    assert (except_user is None) or (for_user is None)

    query = select(orm.Lock).options(selectinload("*")).where(orm.Lock.saveinfo_id == saveinfo_id)

    if except_user:
        query = query.where(orm.Lock.by_user_id != str(except_user.id))
    elif for_user:
        query = query.where(orm.Lock.by_user_id == str(for_user.id))

    return list(db.execute(query).scalars().all())


def release_locks(db: Session, saveinfo_id=None):
    """
    Release all locks of a selected saveinfo. If saveinfo_id=None - clear ALL locks.
    """

    query = delete(orm.Lock)

    if saveinfo_id:
        query = query.where(orm.Lock.saveinfo_id == saveinfo_id)

    db.execute(query)
    db.commit()


def release_lock(db: Session, lock: orm.Lock):
    db.delete(lock)
    db.commit()


def release_user_locks(db, saveinfo_id, user_id):
    query = delete(orm.Lock).where(orm.Lock.saveinfo_id == saveinfo_id).where(orm.Lock.by_user_id == user_id)
    db.execute(query)
    db.commit()


def get_saveinfo(db: Session, saveinfo_id: str) -> orm.SaveInfo | None:
    return db.get(orm.SaveInfo, saveinfo_id, options=[selectinload("*")])


def get_saveinfo_compare(
    db: Session, saveinfo_compare_id: str | None = None, project_id: str | None = None
) -> orm.SaveInfoCompare | None:
    """
    Find SaveInfoCompare either by its id, or by an id of a project associated with it.

    Note: either saveinfo_compare_id or project_id should be passed, not both!

    :param: saveinfo_compare_id - string id of the SaveInfoCompare to find
    :param: project_id - string id of the associated project, SaveInfoCompare of which we want to get

    :return: orm.SaveInfoCompare or None depending on whether there is an associated result
    """

    # only one of the two should be provided
    by_id = saveinfo_compare_id is not None and project_id is None
    by_project_id = saveinfo_compare_id is None and project_id is not None

    assert (by_id or by_project_id) and (not (by_id and by_project_id))

    query = select(orm.SaveInfoCompare)

    if saveinfo_compare_id:
        query = query.where(orm.SaveInfoCompare.id == saveinfo_compare_id)
    else:
        query = query.where(orm.SaveInfoCompare.project_id == project_id)

    return db.execute(query).scalar_one_or_none()


def create_saveinfo_compare(db: Session, project_id: str) -> orm.SaveInfoCompare:
    """
    Create a SaveInfoCompare for the specified project

    :param: project_id - string id of the associated project, SaveInfoCompare of which we want to get

    :return: orm.SaveInfoCompare or None depending on whether there is an associated result
    """
    project_uid = uuid.UUID(str(project_id))  # force revalidate UUID
    saveinfo_compare = orm.SaveInfoCompare(project_id=project_uid, saveinfos=[], description="")
    db.add(saveinfo_compare)
    db.commit()

    return saveinfo_compare


def add_saveinfo_to_compare(db: Session, saveinfo_id: str, saveinfo_compare_id: str):
    """
    Adding a relationship between SaveInfo (SI) and SaveInfoCompare (SIC).

    We add SIC to the SI's list of SICs. But because we have a many-to-many relationship
    between SaveInfo and SaveInfoCompare, we could've equivalently added SI to the SIC's list of SIs.
    Ensuring the consistency of the other direciton is done automatically by SQLAlchemy.

    :param: saveinfo_id - string id of the SaveInfoCompare
    :param: saveinfo_compare_id - string id of the SaveInfoCompare

    :return: None
    """

    saveinfo = get_saveinfo(db, saveinfo_id)
    if saveinfo is None:
        raise ValueError(f"No SaveInfo with id: {saveinfo_id}")

    saveinfo_compare = get_saveinfo_compare(db=db, saveinfo_compare_id=saveinfo_compare_id)

    saveinfo.saveinfo_compares.append(saveinfo_compare)

    db.commit()


def remove_saveinfo_to_compare(db: Session, saveinfo_id: orm.SaveInfo, saveinfo_compare_id: str) -> None:
    """
    Removing a relationship between SaveInfo (SI) and SaveInfoCompare (SIC).

    We remove SIC from the SI's list of SICs. But because we have a many-to-many relationship
    between SaveInfo and SaveInfoCompare, we could've equivalently removed SI from the SIC's list of SIs.
    Ensuring the consistency of the other direciton is done automatically by SQLAlchemy.

    :param: saveinfo_id - string id of the SaveInfoCompare
    :param: saveinfo_compare_id - string id of the SaveInfoCompare

    :return: None
    """

    saveinfo = get_saveinfo(db, saveinfo_id)
    saveinfo_compare = get_saveinfo_compare(db=db, saveinfo_compare_id=saveinfo_compare_id)

    saveinfo.saveinfo_compares.remove(saveinfo_compare)

    db.commit()


def check_saveinfo_in_compare(db: Session, saveinfo_id: str, saveinfo_compare_id: str) -> bool:
    """
    Checking whether there is a connection between the specified SaveInfo (SI) and SaveInfoCompare (SIC).

    We check this by checking whether the SI is in the list of SIs of the SIC.
    But because we have a many-to-many relationship between SaveInfo and SaveInfoCompare,
    we could've equivalently checked whether the SIC is in the list of SICs of the SI.

    :param: saveinfo_id - string id of the SaveInfoCompare
    :param: saveinfo_compare_id - string id of the SaveInfoCompare

    :return: True or False depending on whehter there is a connection or not
    """
    saveinfo = get_saveinfo(db, saveinfo_id)
    saveinfo_compare = get_saveinfo_compare(db=db, saveinfo_compare_id=saveinfo_compare_id)

    return saveinfo in saveinfo_compare.saveinfos


@log_inputs_and_outputs
def find_saveinfo_by_name(db: Session, name: str, project: Project) -> orm.SaveInfo | None:
    """
    Find a saved object within a project.

    Args:
        db:
        name:
        project:

    Returns:

    """
    query = select(orm.SaveInfo).filter(orm.SaveInfo.name == name)
    if project is not None:
        query = query.filter(orm.SaveInfo.project_id == project.id)
    return db.execute(query).scalar()


@log_inputs_and_outputs
def list_saved_items_for_project(
    db: Session, project: Project | uuid.UUID | str, order_by=None
) -> list[orm.SaveInfo]:
    if hasattr(project, "id"):
        project = project.id
    query = select(orm.SaveInfo).where(orm.SaveInfo.project_id == project)
    if order_by is not None:
        query = query.order_by(order_by)
    else:
        query = query.order_by(orm.SaveInfo.created_on)
    return list(db.execute(query).scalars().all())


@log_inputs_and_outputs
def find_object_for_saveinfo_as(db: Session, info: orm.SaveInfo, orm_type, model_type: type[BaseModel]):
    db_obj = db.execute(select(orm_type).where(orm_type.info_id == info.id)).scalar()
    if db_obj is None:
        return None
    return model_type.model_validate(db_obj)


@log_inputs_and_outputs
def create_saveinfo(
    db: Session,
    save_name: str,
    project: Project,
    description: str | None = None,
    user: User | None = None,
    kind: str | None = None,
    saveinfo_compares: list | None = None,
) -> orm.SaveInfo:
    now = datetime.datetime.utcnow()
    info = orm.SaveInfo(
        name=save_name,
        description=description,
        created_on=now,
        modified=now,
        kind=kind,
        project_id=project.id,
        saveinfo_compares=saveinfo_compares if saveinfo_compares else [],
    )
    if user is not None:
        info.created_by = user.full_name
        info.modified_by = user.full_name
    db.add(info)
    db.commit()
    db.refresh(info)
    return info


@log_inputs_and_outputs
def delete_saveinfo(db: Session, save_info: orm.SaveInfo):
    db.delete(save_info)
    db.commit()


@log_inputs_and_outputs
def get_district_from_info(db: Session, info: orm.SaveInfo) -> District | None:
    log.warning(
        "get_district_from_info is deprecated, use list_districts_from_info instead and don't assume one district in save"
    )
    db_obj = db.execute(select(orm.District).where(orm.District.info_id == info.id)).scalar()
    if db_obj is None:
        return None

    return District.model_validate(db_obj)


@log_inputs_and_outputs
def list_districts_from_info(db: Session, info: orm.SaveInfo | str) -> list[District]:
    if hasattr(info, "id"):
        info_id = info.id
    else:
        info_id = uuid.UUID(info)

    db_obj = (
        db.execute(
            select(orm.District)
            .options(selectinload("*"))
            .where(orm.District.info_id == info_id)
            .order_by(orm.District.uid)
        )
        .scalars()
        .fetchall()
    )
    if db_obj is None:
        return []
    else:
        out = []
        for obj in db_obj:
            o = District.model_validate(obj)
            db.expunge(obj)
            out.append(o)
        return out


@log_inputs_and_outputs
def get_district(db: Session, uid):
    db_dst = db.get(orm.District, uid, options=[selectinload("*")])
    if db_dst is not None:
        return District.model_validate(db_dst)


@log_inputs_and_outputs
def create_district(db: Session, district: District):
    return tessa.core.orm.create_as(db, district, orm.District)


@log_inputs_and_outputs
def save_district_as(
    db: Session,
    district: District,
    save_name: str,
    project: Project,
    erase_history: bool = True,
    description: str | None = None,
    user: User | None = None,
    overwrite_save_name: bool = True,
    kind: str | None = None,
):
    if not erase_history:
        raise NotImplementedError

    info = create_or_update_save_info(
        db, save_name, project, description, user, overwrite_save_name, kind=kind
    )

    save_district_to_db(db, district, save_as=info)


@log_inputs_and_outputs
def create_or_update_save_info(
    db: Session,
    save_name: str,
    project: Project,
    description: str | None = None,
    user: User | None = None,
    overwrite_save_name=True,
    kind=None,
):
    now = datetime.datetime.utcnow()

    info = find_saveinfo_by_name(db, save_name, project)
    if info is None:
        info = create_saveinfo(db, save_name, project, description, user=user, kind=kind)
    else:
        if not overwrite_save_name:
            raise ValueError(
                "Save info object already exists with this name and overwrite_save_name is False"
            )
        info.modified = now
        if user is not None:
            info.modified_by = user.email
    return info


@log_inputs_and_outputs
def save_district_to_db(
    db: Session,
    district: District,
    save_as: orm.SaveInfo,
    # erase_history: bool = False,
):
    """
    Important: saving behaviour is quite tricky with many conditions we want to decide what to do,
    when to raise errors and when to defer to the caller.

    Districts must be saved to a project.

    Args:
        db:
        district:
        save_as:
        erase_history:
        description:
        user:

    Returns:

    """
    # if not erase_history:
    #     raise NotImplementedError()
    # else:
    #     delete_district_from_save(db, save_as)

    # Important: because saveas info object already exists in db, set it as relation only
    # after adding the new district, otherwise get error that it already exists (even though
    # normally is detatched from Session. No need to Merge.

    db_district_new = orm.District.from_object(district, include_relations=True, save_info=save_as)
    db.add(db_district_new)

    db.commit()
    db.refresh(db_district_new)
    # return db_district_new.id


@log_inputs_and_outputs
def save_districts(
    db: Session,
    save_info: orm.SaveInfo,
    districts: tuple[District, ...],
    replace_only: set | None = None,
    modified_by=None,
) -> orm.SaveInfo:
    """
    Save a tuple/list of districts to the save info. Note that this will DELETE existing
    saved data and overwrite with the contents of the districts tuple, which may be empty.

    Args:
        db:
        save_info:
        districts:
        replace_only:
        modified_by:

    Returns:

    """

    # refresh db data
    save_info = db.get_one(orm.SaveInfo, save_info.id)
    save_info.kind = "district"
    save_info.modified = datetime.datetime.utcnow()
    save_info.modified_by = modified_by

    # Delete ALL data so far associated with this save
    # This was easier than figuring out what changed.
    # Later we could use sqlalchemy to handle this directly.
    delete_query = delete(orm.District).where(orm.District.info_id == save_info.id)

    # Optionally update only some districts.
    if replace_only is not None:
        for uid in replace_only:
            uid = uuid.UUID(uid)
            delete_query = delete_query.where(orm.District.uid == uid)
    db.execute(delete_query)

    for district_ in districts:
        if replace_only is not None and district_.uid in replace_only:
            continue
        db_district_new = orm.District.from_object(district_, include_relations=True, save_info=save_info)
        db_district_new.info_id = save_info.id
        db.add(db_district_new)

    # Commit only after we have deleted and re added everything to avoid clearing
    # districts and failing to re save
    db.commit()
    db.refresh(save_info)
    # this is here to change SAVE_INFO from a "lazy" db object to an actual..
    assert save_info.id is not None

    return save_info


@log_inputs_and_outputs
def delete_districts_from_save(db: Session, save_as: orm.SaveInfo, district=None):
    """
    Delete districts attached to the given save info object, optionally
    specifying a  single district to be deleted

    Args:
        db:
        save_as:
        district:

    Returns:

    """

    query = delete(orm.District).where(orm.District.info_id == save_as.id)
    if district is not None:
        query = query.where(orm.District.uid == district.uid)
    db.execute(query)
    db.commit()


def create_or_update_district(db: Session, district: District) -> District:
    db_model = db.get(orm.District, district.uid)
    if not db_model:
        db_model = orm.District.from_object(district)
        db.add(db_model)
        db.commit()
        db.refresh(db_model)
        return District.model_validate(db_model)
    else:
        attrs = tessa.core.orm.get_common_attributes(District, orm.District)
        # db.execute(select(orm_type).where(orm_type.id == obj.id).update(obj.dict(include=attrs)))
        for field in attrs:
            db_attr = getattr(db_model, field)
            ob_attr = getattr(district, field)
            if db_attr != ob_attr:
                setattr(db_model, field, ob_attr)
        db.add(db_model)
        db.commit()
        db.refresh(db_model)
        return District.model_validate(db_model)


@log_inputs_and_outputs
def create_saveinfo_from_existing(db: Session, existing_saveinfo: orm.SaveInfo, new_name: str | None = None):
    """
    Create a new saveinfo within the same project from an existing saveinfo

    Note that we should also recursively create copies of some objects(rows)
    that reference the SaveInfo that we are copying.

    In this particular case we should copy the following tree:

    orm.District - orm.ClimateData
                 - orm.UtilityNetwork - orm.DistrictNetwork - orm.FlowProperties
                                                            - orm.NetworkCosts
                                                            - orm.ThermalSources - orm.ThermalSource - orm.Fuel
                                                                                                     - orm.ThermalInjectionPoint
    """
    log.warning("Duplicating save currently only includes one district and no extra info, this is a bug.")
    respective_project: orm.Project = db.get_one(orm.Project, existing_saveinfo.project_id)

    new_saveinfo = create_saveinfo(
        db=db,
        save_name=existing_saveinfo.name + " copy" if not new_name else new_name,
        project=respective_project,
        description=existing_saveinfo.description,
        user=None,  # None so it is set to current user
        kind=existing_saveinfo.kind,
        saveinfo_compares=[],
    )

    existing_district_model: District = get_district_from_info(db, existing_saveinfo)
    save_district_to_db(db, existing_district_model, save_as=new_saveinfo)

    # Everything that is dependent on District gets deepcopied automatically
    return new_saveinfo


@log_inputs_and_outputs
def duplicate_save_with_layers(db: Session, save_info: orm.SaveInfo, layers, new_name=None, modified_by=None):
    """

    Args:
        db:
        save_info:
        layers: list of orm type
        modified_by:

    Returns:

    """
    respective_project: orm.Project = db.get_one(orm.Project, save_info.project_id)
    info_id = save_info.id
    new_saveinfo = create_saveinfo(
        db=db,
        save_name=save_info.name + " copy" if not new_name else new_name,
        project=respective_project,
        description=save_info.description,
        user=modified_by,
        kind=save_info.kind,
        saveinfo_compares=[],
    )
    db.add(new_saveinfo)

    for orm_type in layers:
        log.debug(orm_type)
        query = select(orm_type).where(orm_type.info_id == info_id)
        layer_data = db.execute(query).scalars()
        for data in layer_data:
            db.expunge(data)

            # make it transient
            make_transient(data)

            # now set all primary keys to None so there are no conflicts later
            for pk in inspect(data).mapper.primary_key:
                setattr(data, pk.name, None)

            # data = tessa.core.orm.clone_orm_object(data, db)
            data.info_id = new_saveinfo.id
            db.add(data)

    db.commit()
    db.refresh(new_saveinfo)
    # this is here to change SAVE_INFO
    # from a "lazy" db object to an actual.
    assert new_saveinfo.id is not None

    return new_saveinfo


@log_inputs_and_outputs
def list_all_saved_layers(
    db: Session, info: orm.SaveInfo | str, types_map: list[tuple[type[orm.Base], type[BaseModel]]]
):
    if hasattr(info, "id"):
        info_id = info.id
    else:
        info_id = uuid.UUID(info)

    all_layers = []
    for orm_type, obj_type in types_map:
        db_obj = db.execute(
            select(orm_type).options(selectinload("*")).where(orm_type.info_id == info_id)
        ).scalars()

        if db_obj is None:
            all_layers.append([])
        else:
            if obj_type is None:
                db.expunge(db_obj)
                all_layers.append(db_obj)
            else:
                all_layers.append([obj_type.model_validate(d) for d in db_obj])
    return all_layers


@log_inputs_and_outputs
def save_all_layers(db, save_info: orm.SaveInfo, layers: list[tuple[type, tuple[Any]]], modified_by=None):
    """

    Args:
        db:
        save_info:
        layers: list of (Orm Type, [List of Objects]) so that the right orm type
         can be generated for each layer
        modified_by:

    Returns:

    """
    db.add(save_info)
    db.commit()
    save_info.kind = "district"
    save_info.modified = datetime.datetime.utcnow()
    save_info.modified_by = modified_by

    # Delete ALL data so far associated with this save

    for orm_type, layer_data in layers:
        delete_query = delete(orm_type).where(orm_type.info_id == save_info.id)
        db.execute(delete_query)

        for data in layer_data:
            db_obj = orm_type.from_object(data, include_relations=True)
            db_obj.info_id = save_info.id
            db.add(db_obj)

    # Commit only after we have deleted and re added everything to avoid clearing
    # districts and failing to re save
    db.commit()
    db.refresh(save_info)
    # this is here to change SAVE_INFO
    # from a "lazy" db object to an actual..
    assert save_info.id is not None

    return save_info


@log_inputs_and_outputs
def save_layer_type(
    db, save_info: orm.SaveInfo, orm_type: type[orm.Base], layer_data: list, modified_by=None
):
    """

    Args:
        db:
        save_info:
        layers: list of (Orm Type, [List of Objects]) so that the right orm type
         can be generated for each layer
        modified_by:

    Returns:

    """
    if save_info not in db:
        db.add(save_info)
        db.commit()
    save_info.kind = "district"
    save_info.modified = datetime.datetime.utcnow()
    save_info.modified_by = modified_by

    # Delete ALL data so far associated with this save
    delete_query = delete(orm_type).where(orm_type.info_id == save_info.id)
    db.execute(delete_query)

    for data in layer_data:
        db_obj = orm_type.from_object(data, include_relations=True)
        db_obj.info_id = save_info.id
        db.add(db_obj)

    # Commit only after we have deleted and re added everything to avoid clearing
    # districts and failing to re save
    db.commit()
    db.refresh(save_info)
    # this is here to change SAVE_INFO
    # from a "lazy" db object to an actual..
    assert save_info.id is not None

    return save_info
