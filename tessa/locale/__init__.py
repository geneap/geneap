from babel import Locale

from .translate import set_app_locale, translate

LANGUAGE_CODE_OPTIONS = ["en", "fr", "de"]
LANGUAGE_NAME_OPTIONS = [Locale(language=code).get_language_name("en") for code in LANGUAGE_CODE_OPTIONS]

LANG_NAME_TO_CODE = {
    LANGUAGE_NAME_OPTIONS[i]: LANGUAGE_CODE_OPTIONS[i] for i in range(len(LANGUAGE_CODE_OPTIONS))
}
