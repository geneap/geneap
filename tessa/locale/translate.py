from importlib.resources import files

from babel.support import Translations


def translate(text: str):
    # The name "_" for the function is reserved by gettext
    # and Babel for the translation function.
    # It is set after the .install() function of the translation object
    # was called
    import builtins

    if "_" in builtins.__dict__.keys():
        return builtins.__dict__["_"](text)
    else:
        # print(
        #     "Warning! There was an attempt to call the \"_\" function for localisation, but the user's local was not set yet"
        # )
        return text


def set_app_locale(locale_tag: str):
    from . import translations

    files(translations)
    lang = Translations.load(files(translations), [locale_tag])
    lang.install()
