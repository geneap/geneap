from pydantic import BaseModel, ConfigDict

from tessa.utils.pydantic_shapely import Point


class Location(BaseModel):
    name: str
    point: Point
    model_config = ConfigDict(arbitrary_types_allowed=True)

    def to_dict(self):
        d: dict = self.model_dump(exclude={"point"})
        d.update({"x": self.point.x, "y": self.point.y})
        return d
