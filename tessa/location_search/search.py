import requests
import structlog
from bs4 import BeautifulSoup

from tessa.location_search import Location
from tessa.utils.pydantic_shapely import Point

log = structlog.get_logger()


def find_locations(query: str, limit=3) -> list[Location]:
    return GeoAdminCh().get_top_k(query=query, k=3)


# Classes with implementations of Location search depending on the source
class GeoAdminCh:
    base_url = "https://api3.geo.admin.ch/rest/services/api/SearchServer"

    def _get_request_results(self, query: str, k) -> list[dict] | None:
        ans = requests.get(
            self.base_url,
            params={
                "type": "locations",
                "limit": k,
                "searchText": query,
                "geometryFormat": "geojson",
                "sr": "4326",
            },
        )

        if not ans.ok:
            log.warning("Error during GeoAdminCh request:", ans.text)
            return None

        return ans.json()["features"]

    def get_top_k(self, query: str, k: int = 3) -> list[Location]:
        if not query.strip(" .,/'\"[]|+-:;{}"):
            return list()

        request_results = self._get_request_results(query, k)

        if not request_results:
            return list()

        locations = []

        for i in range(min(k, len(request_results))):
            # This API gives the name with the <b> ... </b> tags
            # so we get rid of them
            item = request_results[i]["properties"]
            geojson = request_results[i]
            name = BeautifulSoup(item["label"], parser="lxml").get_text()

            locations.append(Location(name=name, point=Point.from_geojson(geojson)))

        return locations
