from functools import cache
from importlib.resources import as_file, files
from typing import TYPE_CHECKING

import yaml

if TYPE_CHECKING:
    from tessa.core.models import Fuel
    from tessa.thermal_system_models import ThermalSource


@cache
def get_country_data(iso_a2_code) -> dict[str, dict[str, dict]]:
    from . import ch

    country_data = {}
    with as_file(files(ch).joinpath("ch_heat_sources.yml")) as file_source:
        with open(file_source) as file:
            country_data["ch"] = yaml.safe_load(file)
    return country_data[iso_a2_code]


def list_fuels_data(iso_a2_code) -> dict[str, "Fuel"]:
    from tessa.thermal_system_models.models import Fuel

    fuels = {}
    for code, fdata in get_country_data(iso_a2_code)["fuels"].items():
        fuels[code] = Fuel(**(fdata | {"code": code, "currency": "chf"}))
    return fuels


def list_heat_source_data(iso_a2_code) -> dict[str, "ThermalSource"]:
    from tessa.thermal_system_models.models import ThermalSource

    fuels = list_fuels_data(iso_a2_code)

    thermal_sources = {}
    for code, hdata in get_country_data(iso_a2_code)["heat_sources"].items():
        thermal_sources[code] = ThermalSource(
            code=code, name=hdata.pop("name"), fuel=fuels[hdata.pop("fuel")], **hdata
        )

    return thermal_sources
