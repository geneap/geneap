import uuid
from typing import Any, Dict, Literal, Optional

import numpy as np
import pandas as pd
import pandera as pa
import xarray as xr
from pandera.typing import DateTime, Index, Series
from pydantic import (
    UUID4,
    BaseModel,
    ConfigDict,
    Field,
    GetCoreSchemaHandler,
    field_validator,
    model_serializer,
    model_validator,
)
from pydantic_core import CoreSchema, core_schema

from tessa.utils.pydantic_shapely import Feature, Polygon


class DatasetType(xr.Dataset):
    """No-op type annotation handler for Xarray Datasets, so that we
    can annotate a dataset type that is recognised by pydantic. Later
    would like to add serialisation (e.g. json) and deserialisation
    customisations. Alternatively we might be able to use future versions
    of Pandera that might support N-dimensional datasts.
    """

    # Address Xarray warning that Dataset subclasses must define __slots__
    __slots__ = []

    @classmethod
    def __get_pydantic_core_schema__(cls, source_type: Any, handler: GetCoreSchemaHandler) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls, core_schema.any_schema())


class TSFrame(pa.DataFrameModel):
    index: Index[pa.Timestamp] = pa.Field(check_name=False)


class RegionPolygon(Feature[Polygon, dict]):
    model_config = ConfigDict(
        json_schema_extra={
            "example": {
                "id": 1,
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "Polygon",
                    "coordinates": (
                        (
                            (7.028243484899214, 47.02273830567231),
                            (7.028400917568211, 47.02007886159054),
                            (7.029351577920189, 47.01997750944483),
                            (7.031950585569127, 47.02149232971329),
                            (7.032322252829484, 47.021807411272235),
                            (7.03191611204038, 47.02301788751758),
                            (7.031695357128283, 47.0233246832683),
                            (7.031384014349968, 47.02334897755948),
                            (7.031072684648783, 47.02337324406725),
                            (7.030801333948245, 47.02336762639582),
                            (7.028508546919376, 47.023198262112835),
                            (7.028243484899214, 47.02273830567231),
                        ),
                    ),
                },
            }
        }
    )


class UtilityNetwork(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    uid: UUID4 = Field(default_factory=uuid.uuid4)
    # info: dict = {}
    kind: Literal["utilitynetwork"] = "utilitynetwork"
    name: str = "utility_network"


class PowerLoad(pa.DataFrameModel):
    timestamp: Index[DateTime] = pa.Field(check_name=True, coerce=True)
    p_c_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_c_final_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_h_kw: Series[float] = pa.Field(coerce=True)
    p_ww_kw: Series[float] = pa.Field(coerce=True)
    p_hww_kw: Series[float] = pa.Field(coerce=True)
    p_h_final_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_ww_final_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)
    p_hww_final_kw: Optional[Series[float]] = pa.Field(nullable=True, coerce=True)


class Fuel(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    # TODO: (t)he price needs to be given in terms of units of fuel because
    #  the energy value of the fuel will change, so shouldn't have a constant price per
    #  kWh equivalent (e.g. wood has lower and higher heating values with a constant price per mass)

    # TODO: (t)he price can also be time varying... basically our fuel model is too restrictive.
    code: str
    name: str
    description: str | None = None
    co2_eq_kwh: float | None = None
    price: float | None = float("nan")
    price_unit: str | None = "CHF/kWh"

    @field_validator("co2_eq_kwh")
    def check_co2_eq_kwh(cls, v):
        # Turn non-finite floats to None
        if v is not None and (pd.isna(v) or not np.isfinite(v)):
            return None
        return v

    @field_validator("price")
    def check_price(cls, v):
        # Turn non-finite floats to None
        if v is not None and (pd.isna(v) or not np.isfinite(v)):
            return None
        return v


class ThermalGenerator(BaseModel):
    """
    Generic base class for thermal generator types

    Attributes:
        code (str): Thermal generator code name, should by a python safe name (no spaces or special characters)
        name (str): Thermal generator name
        description (str): Thermal generator description
        efficiency: Thermal generator efficiency (fraction) or SPF
        fuel: Thermal generator fuel/electricity supply definition
    """
    model_config = ConfigDict(from_attributes=True)

    code: str
    name: str
    description: str | None = None
    efficiency: float | None = None
    fuel: Fuel | str | None = None


def replace_by_uid(new, items):
    items_new = []
    for d in items:
        if d.uid == new.uid:
            items_new.append(new)
        else:
            items_new.append(d)
    return items_new
