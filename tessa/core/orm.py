import base64
import datetime
import io
import uuid
from collections.abc import Container, Sequence
from dataclasses import asdict
from typing import (
    Annotated,
    Any,
    Dict,
    Generic,
    List,
    Optional,
    Type,
    TypeVar,
    Union,
    get_args,
    overload,
)

import geoalchemy2
import geoalchemy2.shape
import geopandas as gpd
import pandas as pd
import sqlalchemy
import structlog
from geoalchemy2 import WKBElement, WKTElement
from pandera.typing.geopandas import GeoDataFrame
from pydantic import BaseModel, ConfigDict, create_model
from sqlalchemy import DateTime, Identity, LargeBinary, TypeDecorator, func, select
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.inspection import inspect
from sqlalchemy.orm import (
    DeclarativeBase,
    MappedAsDataclass,
    Session,
    class_mapper,
    declared_attr,
    mapped_column,
)
from sqlalchemy.orm.properties import ColumnProperty
from sqlalchemy.orm.session import make_transient

from tessa.utils.pydantic_shapely import LineString, MultiLineString, MultiPoint, MultiPolygon, Point, Polygon

log = structlog.get_logger()

UuidIndex = Annotated[uuid.UUID, mapped_column(index=True)]
# Define a UuidPk without the default value to use in datclass case where default needs to be
# to be explicit in the class declaration
UuidPk = Annotated[uuid.UUID, mapped_column(primary_key=True, index=True, unique=True)]
# Define a default UUID primary key, but BE CAREFUL because when usinng MappedAsDataclass
# the default value needs to be on the right hand side due to sqlalchemy limitations
UuidPkDefault = Annotated[
    uuid.UUID, mapped_column(primary_key=True, index=True, unique=True, default=uuid.uuid4)
]
DateTimeDefault = Annotated[
    datetime.datetime, mapped_column(DateTime(timezone=True), server_default=func.now())
]
DateTime = Annotated[datetime.datetime, mapped_column(DateTime(timezone=True))]
StrIndex = Annotated[str, mapped_column(index=True)]
StrUnique = Annotated[str, mapped_column(index=True, unique=True)]


def set_common_attrs(db_obj, obj, db_type, obj_type):
    attrs = get_common_attributes(obj_type, db_type)
    for field in attrs:
        ob_attr = getattr(obj, field)
        setattr(db_obj, field, ob_attr)


def parquet_bytes(dataframe) -> bytes | None:
    if dataframe is None:
        return None
    the_bytes = io.BytesIO()
    dataframe.to_parquet(the_bytes)
    the_bytes.seek(0)
    return the_bytes.getvalue()


def parquet_b64(dataframe):
    return base64.b64encode(parquet_bytes(dataframe)).decode("ascii")


def feather_bytes(dataframe):
    the_bytes = io.BytesIO()
    dataframe.to_feather(the_bytes)
    the_bytes.seek(0)
    return the_bytes.getvalue()


class PydanticType(TypeDecorator):
    """Pydantic type.
    SAVING:
    - Uses SQLAlchemy JSON type under the hood.
    - Acceps the pydantic model and converts it to a dict on save.
    - SQLAlchemy engine JSON-encodes the dict to a string.
    RETRIEVING:
    - Gets the JSON as a dict.
    - Uses the dict to create a pydantic model.

    Note: when using with mapped_column, need to specify the attribute annotation
    with Mapped[Type] otherwise sqlalchemy cannot find the column name.
    """

    cache_ok = True
    impl = sqlalchemy.JSON
    pydantic_type: type[BaseModel]

    def __init__(self, pydantic_type):
        super().__init__()
        self.pydantic_type = pydantic_type

    def load_dialect_impl(self, dialect):
        # Use JSONB for PostgreSQL and JSON for other databases.
        if dialect.name == "postgresql":
            return dialect.type_descriptor(JSONB())
        else:
            return dialect.type_descriptor(sqlalchemy.JSON())

    def process_bind_param(self, value: BaseModel, dialect):
        return value.model_dump() if value else None

    def process_result_value(self, value, dialect):
        return self.pydantic_type(**value) if value else None


class DataclassType(TypeDecorator):
    """Dataclass type.
    SAVING:
    - Uses SQLAlchemy JSON type under the hood. (JSONB for postgres)
    - Acceps the pydantic model and converts it to a dict on save.
    RETRIEVING:
    - Gets the JSON as a dict.
    - Uses the dict to create a dataclass instance.

    Note: when using with mapped_column, need to specify the attribute annotation
    with Mapped[Type] otherwise sqlalchemy cannot find the column name.
    """

    cache_ok = True

    impl = sqlalchemy.JSON
    dataclass_type: type[Any]

    def __init__(self, dataclass_type):
        super().__init__()
        self.dataclass_type = dataclass_type

    def load_dialect_impl(self, dialect):
        # Use JSONB for PostgreSQL and JSON for other databases.
        if dialect.name == "postgresql":
            return dialect.type_descriptor(JSONB())
        else:
            return dialect.type_descriptor(sqlalchemy.JSON())

    def process_bind_param(self, value, dialect):
        return asdict(value) if value else None

    def process_result_value(self, value, dialect):
        return self.dataclass_type(**value) if value else None


class ParquetDataframe(TypeDecorator):
    """
    Store a dataframe as a parquet binary blob. Allows to quick save/load of
    data tables that make sense to store as a single immutable and non-queryable
    datasets.

    Pros:
    - Much faster to save and load compared to converting dataframes to own sql table
    - Much smaller than sql table data thanks to parquet compression
    - Easy to store variable number of columns
    - Combine with pandera table schema for type validation and describing requried/optional content
    - parquet format can be read by many different tools

    Cons
    - Data contained is opaque, not possible to check or modify tables within SQL
    - Data migrations must go through a load-save step (in python or other parquet-compatible tool)
    - Doesn't follow typical data normalisation practise
    - Storing file blob directly in the database not considered best practise

    Alternatives
    - Use a real data table with an extra table id key
    - Save as parquet in a file system or object store and store the URI in the database instead.

    Note: when using with mapped_column, need to specify the attribute annotation
    with Mapped[Type] otherwise sqlalchemy cannot find the column name.
    """

    impl = LargeBinary
    cache_ok = False
    table_type = None

    def __init__(self, table_type=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.table_type = table_type

    def process_bind_param(self, value, dialect) -> bytes | None:
        return parquet_bytes(value)

    def process_result_value(self, value, dialect):
        if value is None or len(value) == 0:
            return None
        df = pd.read_parquet(io.BytesIO(value))
        if self.table_type is not None:
            from pandera.typing import DataFrame

            return DataFrame[self.table_type](df)
        else:
            return df

    def compare_values(self, x, y):
        """Override compare values to deal with difference cases where we might compare
        with other python objects (in normal Python mode) or with database serialised objects
        (in SQLAlchemy mode)
        e.g. when SQLAlchemy is checking whether values have changed relative to the database
        """
        if x is None and y is None:
            return True

        if isinstance(x, pd.DataFrame) and isinstance(y, pd.DataFrame):
            return x.equals(y)

        elif isinstance(x, pd.DataFrame) and not isinstance(y, bytes):
            y = pd.read_parquet(io.BytesIO(y))
            return x.equals(y)
        elif isinstance(y, pd.DataFrame) and not isinstance(x, bytes):
            x = pd.read_parquet(io.BytesIO(x))
            return x.equals(y)
        elif isinstance(x, bytes) and isinstance(y, bytes):
            return x == y
        return x == y


class ParquetGeoDataFrame(ParquetDataframe):
    """
    Store a Geopandas GeoDataFrame in parquet format as a binary blob. See :class:`ParquetDataframe`.
    """

    cache_ok = False

    def process_result_value(self, value, dialect):
        if value is None or len(value) == 0:
            return None
        df = gpd.read_parquet(io.BytesIO(value))
        if self.table_type is not None:
            df = GeoDataFrame[self.table_type](df)
        return df


class _ParquetDataframeWithTupleColumns(ParquetDataframe):
    def process_bind_param(self, value, dialect) -> bytes:
        value.columns = [str(c) for c in value.columns]
        return parquet_bytes(value)

    def process_result_value(self, value, dialect):
        df = pd.read_parquet(io.BytesIO(value))
        from ast import literal_eval as make_tuple

        cols = []
        for s in df.columns:
            try:
                cols.append(make_tuple(s))
            except ValueError:
                cols.append(s)

        df.columns = cols
        if self.table_type is not None:
            self.table_type(df)
        else:
            return df


class ShapelyGeometry(geoalchemy2.Geometry):
    """
    Subclassof Geoalechemy Geometry that loads a postgis geom
    as a shapely geometry and optionally wraps in a custom type

    IMPORTANT, could not use typedecorator to customize geoalechmy behaviour
    instead needed to subclass the geometry type
    """

    result_type = None
    """ Additional type to wrap the result in (e.g. pydantic-shapely compat types) """

    def __init__(
        self,
        result_type=None,
        geometry_type="GEOMETRY",
        srid=-1,
        dimension=2,
        spatial_index=True,
        use_N_D_index=False,
        use_typmod=None,
        from_text=None,
        name=None,
        nullable=True,
        _spatial_index_reflected=None,
    ):
        super().__init__(
            geometry_type=geometry_type,
            srid=srid,
            dimension=dimension,
            spatial_index=spatial_index,
            use_N_D_index=use_N_D_index,
            use_typmod=use_typmod,
            from_text=from_text,
            name=name,
            nullable=nullable,
            _spatial_index_reflected=_spatial_index_reflected,
        )
        self.result_type = result_type

    def result_processor(self, dialect, coltype):
        """Specific result_processor that automatically process spatial elements."""

        def process(value):
            if value is not None:
                kwargs = {}
                if self.srid > 0:
                    kwargs["srid"] = self.srid
                if self.extended is not None:
                    kwargs["extended"] = self.extended
                el = self.ElementType(value, **kwargs)

                el = geoalchemy2.shape.to_shape(el)
                if self.result_type is not None:
                    return self.result_type(el)
                else:
                    return el

        return process

    def bind_processor(self, dialect):
        """Specific bind_processor that automatically process spatial elements."""
        proc = super().bind_processor(dialect)

        def process(bindvalue):
            if hasattr(bindvalue, "wkb"):
                bindvalue = WKBElement(bindvalue.wkb)
            elif hasattr(bindvalue, "wkt"):
                bindvalue = WKTElement(bindvalue.wkt)
            return proc(bindvalue)

        return process


#
# class ShapelyGeometry(TypeDecorator):
#     """
#     """
#     impl = geoalchemy2.Geometry
#     cache_ok = False
#     result_type = None
#     """ Additional type to wrap the result in (e.g. pydantic-shapely compat types) """
#
#     def __init__(
#         self,
#         result_type=None,
#         geometry_type="GEOMETRY",
#         srid=-1,
#         dimension=2,
#         spatial_index=True,
#         use_N_D_index=False,
#         use_typmod=None,
#         from_text=None,
#         name=None,
#         nullable=True,
#         _spatial_index_reflected=None,
#     ):
#         self.impl = geoalchemy2.Geometry(
#             geometry_type=geometry_type,
#             srid=srid,
#             dimension=dimension,
#             spatial_index=spatial_index,
#             use_N_D_index=use_N_D_index,
#             use_typmod=use_typmod,
#             from_text=from_text,
#             name=name,
#             nullable=nullable,
#             _spatial_index_reflected=_spatial_index_reflected,
#         )
#         self.result_type = result_type
#
#     def process_result_value(self, value, dialect):
#         if value is not None:
#             kwargs = {}
#             if self.srid > 0:
#                 kwargs["srid"] = self.srid
#             if self.extended is not None:
#                 kwargs["extended"] = self.extended
#             el = self.ElementType(value, **kwargs)
#
#             el = geoalchemy2.shape.to_shape(el)
#             if self.result_type is not None:
#                 return self.result_type(el)
#             else:
#                 return el
#         return value
#
#     def process_bind_param(self, value, dialect):
#         """Specific bind_processor that automatically process spatial elements."""
#         proc = self.impl.bind_processor(dialect)
#
#         if hasattr(value, "wkb"):
#             value = WKBElement(value.wkb)
#         elif hasattr(value, "wkt"):
#             value = WKTElement(value.wkt)
#         return proc(value)


PointGeom = Annotated[Point, mapped_column(ShapelyGeometry(result_type=Point, geometry_type="POINT"))]
PolygonGeom = Annotated[Polygon, mapped_column(ShapelyGeometry(result_type=Polygon, geometry_type="POLYGON"))]
LineStringGeom = Annotated[
    LineString, mapped_column(ShapelyGeometry(result_type=LineString, geometry_type="LINESTRING"))
]

MultiPointGeom = Annotated[
    MultiPoint, mapped_column(ShapelyGeometry(result_type=MultiPoint, geometry_type="MULTIPOINT"))
]
MultiPolygonGeom = Annotated[
    MultiPolygon, mapped_column(ShapelyGeometry(result_type=MultiPolygon, geometry_type="MULTIPOLYGON"))
]
MultiLineStringGeom = Annotated[
    MultiLineString,
    mapped_column(ShapelyGeometry(result_type=MultiLineString, geometry_type="MULTILINESTRING")),
]


class Base(DeclarativeBase):
    __allow_unmapped__ = True
    type_annotation_map = {dict[str, Any]: JSONB}

    @declared_attr
    def __tablename__(cls) -> str:
        """
        Generate table name based on class name.
        """
        name = cls.__name__.lower()
        return name

    @classmethod
    def from_object(cls, obj, include_relations=False):
        if obj is None:
            return None
        obj_attrs = set(obj.__dict__.keys())
        db_relationships = {a.key for a in inspect(cls).relationships}
        db_attrs = {
            a.key
            for a in inspect(cls).attrs
            # Remove id keys if there is a corresponding relationship
            if a.key.replace("_id", "") not in db_relationships
        }
        if include_relations:
            # add back the relatioship cols
            db_attrs |= db_relationships
        common_attrs = obj_attrs.intersection(db_attrs)
        data = {n: getattr(obj, n) for n in common_attrs}
        return cls(**data)

    @classmethod
    def common_attrs_no_relations(cls, obj):
        obj_attrs = set(obj.__dict__.keys())
        db_relationships = {a.key for a in inspect(cls).relationships}

        db_attrs = {
            a.key
            for a in inspect(cls).attrs
            # Remove id keys if there is a corresponding relationship
            if a.key.replace("_id", "") not in db_relationships
        }

        return obj_attrs.intersection(db_attrs)


class BaseDataclass(MappedAsDataclass, Base):
    __abstract__ = True


OrmType = TypeVar("OrmType", bound=Base)
ModelType = TypeVar("ModelType", bound=BaseModel)


def clone_orm_object(obj, session):
    # remove the object from the session (set its state to detached)
    session.expunge(obj)

    # make it transient (set its state to transient)
    make_transient(obj)

    # now set all primary keys to None so there are no conflicts later
    for pk in inspect(obj).mapper.primary_key:
        setattr(obj, pk.name, None)

    return obj


def get_common_attributes(
    object_model: type[ModelType], orm_model: type[OrmType], error_on_mismatch: bool = False
):
    """Gives list of common attributes between python schema class and SQLAlquemy model class.
    Provides a warning for unexpected non-common attributes."""
    attributes_py = set(object_model.model_fields.keys())
    attributes_db = {
        a.key
        for a in class_mapper(orm_model).iterate_properties
        if isinstance(a, sqlalchemy.orm.ColumnProperty)
    }
    attributes = attributes_py.intersection(attributes_db)
    if error_on_mismatch and len(attributes_py.union(attributes_db)) != len(attributes):
        raise AttributeError("Object attributes and ORM attributes dont match")

    if len(attributes) == 0:
        raise AttributeError("No common attributes")
    return attributes


def query_as(
    db: Session,
    model_type: type[ModelType],
    orm_type: type[OrmType],
    *,
    offset: int | None = None,
    limit: int | None = None,
    where=None,
) -> list[ModelType]:
    """Queries an SQLAlchemy model and returns as a Pydantic model type"""
    query = select(orm_type)
    if where:
        query = query.where(where)

    if offset:
        query = query.offset(offset)
    if limit:
        query = query.limit(limit)
    return [model_type.model_validate(o) for o in db.scalars(query)]


def get_or_create_as(db: Session, obj: ModelType, orm_type: type[OrmType]) -> ModelType:
    """
    Get existing db object or Create a pydantic model 'as' a provided SQLAlchemy model.

    This allows manual mapping between a pydantic type and its representation in the DB.

    Args:
        db: sqlalchemy database session
        obj: pydantic object
        orm_type: sqlachemy mapped model type

    Returns:

    """
    db_model = db.get(orm_type, obj.id)
    if not db_model:
        db_model = orm_type.from_object(obj)
        db.add(db_model)
        db.commit()
        db.refresh(db_model)
    return obj.model_validate(db_model)


def create_or_update_as(db: Session, obj: ModelType, orm_type: type[OrmType]) -> ModelType:
    """
    Get existing db object or Create a pydantic model 'as' a provided SQLAlchemy model.

    This allows manual mapping between a pydantic type and its representation in the DB.

    Args:
        db: sqlalchemy database session
        obj: pydantic object
        orm_type: sqlachemy mapped model type

    Returns:

    """
    db_model = db.get(orm_type, obj.id)
    if not db_model:
        db_model = orm_type.from_object(obj)
        db.add(db_model)
        db.commit()
        db.refresh(db_model)
        return type(obj).model_validate(db_model)
    else:
        return update_as(db, obj, orm_type)


@overload
def create_as(db: Session, obj: ModelType, orm_type: type[OrmType]) -> ModelType:
    ...


@overload
def create_as(db: Session, obj: list[ModelType], orm_type: type[OrmType]) -> list[ModelType]:
    ...


def create_as(
    db: Session, obj: ModelType | list[ModelType], orm_type: type[OrmType]
) -> ModelType | list[ModelType]:
    """
    Insert one or mode pydantic model 'as' a provided SQLAlchemy model. Will raise error if you try to insert object
    with existing ID

    This allows manual mapping between a pydantic type and its representation in the DB.

    Args:
        db: sqlalchemy database session
        obj: pydantic object(s)
        orm_type: sqlachemy mapped model type

    Returns:

    """
    return_list = True
    if not isinstance(obj, Sequence):
        obj = [obj]
        return_list = False

    db_result = []
    for obj_in in obj:
        db_model = orm_type.from_object(obj_in)
        db.add(db_model)
        # db.refresh(db_model)
        db_result.append(db_model)
        # obj_result.append(ModelType.model_validate(db_model))
    db.commit()
    # Make sure objects are synced to DB, for example if the DB sets
    # autogenerated fields
    obj_result = []
    for db_obj, obj_in in zip(db_result, obj, strict=True):
        db.refresh(db_obj)
        obj_result.append(obj_in.model_validate(db_obj))
    if return_list:
        return obj_result
    else:
        assert len(obj_result) == 1
        return obj_result[0]


def get_as(db: Session, id: Any, model_type: type[ModelType], orm_type: type[OrmType]) -> Optional[ModelType]:
    """
    Get a pydantic model by id from a provided SQLAlchemy model.

    This allows manual mapping between a pydantic type and its representation in the DB.

    Args:
        db: sqlalchemy database session
        id: model unique id
        model_type: Pydantic model type
        orm_type: Sqlalchemy model type

    Returns:

    """
    db_object = db.get(orm_type, id)
    if db_object is None:
        return None
    else:
        return model_type.model_validate(db_object)


def list_as(
    db: Session,
    model_type: type[ModelType],
    orm_type: type[OrmType],
    *,
    offset: int = 0,
    limit: int = 100,
    conditions: dict | None = None,
    order_by=None,
) -> list[ModelType]:
    query = select(orm_type).offset(offset).limit(limit)
    if conditions is not None:
        query = query.where(**conditions)
    if order_by is not None:
        query = query.order_by(order_by)

    return [model_type.model_validate(o) for o in db.scalars(query)]


def update_as(
    db: Session, obj: ModelType, orm_type: type[OrmType], model_type: type[ModelType] | None = None
) -> ModelType:
    """
    Update a pydantic model 'as' a provided SQLAlchemy model.

    This allows manual mapping between a pydantic type and its representation in the DB.

    Args:
        db: sqlalchemy database session
        obj: pydantic object
        orm_type: sqlachemy mapped model type

    Returns:

    """

    db_obj = db.get(orm_type, obj.id)
    if db_obj is None:
        raise ValueError(f"No object exists with id={obj.id}")

    if model_type is None:
        model_type: type[ModelType] = obj.__class__

    attrs = get_common_attributes(model_type, orm_type)
    # db.execute(select(orm_type).where(orm_type.id == obj.id).update(obj.dict(include=attrs)))
    for field in attrs:
        db_attr = getattr(db_obj, field)
        ob_attr = getattr(obj, field)
        if db_attr != ob_attr:
            setattr(db_obj, field, ob_attr)
    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)
    return model_type.model_validate(db_obj)


def delete_as(db: Session, obj: ModelType, orm_type: type[OrmType]):
    obj_db = db.get(orm_type, obj.id)
    if obj_db is not None:
        db.delete(obj_db)
        db.commit()


IdentityPk = Annotated[int, mapped_column(Identity(), primary_key=True)]
