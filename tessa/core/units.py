import csv
from dataclasses import dataclass
from importlib.resources import as_file, files
from pathlib import Path
from typing import Optional

import numpy as np
from pint import Unit, UnitRegistry

ureg = UnitRegistry()
ureg.load_definitions(
    """
CHF = [currency]
""".splitlines()
)

Q_ = ureg.Quantity


@dataclass
class DisplayVariable:
    name: str
    title: str
    calculation_unit: Unit
    display_unit: Optional[Unit] = None
    description: str | None = None


def __setup_dv():
    from . import data

    dv = {}
    with as_file(files(data).joinpath(Path("display_variable_definitions.csv"))) as data_file:
        with open(data_file) as dvd:
            reader = csv.DictReader(dvd)
            for row in reader:
                display_unit = row.get("display_unit")
                if display_unit is None or display_unit == "":
                    display_unit = None
                else:
                    display_unit = ureg(display_unit)
                dv[row["name"]] = DisplayVariable(
                    name=row["name"],
                    title=row["title"],
                    calculation_unit=ureg(row["calculation_unit"]),
                    display_unit=display_unit,
                    description=row.get("description"),
                )

    return dv


dv = __setup_dv()


def format_display_value(value, opts: DisplayVariable):
    if value is None:
        display_value = np.nan * opts.calculation_unit
    else:
        display_value = value * opts.calculation_unit
    if opts.display_unit is not None:
        display_value = display_value.to(opts.display_unit)
    else:
        display_value = display_value.to_compact()
    return display_value


fmt_dv = format_display_value


def format_datetime(timestamp, format="%Y-%m-%d %H:%M", locale=None):
    if locale is not None:
        raise NotImplementedError
    return timestamp.strftime(format)


fmt_dt = format_datetime
