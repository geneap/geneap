import secrets
from typing import TYPE_CHECKING, List, Optional

from pydantic import AnyHttpUrl, EmailStr
from pydantic_settings import BaseSettings, SettingsConfigDict
from sqlalchemy import create_engine
from sqlalchemy.future import Engine
from sqlalchemy.orm import sessionmaker

if TYPE_CHECKING:
    from tessa.thermal_system_models.network_builder.shim import DistrictNetworkBuilder


class Settings(BaseSettings):
    model_config = SettingsConfigDict(
        env_prefix="tessa_", env_file_encoding="utf-8", env_file=".env", secrets_dir="/var/run"
    )

    project_name: str = "tessa"
    pg_dsn: str = "postgresql+psycopg://tessa@/tessa?host=/var/run/postgresql"
    api_v1_str: str = "/api/v1"
    export_tmp_dir: str = "/var/lib/tessa"
    mapbox_token: str | None = None

    s3_id: str | None = None
    s3_secret: str | None = None
    s3_endpoint_override: str = ""
    s3_use_virtual_addressing: str = "false"

    # settings needed for useridentification
    secret_key: str = secrets.token_urlsafe(32)
    # 60 minutes * 24 hours * 8 days = 8 days
    access_token_expire_minutes: int = 60 * 24 * 8

    # emails
    smtp_tls: bool = True
    smtp_port: int | None = None
    smtp_host: str | None = None
    smtp_user: str | None = None
    smtp_password: str | None = None
    emails_from_email: EmailStr | None = None
    emails_from_name: str | None = None
    email_reset_token_expire_hours: int = 48
    email_templates_dir: str = ""
    emails_enabled: bool = True
    server_host: AnyHttpUrl = "http://localhost:8765"

    backend_cors_origins: list[AnyHttpUrl] = [
        "http://localhost:8080",
        "http://localhost",
        "https://localhost",
    ]

    # Dataset configurations
    routing_tiledb_store: str = "/var/lib/tessa/clusters_distances_tiledb_v3"
    pace_dataset: str = "s3://tessa/pace_empa_hourly.zarr"

    # themes and colors
    theme_primary_color: str = "#004568"


class TessaCore:
    def __init__(
        self,
        config: Settings,
        heat_model=None,
        climate_data_source=None,
        building_data_source=None,
    ):
        self.config = config
        self.database: Engine = create_engine(config.pg_dsn, pool_pre_ping=True)
        self.Session = sessionmaker(bind=self.database, future=True)
        if building_data_source is None:
            from tessa.building_data import RegblDataSource

            self.building_data_source = RegblDataSource(engine=self.database)
        else:
            self.building_data_source = building_data_source

        if climate_data_source is None:
            from tessa.climate_data import OneBuildingTMYSource

            # self.climate_data_source = SIAClimateData(database=self.database)
            self.climate_data_source = OneBuildingTMYSource(engine=self.database)
        else:
            self.climate_data_source = climate_data_source

        if heat_model is None:
            from tessa.heat_demand_models import SwissHeatDemandModel

            self.heat_model = SwissHeatDemandModel()
        else:
            self.heat_model = heat_model

        from tessa.road_network_routing import BuildingRouting
        from tessa.thermal_system_models import SwissDistrictNetworkBuilder

        self.district_network_builder: DistrictNetworkBuilder = SwissDistrictNetworkBuilder(
            database=self.database,
            routing_service=BuildingRouting(
                database=self.database,
                data_directory=config.routing_tiledb_store,
                tiledb_opts={
                    "vfs.s3.aws_access_key_id": config.s3_id,
                    "vfs.s3.aws_secret_access_key": config.s3_secret,
                    "vfs.s3.endpoint_override": config.s3_endpoint_override,
                    "vfs.s3.use_virtual_addressing": config.s3_use_virtual_addressing,
                },
            ),
        )

    @property
    def engine(self) -> Engine:
        return self.database
