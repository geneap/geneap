from collections.abc import Iterable
from dataclasses import dataclass
from functools import partial
from typing import Optional

import geopandas as _gpd
import pandas as _pd
import structlog
from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import sessionmaker

from tessa.building_data import BuildingDataSource
from tessa.climate_data import ClimateDataSource
from tessa.core import Settings
from tessa.district import (
    ApplyCoolingModel,
    ApplyHeatModel,
    District,
    DistrictBaseData,
    DistrictQuery,
    SetClimate,
)
from tessa.heat_demand_models.common import CoolingDemandModel, HeatDemandModel
from tessa.road_network_routing import BuildingRouting
from tessa.thermal_system_models import (
    DistrictNetwork,
    FlowProperties,
    Fuel,  # F401
    NetworkCosts,
    Substation,
    SwissDistrictNetworkBuilder,
    ThermalInjectionPoint,  # F401
    ThermalSource,  # F401
    network_builder,
)

log = structlog.get_logger()



@dataclass
class Tessa:
    config: Settings
    database: Engine = None
    heat_model: HeatDemandModel = None
    cooling_model: CoolingDemandModel = None
    climate_data_source: ClimateDataSource = None
    building_data_source: BuildingDataSource = None
    routing_service: BuildingRouting = None

    def __post_init__(self):
        if self.database is None:
            self.database: Engine = create_engine(self.config.pg_dsn, pool_pre_ping=True)

        self.Session = sessionmaker(bind=self.database, future=True)
        if self.building_data_source is None:
            from .building_data import RegblDataSource

            self.building_data_source = RegblDataSource(engine=self.database)

        if self.climate_data_source is None:
            from .climate_data import OneBuildingTMYSource

            # self.climate_data_source = SIAClimateData(database=self.database)
            self.climate_data_source = OneBuildingTMYSource(engine=self.database)

        if self.heat_model is None:
            from .heat_demand_models import SwissHeatDemandModel

            self.heat_model = SwissHeatDemandModel()

        if self.cooling_model is None:
            from .heat_demand_models.ch_tessa import SwissCoolingDemandModel

            self.cooling_model = SwissCoolingDemandModel(
                pace_dataset=self.config.pace_dataset,
                s3_id=self.config.s3_id,
                s3_secret=self.config.s3_secret,
                s3_endpoint_override=self.config.s3_endpoint_override,
            )

        if self.routing_service is None:
            self.routing_service = BuildingRouting(
                database=self.database,
                data_directory=self.config.routing_tiledb_store,
                tiledb_opts={
                    "vfs.s3.aws_access_key_id": self.config.s3_id,
                    "vfs.s3.aws_secret_access_key": self.config.s3_secret,
                    "vfs.s3.endpoint_override": self.config.s3_endpoint_override,
                    "vfs.s3.use_virtual_addressing": self.config.s3_use_virtual_addressing,
                },
            )

        self.district_network_builder = SwissDistrictNetworkBuilder(
            database=self.database, routing_service=self.routing_service
        )

    @property
    def engine(self) -> Engine:
        return self.database

    def district_from_polygon(self, polygon, filter_query=None, **kwargs) -> District:
        """
        Create a district from a geospatial polygon (any object supporting __geo_interface__)
        or a GeoJson compatible dict

        Args:
            polygon:
            filter_query:

        Returns:

        """
        district = DistrictBaseData(building_data_source=self.building_data_source).from_region(
            polygon, **kwargs
        )
        if filter_query:
            district = DistrictQuery()(district, query=filter_query)
        return (
            district.apply(SetClimate(climate_data_source=self.climate_data_source, climate_year=2025))
            .apply(ApplyHeatModel(heat_model=self.heat_model))
            .apply(ApplyCoolingModel(cooling_model=self.cooling_model))
        )

    def district_from_buildings(
        self, buildings: _pd.DataFrame | _gpd.GeoDataFrame, filter_query=None, **kwargs
    ) -> District:
        """
        Create a district object from a table of buildings. Will fill missing data from the
        buildings table with data from the configured buildings data source.

        Args:
            buildings:
            filter_query:

        Returns:

        """
        district = DistrictBaseData(building_data_source=self.building_data_source).from_buildings(
            buildings, **kwargs
        )
        if filter_query:
            district = DistrictQuery()(district, query=filter_query)
        return (
            district.apply(SetClimate(climate_data_source=self.climate_data_source, climate_year=2015))
            .apply(ApplyHeatModel(heat_model=self.heat_model))
            .apply(ApplyCoolingModel(cooling_model=self.cooling_model))
        )

    def district_from_ids(self, building_ids: Iterable[int], filter_query=None, **kwargs) -> District:
        """
        Create a district object from a list of building ids, where the ids correspond
        to the building identifiers in the configured building data source.

        Args:
            building_ids:
            filter_query:

        Returns:

        """
        district = DistrictBaseData(building_data_source=self.building_data_source).from_ids(
            building_ids, **kwargs
        )
        if filter_query:
            district = DistrictQuery()(district, query=filter_query)
        return (
            district.apply(SetClimate(climate_data_source=self.climate_data_source, climate_year=2025))
            .apply(ApplyHeatModel(heat_model=self.heat_model))
            .apply(ApplyCoolingModel(cooling_model=self.cooling_model))
        )

    def district_heating_network_for_district(
        self,
        district: District,
        heat_sources: Iterable[Substation] | None = None,
        flow_properties: FlowProperties | None = None,
        costs: NetworkCosts | None = None,
        ways=None,
        district_network: Optional[DistrictNetwork] = None,
        name: str = "district_heating",
    ) -> DistrictNetwork:
        """

        Args:
            district: The district of buildings for which to generate a network. All buildings will
                be connected
            heat_sources: Set of heat source for the network.
            flow_properties: Model for transport fluid within the network including circulation temperature
            costs: Cost model for pipes and digging
            load_pipes: Whether to load the detailed pipe geometry (from road network) corresponding to
                the simplified network connectivity graph (straight lines between neighbouring buildings)
            ways: Extra geometries ("shortcuts") to add to the network route finder to apply (limited)
                modifications to the road network used to generate the pipes.
            name: Name to set for the network. IMPORTANT most of the codes ASSUMES this is not changed
                from the default value.
            district_network: Optione, an existing district heat network to be updated with the district,
                heat source, cost, and flow property data.

        Returns:
            district heat network for this district.
        """
        district_new = self.add_district_heating_network(
            district, heat_sources, district_network, costs, flow_properties, name, ways
        )

        network = next(u for u in district_new.utility_networks if isinstance(u, DistrictNetwork))
        return network

    def add_district_heating_network(
        self,
        district,
        heat_sources=None,
        district_network=None,
        costs=None,
        flow_properties=None,
        name: str = "district_heating",
        ways=None,
    ):
        """

        Args:
            district: The district of buildings for which to generate a network. All buildings will
                be connected
            heat_sources: Set of heat source for the network.
            flow_properties: Model for transport fluid within the network including circulation temperature
            costs: Cost model for pipes and digging
            load_pipes: Whether to load the detailed pipe geometry (from road network) corresponding to
                the simplified network connectivity graph (straight lines between neighbouring buildings)
            ways: Extra geometries ("shortcuts") to add to the network route finder to apply (limited)
                modifications to the road network used to generate the pipes.
            name: Name to set for the network. IMPORTANT most of the codes ASSUMES this is not changed
                from the default value.
            district_network: Optional, an existing district heat network to be updated with the district,
                heat source, cost, and flow property data.

        Returns:
            district with district heat network added to the utility_networks list

        """
        flow_properties = flow_properties or FlowProperties()
        costs = costs or NetworkCosts()
        heat_sources = heat_sources or []
        dn_layout = network_builder.DnMstLayout(routing_service=self.routing_service, ways=ways)
        substation_locator = network_builder.SubstationLocator(routing_service=self.routing_service)
        solve_heat_flow = network_builder.SwissDnHeatFlowClassic(fluid_properties=flow_properties)
        est_pipe_costs = network_builder.DNCosts(pipe_costs=costs)
        if district_network is None:
            district_new = dn_layout.create(district, name=name)
        else:
            district_new = dn_layout.update(district, district_network=district_network.uid)
        district_new = (
            district_new.apply(
                partial(substation_locator, substations=[Substation(sources=[hs]) for hs in heat_sources])
            )
            .apply(solve_heat_flow)
            .apply(est_pipe_costs)
        )
        return district_new
