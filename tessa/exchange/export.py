import collections
import json
import sqlite3
import tempfile
import uuid
import zipfile
from collections.abc import Iterable
from pathlib import Path
from types import NoneType, UnionType
from typing import Annotated, Literal, Union, get_args, get_origin

import fiona
import fiona.crs
import pandas as pd
from pydantic import BaseModel

from tessa.district.models import District
from tessa.thermal_system_models.models import Substation
from tessa.utils.pydantic_shapely import LineString


def flat_recursive(key, value, accumulator):
    if not isinstance(value, dict):
        accumulator[key] = value
    else:
        for k, v in value.items():
            flat_recursive(key + "__" + k, v, accumulator)

