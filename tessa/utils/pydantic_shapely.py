from collections.abc import Iterable
from typing import Any, Dict, List, Literal, Optional, TypeVar, Union

import geojson_pydantic
import geojson_pydantic.types
import numpy as np
import pyproj
import shapely
import shapely.geometry
from geojson_pydantic import Feature as Feature_
from geojson_pydantic import FeatureCollection as FeatureCollection_
from geojson_pydantic.geometries import Geometry
from pydantic import BaseModel, PrivateAttr
from shapely.ops import transform


class _GeoMixin:
    # NOTE it seems defining the geom attr in the mixin breaks the attr setting
    # in the subclasses.
    # _geom: shapely.geometry.base.BaseGeometry = PrivateAttr()
    def to_feature(self, id=None, properties=None):
        if not properties:
            properties = {}
        return Feature(properties=properties, geometry=self, bbox=self.bbox, id=id, type="Feature")

    @classmethod
    def from_geojson(cls, geojson):
        if hasattr(geojson, "__geo_interface__"):
            geojson = geojson.__geo_interface__
        if "geometry" in geojson:
            geojson = geojson["geometry"]
        return cls(shapely.geometry.shape(geojson))

    @property
    def geometry(self) -> shapely.Geometry:
        return self._geom

    @property
    def wkb(self):
        return self._geom.wkb

    @property
    def area(self) -> float:
        return shapely.area(self._geom)

    @property
    def geom_type(self):
        return self._geom.geom_type

    def representative_point(self) -> "Point":
        return Point(self._geom.representative_point())

    def buffer(self, *args, **kwargs):
        return wrap_shape(self._geom.buffer(*args, **kwargs))

    # def concave_hull(self):
    #     return wrap_shape(shapely.concave_hull(self._geom))

    # def convex_hull(self):
    #     return wrap_shape(shapely.convex_hull(self._geom))

    def intersection(self, other, grid_size=None):
        if hasattr(other, "geometry"):
            other = other.geometry
        result = self._geom.intersection(other, grid_size=grid_size)
        return wrap_shape(result)

    def difference(self, other, grid_size=None):
        if hasattr(other, "geometry"):
            other = other.geometry
        return wrap_shape(self._geom.difference(other, grid_size=grid_size))

    def equals(self, other):
        if hasattr(other, "geometry"):
            other = other.geometry
        return self._geom.equals(other)


class Point(geojson_pydantic.Point, _GeoMixin):
    _geom: shapely.geometry.Point = PrivateAttr()

    def __init__(self, *args, **kwargs):
        if not args and "coordinates" in kwargs:
            args = (shapely.geometry.shape(kwargs),)
        elif not args and "x" in kwargs:
            x = kwargs.pop("x")
            y = kwargs.pop("y")
            z = kwargs.pop("z", None)
            if z:
                args = (x, y, z)
            else:
                args = (x, y)
        elif args and isinstance(args[0], dict) and "coordinates" in args[0]:
            args = (shapely.geometry.shape(args[0]),)

        _geom = shapely.geometry.Point(*args)

        coords = np.array(_geom.coords).tolist()[0]
        kwargs.pop("coordinates", None)
        type_ = kwargs.pop("type", "Point")
        super().__init__(type=type_, coordinates=coords, **kwargs)
        self._geom = _geom

    @property
    def x(self):
        return self._geom.x

    @property
    def y(self):
        return self._geom.y

    @property
    def z(self):
        return self._geom.z


class Polygon(geojson_pydantic.Polygon, _GeoMixin):
    _geom: shapely.geometry.Polygon = PrivateAttr()

    def __init__(self, coordinates=None, shell=None, holes=None, **kwargs):
        """
           Wrapper combining a geojson-compatible Polygon defined as a pydantic object
           that can be used as a type annotation and FastAPI return type, and a shapely
            geometry object than can perform geometric calculations.

        Juggle between shapely polygon spec and geojson spec.
        Shapely asks for outer shell and inner holes. Geojson specifies list of coordinates
        with first item as shell and subsequent items as holes.
        Args:
            coordinates: geojson spec coordinate list or a shapely or geojson Polygon instance
            shell:
            holes:
            **kwargs:
        """
        type_ = kwargs.pop("type", "Polygon")

        if coordinates is not None and shell is not None and holes is not None:
            raise ValueError("Must provide either `coordinates` or `shell` and `holes`")
        elif coordinates is None and shell is not None:
            # passed shell, holes shapely style
            _geom = shapely.geometry.Polygon(shell=shell, holes=holes)
            super().__init__(**_geom.__geo_interface__)
        elif coordinates is not None and shell is not None:
            # passed shell, holes as arg style instead of kargs
            _geom = shapely.geometry.Polygon(shell=coordinates, holes=shell)
            super().__init__(**_geom.__geo_interface__)
        elif isinstance(coordinates, shapely.geometry.Polygon):
            _geom = coordinates
            super().__init__(**coordinates.__geo_interface__)
        elif isinstance(coordinates, (Polygon, geojson_pydantic.Polygon)):
            _geom = shapely.geometry.shape(coordinates.__geo_interface__)
            super().__init__(**coordinates.__geo_interface__)
        elif coordinates and len(coordinates) > 0 and len(kwargs) == 0:
            shell = coordinates[0]
            if len(coordinates) > 1:
                holes = coordinates[1:]
            _geom = shapely.geometry.Polygon(shell=shell, holes=holes)
            super().__init__(**_geom.__geo_interface__)
        else:
            super().__init__(type=type_, coordinates=coordinates, **kwargs)
            _geom = shapely.geometry.shape(self.model_dump(mode="json"))
        self._geom = _geom


class LineString(geojson_pydantic.LineString, _GeoMixin):
    _geom: shapely.geometry.LineString = PrivateAttr()

    def __init__(
        self,
        coordinates: Optional[geojson_pydantic.types.LineStringCoords | shapely.geometry.LineString] = None,
    ):
        from shapely.geometry.base import BaseGeometry

        if isinstance(coordinates, Iterable) and not isinstance(LineString, BaseGeometry):

            def _coords(o):
                if hasattr(o, "_geom"):
                    return o._geom
                else:
                    return o

            coordinates = [_coords(o) for o in coordinates]
        _geom = shapely.geometry.LineString(coordinates)
        coords = list(_geom.coords)
        super().__init__(type="LineString", coordinates=coords)
        self._geom = _geom


class MultiPoint(geojson_pydantic.MultiPoint, _GeoMixin):
    _geom: shapely.geometry.MultiPoint = PrivateAttr()

    def __init__(self, coordinates=None, **kwargs):
        if isinstance(coordinates, shapely.MultiPolygon):
            kwargs = coordinates.__geo_interface__
            super().__init__(**kwargs)
            self._geom = coordinates
            return
        else:
            kwargs.pop("type", "MultiPoint")  # remove reduntant type parameter
            super().__init__(type="MultiPoint", coordinates=coordinates, **kwargs)
            self._geom = shapely.geometry.MultiPolygon(coordinates)


class MultiPolygon(geojson_pydantic.MultiPolygon, _GeoMixin):
    _geom: shapely.geometry.MultiPolygon = PrivateAttr()

    def __init__(self, coordinates=None, **kwargs):
        if isinstance(coordinates, shapely.MultiPolygon):
            kwargs = coordinates.__geo_interface__
            super().__init__(**kwargs)
            self._geom = coordinates
        else:
            type_ = kwargs.pop("type", "MultiPolygon")  # remove reduntant type parameter
            super().__init__(type=type_, coordinates=coordinates, **kwargs)
            self._geom = shapely.geometry.shape(self.model_dump(mode="json"))


class MultiLineString(geojson_pydantic.MultiLineString, _GeoMixin):
    _geom: shapely.geometry.MultiLineString = PrivateAttr()

    def __init__(self, coordinates=None, **kwargs):
        if isinstance(coordinates, shapely.MultiLineString):
            kwargs = coordinates.__geo_interface__
            super().__init__(**kwargs)
            self._geom = coordinates
        else:
            type_ = kwargs.pop("type", "MultiLineString")
            super().__init__(type=type_, coordinates=coordinates, **kwargs)
            self._geom = shapely.geometry.MultiLineString(coordinates)


def wrap_shape(shape):
    if hasattr(shape, "geom_type"):
        type_ = shape.geom_type
    elif hasattr(shape, "type"):
        type_ = shape.type
    else:
        type_ = type(shape).__name__

    match type_:
        case "Point":
            return Point(shape)
        case "LineString":
            return LineString(shape)
        case "Polygon":
            return Polygon(shape)
        case "MultiPoint":
            return MultiPoint(shape)
        case "MultiLineString":
            return MultiLineString(shape)
        case "MultiPolygon":
            return MultiPolygon(shape)


Props = TypeVar("Props", bound=Union[dict[str, Any], BaseModel])
Geom = TypeVar("Geom", bound=Geometry)


class Feature(Feature_[Geom, Props]):
    # Add this default override because it's annoying to get errors if you forget to specfiy
    type: Literal["Feature"] = "Feature"


Feat = TypeVar("Feat", bound=Feature)


class FeatureCollection(FeatureCollection_[Feat]):
    """FeatureCollection Model"""

    type: Literal["FeatureCollection"] = "FeatureCollection"
    features: list[Feature]

    def __init__(self, *args, **kwargs):
        # Allow to pass a geojson formatted dict instead of features=... kwarg
        if len(args) == 1 and isinstance(args[0], dict) and "features" in args[0] and len(kwargs) == 0:
            # assume this is already a geojson like dict
            kwargs = {"features": args[0]["features"]}
        super().__init__(**kwargs)


def as_crs(geom, source_crs, target_crs):
    """
    Convert the CRS of a geom from the source to the target CRS.

    Args:
        geom:
        source_crs:
        target_crs:

    Returns:

    """
    if geom.geom_type == "Point":
        transformer = pyproj.Transformer.from_crs(source_crs, target_crs, always_xy=True)
        x_, y_ = transformer.transform(geom.x, geom.y)
        return Point(x_, y_)
    else:
        project = pyproj.Transformer.from_crs(source_crs, target_crs, always_xy=True).transform
        out = transform(project, geom)
        return wrap_shape(out)
