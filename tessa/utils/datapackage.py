import logging

import pandas as pd

logger = logging.getLogger()


def read_datapackage_table(path, data="data", py_schema=None):
    """Read timeseries data and meta-indicators from frictionless Data Package

    Parameters
    ----------
    path : string or path object
        any valid string path or :class:`pathlib.Path`, |br|
        passed to :class:`datapackage.Package` (|datapackage.Package.docs|)
    data : str, optional
        resource containing timeseries data in IAMC-compatible format
    """
    from frictionless import Package

    package = Package(path)

    # read `data` table, if not found and there is only one resource assume this is the one your want
    if data not in {r.name for r in package.resources} and len(package.resources) == 1:
        resource_data = package.resources[0]
    else:
        resource_data = package.get_resource(data)
    _data = resource_data.to_pandas()
    _data.columns = [i.name for i in resource_data.schema.fields]

    # IDEA Could use Pint to also add units.
    # IDEA Could dynamically generate a pydantic class for tabular datasets.
    df = pd.DataFrame(_data)

    if py_schema:
        df = py_schema(df)
    return df
