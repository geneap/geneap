import numba
import numpy as np
import pandas as pd
import pyproj
import shapely.ops
import xarray as xr
from geopandas import GeoDataFrame, GeoSeries
from shapely.geometry.base import BaseGeometry


def to_crs(geom: BaseGeometry | GeoSeries | GeoDataFrame, output_crs, input_crs=None):
    """simple tool to convert shapley geoms"""
    if isinstance(geom, (GeoSeries, GeoDataFrame)):
        return geom.to_crs(output_crs)
    else:
        from_crs = pyproj.CRS(input_crs)
        to_crs = pyproj.CRS(output_crs)
        project = pyproj.Transformer.from_crs(from_crs, to_crs, always_xy=True).transform
        geom_out = shapely.ops.transform(project, geom)
        return geom_out


def approx_bounds(x, y, pad=0, round_=None):
    bounds = np.array([x.min() - pad, x.max() + pad, y.min() - pad, y.max() + pad])
    if round_ is not None:
        bounds = np.round(bounds, round_)
    return tuple(bounds)


def save_geotiff(path, data, crs=None, transform=None, dtype=None, colormap=None, **rasterio_kws):
    """

    Parameters
    ----------
    path
    data: numpy array or Xarray DataArray
    crs: CRS to set on GeoTiff, optional, if ommitted will try to get from DataArray attributes
    transform
    dtype
    colormap
    rasterio_kws

    Returns
    -------

    """
    import rasterio.crs
    import rasterio.features

    if colormap is not None and data.dtype != np.int8:
        raise RuntimeError("Must use int8 data if supplying a colormap")

    # Support using an Xarray dataarray with crs and transform as attributes
    if crs is None and hasattr(data, "crs"):
        crs = data.crs
        if not isinstance(crs, rasterio.crs.CRS):
            crs = rasterio.crs.CRS.from_string(crs)

    if transform is None and hasattr(data, "transform"):
        transform = data.transform
        if not isinstance(transform, rasterio.Affine):
            transform = rasterio.Affine(*transform[:6])

    # If it is an xarray dataarray, need to access the underlying numpy array to write
    # TODO: (s)hould check grid is 2D only! If not, could write each band
    if hasattr(data, "data"):
        data = data.data
    else:
        data = data

    if dtype is not None:
        data = data.astype(dtype)

    # Set defaults
    extra_kws = dict(
        driver="GTiff",
        compress="lzw",
    )
    # Override with supplied kws
    extra_kws.update(rasterio_kws)
    # Save a nice geoTiff version
    with rasterio.open(
        path,
        "w",
        height=data.shape[0],
        width=data.shape[1],
        crs=crs,
        transform=transform,
        dtype=data.dtype,
        count=1,
        **extra_kws,
    ) as dataset:
        dataset.write(data, 1)

        if colormap is not None:
            dataset.write_colormap(1, colormap)


def polygons_from_boolean_raster(bool_raster, transform):
    if isinstance(bool_raster, xr.DataArray):
        bool_raster = bool_raster.data

    mask = bool_raster
    raster = bool_raster.astype(np.int16)
    import rasterio.features

    shapes = rasterio.features.shapes(raster, mask=mask, transform=transform)
    return shapes


# TODO: (c)ould try for higher perf using a bulk sum function
# that iterates once and sums all the values to the out.
@numba.njit(nogil=True)
def sum_to_raster(i_s, j_s, values, out):
    """Given 1D arrays of 2D indexes and corresponding values,
    calculate the sum of values at each index location (i,j)
    and add to the provided `out` variable.

    Note that `out` is not initialised, so it may already contain
    values from a previous operation, making it easier to accumulate
    results.
    """
    n_items = len(i_s)
    for c in range(n_items):
        i = int(i_s[c])
        j = int(j_s[c])
        h = values[c]
        # Need to check that the value we assign is not nan,
        # if it is init to zero. Don't init the whole array to zero
        # because we want to be able to supply a partially procesed
        # out arry
        v = out[i, j]
        if np.isnan(v):
            out[i, j] = 0
        if not np.isnan(h):
            out[i, j] += h

    return out


def affine_from_coords(xcoord, ycoord):
    min_x = np.min(xcoord).item()
    max_y = np.max(ycoord).item()
    import rasterio

    pixel_size_x = xcoord[1].item() - xcoord[0].item()
    pixel_size_y = ycoord[1].item() - ycoord[0].item()
    return rasterio.transform.from_origin(min_x, max_y, pixel_size_x, pixel_size_y)


def affine_from_bounds(bbox, pixel_size):
    min_x, max_x, min_y, max_y = bbox
    if np.isscalar(pixel_size):
        pixel_size_x = pixel_size_y = pixel_size
    else:
        # any wrong types here will generate their own errors
        pixel_size_x = pixel_size[0]
        pixel_size_y = pixel_size[1]
    import rasterio

    affine = rasterio.transform.from_origin(min_x, max_y, pixel_size_x, pixel_size_y)
    return affine


def raster_shape_from_bounds(bbox, affine=None, pixel_size=None):
    if affine is not None:
        pixel_size_x = abs(affine.a)
        pixel_size_y = abs(affine.e)
    elif pixel_size is not None:
        if np.isscalar(pixel_size):
            pixel_size_x = pixel_size_y = pixel_size
        else:
            # any wrong types here will generate their own errors
            pixel_size_x = pixel_size[0]
            pixel_size_y = pixel_size[1]
    else:
        raise ValueError(
            """Not able to determine raster output shape. Must provide
               `bbox` and `pixel_size` or `affine`."""
        )

    min_x, max_x, min_y, max_y = bbox

    len_x = int(np.ceil((max_x - min_x) / pixel_size_x))
    len_y = int(np.ceil((max_y - min_y) / pixel_size_y))

    return len_y, len_x


def rowcol(xs, ys, transform, index_op=np.round, eps=0):
    import rasterio

    if not isinstance(transform, rasterio.Affine):
        transform = rasterio.Affine(*transform)
    invtransform = ~transform
    j_s, i_s = invtransform * (xs + eps, ys - eps)

    i_s = index_op(i_s).astype(int)
    j_s = index_op(j_s).astype(int)
    return i_s, j_s


# TODO: (t)ests for all different input combos
class RasterSummary:
    """Aggregates point data (given as postion and value) to a 2D raster"""

    def __init__(
        self,
        data,
        x,
        y,
        crs=None,  # crs only really needed to pass thru to xarray output
        transform=None,
        shape=None,  # need shape or bbox
        coords=None,
        bbox=None,
        resolution=None,  # TODO: (r)ename to 'resolution'
        pixel_ref="center",  # centre matches default Xarray convention
    ):
        """

        Note: the 'canonical' representation for a raster is given
        by it's affine transform and shape. For any other combinations of arguments
        we try to infer these parameters, but in certain cases this leads to
        unexpected results.

        Parameters
        ----------
        data the dataframe
        x the column for the x coordinate
        y the column for the y coordinate
        crs
        transform
        shape
        coords : coordinates arrays (e.g. from existing dataset) from which affine and bounding box will be
        inferred. NOTE that irregular coordinates are not supported.
        bbox : min_x, max_x, min_y, max_y
        resolution : pixel size in CRS units
        pixel_ref : How the pixel is referenced - pixel coordinates corrspond to the `corner` or the `centre`
        of the pixel
        """
        # TODO: (a)llow to pass the actual column series for the coords not only the name of the column
        # TODO: (c)ould allow for a geopandas dataframe and convert from point geom
        # TODO: (a)llow 'autobounds' from input data (still require pixel size) - take min value and snap to closest multiple of pixel size(?)
        # TODO: (c)heck that transform is of type Affine, otherwise try to convert from tuple form of Affine
        import rasterio
        import rasterio.crs

        # TODO: (p)refer weakref, don't want to accidentally store data in the summarizer
        self.x_col = x
        self.y_col = y
        self.data = data
        self.crs: rasterio.crs.CRS = crs

        if transform is None:
            # if no affine, can get it from either pixel coords or bbox+pixel size
            if coords is not None:
                xcoord = coords[0]
                ycoord = coords[1]
                # add .item() to make sure we get scalar value
                min_x = np.min(xcoord).item()
                max_y = np.max(ycoord).item()

                pixel_size_x = xcoord[1].item() - xcoord[0].item()
                pixel_size_y = ycoord[1].item() - ycoord[0].item()
                self.transform = rasterio.transform.from_origin(min_x, max_y, pixel_size_x, pixel_size_y)
            elif bbox is not None and resolution is not None:
                min_x, max_x, min_y, max_y = bbox
                if np.isscalar(resolution):
                    pixel_size_x = pixel_size_y = resolution
                else:
                    # any wrong types here will generate their own errors
                    pixel_size_x = resolution[0]
                    pixel_size_y = resolution[1]
            else:
                raise ValueError(
                    """Not able to determine affine. Must provide either
                `affine`, `pixel_coords`, or `bbox` and `pixel_size`"""
                )

            self.transform = rasterio.transform.from_origin(min_x, max_y, pixel_size_x, pixel_size_y)
        else:
            self.transform = transform

        if coords is not None:
            self.shape = (len(coords[1]), len(coords[0]))
        elif shape is not None:
            self.shape = shape
        elif bbox is not None:
            self.shape = raster_shape_from_bounds(bbox, self.transform)
        else:
            raise ValueError(
                """Not able to determine raster output shape. Must provide either
              `shape`, `pixel_coords`, or `bbox` and {`pixel_size` or `affine`}."""
            )

        # allowed 'center', 'corner'
        if pixel_ref == "center":
            self.index_op = np.round
        elif pixel_ref == "corner":
            self.index_op = np.floor
        else:
            raise ValueError(
                f"Pixel coordinate convention {pixel_ref} not supported, must be `center` or `corner`"
            )
        self.pixel_ref = pixel_ref

        self.coords = {}
        # Disable use of passed coords - instead generate self consistent
        # coords from the affine/bbox.
        # FIXME not sure if this is the right approach
        # if coords is not None:
        #     self.coords[self.y_col] = coords[1]
        #     self.coords[self.x_col] = coords[0]
        # else:

        self._prepare_coords()

        self._prepare_indexes()

    def _prepare_indexes(self):
        eps = 0  # no tolerance
        xs = np.asarray(self.data[self.x_col])
        ys = np.asarray(self.data[self.y_col])

        # Apply the inverse of the affine transform to get back the indexes
        # TODO: (w)hat about giving coords (maybe irregular) and indexing with them.
        # => I think not, this should be about 'true' rasters defined by regular
        # affine transforms
        # ii = pd.Index(xs)
        # ii.get_indexer(self.data[self.x], method='nearest', tolerance=100)
        self.i_s, self.j_s = rowcol(xs, ys, self.transform, index_op=self.index_op, eps=eps)

    def _prepare_coords(self):
        """Generate coordinates for pixels (assume rectilinear coords)

        Returns
        -------

        """
        # Depending on the pixel ref system, set appropriate padding.
        pad = 0 if self.pixel_ref == "corner" else 0.5
        ny, nx = self.shape
        x, _ = self.transform * (np.arange(nx) + pad, np.zeros(nx) + pad)
        _, y = self.transform * (np.zeros(ny) + pad, np.arange(ny) + pad)
        self.coords[self.y_col] = y
        self.coords[self.x_col] = x

    def _to_xarray(self, data, name=None):
        import xarray

        da = xarray.DataArray(data, coords=self.coords, dims=[self.y_col, self.x_col], name=name)
        if self.crs is not None:
            # Use CF format CRS since we have an 'affinity' with climate style data formats?
            #             da.attrs['crs'] = self.crs.to_cf()
            da.attrs["crs"] = self.crs.to_string()

        da.attrs["transform"] = tuple(
            list(self.transform)[:6]
        )  # must be a nicer way to get first 6 values that matter
        da.attrs["res"] = (abs(self.transform.a), abs(self.transform.e))
        da.attrs["AREA_OR_POINT"] = "Area"

        return da

    def __repr__(self):
        return f"""{self.__class__}(data, x={self.x_col}, y={self.y_col}, crs={self.crs},
shape={self.shape}, pixel_ref={self.pixel_ref})"""

    def sum(self, col, dtype=None, out=None, skipna=True):
        _dtype = dtype if dtype is not None else np.dtype(self.data[col])
        if out is None:
            out = np.zeros(self.shape, dtype=_dtype)

        data_to_sum = np.asarray(self.data[col])
        # Ignore NAN by default
        # TODO: (a)llow optional nan propagation - but means making a copy of the data
        # since we have to first modify it (fill nans with zeros)
        if skipna is True:
            data_to_sum = np.nan_to_num(data_to_sum)

        # TODO: (m)aybe allow to choose whether to generate xarray or instead match input dtype (e.g. raw numpy)
        return self._to_xarray(sum_to_raster(self.i_s, self.j_s, data_to_sum, out), col)

    def count(self, dtype=None, out=None):
        _dtype = dtype if dtype is not None else np.int
        if out is None:
            out = np.zeros(self.shape, dtype=_dtype)
        return self._to_xarray(sum_to_raster(self.i_s, self.j_s, np.ones_like(self.i_s), out), "count")

    def mean(self, col, dtype=None, skipna=True):
        # NOTE don't allow 'out' param for mean because it doesn't make sense
        # IDEA could allow multi simultaneous aggregations with optimization that avoids calculating sum and count
        # several times
        _dtype = dtype if dtype is not None else np.dtype(self.data[col])

        out1 = np.zeros(self.shape, dtype=_dtype)
        out2 = np.zeros(self.shape, dtype=_dtype)
        data_to_sum = np.asarray(self.data[col])
        if skipna is True:
            data_to_sum = np.nan_to_num(data_to_sum)
        data = sum_to_raster(self.i_s, self.j_s, data_to_sum, out1) / sum_to_raster(
            self.i_s, self.j_s, np.ones_like(self.i_s), out2
        )
        return self._to_xarray(data, col)


def pixel_table_to_sparse_dataset(
    data,
    concat_dim="dim_0",
    x_coord=None,
    y_coord=None,
    shape=None,
    i_coord="i_s",
    j_coord="j_s",
    transform=None,
):
    """
    For a (possibly multi-index) table representing sparse pixel data
    where the first levels of the index represent sub-pixel categories and
    the final two level represent grid locations i and j (i_s, j_s)
    convert the pixel coordinates of values into a sparse grid of values
    and return a Dataset where each variable is a column in the input data

    Note that xarray also has Dataset.from_dataframe(..., sparse=True)
    """

    import sparse

    if x_coord is None and y_coord is None and shape is None:
        raise RuntimeError("Must supply x_coord and y_coord and/or shape")
    if (x_coord is not None) and (y_coord is not None):
        coord_2d = (y_coord, x_coord)
    else:
        coord_2d = None

    if shape is None:
        # infer row, col size from coordinate arrays.
        shape = (len(y_coord), len(x_coord))

    if isinstance(data, pd.DataFrame):
        data_col = data.columns
    elif isinstance(data, pd.Series):
        data_col = [data.name]
    else:
        raise ValueError("data must be DataFrame or Series")

    # Support a non-multindex table with index i, j
    if len(data.index.names) == 2:
        res_per_col = {}
        pix_data = data.reset_index()
        for col in data_col:
            d = pix_data[col].to_numpy().squeeze()
            if d.ndim != 1:
                raise ValueError("data must be a scalar or 1-dimensional.")

            res_per_col[col] = xr.DataArray(
                sparse.COO(
                    pix_data[[i_coord, j_coord]].to_numpy().T,
                    # seems like sparse accepts 2D data array with shape (N,1) but doesn't like it later
                    data=d,
                    shape=shape,
                ),
                coords=coord_2d,
                dims=["y", "x"],
                name=col,
            )
    else:
        group_levels = data.index.names[:-2]

        res_per_col = {c: {} for c in data_col}
        for k, pix_data in data.groupby(level=group_levels):
            # cleanup the multi index to keep only last two levels (i,j)
            pix_data = pix_data.droplevel(group_levels).reset_index()

            for col in data_col:
                d = pix_data[col].to_numpy().squeeze()
                if d.ndim != 1:
                    raise ValueError("data must be a scalar or 1-dimensional.")
                res_per_col[col][k] = sparse.COO(
                    pix_data[[i_coord, j_coord]].to_numpy().T, data=d, shape=shape
                )

        if x_coord is not None and y_coord is not None:
            for col in data_col:
                res_per_col[col] = xr.DataArray(
                    sparse.stack(res_per_col[col].values()),
                    coords=(pd.Index(res_per_col[col].keys(), names=group_levels), *coord_2d),
                    dims=[concat_dim, "y", "x"],
                    name=col,
                )

    if isinstance(data, pd.DataFrame):
        ds = xr.Dataset(res_per_col)
        if transform is not None:
            ds.attrs["transform"] = transform
        return ds
    elif isinstance(data, pd.Series):
        ds = res_per_col[data_col[0]]
        if transform is not None:
            ds.attrs["transform"] = transform
        return ds
