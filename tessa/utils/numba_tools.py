import numba
import numpy as np
from numba.extending import overload, register_jitable
from numba.np.arraymath import check_array, determine_dtype, greater_than, less_than
from numba.typed import Dict, List


def _list_to_numba(the_list, dtype):
    new_list = List(dtype)
    for e in the_list:
        new_list.append(e)
    return new_list


def _dict_to_numba(the_dict):
    d = Dict.empty(key_type=numba.types.string, value_type=numba.types.float64)
    for k, v in the_dict.items():
        d[k] = v
    return d


def _list_of_dict_to_numba(the_list):
    new_list = List()
    for e in the_list:
        new_list.append(_dict_to_numba(e))
    return new_list


@numba.jit
def grid_sum(x, y, min_x, min_y, max_x, max_y, values):
    """Fast grid sum with numba

    NOTE: probably better to use the GIS and Pandas
    enabled version in the gis module

    Parameters
    ----------
    x
    y
    min_x
    min_y
    max_x
    max_y
    values

    Returns
    -------

    """
    len_x = int(np.ceil((max_x - min_x) / 100))
    len_y = int(np.ceil((max_y - min_y) / 100))

    j_s = np.floor((x - min_x) / 100).astype(int)
    i_s = np.floor((y - min_y) / 100).astype(int)

    grid = np.zeros((len_y, len_x), dtype=np.float32)
    for i, j, h in zip(i_s, j_s, values, strict=True):
        grid[i, j] += h
    # Increasing Y from bottom corner
    return np.flipud(grid)


def bool_clip(ndarray, a_min=None, a_max=None):
    """Create a mask where values between a_min and a_max are True
    the rest are False

    if a_min or a_max are None, no clipping is performed
    """
    result = np.ones_like(ndarray, dtype=bool)

    if a_min is not None:
        result = (ndarray >= a_min) & result

    if a_max is not None:
        result = (ndarray <= a_max) & result

    return result


def clip_nan(ndarray, a_min, a_max):
    """Similar to np.clip but values outside range are set to NaN"""
    mask = bool_clip(ndarray, a_min, a_max)
    ndarray[~mask] = np.nan
    return ndarray


################
# Numba overloads
# Add some extra Numba functions that are not supported in the current version.


# Cool idea, chokes on scalars :(
# @overload(np.clip)
# def np_clip(a, a_min, a_max, out=None):
#     def np_clip_impl(a, a_min, a_max, out=None):
#         if a_min is None and a_max is None:
#             raise ValueError("array_clip: must set either max or min")
#         if out is None:
#             out = np.empty_like(a)
#         for i in range(len(a)):
#             if a_min is not None and a[i] < a_min:
#                 out[i] = a_min
#             elif a_max is not None and a[i] > a_max:
#                 out[i] = a_max
#             else:
#                 out[i] = a[i]
#         return out
#     return np_clip_impl


@overload(np.nanargmin)
def np_nanargmin(a):
    dt = determine_dtype(a)
    if np.issubdtype(dt, np.complexfloating):
        return complex_nanargmin
    else:
        return real_nanargmin


@overload(np.nanargmax)
def np_nanargmax(a):
    dt = determine_dtype(a)
    if np.issubdtype(dt, np.complexfloating):
        return complex_nanargmax
    else:
        return real_nanargmax


def nan_arg_min_max_factory(comparison_op, is_complex_dtype):
    if is_complex_dtype:

        def impl(a):
            arr = np.asarray(a)
            check_array(arr)
            it = np.nditer(arr)
            val = next(it).take(0)
            return_idx = 0
            for i, view in enumerate(it):
                v = view.item()
                if np.isnan(val.real) and not np.isnan(v.real):
                    val = v
                    return_idx = i + 1
                else:
                    if comparison_op(v.real, val.real):
                        val = v
                        return_idx = i + 1
                    elif v.real == val.real:
                        if comparison_op(v.imag, val.imag):
                            val = v
                            return_idx = i + 1
            return return_idx

    else:

        def impl(a):
            arr = np.asarray(a)
            check_array(arr)
            it = np.nditer(arr)
            val = next(it).take(0)
            return_idx = 0
            for i, view in enumerate(it):
                v = view.item()
                if not np.isnan(v):
                    if not comparison_op(val, v):
                        val = v
                        return_idx = i + 1
            return return_idx

    return impl


real_nanargmin = register_jitable(nan_arg_min_max_factory(less_than, is_complex_dtype=False))
real_nanargmax = register_jitable(nan_arg_min_max_factory(greater_than, is_complex_dtype=False))
complex_nanargmin = register_jitable(nan_arg_min_max_factory(less_than, is_complex_dtype=True))
complex_nanargmax = register_jitable(nan_arg_min_max_factory(greater_than, is_complex_dtype=True))
