from collections.abc import Callable, Iterable, MutableSequence
from typing import Dict, List, TypedDict, TypeVar

import numpy as np


class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def consecutive_slices(data, stepsize=1):
    return np.split(data, np.where(np.diff(data) > stepsize)[0] + 1)


class Observer(TypedDict):
    callback: Callable
    args: list
    kwargs: dict


OPATH_SEP = "/"


T = TypeVar("T")


def get_path_parts(path: str) -> list[str]:
    return path.split(OPATH_SEP)


def root_object_from_path(path: str, objects: Iterable[T]) -> T | None:
    """
    Given a path for an element within a list of objects,
    returns the object in the root list that contains the element

    Args:
        path:
        objects:

    Returns:

    """
    path_parts = path.split(OPATH_SEP)
    if len(path_parts) == 0:
        return None
    name, query = get_query(path_parts[0])
    if len(query) == 0:
        return None
    key, value = query
    try:
        return next(d for d in objects if hasattr(d, key) and str(getattr(d, key)) == str(value))
    except StopIteration:
        return None


def strip_path_query(path, only_at_levels=None) -> str:
    if only_at_levels is None:
        only_at_levels = []
    elif not isinstance(only_at_levels, Iterable):
        only_at_levels = [only_at_levels]

    _init_path_parts = path.split(OPATH_SEP)
    _tmp = []
    for level in only_at_levels:
        if abs(level) > len(_init_path_parts):
            raise IndexError(f"Invalid level {level}")
        elif level < 0:
            level = len(_init_path_parts) + level
        _tmp.append(level)
    only_at_levels = _tmp

    path_parts = []
    for i, p in enumerate(_init_path_parts):
        if len(only_at_levels) > 0:
            if i in only_at_levels:
                path_parts.append(p.split(":")[0])
            else:
                path_parts.append(p)
        else:
            path_parts.append(p.split(":")[0])

    return OPATH_SEP.join(path_parts)


def get_path_part(path, part) -> str:
    if isinstance(part, slice):
        path_parts = path.split(OPATH_SEP)
        return OPATH_SEP.join(path_parts[part])
    else:
        path_parts = path.split(OPATH_SEP)
        return path_parts[part]


def path_up_to_key(path, key) -> str:
    path_parts = path.split(OPATH_SEP)
    new_parts = []
    for part in path_parts:
        new_parts.append(part)
        name, query = get_query(part)
        if name == key:
            break
    return OPATH_SEP.join(new_parts)


def get_query(path_part) -> tuple[str, tuple]:
    """Get the query part of a path, i.e. the attr_name=value
    mapping that is used to select the object.
    """
    maybe_query = path_part.split(":")
    if len(maybe_query) == 2:
        name, query = maybe_query
        attr, value = query.split("=", 1)
        return name, (attr, value)
    else:
        return path_part, ()


def get_parent_path(path) -> str:
    """
    Get the path to the parent object of the given path.
    In the case that the path contains a query, this implies that
    the object is in an iterable attribute of a parent object.
    The parent in this case is considered to be the iterable attribute
    container.
    Otherwise, if the path just points to a named attribute, the parent
    is the object that has that attribute.
    """
    # If already at the root, this should have a query, just return the name.
    path_parts = path.split(OPATH_SEP)
    if len(path_parts) == 1:
        name, query = get_query(path_parts[0])
        if query == ():
            raise ValueError("No query specified")
        return name

    target = get_path_part(path, -1)
    name, query = get_query(target)
    if query:
        return strip_path_query(path, only_at_levels=-1)
    else:
        return get_path_part(path, slice(-1))


def path_to_indexes(insp_path: str, objects) -> list[tuple[str, int | None]] | None:
    """
    Convert a search path that uses queries to a path giving attribute names and iterable
    indexes.
    """
    parts = insp_path.split(OPATH_SEP)
    name, query = get_query(parts[0])
    if len(parts) == 1 and query == ():
        # already the root
        return [(name, None)]

    root_object = root_object_from_path(insp_path, objects)
    idx = objects.index(root_object)
    index_path = [(name, idx)]
    obj = root_object

    # DOES terminate because parts is fixed length.
    for part in parts[1:]:
        name, query = get_query(part)
        if hasattr(obj, name):
            obj = getattr(obj, name)

            # For iterable attributes we might want to select one and continue.
            if isinstance(obj, Iterable) and query != ():
                # needs to be iterable to query
                key, value = query
                for i, el in enumerate(obj):
                    if hasattr(el, key) and str(getattr(el, key)) == str(value):
                        obj = el
                        idx = i
                        index_path.append((name, idx))
                        break
                else:
                    # for/else - nothing found for query
                    return None
            else:
                index_path.append((name, None))
    return index_path


def index_path_to_strings(index_path: str):
    index_path_str = []
    for part in index_path:
        if isinstance(part, tuple):
            name, idx = part
            index_path_str.append(f"{name}[{idx}]")
        else:
            index_path_str.append(f"{part}")
    return index_path_str


def find_recursive(obj, parts):
    name, query = get_query(parts[0])
    if hasattr(obj, name):
        attr = getattr(obj, name)

        # For iterable attributes we might want to select one and continue.
        if isinstance(attr, Iterable) and query != ():
            # needs to be iterable to query
            key, value = query
            for el in attr:
                if hasattr(el, key) and str(getattr(el, key)) == str(value):
                    attr = el
                    break
            else:
                return None  # nothing found for query

        if len(parts) > 1:
            # need to go deeper
            attr = find_recursive(attr, parts[1:])

        return attr
    else:
        return None


def find_by_path(insp_path: str, objects):
    """
    Find an object defined by an XPath-like query within the list of
    objects.

    The path has the form

    <parent>/<child_attr>/...

    We can find objects nested within list attributes by using a simple
    attribute query of the form

    <object_name>:<attr_name>=<attribute_value>

    """
    # TODO: (c)onvert to non-recursive - not needed because path length is fixed
    #  so can just iterate over path parts and error out if item not found
    path_parts = insp_path.split(OPATH_SEP)
    inspected_object = root_object_from_path(insp_path, objects)
    if len(path_parts) > 1:
        # search inside the district
        path_parts = path_parts[1:]
        inspected_object = find_recursive(inspected_object, path_parts)

    return inspected_object


def find_parent_by_path(insp_path: str, objects):
    path_parts = insp_path.split(OPATH_SEP)
    if len(path_parts) <= 1:
        return objects
    inspected_object = root_object_from_path(insp_path, objects)
    if len(path_parts) > 2:
        # search inside the district
        path_parts = path_parts[1:-1]
        inspected_object = find_recursive(inspected_object, path_parts)
    return inspected_object


def _del_recursive(obj, parts, overwrite_attr_with=None):
    name, query = get_query(parts[0])
    if hasattr(obj, name):
        attr = getattr(obj, name)
        if isinstance(attr, MutableSequence) and query != ():
            # First check if this is a list we need to dive into
            key, value = query
            item_to_delete = None
            for el in attr:
                if hasattr(el, key) and str(getattr(el, key)) == str(value):
                    if len(parts) > 1:
                        # need to go deeper
                        _del_recursive(el, parts[1:])
                    else:
                        # Remove the item from the list
                        # NOTE this is tricky because we need to change to iter attr of the parent object
                        # not create a new list. For now do this by mutating list, which can caus
                        # other issues, requires attr to be a Mutable Sequence
                        item_to_delete = el
                    break
            else:
                raise RuntimeError(f"No element found for {query}")
            if item_to_delete is not None:
                # need to mutate list outside of loop.
                attr.remove(item_to_delete)
        elif len(parts) > 1:
            # need to go deeper
            _del_recursive(attr, parts[1:])
        else:
            setattr(obj, name, overwrite_attr_with)
    else:
        raise RuntimeError(f"Name not found a this level {name}")


def delete_by_path(insp_path: str, objects: list, overwrite_attr_with=None):
    path_parts = insp_path.split(OPATH_SEP)

    object = root_object_from_path(insp_path, objects)
    if len(path_parts) > 1:
        # search inside the district
        path_parts = path_parts[1:]
        _del_recursive(object, path_parts, overwrite_attr_with=overwrite_attr_with)
    else:
        objects.remove(object)

    return objects
