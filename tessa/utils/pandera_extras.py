"""Typing definitions and helpers."""
from datetime import datetime
from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Generic,
    List,
    Optional,
    Tuple,
    Type,
    TypeVar,
    Union,
    get_args,
)

import pandas as pd
import pandera as pa
from pandera.typing import DataFrame
from pydantic.fields import FieldInfo


def ensure_columns_for_schema(schema_model: type[pa.DataFrameModel], dataframe):
    """
    By default, Pandera won't add missing columns but we do want this behaviour.
    NOTE: Config add_missing_columns should also do this.

    Args:
        schema_model:
        dataframe:

    Returns:

    """
    schema = schema_model.to_schema()

    col: pa.Column
    for name, col in schema.columns.items():
        if name not in dataframe.columns:
            # TODO: (g)et the dtype from the column def.
            dataframe[name] = pd.Series(index=dataframe.index, name=name, dtype=object)
    return DataFrame[schema_model](dataframe)


def pandera_to_pydantic_model(cls, name):
    import numpy as np
    import pydantic

    type_lookup = {
        pd.Int64Dtype(): int,
        np.dtype("int64"): int,
        np.dtype("bool"): bool,
        np.dtype("float64"): float,
        np.dtype("<M8[ns]"): datetime,
        pd.StringDtype(): str,
        pd.StringDtype: str,
        np.dtype("<U"): str,
    }

    schema = cls.to_schema()
    fields = {}
    for col_name, c in schema.columns.items():
        type_ = type_lookup[c.dtype.type]
        if c.nullable:
            type_ = Optional[type_]

        fields[c.name] = (type_, FieldInfo(annotation=type_, default=c.default, examples=[c.default]))

    idx = schema.index
    if idx.name is None:
        idx_name = "Index"
    else:
        idx_name = idx.name
    fields[idx_name] = (
        type_lookup[idx.dtype.type],
        FieldInfo(annotation=type_lookup[idx.dtype.type], default=None),
    )

    return pydantic.create_model(name, **fields)
