from io import StringIO as _StringIO
from pathlib import Path

import pandas as pd
import sparse
import xarray as xr


def pd_read_sql_copy(query, db_engine):
    copy_sql = "COPY ({query}) TO STDOUT WITH CSV {head}".format(query=query, head="HEADER")
    conn = db_engine.raw_connection()
    cur = conn.cursor()
    store = _StringIO()
    cur.copy_expert(copy_sql, store)
    store.seek(0)
    df = pd.read_csv(store)
    return df


#############
# WIP some ideas to save sparse arrays to HDF5 or Zarr.


def sparse_dataarray_to_hdf5(da, filename):
    """
    Save an Xarray DataArray where the underlying array is a sparse array to an hdf5 file

    Tries to directly save the data and coords numpy arrays to an hdf5 file, plus the dims and coords
    from the wrapping DataArray

    TODO better to subclass the Xarray BackendArray
    TODO: currently doesn't save dataset attrs
    Parameters
    ----------
    da
    filename

    Returns
    -------

    """
    import h5py

    if not isinstance(da, xr.DataArray):
        raise NotImplementedError(f"Data of type {type(da)} is not supported, only DataArray is supported.")

    with h5py.File(filename, "w") as f:
        f.create_group("data")
        f.create_group("coords")

        for n, a in da.coords.items():
            f["coords"][n] = a.values

        # NOTE: is dims always string names? Do we need to explicitly encode?
        f["dims"] = da.dims
        # f['dims'] = [s.encode() for s in da.dims]
        if da.name is not None:
            f["name"] = da.name

        f["data"]["data"] = da.data.data
        f["data"]["nnz"] = da.data.nnz
        f["data"]["shape"] = da.data.shape

        if isinstance(da.data, sparse.COO):
            f["data"]["coords"] = da.data.coords
            # Could also use numeric indicators for type
            f["data"]["__sparse_type"] = "COO"

        elif isinstance(da.data, sparse.GCXS):
            f["data"]["indices"] = da.data.indices
            f["data"]["indptr"] = da.data.indptr
            f["data"]["compressed_axes"] = da.data.compressed_axes

            f["data"].attrs["__sparse_type"] = "GCXS"


def open_sparse_dataarray_hdf5(file):
    """
    Open an HDF5 file saved using the `sparse_dataarray_to_hdf5` format as a sparse dataarray

    Maps the hdf5 arrays to the data and coords of the sparse array and sets that as the data in
    the xr DataArray. Broadly works because everything implements the numpy array interface

    Parameters
    ----------
    file

    Returns
    -------

    """
    import h5py

    f = h5py.File(file, "r")

    if f["data"].attrs.get("__sparse_type", "COO") == "COO":
        _data = sparse.COO(f["data"]["coords"], f["data"]["data"], shape=f["data"]["shape"])
    elif f["data"].attrs["__sparse_type"] == "GCXS":
        arg = (f["data"]["data"], f["data"]["indices"], f["data"]["indptr"])
        _data = sparse.GCXS(arg, shape=f["data"]["shape"], compressed_axes=f["data"]["compressed_axes"])
    else:
        raise RuntimeError("Could not load sparse dataset from file")
    _name = f.get("name")

    return xr.DataArray(
        _data, coords=list(f["coords"].values()), dims=[s.decode() for s in f["dims"]], name=_name
    )


def sparse_dataarray_to_zarr(da, store):
    """
    Save an Xarray DataArray where the underlying array is a sparse array to an hdf5 file

    Tries to directly save the data and coords numpy arrays to an hdf5 file, plus the dims and coords
    from the wrapping DataArray

    TODO better to subclass the Xarray BackendArray
    TODO: currently doesn't save dataset attrs
    Args:
        da
        store

    """
    import zarr

    if not isinstance(da, xr.DataArray):
        msg = f"Data of type {type(da)} is not supported, only DataArray is supported."
        raise NotImplementedError(msg)

    if isinstance(store, (str, Path)):
        store = zarr.DirectoryStore(store)

    with zarr.open(store, "w") as f:
        f.create_group("data")
        f.create_group("coords")

        for n, a in da.coords.items():
            f["coords"][n] = a.values

        # NOTE: is dims always string names? Do we need to explicitly encode?
        f["dims"] = da.dims
        # f['dims'] = [s.encode() for s in da.dims]
        if da.name is not None:
            f.attrs["name"] = da.name

        f["data"]["data"] = da.data.data
        f["data"]["nnz"] = da.data.nnz
        f["data"]["shape"] = da.data.shape

        if isinstance(da.data, sparse.COO):
            f["data"]["coords"] = da.data.coords
            # Could also use numeric indicators for type
            f["data"]["__sparse_type"] = "COO"

        elif isinstance(da.data, sparse.GCXS):
            f["data"]["indices"] = da.data.indices
            f["data"]["indptr"] = da.data.indptr
            f["data"]["compressed_axes"] = da.data.compressed_axes

            f["data"].attrs["__sparse_type"] = "GCXS"


def open_sparse_dataarray_zarr(store):
    """
    Open an HDF5 file saved using the `sparse_dataarray_to_hdf5` format as a sparse dataarray

    Maps the hdf5 arrays to the data and coords of the sparse array and sets that as the data in
    the xr DataArray. Broadly works because everything implements the numpy array interface

    Parameters
    ----------
    file

    Returns
    -------

    """
    import zarr

    if isinstance(store, (str, Path)):
        store = zarr.DirectoryStore(store)

    f = zarr.open(store, "r")

    if f["data"].attrs.get("__sparse_type", "COO") == "COO":
        _data = sparse.COO(f["data"]["coords"], f["data"]["data"], shape=f["data"]["shape"])
    elif f["data"].attrs["__sparse_type"] == "GCXS":
        arg = (f["data"]["data"], f["data"]["indices"], f["data"]["indptr"])
        _data = sparse.GCXS(arg, shape=f["data"]["shape"], compressed_axes=f["data"]["compressed_axes"])
    else:
        raise RuntimeError("Could not load sparse dataset from file")
    _name = f.get("name")

    da = xr.DataArray(_data, coords=list(f["coords"].values()), dims=list(f["dims"]), name=_name)
    return da
