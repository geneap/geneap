def annuity_factor(discount_rate, lifetime):
    """
    ..math::
        a= \frac{(1+r)^L  × r}{(1+r)^L  - 1}

    """
    return (((1 + discount_rate) ** lifetime) * discount_rate) / ((1 + discount_rate) ** lifetime - 1)
