import numpy as _np
import pandas as _pd
from numpy import ma


def modified_z_score(points):
    """
    Returns an array of the Modified Z-score of points

    Args:
        points : An num-observations by num-dimensions array of observations

    Returns:
        mask : A num-observations length array.

    References:
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points.shape) == 1:
        points = points[:, None]

    median = _np.median(points, 0)
    diff = points - median
    mad = _np.median(_np.fabs(points - median), 0)
    mod_z_score = 0.6745 * diff / mad
    # thresh = 3.5
    # mod_z_score <= thresh

    return mod_z_score


def is_outlier(points, thresh=3.5):
    """
    Returns a boolean array with True if points are outliers and False
    otherwise, using Modified Z-score method.
    Handles arrays with nan values via masked arrays.

    Args:
        points : A num-observations by num-dimensions array of observations
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.

    Returns:
        mask : A num-observations length boolean array.

    References:
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points) == 0:
        return _np.array([])

    points_ma = ma.masked_invalid(points)
    median = ma.median(points_ma, axis=0)
    diff = points_ma - median
    mad = ma.median(ma.abs(points_ma - median), axis=0)
    mod_z_score = 0.6745 * diff / mad

    result = mod_z_score > thresh

    return result.filled(False)


def is_not_outlier(points, thresh=3.5):
    """
    Returns the logical inverse of :func:`is_outlier`

    :param points: A num-observations by num-dimensions array of observations
    :param thresh: The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.
    :return:
    """
    return _np.logical_not(is_outlier(points, thresh))


def filter_outlier(points: _pd.Series, thresh=3.5):
    """
    Returns a boolean array with True if points are outliers and False
    otherwise, using Modified Z-score method

    Args:
        points : A Pandas Series
        thresh : The modified z-score to use as a threshold. Observations with
            a modified z-score (based on the median absolute deviation) greater
            than this value will be classified as outliers.

    Returns:
        mask : A num-observations length boolean array.

    References:
        Boris Iglewicz and David Hoaglin (1993), "Volume 16: How to Detect and
        Handle Outliers", The ASQC Basic References in Quality Control:
        Statistical Techniques, Edward F. Mykytka, Ph.D., Editor.
    """
    if len(points) == 0:
        return _np.array([])

    median = points.median()
    diff = points - median
    mad = diff.abs().median()
    mod_z_score = 0.6745 * diff / mad

    return mod_z_score <= thresh
