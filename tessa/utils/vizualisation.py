import matplotlib as mpl
import numpy as np
import pandas as pd
import sparse
import xarray as xr
from matplotlib import pyplot as plt
from matplotlib.collections import LineCollection

from tessa.sparse_graph import is_sparse


def significant_digits(
    df: pd.DataFrame | pd.Series, significance: int, inplace: bool = False
) -> pd.DataFrame | pd.Series | None:
    # Create a positive data vector with a place holder for NaN / inf data
    data = df.values
    data_positive = np.where(np.isfinite(data) & (data != 0), np.abs(data), 10 ** (significance - 1))

    # Align data by magnitude, round, and scale back to original
    magnitude = 10 ** (significance - 1 - np.floor(np.log10(data_positive)))
    data_rounded = np.round(data * magnitude) / magnitude

    # Place back into Series or DataFrame
    if inplace:
        df.loc[:] = data_rounded
    elif isinstance(df, pd.DataFrame):
        return pd.DataFrame(data=data_rounded, index=df.index, columns=df.columns)
    else:
        return pd.Series(data=data_rounded, index=df.index)


def plot_weighted_network(
    graph,
    points,
    row_coord,
    col_coord=None,
    upper_quantile=None,
    lower_quantile=None,
    vmin=None,
    vmax=None,
    color=None,
    cmap="viridis",
    ax=None,
    add_colorbar=False,
    linewidth=1.5,
    linewidth_scale=None,
    linestyle="solid",
):
    """
    Plot a weighted network given as a scipy sparse matrix with values giving edge weights.
    The plotted edges will be color mapped according to the edge weights

    Args:
        graph : sparse matrix
                  NxN shape  sparse matrix representing network edge weights. The edges will be colored
                  based on these weights
        points :  positions of each node.

        row_coord :
        cmap : color map
        upper_quantile : Upper quantile to clip the color bar
        lower_quantile : float
                         Lower quantile to clip the color bar
        vmax : Maximum value for color map range, superceeds upper_quantile
        vmin :  Minimum value for color map range, superceeds lower_quantile
        add_colorbar : Whether to plot the color bar
        color:
        linestyle:
        linewidth_scale:
        linewidth:
        ax:
    Returns:
    """
    if col_coord is None:
        col_coord = row_coord  # support symmetric dataarray

    # Extract vector from sparse graph within Xarray DataArray
    data = sparse.as_coo(graph.data)

    id_row = graph[row_coord][data.coords[0]]
    id_col = graph[col_coord][data.coords[1]]

    segs = np.stack((points.loc[id_row], points.loc[id_col]), 2)
    segs = np.swapaxes(segs, 1, 2)
    val = data.data

    if color is None:
        if upper_quantile is not None:
            max_val = np.quantile(val[np.isfinite(val)], upper_quantile)
        elif vmax is None:
            max_val = np.max(val[np.isfinite(val)])
        else:
            max_val = vmax

        if lower_quantile is not None:
            min_val = np.quantile(val[np.isfinite(val)], lower_quantile)
        elif vmin is None:
            min_val = np.min(val[np.isfinite(val)])
        else:
            min_val = vmin

        norm = mpl.colors.Normalize(vmin=min_val, vmax=max_val)
        mpl_cmap = mpl.cm.get_cmap(cmap)
        normed_values = norm(val)
        color = mpl_cmap(normed_values)
        lw = (normed_values + linewidth) * linewidth_scale if linewidth_scale is not None else linewidth
    else:
        lw = linewidth

    if ax is None:
        ax_none = True
        f, ax = plt.subplots()
    else:
        ax_none = False

    points = np.asarray(points)

    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    if ax_none or (xmin == 0 and ymin == 0):
        # override default coords
        xmin = points[:, 0].min()
        ymin = points[:, 1].min()

        xmax = points[:, 0].max()
        ymax = points[:, 1].max()

    xmin = min(points[:, 0].min(), xmin)
    xmax = max(points[:, 0].max(), xmax)

    ymin = min(points[:, 1].min(), ymin)
    ymax = max(points[:, 1].max(), ymax)

    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)

    line_segments = LineCollection(segs, colors=color, linewidths=lw, linestyle=linestyle)
    ax.add_collection(line_segments)
    ax.set_title(graph.name)

    if add_colorbar:
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        ax.figure.colorbar(sm, ax=ax)
    return ax


def plot_buildings_map(buildings, column="co2eq_hww_kg", ax=None, basemap=True):
    import geopandas as gpd
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    if not isinstance(buildings, gpd.GeoDataFrame):
        try:
            buildings = gpd.GeoDataFrame.from_features(buildings.__geo_interface__, crs=4326)
        except Exception:
            raise ValueError("buildings must be a GeoDataFrame or have a valid __geo_interface__")

    plot_data = buildings.to_crs(epsg=3857)

    if ax is None:
        fig, ax = plt.subplots(1, 1)
    # else:
    #     fig = ax.figure

    divider = make_axes_locatable(ax)

    cax = divider.append_axes("right", size="5%", pad=0.1)
    ax = plot_data.plot(column=column, cmap="RdBu_r", legend=True, ax=ax, cax=cax)
    if basemap is True:
        import contextily as cx

        cx.add_basemap(ax, zoom=16, source=cx.providers.OpenStreetMap.HOT)
    elif basemap:
        import contextily as cx

        cx.add_basemap(ax, zoom=16, source=basemap)

    return ax


def get_hist_limits(data, lower=0.02, upper=0.98) -> list | None:
    """
    Quantile based limits for hist plotting with flexible input data support

    Args:
        data

    Returns:
        List because sometimes you want to modify a limit, can't do that if it's tuple

    """
    if isinstance(data, pd.DataFrame | pd.Series):
        data_col = data.copy()
        data_col = data_col.dropna().astype(float)
        data_col = data_col[np.isfinite(data_col)]
        hist_lims = [data_col.quantile(lower), data_col.quantile(upper)]

    elif isinstance(data, xr.DataArray):
        if is_sparse(data.data):
            # no nanquantile support in sparse at the moment
            # Yes it's really data.data.data
            # (input da)(data array attr of da)(data attr of sparse array)
            data_array = data.data.data
            data_array = data_array[np.isfinite(data_array)]
            hist_lims = [np.quantile(data_array, lower), np.quantile(data_array, upper)]
        else:
            hist_lims = data.quantile([lower, upper]).data
    else:
        hist_lims = np.quantile(data, [lower, upper])

    if np.isnan(hist_lims[0]) or np.isnan(hist_lims[1]):
        # If either are NaN, return default param None and let pandas work it out
        hist_lims = None
    return hist_lims
