import logging
import os
from functools import wraps

import structlog


def configure_global_logger():
    """
    Configure the logger with settings that hold globally.

    All the local differences in logging will be set in each file separately
    """
    level = os.getenv("LOGGER_LEVEL", "INFO")
    if not level:
        level = "INFO"

    logging.basicConfig(level=level)

    structlog.configure(
        wrapper_class=structlog.make_filtering_bound_logger(logging.getLevelName(level)),
        processors=[
            structlog.contextvars.merge_contextvars,
            structlog.processors.add_log_level,
            structlog.processors.TimeStamper(fmt="iso"),
            # structlog.processors.JSONRenderer(),
            structlog.dev.ConsoleRenderer(),
        ],
    )


def configure_thread_logger(**kwargs):
    """
    Configure the logger with settings specific to the page.

    Note that contextvars are values that are set per-thread
    """
    structlog.contextvars.clear_contextvars()
    structlog.contextvars.bind_contextvars(**kwargs)


def log_inputs_and_outputs(func):
    log = structlog.get_logger()

    @wraps(func)
    def with_logging(*args, **kwargs):
        log.debug(f"start {func.__name__}:", args=args, kwargs=kwargs)
        result = func(*args, **kwargs)
        log.debug(f"finish {func.__name__}:", args=args, kwargs=kwargs, result=result)
        return result

    return with_logging
