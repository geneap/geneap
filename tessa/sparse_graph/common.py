"""Functions related to graphs and network theory
using sparse graphs

NOTE: today we need to use a mix of pydata sparse and scipy sparse because
of limitations of both. A long term goal is to improve both and allow to easily
use either interchangably

"""
import functools
from collections.abc import Hashable, Iterable
from typing import Optional, overload

import numpy as np
import pandas as pd
import scipy.sparse
import scipy.sparse.csgraph
import scipy.spatial
import xarray as xr

# Need both pydata/sparse and scipy.sparse
# until the day these projects are merged
from sparse import COO, DOK, GCXS, SparseArray, as_coo


def is_sparse(a):
    return isinstance(
        a,
        (
            SparseArray,
            scipy.sparse.sparray,
            scipy.sparse.coo_array,
            scipy.sparse.csr_array,
            scipy.sparse.csc_array,
            scipy.sparse.dok_array,
            scipy.sparse.dia_array,
        ),
    )


def accept_datarray(func):
    @functools.wraps(func)
    def _unwrap_wrap_datarray(x: SparseArray | xr.DataArray, **kwargs):
        is_xr = False
        dims = da_coords = attrs = name = None
        if isinstance(x, xr.DataArray):
            dims = x.dims
            da_coords = x.coords
            attrs = x.attrs
            name = kwargs.pop("name", x.name)
            is_xr = True
            x = x.data

        out = func(x, **kwargs)

        if is_xr:
            out = xr.DataArray(out, coords=da_coords, dims=dims, attrs=attrs, name=name)
        return out

    return _unwrap_wrap_datarray


def ensure_xarray_sparse_coo(r):
    if not isinstance(r.data, COO):
        r.data = as_coo(r.data)
    return r


def series_to_sparse_with_coords(series, *, graph=None, shape=None, row_index="id_from", col_index="id_to"):
    """
    Convert a Series to a sparse COO matrix using the coordinate and shape data in the graph DataArray.

    Ensure that the resulting sparse matrix has value indexes matching the coordinates of the
    input graph.

    Args:
        series:
        graph:
        shape:
        row_index:
        col_index:

    Returns:

    """
    if hasattr(row_index, "name"):
        row_index_name = row_index.name
    elif row_index is None:
        row_index_name = series.index.names[0]
    else:
        # assume hashable
        row_index_name = row_index

    if hasattr(col_index, "name"):
        col_index_name = col_index.name
    elif col_index is None:
        col_index_name = series.index.names[1]
    else:
        # assume hashable
        col_index_name = col_index

    series_row = series.index.get_level_values(row_index_name)
    series_col = series.index.get_level_values(col_index_name)

    if graph is not None:
        if shape is None:
            shape = graph.shape
        row_index = graph.get_index(row_index_name)
        col_index = graph.get_index(col_index_name)
    elif isinstance(row_index, str) or isinstance(col_index, str):
        row_index = col_index = pd.Index(series_row.unique()).union(pd.Index(series_col.unique()))
    if shape is None:
        shape = (len(row_index), len(col_index))
    # Otherwise use the row and col index directly
    i_s = row_index.get_indexer_for(series_row)
    j_s = col_index.get_indexer_for(series_col)
    coords = np.vstack((i_s, j_s))
    data = series.values
    out = COO(coords=coords, data=data, shape=shape)
    return xr.DataArray(
        out, dims=(row_index_name, col_index_name), coords=(row_index, col_index), name=series.name
    )
    # return out


def dataframe_to_sparse_with_coords(df, graph=None, shape=None, row_index="id_from", col_index="id_to"):
    """
    Convert a dataframe to a sparse COO matrix using the coordinate and shape data in the graph DataArray.

    Converts a "flat" table representation of a graph into a Dataset of sparse dataarrays with coord
    systems matching the reference graph object.

    Args:
        df:
        graph:
        shape:
        row_index:
        col_index:

    Returns:

    """
    if shape is None:
        shape = graph.shape
    if graph is not None:
        row_index_name = row_index
        col_index_name = col_index
        row_index = graph.get_index(row_index_name)
        col_index = graph.get_index(col_index_name)

    ds = []
    for col in df.columns:
        arr = series_to_sparse_with_coords(
            df[col], graph=graph, shape=shape, row_index=row_index, col_index=col_index
        )
        ds.append(arr)

    return xr.merge(ds)


def sparse_dataarray_to_dataframe(da):
    data = as_coo(da.data)

    sparse_coords = data.coords

    sparse_data = data.data
    data_name = getattr(da, "name", "value")
    result = {data_name: sparse_data}
    for i, coord in enumerate(da.coords):
        result[coord] = da.coords[coord].data[sparse_coords[i]]

    return pd.DataFrame(result)


def sparse_dataset_with_common_coords_to_dataframe(ds: xr.Dataset) -> pd.DataFrame:
    """
    Convert a dataset where the data array variables are all sparse
    and share the same coordinates into a dataframe with a multiindex

    Args:
        ds:

    Returns:

    """
    data_cols = []
    for var in ds.data_vars:
        da = ds[var]
        data = as_coo(da.data)
        sparse_coords = data.coords

        sparse_data = data.data
        data_name = getattr(da, "name", "value")

        mindex_arrs = []
        for i, coord in enumerate(da.coords):
            mindex_arrs.append(da.coords[coord].data[sparse_coords[i]])

        mindex = pd.MultiIndex.from_arrays(mindex_arrs, names=da.coords.keys())

        result = pd.Series(sparse_data, index=mindex, name=data_name)
        data_cols.append(result)

    return pd.concat(data_cols, axis=1)


def flatten_directed_2d_graph(graph: xr.Dataset, direct_var: Optional[Hashable] = None):
    """
    For a 2d matrix representing a graph, if we want to flatten it to
    a table then we want to have one row per edge. Since a graph in an
    xarray dataset might contain a mix of directed and undirected variables
    if we just flatten then we will have multiple rows for edges in different
    directions. So we convert all the graph matrices to have the same direction
    as the selected directed variable.

    Args:
        graph:
        direct_var:

    Returns:

    """
    if direct_var is None:
        return sparse_dataset_with_common_coords_to_dataframe(graph)
    # Turn the directed var into a mask which is 1 for the directed edges
    mask = graph[direct_var].astype(bool).astype(int).astype(float)

    for var in graph.data_vars:
        if var == direct_var:
            continue
        elif "is_directed" in graph[var].attrs and graph[var].attrs["is_directed"]:
            continue
        else:
            var_undirected = undirect_full(graph[var])
            graph[var] = mask * var_undirected

    return sparse_dataset_with_common_coords_to_dataframe(graph)


def sparse_2d_dataarray_to_dict(da):
    keys = da.dims
    data = as_coo(da.data)

    d_dict = {
        (i, j): v
        for i, j, v in zip(da[keys[0]][data.coords[0]].values, da[keys[1]][data.coords[1]].values, data.data)
    }
    return d_dict


def pydata_sparse_to_scipy_sparse(s):
    if isinstance(s, COO | GCXS):
        s = s.to_scipy_sparse()
    elif isinstance(s, DOK):
        s = scipy.sparse.dok_array(s.shape, s.data)
    return s


def xarray_sparse_to_scipy_sparse(x):
    if isinstance(x, xr.DataArray):
        return pydata_sparse_to_scipy_sparse(x.data)
    elif isinstance(x, SparseArray):
        return pydata_sparse_to_scipy_sparse(x)
    else:
        return x


def matrix_to_xarray(graph, coords=None, dims=None, name=None):
    if coords is None:
        coords = (np.arange(graph.shape[0]), np.arange(graph.shape[1]))
    elif not isinstance(coords, tuple):
        if dims is None and hasattr(coords, "name"):
            dims = (coords.name, coords.name)
        coords = (coords, coords)

    if dims is not None and len(dims) == 1:
        dims = (dims, dims)

    if isinstance(graph, (scipy.sparse.coo_array, scipy.sparse.coo_matrix)):
        graph = COO.from_scipy_sparse(graph)
    elif isinstance(
        graph,
        scipy.sparse.csc_array | scipy.sparse.csc_matrix | scipy.sparse.csr_array | scipy.sparse.csr_matrix,
    ):
        graph = GCXS.from_scipy_sparse(graph)

    da = xr.DataArray(graph, coords=coords, dims=dims, name=name)
    return da


@accept_datarray
def sparse_with_attr(x: SparseArray | xr.DataArray, **kwargs) -> SparseArray:
    """
    Return a new SparseArray object with attributes overriden by the values
    provided in the keyword arguments.

    Args:
        x:
        **kwargs:

    Returns:

    """
    if isinstance(x, COO):
        coords = kwargs.pop("coords", x.coords)
        data = kwargs.pop("data", x.data)
        shape = kwargs.pop("shape", x.shape)
        fill_value = kwargs.pop("fill_value", x.fill_value)
        out = COO(coords=coords, data=data, shape=shape, fill_value=fill_value, **kwargs)
    elif isinstance(x, GCXS):
        indices = kwargs.pop("indices", x.indices)
        indptr = kwargs.pop("indices", x.indptr)
        data = kwargs.pop("data", x.data)
        shape = kwargs.pop("shape", x.shape)
        fill_value = kwargs.pop("fill_value", x.fill_value)
        compressed_axes = kwargs.pop("compressed_axes", x.compressed_axes)

        arg = data, indices, indptr
        out = GCXS(arg, shape=shape, fill_value=fill_value, compressed_axes=compressed_axes, **kwargs)
    else:
        raise NotImplementedError(f"tril not implemented for type {type(x)}")

    return out


@accept_datarray
def ones_where_non_empty(x: SparseArray | xr.DataArray, dtype=bool):
    """
    Return an array like x with 1s where x is not 'empty'.
    For sparse array inputs this means where x has a value set.
    For dense array inputs this means where values are not fill_value=0 or non.
    Args:
        x:
        dtype:

    Returns:

    """
    if np.isscalar(x) or (len(x.shape) == 1 and len(x.shape) == 1):
        return 1

    if isinstance(x, COO):
        data = np.isfinite(x.data).astype(int).astype(dtype)
        out = COO(coords=x.coords, data=data, shape=x.shape, fill_value=x.fill_value)
    elif isinstance(x, GCXS):
        data = np.isfinite(x.data).astype(int).astype(dtype)
        arg = data, x.indices, x.indptr
        out = GCXS(arg, shape=x.shape, fill_value=x.fill_value, compressed_axes=x.compressed_axes)
    elif isinstance(x, np.ndarray):
        out = np.where(x != 0, np.ones_like(x), np.zeros_like(x)).astype(int).astype(dtype)
        # out = np.ones_like(x, dtype=dtype)
    else:
        raise NotImplementedError(f"ones like not implemented for type {type(x)}")

    return out


@accept_datarray
def triu(x, k: int = 0):
    """
    Returns an array with all elements below the k-th diagonal set to zero.

    Args:
    x : The input array.
    k : The diagonal below which elements are set to zero. The default is
        zero, which corresponds to the main diagonal.

    Returns:
    COO
        The output upper-triangular matrix.

    Raises
    ------
    ValueError
        If :code:`x` doesn't have zero fill-values.

    See Also
    --------
    numpy.triu : NumPy equivalent function
    """
    if not x.ndim >= 2:
        raise NotImplementedError("sparse.triu is not implemented for scalars or 1-D arrays.")
    if isinstance(x, COO):
        mask = x.coords[-2] + k <= x.coords[-1]

        coords = x.coords[:, mask]
        data = x.data[mask]

        out = COO(coords, data, shape=x.shape, has_duplicates=False, sorted=True, fill_value=x.fill_value)
    elif isinstance(x, GCXS):
        data = x.tocoo()
        out = GCXS.from_coo(triu(data, k=k), compressed_axes=x.compressed_axes)
    elif isinstance(x, np.ndarray):
        out = np.triu(x, k=k)
    else:
        raise NotImplementedError(f"triu not implemented for type {type(x)}")

    return out


@accept_datarray
def tril(x, k: int = 0):
    """
    Returns an array with all elements above the k-th diagonal set to zero.

    Args
    x : The input array.
    k : The diagonal above which elements are set to zero. The default is
        zero, which corresponds to the main diagonal.


    Returns:
    COO
        The output lower-triangular matrix.

    Raises:
    ValueError
        If :code:`x` doesn't have zero fill-values.

    See Also
    --------
    numpy.tril : NumPy equivalent function
    """

    if not x.ndim >= 2:
        raise NotImplementedError("sparse.tril is not implemented for scalars or 1-D arrays.")

    if isinstance(x, COO):
        mask = x.coords[-2] + k >= x.coords[-1]

        coords = x.coords[:, mask]
        data = x.data[mask]

        out = COO(coords, data, shape=x.shape, has_duplicates=False, sorted=True, fill_value=x.fill_value)
    elif isinstance(x, GCXS):
        data = x.tocoo()
        out = GCXS.from_coo(tril(data, k=k), compressed_axes=x.compressed_axes)
    elif isinstance(x, np.ndarray):
        out = np.tril(x, k=k)
    else:
        raise NotImplementedError(f"tril not implemented for type {type(x)}")

    return out


@accept_datarray
def undirect_lower(g: np.ndarray | SparseArray | xr.DataArray, agg="mask"):
    """
    Ensure that a graph matrix is lower triangular, folding the upper triangle
    into the lower one.

    Args:
        g:
        agg: how to aggregate upper and lower triangles if values exist in both.
            'mask' -> keep values in lower tri, drop overlapping values in upper triangle
            'sum' -> sum upper and lower tri values
            'mean' -> average of upper and lower tri values

    Returns:

    """
    # if g.fill_value is np.nan:
    #     o = tril(g) + (triu(g).T * ~tril(np.isfinite(g)))
    # else:
    mask = tril(ones_where_non_empty(g, dtype=bool))
    if agg == "mask":
        g_triu = triu(g, k=1).T * ~mask
    elif agg == "sum" or agg == "mean":
        g_triu = triu(g, k=1).T
    else:
        raise NotImplementedError(f"value agg={agg} not supported, use mask, sum, mean")

    o = tril(g) + g_triu

    if agg == "mean":
        o = o / 2
        raise NotImplementedError(f"value agg={agg} not yet supported, use mask, sum")
    return o


@overload
def undirect_full(g: SparseArray, agg="mask") -> SparseArray:
    ...


@overload
def undirect_full(g: xr.DataArray, agg="mask") -> xr.DataArray:
    ...


@accept_datarray
def undirect_full(g: SparseArray | xr.DataArray, agg="mask") -> SparseArray | xr.DataArray:
    """
    Create a symmetrical matrix
    """
    o = undirect_lower(g, agg=agg)
    o = o + triu(o.T, k=1)
    return o


def undirected_multiply(
    g1: SparseArray | xr.DataArray, g2: SparseArray | xr.DataArray
) -> SparseArray | xr.DataArray:
    """Multi values of edges of graphs g1 by g2, ignoring directedness of graph.

    if g1 is a DataArray, output will be DataArray with same coords and dims as g1

    if either g1 or g2 are scalar or 1d arrays, do a simple multiply

    Args:
    g1: ArrayLike
        Square numpy, sparse, or DataArray
    g2: ArrayLike
        Square numpy, sparse, or DataArray

    Returns:
        lower triangular matrix
    """
    if np.isscalar(g1) or np.isscalar(g2) or (len(g1.shape) == 1 and len(g2.shape) == 1):
        return g1 * g2

    dims = None
    coords = None
    if isinstance(g1, xr.DataArray):
        dims = g1.dims
        coords = g1.coords
        g1 = g1.data
    if is_sparse(g1):
        g1 = sparse_with_attr(g1, fill_value=0.0)
    if isinstance(g2, xr.DataArray):
        g2 = g2.data
    if is_sparse(g2):
        g2 = sparse_with_attr(g2, fill_value=0.0)
    # multiply edge values of graphs g1 by g2, assuming undirected graphs
    r = tril(undirect_full(g1) * undirect_full(g2))

    if dims is not None:
        r = xr.DataArray(r, coords=coords, dims=dims, attrs=dict(is_directed=False))
    return r


def directed_multiply(directed, undirected):
    """
    Multiply values of edges of directed graph by the values of the undirected graph,
    such that directedness of the directed graph is preserved.

    Will preserve the adjacency order of the directed graph

    if directed is a DataArray, output will be DataArray with same coords and dims as g1

    Args:
    directed: ArrayLike
        Square numpy, sparse, or DataArray
    undirected: ArrayLike
        Square numpy, sparse, or DataArray

    Returns:
    ArrayLike
    lower triangular matrix
    """
    if (
        np.isscalar(directed)
        or np.isscalar(undirected)
        or (len(directed.shape) == 1 and len(undirected.shape) == 1)
    ):
        return directed * undirected

    dims = None
    coords = None
    if isinstance(directed, xr.DataArray):
        dims = directed.dims
        coords = directed.coords
        directed = directed.data
    if isinstance(undirected, xr.DataArray):
        undirected = undirected.data

    # multiply edge values of graphs g1 by g2, assuming undirected graphs
    r = directed * undirect_full(undirected)
    r = r.astype(directed.dtype)
    if dims is not None:
        r = xr.DataArray(r, coords=coords, dims=dims, attrs=dict(is_directed=True))
    return r


def directed_div(directed, maybe_directed):
    """Divide values of edges of directed graph by the values of the undirected graph.

    Will preserve the adjaceny order of the directed graph

    if directed is a DataArray, output will be DataArray with same coords and dims as g1

    Args:
    directed: ArrayLike
        Square numpy, sparse, or DataArray
    maybe_directed: ArrayLike
        Square numpy, sparse, or DataArray

    Returns:
        ArrayLike lower triangular matrix
    """
    dims = None
    coords = None
    if isinstance(directed, xr.DataArray):
        dims = directed.dims
        coords = directed.coords
        directed = directed.data
    if isinstance(maybe_directed, xr.DataArray):
        maybe_directed = maybe_directed.data

    # div edge values of graphs g1 by g2, assuming undirected graphs
    r = directed / undirect_full(maybe_directed)
    r = r.astype(directed.dtype)

    if dims is not None:
        r = xr.DataArray(r, coords=coords, dims=dims, attrs=dict(is_directed=True))
    return r


def merge_sparse_dataarrays(dataarrays: Iterable[xr.DataArray]):
    """
    Merge an `Iterable` of `DataArray` backed by a sparse ndarray
    extending the dimensions as needed.

    Xarray doesn't handle merging sparse datasets yet.

    Note 1: this does not attempt to re-sort coordinates, they are simply appended
    as a result this will not interleave sparse data where coordinate dimension order
    is important.
    TODO: could be added later.

    Note 2: IMPORTANT this assumes coordinate values are non-overlapping in the input data;
    this is the case when handling a large array that has been broken up and is being
    're-assembled'. Overlapping coordinate locations will be overwritten rather than summed.

    Args:
        dataarrays: `Iterable` of `DataArray`

    Returns:

    """
    #
    merged = None
    for d in dataarrays:
        d.data = as_coo(d.data)
        if merged is None:
            merged = d
        else:
            shape = tuple(np.asarray(merged.shape) + np.asarray(d.shape))
            s_data = np.concatenate([merged.data.data, d.data.data])
            s_coords = np.concatenate([merged.data.coords, d.data.coords], axis=1)
            d_coords = {}
            for dim in merged.dims:
                d_coords[dim] = np.concatenate([merged.coords[dim].data, d.coords[dim].data])
            merged = xr.DataArray(
                COO(s_coords, s_data, shape=shape), coords=d_coords, dims=merged.dims, name=merged.name
            )

    return merged


def merge_sparse_datasets(datasets: list[xr.Dataset]):
    # NOTE could make a lazy version that merges as it goes
    # would mean multple array merges instead of one...
    ds_merged = []
    for var in datasets[0].data_vars:
        dataarrays = [d[var] for d in datasets]
        ds_merged.append(merge_sparse_dataarrays(dataarrays))
    return xr.merge(ds_merged)


def sparse_dataarraay_2d_selection(da, source, target, source_dim="id_from", target_dim="id_to"):
    """
    Since sparse doesn't support numpy advanced indexing, do it manually in a loop

    Args:
        da:
        source:
        target:
        source_dim:
        target_dim:

    Returns:

    """
    # NOTE: using get indexer is order of magnitude faster than looping using xarray sel()
    source_indexer = da.indexes[source_dim].get_indexer_for(source)
    target_indexer = da.indexes[target_dim].get_indexer_for(target)
    out = np.ones(len(source), dtype=float) * np.nan
    for e, (i, j) in enumerate(zip(source_indexer, target_indexer)):
        out[e] = da.data[i, j]
    return out


def sparse_dataarray_2d_set_value(
    da: xr.DataArray, source, target, data, source_dim="id_from", target_dim="id_to"
):
    """
    Set values in a 2D data array from coordinates source to coorinates target to values
    from data.

    Performance warning:

    Avoid doing this if possible, since we have to roundtrip convert sparse aray

    Args:
        da:
        source:
        target:
        data:
        source_dim:
        target_dim:

    Returns:

    """

    if len(source) == 0:
        return da

    # NOTE: order of magnitude faster than looping and using xarray sel()
    source_indexer = da.indexes[source_dim].get_indexer_for(source)
    target_indexer = da.indexes[target_dim].get_indexer_for(target)
    # out = np.ones(len(source), dtype=float) * np.nan

    da.data = DOK.from_coo(da.data)

    for e, (i, j) in enumerate(zip(source_indexer, target_indexer)):
        da.data[i, j] = data[e]
    da.data = da.data.to_coo()

    return da
