from typing import Optional, overload

import numba
import numpy as np
import pandas as pd
import scipy.sparse
import scipy.spatial
import xarray as xr
from joblib import Parallel, delayed
from numpy.typing import ArrayLike
from scipy.sparse import csgraph

# Need both pydata/sparse and scipy.sparse
# until the day these projects are merged
from sparse import COO, DOK, GCXS, SparseArray, concatenate

from .common import pydata_sparse_to_scipy_sparse, xarray_sparse_to_scipy_sparse


def connected_components(adjacency, directed=False):
    g = xarray_sparse_to_scipy_sparse(adjacency)
    # resulting route network might not be fully connected
    return csgraph.connected_components(g, directed=directed)


def shortest_path(adjacency, directed=False, return_predecessors=False, indices=None, **kwargs):
    g = xarray_sparse_to_scipy_sparse(adjacency)
    return csgraph.shortest_path(
        g, directed=directed, return_predecessors=return_predecessors, indices=indices, **kwargs
    )


def tesselate_points(points: ArrayLike, distances: np.ndarray, min_distance=None, max_distance=None):
    """
    Generate delaunay tesselation for a set of points setting
    edge weights from pre-calculated distances.

    NOTE that we use pre-calculated distances because we want to use
    distances that are not only the Euclidean distances between points,
    e.g. could be pre-calculated road distances

    Args:
    points : array
        Shape (N,2) giving 2D positions of the nodes
    distances : 2d matrix
        NxN matrix of distances
    min_distance
    max_distance

    Returns:
    coo_matrix
    """
    delaunay = scipy.spatial.Delaunay(points)
    simplices = delaunay.simplices

    # extract the index positions from the sample index
    edges_i = np.concatenate((simplices[:, 0], simplices[:, 1], simplices[:, 2]))
    edges_j = np.concatenate((simplices[:, 1], simplices[:, 2], simplices[:, 0]))

    # Remove duplicates by stacking indexes, sorting, and dropping duplicates
    # NOTE unfortunately, this precludes applying numba nogil because of lack
    # of support for axis arg in sort.
    a = np.vstack((edges_i, edges_j))
    a = np.sort(a, axis=0)
    a = np.unique(a, axis=1)
    edges_i, edges_j = a[0, :], a[1, :]

    # TODO: (s)parse array not supported
    # Extract the sample distances from full matrix
    d = distances[edges_i, edges_j]

    # Remove any nan/inf values
    edges_i = edges_i[np.isfinite(d)]
    edges_j = edges_j[np.isfinite(d)]
    d = d[np.isfinite(d)]

    if max_distance is not None:
        edges_i = edges_i[d <= max_distance]
        edges_j = edges_j[d <= max_distance]
        d = d[d <= max_distance]

    if min_distance is not None:
        edges_i = edges_i[d >= max_distance]
        edges_j = edges_j[d >= max_distance]
        d = d[d >= max_distance]

    # Important to include the shape after dropping dupes to make sure output matrix is square
    return scipy.sparse.coo_array((d, (edges_i, edges_j)), shape=distances.shape)


def unique_delaunay_edges_from_points(points: ArrayLike, point_ids: Optional[ArrayLike] = None) -> ArrayLike:
    """
    Generate unique (undirected) edges from delaunay triangulation of the input points.
    Vertices are identified by the point ids

    Values of the simplices are the indexes of corresponding points.
    We can use these indexes to look up the corresponding point ID in the egids array.
    Each row is a Delaunay triangle, so edges between points can appear twice in
    different rows with the same or reversed order. Therefore we need to remove duplicate
    edges (ignoring order) to produce the edge lists.

    simplex row:    (idx0, idx1, idx2)

    edges: (idx0 -> idx1), (idx1 -> idx2), (idx2 -> idx0)

    Args:
    points: ndarray
        2D array, shape (n_points, 2) (one row per point)
    point_ids: ndarray
        1D array of point identifiers (usually EGIDS). If not supplied, points will
        be numbered consecutively

    Returns:
    ndarray
        2D array, shape (n_edges, 2), elements are the vertex (point) ids
        for the given edge.

    """
    if point_ids is None:
        point_ids = np.arange(np.shape(points)[0])

    delaunay = scipy.spatial.Delaunay(points)
    simplices = delaunay.simplices

    # extract the index positions from the sample index
    edges_i = np.concatenate((simplices[:, 0], simplices[:, 1], simplices[:, 2]))
    edges_j = np.concatenate((simplices[:, 1], simplices[:, 2], simplices[:, 0]))

    # Remove duplicates by
    # 1. stacking indexes
    # 2. sorting along the edge axis (idx0, idx1) so that the indexes are always in ascending order
    # in the axis direction, which solves the 'reverse edge' problem
    # 3. dropping duplicates along stacking axis.
    edges_dedup = np.vstack((edges_i, edges_j))
    edges_dedup = np.sort(edges_dedup, axis=0)
    edges_dedup = np.unique(edges_dedup, axis=1)

    ids_edges = point_ids[edges_dedup].T
    return ids_edges


def adjacency_matrix_from_edges(source, target, cost, vertex_ids) -> scipy.sparse.coo_array:
    """
    Transform source/target egids into linear indexes as numpy arrays

    REQUIRES that source and target egids exist in the vertext id array

    Args:
    source: ndarray 1D
    target: ndarray 1D
    cost: ndarray 1D
    vertex_ids: ndarray 1D

    Returns:
    scipy.sparse.coo_matrix
        2D sparse matrix of shape (N,N) where N is len(vertex_ids)
    """
    # Need to use pandas indexing because we need to generate repeats
    # of the linear index - it's a "relational style" operation
    # (join) so much easier to do in pandas
    lookup_table = pd.DataFrame(np.arange(len(vertex_ids)), columns=["linear_index"], index=vertex_ids)

    # IMPORTANT: Scipy sparse requires indexes to be int32, but will not enforce this
    # during array creation so do it here.
    idx_src = lookup_table.loc[source].linear_index.to_numpy().astype(np.int32)
    idx_tar = lookup_table.loc[target].linear_index.to_numpy().astype(np.int32)

    # Output matrix should be same shape as coordinates
    size = len(vertex_ids)

    # NOTE: this should be without duplicates because we previously cleaned dupes
    # when generating the triangulation!
    return scipy.sparse.coo_array((cost, (idx_src, idx_tar)), shape=(size, size))


def calculate_distance_matrix(graph, n_jobs=-1, limit=np.inf):
    """
    Get the matrix of distances between every node pair in a graph
    Optionally process in parallel

    NOTE for large clusters could be very big

    Args:
    graph : csgraph
        NxN scipy sparse matrix representing the graph
    n_jobs : int, default -1
        Number of parallel jobs to use
    limit: float, optional
        The maximum distance to calculate, must be >= 0. Using a smaller limit will decrease computation time
        by aborting calculations between pairs that are separated by a distance > limit.
        If limit!=np.inf, returns a sparse graph
    Returns:
    ndarray
        dense NxN array of distances
    """
    # TODO: (c)ould convert to triangular sparse matrix to save memory...
    # TODO: (e)ven better, avoid calculating distances twice to produce a triangular matrix in the first place
    #  think i can do it by masking the indexes to be processed for each row.

    # Make it possible to parallel process the graph
    # by doing sets of indices in parallel
    if n_jobs == -1:
        import os

        n_jobs = os.cpu_count()

    # Don't actually try to do in parallel if N IDX is not >> N cores
    # Roughly say we need to have 6x as many nodes to process as cores
    # before there might be an advantage to Parallel (6xN distances per core)
    if n_jobs == 1 or graph.shape[0] < (6 * n_jobs):
        dist: np.ndarray = csgraph.dijkstra(graph, directed=False, limit=limit)
    else:
        index = np.arange(graph.shape[0])
        index_chunks = np.array_split(index, n_jobs)

        # Convert each chunk to sparse as we go instead of generating the whole thing first.
        def _do_chunk(a, directed=False, indices=None, limit=np.inf):
            # dist is dense
            distances: np.ndarray = csgraph.dijkstra(a, directed=directed, indices=indices, limit=limit)

            # if limit is set, the output might be quite sparse, use a sparse matrix
            # to reduce size.
            if limit != np.inf:
                distances[np.triu_indices_from(distances)] = np.inf
                return GCXS.from_numpy(distances, fill_value=np.inf, compressed_axes=[0])
            else:
                return distances

        # TODO:running into readonly memory buffer issues
        parallel_result: list = Parallel(n_jobs=n_jobs, prefer="threads")(
            delayed(_do_chunk)(graph, directed=False, indices=idx_chunk, limit=limit)
            for idx_chunk in index_chunks
        )

        # GXCS can apply the compression axis -> having issues, check again later
        if limit != np.inf:
            dist: SparseArray = concatenate(parallel_result, axis=0, compressed_axes=[0])
        else:
            dist: np.ndarray = np.vstack(parallel_result)

        return dist

    if limit != np.inf:
        dist[np.triu_indices_from(dist)] = np.inf
        dist = GCXS.from_numpy(dist, fill_value=np.inf)
    return dist


def labelled_distance_matrix(graph, coords=None, dims=None, name=None, n_jobs=-1, limit=np.inf):
    if isinstance(graph, xr.DataArray):
        coords = graph.coords
        dims = graph.dims
        graph = graph.data

    # Important to use scipy format sparse data, not pydata/sparse
    if isinstance(graph, COO | GCXS):
        graph = graph.to_scipy_sparse()
    dist = calculate_distance_matrix(graph, n_jobs=n_jobs, limit=limit)

    if not (isinstance(dist, COO | GCXS)):
        dist[np.isinf(dist)] = np.nan

    dist = xr.DataArray(dist, coords=coords, dims=dims, name=name)
    return dist


def closeness_centrality(distances):
    r"""Simple calculation of closeness centrality from a distance matrix

    ..math:: C(x)= \frac{1}{\sum_y d(y,x)}

    """
    return 1 / distances.sum(axis=1)


def index_of_central_node(distances):
    """
    Return the linear index in {0, len(subgraph)} of the
    node with the highest closeness centrality
    """
    centrality = 1 / np.sum(np.nan_to_num(distances, nan=1e9), axis=1)
    return int(centrality.argmax().item())


def label_of_central_node(distances_ds):
    """
    Return the index label of the node with the highest
    closeness centrality, for an Xarray  dataset of labeled
    distance matrix.
    """
    centrality = 1 / np.sum(distances_ds, axis=1)
    return centrality.idxmax().item()


def sparse_centrality(d, penality=1000):
    """
    Calculate betweenness centrality on a sparse graph

    Need to penalise points with few connections by replacing 'no connection' with arbitrary extra distance
    while still avoiding converting to dense array and not adding inf since otherwise a single missing connection
    on a row results in inf everywhere => therefore need to manually set penality for missing connection.
    """
    # NOTE could be a good candidate for numba JIT
    to_add = np.zeros(d.data.shape[0])
    for i in range(d.data.shape[0]):
        to_add[i] = d.data.shape[0] - d.data[i, :].nnz

    centrality = np.sum(d, axis=1)
    centrality.data = centrality.data.todense() + to_add * penality

    centrality = 1 / centrality

    return int(centrality.argmax().item())


# @numba.njit(nogil=True, parallel=True)
# def dict_to_coo_args(numba_dict):
#     """
#     Utility function to quickly convert a numba typed dict to data, (row, col)
#     format required to construct a scipy COO sparse matrix

#     Parameters
#     ----------
#     numba_dict: TypedDict

#     Returns
#     -------
#     tuple
#           (data, (row, col))

#     """
#     n = len(numba_dict)
#     rows = np.zeros(n, dtype=np.int64)
#     cols = np.zeros(n, dtype=np.int64)
#     data = np.zeros(n, dtype=np.float64)
#     for i, (k, v) in enumerate(numba_dict.items()):
#         rows[i] = k[0]
#         cols[i] = k[1]
#         data[i] = v
#     return data, (rows, cols)


# @numba.njit(nogil=True)
def _fill_zero_tril(dd, n):
    for i in range(n):
        for j in range(i):
            key = (i, j)
            dd[key] = 0


# @numba.njit(nogil=True)
def _fill_zero_symmetric(dd, row, col):
    n = len(row)
    for i in range(n):
        key = (row[i], col[i])
        dd[key] = 0
        key = (col[i], row[i])
        dd[key] = 0


@numba.njit(nogil=True)
def walk_predecessors(out_dict, root_node, origin, origin_data, preds):
    """
    Walk through a predecessors array generated by scipy shortest path
    until the root node (given by root index) is reached, accumulating
    the value of origin node data into the provided numba typed dict
    to each edge traversed.
    Args:
        out_dict:
        root_node:
        origin:
        origin_data:
        preds:

    Returns:

    """

    current_node = origin
    while current_node != -9999 and current_node != root_node:
        prev_node = current_node
        current_node = preds[current_node]
        if current_node == -9999:
            break

        key = prev_node, current_node
        if key not in out_dict:
            out_dict[key] = 0

        out_dict[key] += origin_data

    return out_dict


@numba.njit(nogil=True)
def _iter_sum_nogil(acc, root_idx, node_data, preds):
    for j in range(len(preds)):
        # Add the origin data to each upstream node
        origin_data = node_data[j]
        acc = walk_predecessors(acc, root_idx, j, origin_data, preds)
    return acc


@numba.jit(forceobj=True)
def cumulative_sum_over_routes(graph, node_data, root_idx, preds=None):
    if preds is None:
        # Allow to supply pre-calculated preds that might be shared across several calculations
        _, preds = csgraph.shortest_path(graph, directed=False, return_predecessors=True, indices=[root_idx])
        # Since we asked for shortest paths for a single node, preds is 1D row
        # of indexes describing the path
        if np.all(preds == -9999):
            raise RuntimeError("No paths to provided route node")
    preds = preds.squeeze()

    acc = numba.typed.Dict.empty(
        key_type=numba.types.UniTuple(numba.types.int64, 2),
        value_type=numba.types.float64,
    )

    # Moving the iterator into nogil function gives 3x speedup
    acc = _iter_sum_nogil(acc, root_idx, node_data, preds)
    return DOK(graph.shape, data=dict(acc))


@numba.njit(nogil=True)
def _iter_div_nogil(div_data, row, col, acc, root_idx, node_data, preds):
    for j in range(len(preds)):
        origin_data = node_data[j]
        acc = walk_predecessors(acc, root_idx, j, origin_data, preds)

    # NOTE: don't use matrix division b/c the
    # network edge direction might be
    # reversed w.r.t. input graph
    n = len(row)
    for k in range(n):
        i = row[k]
        j = col[k]

        # NOTE: this should always work one way or the other
        # otherwise something is fucked.
        if (i, j) in acc:
            acc[i, j] = acc[i, j] / div_data[k]
        elif (j, i) in acc:
            acc[j, i] = acc[j, i] / div_data[k]
        else:
            print(i, j)
            raise ValueError("Expected i,j index pair not found")

    return acc


@numba.jit(forceobj=True)
def cumulative_div_over_routes(graph, node_data, root_idx, preds=None):
    if isinstance(graph, xr.DataArray):
        g = xarray_sparse_to_scipy_sparse(graph)
    else:
        g = pydata_sparse_to_scipy_sparse(graph)

    if preds is None:
        # Allow to supply pre-calculated preds that might be shared across several calculations
        _, preds = csgraph.shortest_path(g, directed=False, return_predecessors=True, indices=[root_idx])
        # Since we asked for shortest paths for a single node, preds is 1D row
        # of indexes describing the path

        if np.all(preds == -9999):
            raise RuntimeError("No paths to provided route node")
    preds = preds.squeeze()

    acc = numba.typed.Dict.empty(
        key_type=numba.types.UniTuple(numba.types.int64, 2),
        value_type=numba.types.float64,
    )

    g = g.tocoo()
    # Moving the iterator into nopython/nogil function gives 3x speedup
    acc = _iter_div_nogil(g.data, g.row, g.col, acc, root_idx, node_data, preds)
    # Still need to convert to python dict
    return DOK(graph.shape, data=dict(acc))


def id_of_central_node(d):
    # Need to penalise points with few connections by replacing 'no connection' with arbitrary extra distance
    # while still avoiding converting to dense array.
    to_add = np.zeros(d.data.shape[0])
    for i in range(d.data.shape[0]):
        to_add[i] = d.data.shape[0] - d.data[0, :].nnz

    centrality = np.sum(d, axis=1)
    centrality.data = centrality.data.todense() + to_add * 2_000

    centrality = 1 / centrality

    return int(centrality.argmax().item())


def graph_flow_density_per_segment(routes, value, root_idx=None, distance=None, preds=None):
    if root_idx is None:
        if distance is None:
            raise ValueError("must provide distance matrix if root_idx is None")
        root_idx = id_of_central_node(distance)

    r = xarray_sparse_to_scipy_sparse(routes)

    h = np.asarray(value)
    h = np.nan_to_num(h)

    density = cumulative_div_over_routes(r, h, root_idx, preds=preds)
    # Generally we want a COO matrix afer constructing a DOK matrix
    if isinstance(routes, xr.DataArray):
        return xr.DataArray(
            density.to_coo(), coords=routes.coords, dims=routes.dims, name=getattr(value, "name", None)
        )
    else:
        return density.to_coo()


def graph_flow_total_per_segment(routes, value, root_idx=None, distance=None, preds=None):
    if root_idx is None:
        if distance is None:
            raise ValueError("must provide distance matrix if root_idx is None")
        root_idx = id_of_central_node(distance)

    v_filled = np.asarray(value, dtype=np.float64)
    v_filled = np.nan_to_num(v_filled)

    r_sparse = xarray_sparse_to_scipy_sparse(routes)

    total = cumulative_sum_over_routes(r_sparse, v_filled, root_idx, preds=preds)

    # Generally we want a COO matrix after constructing a DOK matrix
    if isinstance(routes, xr.DataArray):
        return xr.DataArray(
            total.to_coo(), coords=routes.coords, dims=routes.dims, name=getattr(value, "name", None)
        )
    else:
        return total.to_coo()


@overload
def fast_minimum_spanning_tree(
    distance: np.ndarray,
    points: np.ndarray | None = None,
    max_distance: float | None = None,
    avoid_zero_distance: bool = True,
) -> scipy.sparse.csr_array:
    ...


@overload
def fast_minimum_spanning_tree(
    distance: SparseArray,
    points: np.ndarray | None = None,
    max_distance: float | None = None,
    avoid_zero_distance: bool = True,
) -> GCXS:
    ...


@overload
def fast_minimum_spanning_tree(
    distance: scipy.sparse.sparray,
    points: np.ndarray | None = None,
    max_distance: float | None = None,
    avoid_zero_distance: bool = True,
) -> scipy.sparse.csr_array:
    ...


@overload
def fast_minimum_spanning_tree(
    distance: xr.DataArray,
    points: ArrayLike | None = None,
    max_distance: float | None = None,
    avoid_zero_distance: bool = True,
) -> xr.DataArray:
    ...


def fast_minimum_spanning_tree(
    distance: np.ndarray | SparseArray | scipy.sparse.sparray | xr.DataArray,
    points: np.ndarray | None = None,
    max_distance: float | None = None,
    avoid_zero_distance: bool = True,
) -> scipy.sparse.csr_array | xr.DataArray:
    """

    Args:
    distance: 2D (N points, N points)
    points:  2D (N points, 2)
    max_distance
    avoid_zero_distance

    Returns:
    ndarray or DataArray
        Upper triangular matrix representing minimum spanning tree
        If distance is ndarray, returns scipy sparse matrix
        If distance is DataArray, returns DataArray with same dims and coords.

        NOTE: because there is not yet full compatibility between Scipy sparse,
        Xarray, and PyData Sparse libraries, if DataArray is returned the data must be
        converted to PyData Sparse format (GCXS). This should not be needed with future
        versions of scipy and sparse
    """
    if isinstance(distance, xr.DataArray):
        dist = distance.data
    else:
        dist = distance
    if isinstance(dist, SparseArray | scipy.sparse.sparray):
        if avoid_zero_distance:
            dist += 1e-9

    if isinstance(dist, COO):
        # we still need scipy sparse types with zero fill value
        dist = COO(dist.coords, dist.data, dist.shape, fill_value=0).to_scipy_sparse()
    elif isinstance(dist, GCXS):
        # we still need scipy sparse types with zero fill value
        arg = dist.data, dist.indices, dist.indptr
        dist = GCXS(arg, dist.shape, compressed_axes=dist.compressed_axes, fill_value=0).to_scipy_sparse()

    if points is not None:
        routes = tesselate_points(points, dist, max_distance=max_distance)
    else:
        # NOTE: you can directly use the distance matrix without re-tesselating.
        routes = dist

    routes = csgraph.minimum_spanning_tree(routes)

    if isinstance(distance, xr.DataArray):
        # Since csgraph is a CSR, use GCXS -> note that for now most things need COO
        routes = xr.DataArray(GCXS.from_scipy_sparse(routes), coords=distance.coords, dims=distance.dims)
    elif isinstance(distance, SparseArray):
        routes = GCXS.from_scipy_sparse(routes)

    return routes


def tesselate_point_pairs(points) -> pd.DataFrame:
    """
    Perform Delaunay triangulation on the given points dataframe
    and return the point index pairs.

    Args:
    points

    Returns:
    DataFrame:
        Edge lists with columns source_id, target_id using the input
        dataframe index as identifiers

    """
    #     if isinstance(points, pd.DataFrame)
    delaunay = scipy.spatial.Delaunay(points.to_numpy())
    simplices = delaunay.simplices

    # extract the index positions from the sample index
    edges_i = np.concatenate((simplices[:, 0], simplices[:, 1], simplices[:, 2]))
    edges_j = np.concatenate((simplices[:, 1], simplices[:, 2], simplices[:, 0]))

    # Remove duplicates by stacking indexes, sorting, and dropping duplicates
    # NOTE unfortunately, this precludes applying numba nogil because of lack
    # of support for axis arg in sort.
    a = np.vstack((edges_i, edges_j))
    a = np.sort(a, axis=0)
    a = np.unique(a, axis=1)
    edges_i, edges_j = a[0, :], a[1, :]

    pairs = pd.DataFrame({"id_from": points.index[edges_i], "id_to": points.index[edges_j]})
    return pairs
