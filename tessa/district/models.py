import uuid
from collections.abc import Callable, Iterable
from typing import Annotated, Any, Optional, Type, Union

import geopandas as gpd
import pandas as pd
import shapely.geometry
import structlog
from pandera.typing import DataFrame, DateTime, Index
from pandera.typing.geopandas import GeoDataFrame
from pydantic import (
    UUID4,
    BaseModel,
    ConfigDict,
    Field,
    ValidationInfo,
    field_serializer,
    field_validator,
    model_serializer,
)
from pydantic.functional_validators import AfterValidator, BeforeValidator, model_validator

from tessa.building_data import Building, BuildingDataSource
from tessa.climate_data import ClimateData, ClimateDataSource
from tessa.core.models import PowerLoad, UtilityNetwork
from tessa.heat_demand_models.common import CoolingDemandModel, HeatDemandModel
from tessa.spatial_clustering import generate_boundary
from tessa.thermal_system_models.models import DistrictNetwork
from tessa.utils.pydantic_shapely import Polygon

log = structlog.get_logger()


GeneralUtilityNetwork = Annotated[Union[UtilityNetwork, DistrictNetwork], Field(discriminator="kind")]


def maybe_geojson_to_geopandas(df_or_geojson, index_name=None, geom_name=None, crs=None):
    if isinstance(df_or_geojson, dict):
        df = gpd.GeoDataFrame.from_features(df_or_geojson)
        if "geometry" in df.columns:
            # Fix wierd things where geom columns turn to strings??
            df["geometry"] = gpd.GeoSeries.from_wkt(df["geometry"])
            df = df.set_geometry("geometry")
    else:
        assert isinstance(
            df_or_geojson, gpd.GeoDataFrame
        ), f"got {type(df_or_geojson)} instead of GeoDataFrame"
        df = df_or_geojson

    if index_name is not None and index_name in df.columns:
        df = df.set_index(index_name)
    if geom_name is not None and df.geometry.name != geom_name:
        df = df.rename_geometry(geom_name)
    if crs is not None:
        df = df.set_crs(crs)
    return df


def _check_buildings(buildings, *, crs=4326):
    buildings = maybe_geojson_to_geopandas(buildings, index_name="egid", geom_name="point", crs=crs)
    return GeoDataFrame[Building](buildings)


def _check_climate(climate):
    # Fix for Climate Table being reset, shouldn't be needed!
    # TODO: revert this once root cause found.
    if climate is not None and not isinstance(climate, ClimateData):
        return ClimateData.model_validate(climate)
    else:
        return climate


class District(BaseModel):
    uid: uuid.UUID = Field(default_factory=uuid.uuid4)
    name: str = "district"
    buildings: Annotated[GeoDataFrame[Building], Field(repr=False), BeforeValidator(_check_buildings)]
    calculation_year: int = 2015
    climate: Annotated[ClimateData, Field(repr=False), AfterValidator(_check_climate)] | None = None
    load_curve: DataFrame[PowerLoad] | None = Field(default=None, repr=False)
    # Generic holder for all networks like DN, gas grid, etc.
    # Leaving it very lightly defined because we don't know what we will need.
    # Take inspiration from CityGML UtilityNetwork definitions
    utility_networks: list[GeneralUtilityNetwork] = []

    # IMPORTANT declaration order
    # ensure crs is validated after buildings is set
    # ensure boundary is validated after buildings is set.
    crs: int = 4326
    local_crs: int = 2056
    # boundary: Polygon | None = None
    boundary: Polygon

    model_config = ConfigDict(from_attributes=True, arbitrary_types_allowed=True, validate_assignment=True)

    @field_validator("load_curve", mode="before")
    def validate_load_curve(cls, v):
        if v is not None:
            if v.index.name != "timestamp":
                v.index.name = "timestamp"
        return v

    @model_validator(mode="after")
    def _validate_district(self):
        if self.buildings.crs is None:
            self.buildings = self.buildings.set_crs(self.crs)
        return self

    # @field_serializer("load_curve")
    # def _serialize_load_curve(self, load_curve):
    #     return load_curve.to_dict()

    # @field_serializer("buildings")
    # def _serialize_buildings(self, buildings):
    #     return buildings.reset_index().__geo_interface__

    @field_validator("crs")
    def _ensure_crs(cls, crs: int | str, info: ValidationInfo):
        if not crs and "buildings" in info.data:
            return info.data["buildings"].crs.to_epsg()
        return 4326

    @field_validator("boundary")
    def _ensure_boundary(cls, boundary: Polygon, info: ValidationInfo):
        if not boundary:
            if "buildings" in info.data and "crs" in info.data:
                return Polygon(generate_boundary(info.data["buildings"], crs=info.data["crs"]))
            else:
                raise ValueError("Could not generate boundary")
        else:
            if isinstance(boundary, dict):
                geom = boundary.get("geometry", boundary)
                return Polygon(shapely.geometry.shape(geom))
            else:
                return boundary

    @property
    def building_ids(self):
        return self.buildings.index

    @property
    def n_buildings(self):
        return len(self.buildings)

    @property
    def district_networks(self) -> list[DistrictNetwork]:
        return list(u for u in self.utility_networks if isinstance(u, DistrictNetwork))

    def apply(self, operator: Callable[["District"], "District"]) -> "District":
        return operator(self)

    def buildings_table(self, ids=None) -> GeoDataFrame[Building]:
        bt = self.buildings
        if ids is not None:
            bt = self.buildings.reindex(ids)
            bt.index.name = "egid"
            bt = GeoDataFrame[Building](bt)
        return bt

    def get_utility_network(self, name, default=None):
        for item in self.utility_networks:
            if item.name == name or str(item.uid) == str(name):
                return item
        return default

    def query_utility_networks(self, kind="districtnetwork"):
        for item in self.utility_networks:
            if item.kind == kind:
                yield item

    @classmethod
    def model_validate(
        cls,
        obj: Any,
        *,
        strict: bool | None = None,
        from_attributes: bool | None = None,
        context: dict[str, Any] | None = None,
    ) -> "District":
        # if dataclasses.is_dataclass(obj):
        #     return cls(**dataclasses.asdict(obj))
        # else:
        #     return super().model_validate(obj, strict, from_attributes)
        utility_networks = obj.utility_networks.copy()
        out = super().model_validate(obj, strict=strict, from_attributes=from_attributes, context=context)
        utility_networks_out = []
        if utility_networks:
            for utility_network in utility_networks:
                if utility_network.kind == "utilitynetwork":
                    utility_networks_out.append(UtilityNetwork.model_validate(utility_network))
                elif utility_network.kind == "districtnetwork":
                    parsed = DistrictNetwork.model_validate(utility_network)
                    utility_networks_out.append(parsed)
            out.utility_networks = utility_networks_out
        return out
