import uuid
from collections.abc import Iterable
from dataclasses import dataclass, field

import geopandas as gpd
import numpy as np
import pandas as pd
from pandera.typing import DataFrame
from pandera.typing.geopandas import GeoDataFrame
from pydantic import BaseModel, ConfigDict

from tessa.building_data import Building, BuildingDataSource
from tessa.building_data.common import (
    extract_egid_list_from_table,
    fill_table,
    setup_building_index,
    try_add_geometry,
)
from tessa.climate_data import ClimateData, ClimateDataSource
from tessa.climate_data.common import calculate_climate_correction, calculate_heating_degree_days
from tessa.core.models import PowerLoad
from tessa.heat_demand_models.ch_tessa.model_parameters import (
    _load_avg_qhww_sia_age,
    _QhwwAgeSia,
    load_ge_norm_climate,
)
from tessa.heat_demand_models.common import CoolingDemandModel, HeatDemandModel
from tessa.spatial_clustering import generate_boundary
from tessa.utils.pydantic_shapely import Polygon

from .models import District, DistrictTimeSeries


def _apply_load_curves(buildings, climate, heat_model) -> tuple[GeoDataFrame[Building], DataFrame[PowerLoad]]:
    yearly_power, load_curve = heat_model.calculate_load_curves(buildings, climate.timeseries.copy())
    for col in yearly_power.columns:
        if col not in buildings:
            buildings[col] = pd.Series(index=buildings.index, dtype=float)
    buildings.update(yearly_power)
    return buildings, load_curve


class DistrictBaseData(BaseModel):
    building_data_source: BuildingDataSource
    crs: int = 4326
    building_index_name: str = "egid"

    def from_dicts(
        self,
        buildings: list[dict],
        *,
        index_col="egid",
        boundary=None,
        load_curve=None,
        auto_fill: bool = True,
        **kwargs,
    ) -> District:
        """
        From a list of dictionaries of buildings properies
        Args:
            buildings:
            load_curve:
            boundary:
            auto_fill:

        Returns:

        """
        cols: set[str] = set()
        for b in buildings:
            cols.update(b.keys())
        # Don't create the BuildingsTable just yet
        buildings = gpd.GeoDataFrame(buildings, columns=list(cols)).set_index(index_col)
        return self.from_buildings(
            buildings, load_curve=load_curve, auto_fill=auto_fill, boundary=boundary, **kwargs
        )

    def from_buildings(
        self,
        buildings,
        *,
        name="district",
        load_curve=None,
        boundary=None,
        auto_fill: bool = True,
        crs=None,
        **kwargs,
    ) -> District:
        # enforce lower case column names
        buildings.columns = [c.lower() for c in buildings.columns]
        buildings = setup_building_index(buildings)

        # Check if we are passed an empty building set.
        if len(buildings) > 0 and auto_fill and self.building_data_source is not None:
            if not isinstance(buildings, GeoDataFrame):
                buildings = try_add_geometry(buildings)
            else:
                buildings = buildings.rename_geometry("point")

            egid_list = extract_egid_list_from_table(buildings, index_col="egid")
            building_db_data = self.building_data_source.load(egid_list)
            buildings = fill_table(buildings, building_db_data)
        elif len(buildings) > 0 and auto_fill:
            raise RuntimeError("auto_fill set to True but no building data and heat model provided")

        if load_curve is not None:
            buildings["p_h_max1h_kw", "p_ww_max1h_kw", "p_hww_max1h_kw"] = load_curve.max()
            buildings["p_h_max1d_kw", "p_ww_max1d_kw", "p_hww_max1d_kw"] = (
                load_curve.resample("1D").mean().max()
            )
            load_curve = DataFrame[PowerLoad](load_curve)

        # Make sure the index name is the right one.
        buildings.index.name = self.building_index_name
        # Validate BuildingTable
        buildings = GeoDataFrame[Building](buildings).set_geometry("point")
        if buildings.crs is None:
            if crs is None:
                crs = self.crs
            buildings = buildings.to_crs(crs)

        if boundary is None:
            boundary = Polygon(generate_boundary(buildings))
        return District(buildings=buildings, name=name, load_curve=load_curve, boundary=boundary, **kwargs)

    def from_ids(
        self,
        ids: Iterable[int],
        **kwargs,
    ) -> District:
        buildings = self.building_data_source.load(ids)
        if len(buildings) == 0:
            raise RuntimeError("No buildings found for given ids")
        return self.from_buildings(
            buildings,
            **kwargs,
        )

    def from_region(
        self, shape, *, load_curve=None, auto_fill: bool = True, crs=None, **kwargs
    ) -> "District":
        if hasattr(shape, "__geo_interface__"):
            shape = shape.__geo_interface__
        if "geometry" in shape:
            shape = shape["geometry"]
        building_points = self.building_data_source.load_points_in_region(shape)
        return self.from_buildings(
            building_points, boundary=shape, crs=crs, load_curve=load_curve, auto_fill=auto_fill, **kwargs
        )


class SetClimate(BaseModel):
    """
    Operator to add climate data to a district from a climate data source.
    Delegates the selection of the climate data based on the district data to
    the climate data source provided to the constructor.
    """

    model_config = ConfigDict(arbitrary_types_allowed=True)

    climate_data_source: ClimateDataSource | None = None
    climate_year: int = 2015

    def __call__(
        self,
        district: District,
        *,
        climate: ClimateData | None = None,
        climate_year: int | None = None,
    ) -> District:
        buildings = district.buildings
        if climate is None and len(buildings) > 0 and self.climate_data_source is not None:
            if climate_year is None:
                climate_year = self.climate_year
            climate = self.climate_data_source.load(buildings, year=climate_year)

        if climate is not None and climate_year is None:
            climate_year = climate.timeseries.index[0].year  # type:ignore

        return district.model_copy(update=dict(climate=climate, calculation_year=climate_year))


class ApplyHeatModel(BaseModel):
    """
    Apply a heatgin demand model, keeping provided input data where it exists
    and overwriting with model data where it is missing.
    """

    heat_model: HeatDemandModel

    def __call__(self, district: District) -> District:
        # enforce lower case column names
        buildings = district.buildings
        buildings.columns = [c.lower() for c in buildings.columns]
        buildings = setup_building_index(buildings)

        climate = district.climate

        if climate is not None:
            buildings_heat_data = self.heat_model.calculate_yearly(
                buildings.copy(), temperatures=climate.timeseries
            )
            buildings = buildings.combine_first(buildings_heat_data)
            buildings = GeoDataFrame[Building](buildings).set_geometry("point")
            buildings.crs = district.crs

            # Provided load curve overrides gen load curve option
            buildings, load_curve = _apply_load_curves(buildings, climate, self.heat_model)

            return district.model_copy(
                update=dict(
                    buildings=buildings,
                    load_curve=load_curve,
                )
            )
        else:
            raise RuntimeError("District must have climate data set to calculate heat demands")

    def apply_load_curves(self, district: District) -> District:
        if district.buildings is None or len(district.buildings) == 0:
            return district
        buildings, load_curve = _apply_load_curves(district.buildings, district.climate, self.heat_model)
        return district.model_copy(update=dict(buildings=buildings, load_curve=load_curve))


class ApplyCoolingModel(BaseModel):
    """
    Apply a cooling demand model, keeping provided input data where it exists
    and overwriting with model data where it is missing.
    """

    cooling_model: CoolingDemandModel

    def __call__(self, district: District) -> District:
        buildings = district.buildings

        # keep the structure of the heat load curve
        load_curve = district.load_curve
        if load_curve is not None:
            buildings_cooling_data, load_curve = self.cooling_model.calculate_cooling(
                buildings.copy(),
                load_curve.copy(),
                sim_year=district.calculation_year,
            )
            buildings = buildings.combine_first(buildings_cooling_data)

        # Assigning buildings in the class constructor will wrap it in the BuildingTable
        buildings = GeoDataFrame[Building](buildings).set_geometry("point")
        buildings.crs = district.crs
        if load_curve is not None:
            load_curve = DataFrame[PowerLoad](load_curve)

        return district.model_copy(
            update=dict(
                buildings=buildings,
                load_curve=load_curve,
            )
        )


class DistrictQuery(BaseModel):
    heat_model: HeatDemandModel | None = None
    cooling_model: CoolingDemandModel | None = None

    def __call__(self, district: District, query=None) -> District:
        return self.query(district, query=query)

    def query(self, district: District, query):
        """
        Apply a pandas query string. Note that without the heat model,
        the load curves will be invalidated.
        Args:
            district
            query:
            heat_model:

        Returns:

        """
        buildings_filtered = district.buildings.query(query).copy()
        # filter buildings, updsate with new uid and blank load curve aggregate.
        district = district.model_copy(
            update=dict(uid=uuid.uuid4(), buildings=buildings_filtered, load_curve=None)
        )
        if self.heat_model is not None:
            district = ApplyHeatModel(heat_model=self.heat_model)(district)
        if self.cooling_model is not None:
            district = ApplyCoolingModel(cooling_model=self.cooling_model)(district)

        return district


class DistrictBuilder(BaseModel):
    """
    Shim class to transition from old Distrct classmethods to fluent interface
    """

    model_config = ConfigDict(from_attributes=True, validate_assignment=True, arbitrary_types_allowed=True)

    building_data_source: BuildingDataSource | None = None
    climate_data_source: ClimateDataSource | None = None
    heat_model: HeatDemandModel | None = None
    cooling_model: CoolingDemandModel | None = None

    def from_dicts(
        self,
        buildings: list[dict],
        *,
        index_col="egid",
        boundary=None,
        climate_year=2015,
        **kwargs,
    ):
        """
        From a list of dictionaries of buildings properies
        Args:
            buildings:
            boundary:
            climate_year:

        Returns:

        """
        cols: set[str] = set()
        for b in buildings:
            cols.update(b.keys())
        # Don't create the BuildingsTable just yet
        buildings = gpd.GeoDataFrame(buildings, columns=list(cols)).set_index(index_col)
        return self.from_buildings(buildings, boundary=boundary, climate_year=climate_year, **kwargs)

    def from_buildings(
        self,
        buildings,
        *,
        boundary=None,
        climate_year=2015,
        load_curve=None,
        auto_fill: bool = True,
        crs=None,
        **kwargs,
    ) -> District:
        district = (
            DistrictBaseData(building_data_source=self.building_data_source)
            .from_buildings(buildings, load_curve=load_curve, auto_fill=auto_fill, crs=crs, **kwargs)
            .apply(SetClimate(climate_data_source=self.climate_data_source, climate_year=climate_year))
            .apply(ApplyHeatModel(heat_model=self.heat_model))
            .apply(ApplyCoolingModel(cooling_model=self.cooling_model))
        )
        return district

    def from_ids(
        self,
        ids: Iterable[int],
        *,
        boundary=None,
        climate_year=2015,
        load_curve=None,
        auto_fill: bool = True,
        crs=None,
        **kwargs,
    ) -> District:
        if self.building_data_source is None:
            raise ValueError("Must initialise building_data_source to generate district from building ids")
        buildings = self.building_data_source.load(ids)
        if len(buildings) == 0:
            raise RuntimeError("No buildings found for given ids")
        return self.from_buildings(
            buildings,
            boundary=boundary,
            climate_year=climate_year,
            load_curve=load_curve,
            auto_fill=auto_fill,
            crs=crs,
            **kwargs,
        )

    def from_region(
        self, shape, *, climate_year=2015, load_curve=None, auto_fill: bool = True, crs=None, **kwargs
    ) -> District:
        if self.building_data_source is None:
            raise ValueError(
                "Must initialise building_data_source to generate district from polygon selection"
            )
        if "geometry" in shape:
            shape = shape["geometry"]
        building_points = self.building_data_source.load_points_in_region(shape)

        return self.from_buildings(
            building_points,
            boundary=shape,
            climate_year=climate_year,
            load_curve=load_curve,
            auto_fill=auto_fill,
            crs=crs,
            **kwargs,
        )


# def extend(district, new_buildings, building_data_source=None, heat_model=None, auto_fill=True,
#            overwrite=True):
#     if auto_fill and building_data_source is not None:
#         # Try to load any missing data for the added buildings, if we find their IDs in the DB
#         egid_list = extract_egid_list_from_table(new_buildings)
#         building_extra_data = building_data_source.load(egid_list)
#         # add points based on provided positions before loading extra data
#         # in case we wanted to have custom positions
#         district._ensure_geo_columns(new_buildings, district.crs)
#         district._ensure_geo_columns(building_extra_data, district.crs)
#
#         new_buildings.update(building_extra_data)
#         if not {'x', 'y', 'lat', 'lon'}.issubset(new_buildings.columns):
#             raise ValueError('Must provide either x,y or lat, lon coordinates')
#
#     new_index = np.union1d(new_buildings.index, district.buildings.index)
#     buildings = district.buildings.reindex(new_index)
#     buildings.update(new_buildings, overwrite=overwrite)
#
#     if auto_fill and heat_model is not None:
#         # The newly provided data may provide new inputs for heat demand
#         # calc and/or new heat demand values e.g. measurements.
#         # We previously merged the extra data with existing buildings, which might provide
#         # calculation inputs that were previously missing on the already present buildings
#         yearly_heat_demand = heat_model.calculate_yearly(
#             buildings,
#             building_data_source.efficiency,
#             building_data_source.carbon_intensity,
#             district.degree_days
#         )
#         # If we want to overwrite with new buildings, need to use overwrite FALSE
#         # when adding calculated demands
#         buildings.update(yearly_heat_demand, overwrite=False)
#
#     return District(    climate=district.climate,
#     buildings=buildings,
#     load_curve=None
#     )


def initialise_district_timeseries(
    district: District,
    *,
    start_year=None,
) -> DistrictTimeSeries:
    if start_year is None:
        start_year = district.calculation_year

    districts = [district]
    timestamps = [start_year]

    return DistrictTimeSeries(districts=districts, timestamps=timestamps)


def clean_building_demand_data(buildings, index: pd.DatetimeIndex = None):
    # remove building demand data that need to be re-calculated
    columns_to_remove = [
        "q_h_kwh",
        "q_hww_kwh_m2",
        "q_hww_kwh",
        "q_h_final_kwh",
        "q_hww_final_kwh",
        "co2eq_h_kg",
        "co2eq_hww_kg",
        "p_h_max1h_kw",
        "p_ww_max1h_kw",
        "p_hww_max1h_kw",
        "p_h_max1d_kw",
        "p_ww_max1d_kw",
        "p_hww_max1d_kw",
        "q_c_kwh_m2",
        "q_c_kwh",
        "q_c_final_kwh",
        "co2eq_c_kg",
        "p_c_max1h_kw",
        "p_c_max1d_kw",
    ]

    if index is None:
        index = buildings.index

    for column in columns_to_remove:
        if column in buildings.columns:
            buildings.loc[index, column] = None

    return buildings


@dataclass
class ApplySIARetrofit:
    """
    Apply retrofit model. Assign q_h_kwh_m2 to retrofitted buildings, remove pre-retorfit data and load curve.
    We assume the retrofitted buildings reach the average specific heating demand for latest age group per SIA.
    """

    reference_climate: pd.DataFrame = field(default_factory=load_ge_norm_climate)
    reference_year: int = 2015
    qhww_by_sia_age: DataFrame[_QhwwAgeSia] = field(default_factory=_load_avg_qhww_sia_age)

    def __post_init__(self):
        self.reference_heating_degree_days = calculate_heating_degree_days(self.reference_climate["t_air"])

    def __call__(
        self,
        district: District,
        *,
        retr_to_age: str = "2011-2020",
    ) -> District:
        buildings = district.buildings.copy()
        climate = district.climate

        select = buildings.is_retrofitted

        retr_qhww_sia_by_cat = self.qhww_by_sia_age[self.qhww_by_sia_age.const_period == retr_to_age]

        # determine q_h_kwh_m2 according to SIA.
        heat_demand_avg = (
            buildings.loc[select]
            .reset_index()
            .merge(retr_qhww_sia_by_cat, on="cat_sia", how="left")
            .set_index("egid")
        )

        # apply climate correction
        degree_days = calculate_heating_degree_days(climate.timeseries["temp2m"])
        clim_correction = calculate_climate_correction(degree_days, self.reference_heating_degree_days)

        # assign q_h_kwh_m2 to these buildings
        buildings.loc[select, "q_h_kwh_m2"] = heat_demand_avg["avg_q_h_kwh_m2"] * clim_correction
        buildings.loc[select, "qh_origin"] = "AVG Value for Latest Age per SIA"

        # assume building become cooled if retrofitted
        buildings.loc[select, "is_cooled"] = True

        # Remove pre-retrofit data, which will be filled later by ApplyHeatModel and ApplyCoolingModel.
        buildings = clean_building_demand_data(buildings, select)

        # TODO do we apply heat demand and load curve directly here, or have it as a
        #  separate operator class.

        return district.model_copy(
            update=dict(
                buildings=buildings,
                load_curve=None,
            )
        )


class SetFutureClimate(BaseModel):
    """
    Add future climate and apply climate adjustment to building energy demands based on climate change.
    """

    climate_data_source: ClimateDataSource | None = None

    def __call__(
        self,
        district: District,
        *,
        climate: ClimateData | None = None,
    ) -> District:
        # Add future climate
        buildings = district.buildings.copy()
        climate_original = district.climate
        climate_year = district.calculation_year

        if climate is None and len(buildings) > 0 and self.climate_data_source is not None:
            climate = self.climate_data_source.load(buildings, year=climate_year)

        # apply climate adjustment
        degree_days = calculate_heating_degree_days(climate.timeseries["temp2m"])
        degree_days_original = calculate_heating_degree_days(climate_original.timeseries["temp2m"])
        clim_correction = calculate_climate_correction(degree_days, degree_days_original)
        buildings["q_h_kwh_m2"] = buildings["q_h_kwh_m2"] * clim_correction

        # Remove demand data for current climate, which will be filled later by ApplyHeatModel and ApplyCoolingModel.
        buildings = clean_building_demand_data(buildings)

        return district.model_copy(
            update=dict(
                buildings=buildings,
                load_curve=None,
                climate=climate,
            )
        )


class FutureDistrictBuilder(BaseModel):
    """ """
