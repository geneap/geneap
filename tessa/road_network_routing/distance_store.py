import urllib.parse
from collections.abc import Iterable
from pathlib import Path

import numpy as np
import pandas as pd
import scipy.sparse
import scipy.spatial
import shapely
import structlog
import tiledb
import xarray as xr
from geopandas import GeoDataFrame, GeoSeries
from joblib import Parallel, delayed
from pydantic import BaseModel, ConfigDict, PrivateAttr
from sqlalchemy.engine import Engine

from tessa.sparse_graph import (
    adjacency_matrix_from_edges,
    series_to_sparse_with_coords,
    tesselate_point_pairs,
)
from tessa.utils import collection_utils
from tessa.utils.pydantic_shapely import LineString

from .routing import road_map_for_network, routing_distances_for_points, swiss_routing_vertex_for_points


log = structlog.get_logger()


def _make_distance_table(dist, building_points, building_points_from=None):
    building_points_from = building_points_from if building_points_from is not None else building_points
    # Create and clean distance table to keep non inf/nan/zero distances
    distances = xr.DataArray(
        dist,
        coords=(building_points_from.index, building_points.index),
        dims=("id_from", "id_to"),
        name="distance",
    )
    distances = distances.where(np.isfinite(distances)).to_dataframe()
    distances = distances[distances > 0].dropna()
    # NOTE: since we make distances from diskra its already symmetrical
    return distances


class BuildingRouting(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)

    database: Engine
    index_array: str = "idx_map.tiledb"
    data_array: str = "distances.tiledb"
    index_name: str = "egid"
    max_distance: float = 1000
    enable_cache: bool = True
    # CRS in which the building routing is performed
    crs: int | str = 2056

    overread_step_size: int = 5
    parallel_workers: int = 5

    tiledb_opts: dict = tiledb.Config().dict()
    _tiledb_ctx: tiledb.Ctx = PrivateAttr()

    _distances_array_file: str = PrivateAttr()
    _index_array_file: str = PrivateAttr()
    _data_directory: str = PrivateAttr()

    def __init__(
        self,
        data_directory,
        database,
        overread_step_size=5,
        index_name: str = "egid",
        index_array: str = "idx_map.tiledb",
        data_array: str = "distances.tiledb",
        tiledb_opts=None,
        enable_cache=True,
        crs: int | str = 2056,
    ):
        super().__init__(
            database=database,
            index_array=index_array,
            data_array=data_array,
            overread_step_size=overread_step_size,
            index_name=index_name,
            enable_cache=enable_cache,
            crs=crs,
        )

        # could be a folder or a s3 path
        if urllib.parse.urlparse(str(data_directory)).scheme == "s3":
            data_directory = str(data_directory)
            if not data_directory.endswith("/"):
                data_directory += "/"
            self._distances_array_file = data_directory + self.data_array
            self._index_array_file = data_directory + self.index_array
        else:
            data_directory = Path(data_directory)
            self._distances_array_file = str(data_directory / self.data_array)
            self._index_array_file = str(data_directory / self.index_array)

        self._data_directory = data_directory

        if tiledb_opts is None:
            self.tiledb_opts = tiledb.Config().dict()
        else:
            self.tiledb_opts = tiledb_opts

        self._tiledb_ctx = tiledb.Ctx(tiledb.Config(self.tiledb_opts))

        # self.index = tiledb.SparseArray(self._index_array_file, 'r', ctx=self.tiledb_ctx)


    def save(self, dist):
        # get the COO-format EGID coordinate pairs
        i_s = dist.id_from.data[dist.data.coords[0]].astype(np.int32)
        j_s = dist.id_to.data[dist.data.coords[1]].astype(np.int32)

        # Convert the labels (egids) to array positions (linear index)
        # using the previously saved lookup table
        with self._open_index() as index:
            coords_i = index.multi_index[list(i_s)]["index"]
            coords_j = index.multi_index[list(j_s)]["index"]

        with self._open_distances("w") as distances:
            distances[coords_i, coords_j] = dist.data.data

    def save_table(self, distances):
        # Because index lookup will drop ids that are not found and only return unique values
        # and the distance idfrom/idto are not unique (only pair is unique)
        # need to dick about mapping the building IDs to the matrix coordinate indexes
        d_coords = distances.dropna().reset_index().drop_duplicates()

        # Make sure we don't try to save dummy IDs (negative Egids)
        d_coords = d_coords[(d_coords.id_from > 0) & (d_coords.id_to > 0)]
        with self._open_index() as index:
            unique_ids = np.union1d(d_coords.id_from.values, d_coords.id_to.values)
            idx_map = index.multi_index[unique_ids]

        # For turning the EGID -> array indexer into a lookup, use searchsorted
        # following https://stackoverflow.com/questions/3403973/fast-replacement-of-values-in-a-numpy-array
        from_values = idx_map[self.index_name]
        to_values = idx_map["index"]
        sort_idx = np.argsort(from_values)

        idx = np.searchsorted(from_values, d_coords.id_from.values, sorter=sort_idx)
        idx_from = to_values[sort_idx][idx]

        idx = np.searchsorted(from_values, d_coords.id_to.values, sorter=sort_idx)
        idx_to = to_values[sort_idx][idx]

        with self._open_distances("w") as distances_cache:
            # Re-open with write.
            # TODO: especially on s3 this could be fragile without file locking. Might
            #  eventually prefer local file system or custom server
            distances_cache[idx_from, idx_to] = d_coords.distance.values

    def routing_vertex_for_points(self, source_points: GeoSeries) -> GeoDataFrame:
        """
        NOTE: this just gets the closest vertex, but doesn't guarantee that it will be especially sane!

            Args:
                source_points:

            Returns:
                GeoDataFrame with columns point_id, routing_vertex_id,  point

        """
        return swiss_routing_vertex_for_points(self.database.engine, source_points)

    def find_points_on_line(
        self, points: GeoSeries | GeoDataFrame, line: LineString
    ) -> dict[tuple, LineString]:
        """
        Finds the buildings nearest to the start and end of the given line.
        Args:
            points:
            line:

        Returns:

        """
        first_point = shapely.geometry.Point(line.coords[0])
        last_point = shapely.geometry.Point(line.coords[-1])
        # Find the closest buildings to the points of the shortcut
        connected_point_ids = []
        # iterate over first and last point in the line
        for point in [first_point, last_point]:
            in_point_idx, search_geom_idx = points.geometry.sindex.nearest(point)
            closest_point_id = points.index[search_geom_idx].item()
            connected_point_ids.append(closest_point_id)
        connected_point_ids = tuple(connected_point_ids)

        shortcuts = {connected_point_ids: line}
        return shortcuts

    def find_points_on_ways(self, points, ways: Iterable[LineString]) -> dict[tuple, LineString]:
        """
        Create a mapping from building id pairs to lines

        Args:
            points:
            ways:

        Returns:

        """
        out = {}
        for line in ways:
            out |= self.find_points_on_line(points, line)
        return out

    def load_distance_matrix(self, points: GeoSeries | GeoDataFrame, *, ways=None):
        """
        Load the distance matrix for the given points, optionally providing additional
        possible ways in the form of new geometry the overlays the underlying data source.

        Args:
            points:
            ways:

        Returns:

        """
        # load already cached distances by ID
        if self.enable_cache:
            distances = self._cached_distance_matrix(points.index)
        else:
            distances = None

        p = points.geometry.to_crs(self.crs)
        p = pd.DataFrame(index=points.index, data={"x": p.geometry.x, "y": p.geometry.y})
        distances = self._calculate_distance_matrix(p, distances, self.max_distance)

        if ways is not None:
            links = self.find_points_on_ways(points, ways)
            for key, line in links.items():
                building_id_from, building_id_to = key
                distances.loc[(building_id_from, building_id_to)] = line.length
                distances.loc[(building_id_to, building_id_from)] = line.length

        # distances_ = xr.DataArray.from_series(distances_, sparse=True)
        row_index = points.index.copy()
        row_index.name = "id_from"
        col_index = points.index.copy()
        col_index.name = "id_to"
        return series_to_sparse_with_coords(distances, row_index=row_index, col_index=col_index)
        # distances_ = distances_.reindex(id_from=points.index).reindex(id_to=points.index)
        # return distances

    def neighbours_distance_matrix(self, points, max_distance=1000.0):
        pairs_to_route = tesselate_point_pairs(points)
        ids_to_route = pd.concat([pairs_to_route.id_from, pairs_to_route.id_to]).unique()

        points_to_route = points.loc[ids_to_route]
        distances = routing_distances_for_points(self.database, pairs_to_route, points_to_route, max_distance)
        distances[distances == 0] += 1e-9
        return distances

    def full_distance_table(self, building_points, n_jobs=-1, limit=1000):
        # Make it possible to parallel process the graph by doing sets of indices in parallel
        if n_jobs == -1:
            import os

            n_jobs = os.cpu_count()
        # neighbours_distance_matrix (delaunay)
        try:
            # In rare cases the delauny fails, e.g. when many buildings given then same position (usually error)
            edges = self.neighbours_distance_matrix(building_points, max_distance=limit)
        except scipy.spatial.QhullError:
            return False
        edges = edges.to_frame("distance").reset_index()
        # Full distance matrix via neighbours
        adj_array = adjacency_matrix_from_edges(
            edges.id_from, edges.id_to, edges.distance, building_points.index
        )
        # Don't actually try to do in parallel if N IDX is not >> N cores
        # Roughly say we need to have 10x as many nodes to process as cores
        # before there might be an advantage to Parallel (6xN distances per core)
        if n_jobs == 1 or adj_array.shape[0] < (10 * n_jobs):
            dist = scipy.sparse.csgraph.dijkstra(adj_array, directed=False, limit=limit)
            dist = dist.astype("float32")

            distances = _make_distance_table(dist, building_points)
            return distances

        else:
            # need to split by N indexes to avoid blowing up memory
            # So need a chunk size that gives at least N jobs of work but up to a max chunk size.
            n = adj_array.shape[0]
            index = np.arange(n)
            max_chunk = 100000
            # always want at least n_jobs
            splits = int(max(n_jobs, n**2 / max_chunk))
            index_chunks = np.array_split(index, splits)

            # Note, work on 'rows' -  so we have row_coord[index_chunk] row indexes and ALL col_coord values

            # Convert each chunk to sparse as we go instead of generating the whole thing first.
            def _do_chunk(g, indices=None, directed=False, limit=np.inf, chunk_n=None):
                dist = scipy.sparse.csgraph.dijkstra(adj_array, indices=indices, directed=False, limit=limit)
                dist = dist.astype("float32")

                building_points_from = building_points.iloc[indices]
                distances = _make_distance_table(
                    dist, building_points, building_points_from=building_points_from
                )
                return distances

            #         n_jobs = 1 #debug
            distances = Parallel(n_jobs=n_jobs, backend="threading")(
                delayed(_do_chunk)(adj_array, indices=idx_chunk, directed=False, limit=limit, chunk_n=i)
                for i, idx_chunk in enumerate(index_chunks)
            )

            return pd.concat(distances).sort_index()

    def load_routing_geometry(self, points, routes, *, extra_ways=None) -> GeoDataFrame:
        """

        Args:
            points:
            routes:
            extra_ways:

        Returns:

        """
        if hasattr(routes, "data_vars") and "routes" in routes.data_vars:
            routes = routes["routes"]
        else:
            routes = routes
        # FIXME quick fix for simple shortcuts, later want a better linking for extra ways with geom
        #  e.g. using the distance cache to also cache the rout geom ids. If extra ways is not already a
        #  (building key: line) mapping, create it. Otherwise allow it to be calculated once elsewhere.
        #  This should be cleaned up later.
        if extra_ways is not None and not isinstance(extra_ways, dict):
            extra_ways = self.find_points_on_ways(points, extra_ways)
        features = road_map_for_network(self.database, routes, extra_ways)
        return features

    def _calculate_distance_matrix(
        self,
        points: pd.DataFrame,
        cached_distances: pd.Series | None = None,
        max_distance=1000.0,
        update_cache=True,
    ) -> pd.Series:
        pairs_to_route = tesselate_point_pairs(points)

        if cached_distances is not None:
            # If we have cache data only load what's missing
            # Pairs to route are those from the tesselation that don't exist in the cache.
            pairs_to_route = pd.DataFrame(
                index=(
                    pairs_to_route.set_index(["id_from", "id_to"])
                    .sort_index()
                    .index.difference(cached_distances.index)
                )
            ).reset_index()

        # Get the x, y points that need to be routed
        ids_to_route = pd.concat([pairs_to_route.id_from, pairs_to_route.id_to]).unique()

        # TODO: (w)hy not directly get the routing vertex for the IDs?
        points_to_route = points.loc[ids_to_route]
        distances = routing_distances_for_points(self.database, pairs_to_route, points_to_route, max_distance)
        distances[distances == 0] += 1e-8

        # Need to make sure the output is always symmetric
        distances_reversed = (
            distances.reset_index()
            .rename(columns={"id_from": "id_to", "id_to": "id_from"})
            .set_index(["id_from", "id_to"])["distance"]
        )
        distances = pd.concat([distances, distances_reversed]).drop_duplicates().dropna()
        if update_cache and self.enable_cache:
            # Save the links back to the cache (TODO consolidate regularly)
            self.save_table(distances)

        if cached_distances is not None:
            distances = pd.concat([cached_distances, distances]).sort_index()
            assert distances.index.is_unique
        return distances

    def _cached_distance_matrix(self, building_ids):
        """
        Load the distance matrix from the cache for the given coordinates
        NOTE that this should already be a symmetrical matrix!

        Args:
            building_ids:

        Returns:

        """
        # Negative IDs are assigned for non-existing buildings
        building_ids = building_ids[building_ids > 0]
        slice_ids = self._index_from_coords(building_ids)
        # By default use a step size that gets a bit of extra data then throws it away
        # in order to reduce the number of reads.
        slices = self._ids_to_slices(slice_ids.index, stepsize=self.overread_step_size)

        data = self._iterate_fetch_slices(slices)
        # Avoid zero length issues since some sparse ops might assume 0 == missing
        # Add a tiny distance where input is zero
        data["distance"][data["distance"] == 0] += 1e-6

        # The fastest way to address that there may be empty results and/or
        # missing inputs (e.g. when we selected extra data that we didn't need through stepsize)
        # is to use reindex followed by dropna

        # NOTE: because of an earlier fuckup the 'id from/to' from the iterate fetch slices is the
        # INTEGER INDEX of the tiledb cachewhie the id from/to we use here is the BUILDING ID
        out = pd.DataFrame(
            {
                "id_from": slice_ids.reindex(data["id_from"]).values,
                "id_to": slice_ids.reindex(data["id_to"]).values,
                "distance": data["distance"],
            }
        ).dropna()
        # NOTE don't try to tidy this: MUST do dropna, then as int, then set index
        out[["id_from", "id_to"]] = out[["id_from", "id_to"]].astype(int)
        out = out.set_index(["id_from", "id_to"]).distance.sort_index()
        return out

    def _ids_to_slices(self, slice_ids, *, stepsize=1):
        # PERF: fast up to 1000s of egids since only iterate over splits, no need to optimize
        # NOTE could init to len(slice_ids)
        slices = []
        for slc in collection_utils.consecutive_slices(slice_ids, stepsize=stepsize):
            slc_min, slc_max = min(slc), max(slc)
            if slc_min == slc_max:
                slices.append(slc[0])
            else:
                slices.append(slice(slc_min, slc_max))
        return slices

    def _index_from_coords(self, coords):
        """
        Important: some coords might not be found in the index array and the output
        table will be the size of only the found indexes!
        Args:
            coords:

        Returns:

        """
        with self._open_index() as index:
            slice_ids = index.multi_index[list(coords)]
        slice_ids = pd.Series(slice_ids[self.index_name], index=slice_ids["index"])
        slice_ids = slice_ids.sort_index()
        return slice_ids

    def _iterate_fetch_slices(self, slices):
        """
        Iterate over slices, fetch each slice, then concat. Needed because multi_index
        (which was used at first) does not  do 'pairwise' numpy-style slicing.
        Instead returns all values in ranges a and all values in ranges b (so kinda square with gaps).
        Results would actually work but is inefficient.

        Args:
        slices

        Returns:
        """
        res = {
            "distance": [],
            "id_from": [],
            "id_to": [],
        }
        # NOTE hard to pre-allocate because fetch might not have any data.
        with self._open_distances() as distances_cache:
            # res = distances_cache.multi_index[slices, slices] # Works but generates more data than required
            for s in slices:
                data = distances_cache[s, s]

                # skip if empty
                if len(data["distance"]) > 0:
                    for k, v in res.items():
                        res[k].append(data[k])

        res = {k: np.concatenate(v) for k, v in res.items() if len(v) > 0}
        return res

    # NOTE: not really any faster? Maybe fore large number of slices...
    def _fetch_slices_parallel(self, slices, n_jobs=-1):
        def do_slice(a: tiledb.Array, selection):
            return a[selection, selection]

        with self._open_distances() as distances_cache:
            res = Parallel(n_jobs=n_jobs, backend="threading")(
                delayed(do_slice)(distances_cache, sel) for sel in slices
            )
            res = {
                "distance": np.concatenate(list(r["distance"] for r in res if len(r["distance"]) > 0)),
                "id_from": np.concatenate(list(r["id_from"] for r in res if len(r["distance"]) > 0)),
                "id_to": np.concatenate(list(r["id_to"] for r in res if len(r["distance"]) > 0)),
            }
        return res

    def _open_index(self, mode="r"):
        return tiledb.open(self._index_array_file, mode, ctx=self._tiledb_ctx)

    def _open_distances(self, mode="r"):
        return tiledb.SparseArray(self._distances_array_file, mode, ctx=self._tiledb_ctx)

