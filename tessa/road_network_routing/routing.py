from collections import defaultdict

import geopandas as gpd
import numpy as np
import pandas as pd
import pandera
import shapely.geometry
import sparse

# from geopandas import GeoDataFrame, GeoSeries
from shapely.geometry import Point
from shapely.ops import nearest_points
from sqlalchemy import text

from tessa.sparse_graph import (
    adjacency_matrix_from_edges,
    labelled_distance_matrix,
    sparse_dataarray_to_dataframe,
)


def road_distances_for_edges(routing_pairs: pd.DataFrame, engine, search_buffer: float = 1000):
    """ """

    # Maybe use prepared statement try again here (e.g. looping over pairs)
    with engine.begin() as conn:
        conn.execute(
            text("CREATE TEMP TABLE tmp_routing_pairs ( id int, source int, target int) ON COMMIT DROP;")
        )

        # Need to use manual insert b/c using pandas to_sql won't find the temporary table
        for row in routing_pairs.itertuples():
            conn.execute(
                text("INSERT INTO tmp_routing_pairs VALUES (:id, :source, :target)"),
                dict(id=row.Index, source=int(row.source_vert), target=int(row.target_vert)),
            )

        sql = """
        with routing_pairs as (
            select
             tmp_routing_pairs.id,
             source, target,
             st_asewkt(st_buffer(st_envelope(st_collect(a.the_geom, b.the_geom)), :search_buffer))   as search_zone
              from tmp_routing_pairs
                       join swisstopo_streets_routing.routing_edges_vertices_pgr a on a.id = source
                       join swisstopo_streets_routing.routing_edges_vertices_pgr b on b.id = target
        )
        select id, agg_cost as road_distance
               from routing_pairs
                        left join lateral (select agg_cost
                                           from pgr_dijkstraCost(
                                                           'select id, source, target, length as cost from swisstopo_streets_routing.routing_edges where st_contains(st_geomfromewkt(''' ||
                                                           search_zone || '''), geom)',
                                                           source,
                                                           target,
                                                           false
                                               ) ) s on true
        ;
        """
        route_pair_distances = pd.read_sql(
            text(sql), conn, params={"search_buffer": search_buffer}
        ).set_index("id")

    return route_pair_distances.road_distance


def road_map_for_network(db, routes, shortcuts=None) -> gpd.GeoDataFrame:
    """

    Args:
        routes:
        db:
        shortcuts: Using the OSM terminology, gives the possibility to provide extra 'ways' (routes/geometry)
        that could be used for connecting buildings

    Returns:
        GeoDataFrame with columns route_id, geom

    """

    # TODO:eventually use psycopg3 native support for prepared statements
    sql_prep = """
    PREPARE route_lookup(int, int, float) as
    with
         vertex_egid_from as (select id from swisstopo_streets_routing.routing_vertex_egid where egid = $1),
         vertex_egid_to as (select id from swisstopo_streets_routing.routing_vertex_egid where egid = $2),
         tmp_id_pairs as (select vertex_egid_from.id as id_from, vertex_egid_to.id as id_to
            from vertex_egid_to, vertex_egid_from),
         search_zone_query as (select id_from, id_to,
           st_asewkt(
                   st_buffer(st_envelope(st_collect(a.the_geom, b.the_geom)), $3)) as search_zone
    from tmp_id_pairs
             join swisstopo_streets_routing.routing_edges_vertices_pgr a on a.id = id_from
             join swisstopo_streets_routing.routing_edges_vertices_pgr b on b.id = id_to
    )
    select edge as edge_id,
    $1 as id_from,
    $2 as id_to
        from search_zone_query
                    left join lateral (select edge
                                       from pgr_dijkstra(
                                                       'select id, source, target, length as cost from swisstopo_streets_routing.routing_edges where st_contains(st_geomfromewkt(''' ||
                                                       search_zone || '''), geom)',
                                                       id_from,
                                                       id_to,
                                                       false
                                           ) ) s on true;
"""
    sql_exec = text("execute route_lookup(:id_from, :id_to, :route_buffer)")

    # This method minimises the amount of geometry needed to be looked up and
    # Associates one or more building pairs with each geom section.
    # First query looks up the edge IDs needed to route the given points and maps edge id -> list of building id pairs
    # Second query loads the (deduplicated) id list
    # TODO:allow for adding new buildings
    pairs = sparse_dataarray_to_dataframe(routes)
    pairs = {frozenset(t) for t in pairs[["id_from", "id_to"]].itertuples(index=False)}

    # Don't lookup geometry for building pairs that already have shortcuts defined
    if shortcuts:
        shortcut_keys = {frozenset(key) for key in shortcuts.keys()}
        shortcuts = {frozenset(key): line for key, line in shortcuts.items()}
        lookup_pairs = pairs - shortcut_keys
        # nb_vertices = len(list(shortcuts.values())[0].xy[0])
        # closest_to_shortcut = [[]] * nb_vertices
    else:
        lookup_pairs = pairs

    id_lookup_dict = defaultdict(list)
    with db.begin() as conn:
        # Setup the query as a prepared statement, this is much faster!
        conn.execute(text(sql_prep))

        for row in lookup_pairs:
            if len(row) == 1:
                # TODO:warning, this avoids looking up loopbacks (with same start and end ID)
                #  but this shouldn't really happen
                continue
            row = tuple(row)
            # Get the table of edge ids for this path
            sql_exec_comp = sql_exec.bindparams(id_from=int(row[0]), id_to=int(row[1]), route_buffer=300)
            sql_exec_comp = sql_exec_comp.compile(db, compile_kwargs={"literal_binds": True})
            edges_query = conn.execute(sql_exec_comp)
            # edges_query = conn.execute(sql_exec.bindparams(id_from=int(row[0]), id_to=int(row[1]), route_buffer=300))
            # Append the edge id -> building pair to the lookup
            for edges_row in edges_query:
                edge_id = edges_row[0]
                id_lookup_dict[edge_id].append(tuple(row))

        # Since not super clear when the sqlalchemy 'session' ends, remove the prep'ed statement here
        conn.execute(text("deallocate route_lookup"))

    with db.begin() as conn:
        roads = gpd.read_postgis(
            text(
                """select id,
            st_transform( ST_LineMerge(geom), 4326) as geom
            from swisstopo_streets_routing.routing_edges where id=ANY(:edges_ids)"""
            ),
            conn,
            params=dict(edges_ids=list(id_lookup_dict.keys())),
        ).set_index("id")
        roads["route_id"] = pd.Series(id_lookup_dict).reindex(roads.index)
        #         result['route_id'] = id_lookup_dict

        if shortcuts:
            shortcuts_df = gpd.GeoSeries(shortcuts, crs=roads.crs)
            shortcuts_df.index.name = "route_id"

            # Do the lookup process twice, once for the start and once for the end of the line
            # TODO:get the right edge building ID, not just the first one
            # TODO:handle drawing too far from the region of interest, should error gracefully
            # Start of line
            short_points = shortcuts_df.apply(lambda line: shapely.geometry.Point(line.coords[0]))
            firsts = []
            nearest_shortcut_id, nearest_edge_id = roads.sindex.nearest(short_points, return_all=False)
            edge = roads.iloc[nearest_edge_id]
            pnts = short_points.iloc[nearest_shortcut_id]

            for edge, (key, pnt) in zip(edge.itertuples(), pnts.items()):
                edge_point, line_point = nearest_points(edge.geom, pnt)
                line = shapely.geometry.LineString([edge_point, line_point])
                point_key = tuple(key)[0]
                firsts.append(([(edge.route_id[0][0], point_key)], line))
            firsts = gpd.GeoDataFrame(firsts, columns=["route_id", "geom"], geometry="geom", crs=roads.crs)

            # End of line
            short_points = gpd.GeoSeries(shortcuts).apply(
                lambda line: shapely.geometry.Point(line.coords[-1])
            )
            lasts = []
            nearest_shortcut_id, nearest_edge_id = roads.sindex.nearest(short_points, return_all=False)
            edge = roads.iloc[nearest_edge_id]
            pnts = short_points.iloc[nearest_shortcut_id]
            for edge, (key, pnt) in zip(edge.itertuples(), pnts.items()):
                edge_point, line_point = nearest_points(edge.geom, pnt)
                line = shapely.geometry.LineString([edge_point, line_point])

                point_key = tuple(key)[1]
                lasts.append(([(edge.route_id[0][1], point_key)], line))

            lasts = gpd.GeoDataFrame(lasts, columns=["route_id", "geom"], geometry="geom", crs=roads.crs)
            shortcuts_df = shortcuts_df.to_frame(name="geom").reset_index()
            shortcuts_df["route_id"] = shortcuts_df["route_id"].apply(lambda r: [tuple(r)])
            roads = gpd.GeoDataFrame(pd.concat([roads, shortcuts_df, firsts, lasts], ignore_index=True))
    return roads


def routing_vertex_for_buildings(buildings, engine, include_geom=False):
    """
    Fetch the road routing system indexes and optionally geometries for the given buildings
    """
    with engine.begin() as conn:
        conn.execute(text("CREATE TEMP TABLE building_id_search ( bld_id int) ON COMMIT DROP;"))

        # Need to use manual insert b/c using pandas to_sql won't find the temporary table
        for idx in buildings.index:
            conn.execute(text("INSERT INTO building_id_search VALUES (:bld_id)"), bld_id=idx)

        if include_geom:
            import geopandas as gpd

            sql = """
        select id as routing_vertex, bld_id as egid, the_geom as point from building_id_search
            LEFT JOIN swisstopo_streets_routing.routing_vertex_egid ON egid = bld_id
            LEFT JOIN swisstopo_streets_routing.routing_edges_vertices_pgr USING (id)"""
            routing_points = gpd.read_postgis(text(sql), conn, geom_col="point")

        else:
            sql = """
        select id as routing_vertex, bld_id as egid from building_id_search
            LEFT JOIN swisstopo_streets_routing.routing_vertex_egid ON egid = bld_id
        """
            routing_points = pd.read_sql(text(sql), conn)

    return routing_points


def swiss_routing_vertex_for_points(db, points) -> gpd.GeoDataFrame:
    """
    NOTE: this just gets the closest vertex, but doesn't guarantee that it will be especially sane!

    Args:
        points:
        db:

    Returns:
        columns point_id, routing_vertex_id,  point
    """
    db_crs = 2056
    if isinstance(points, gpd.GeoDataFrame | gpd.GeoSeries):
        points = points.to_crs(db_crs)
        points = list(zip(points.index, points.geometry.x, points.geometry.y))
    elif isinstance(points, Point):
        points = [(0, points.x, points.y)]
    elif isinstance(points, tuple) and len(points) == 2:
        # Add an ID if looking for a single point and put it in a list
        points = [(0, points[0], points[1])]
    elif isinstance(points, list) and len(points) > 0 and len(points[0]) == 2:
        # assume a list of points, if only 2 elements add an index
        points = ((i, p[0], p[1]) for i, p in enumerate(points))
    elif isinstance(points, pd.DataFrame) and {"x", "y"}.issubset(points.columns):
        points = [(row.Index, row.x, row.y) for row in points[["x", "y"]].itertuples()]

    with db.begin() as con:
        con.execute(text("CREATE TEMP TABLE pnt ( point_id int, x float, y float) ON COMMIT DROP;"))

        # Need to use manual insert b/c using pandas to_sql won't find the temporary table
        for row in points:
            con.execute(
                text("INSERT INTO pnt VALUES (:point_id, :x, :y)"), dict(point_id=row[0], x=row[1], y=row[2])
            )

        sql = """
        with thepoints as (
            SELECT point_id, st_setsrid(st_point(x, y), 2056) as point
            FROM pnt)
        SELECT point_id, routing_vertex_id,
        st_transform(the_geom, 4326) as point
        from thepoints
            CROSS JOIN LATERAL (
              SELECT route_verts.id as routing_vertex_id, route_verts.the_geom
              FROM swisstopo_streets_routing.routing_edges_vertices_pgr route_verts
              ORDER BY route_verts.the_geom <-> thepoints.point
              LIMIT 1
            ) s;
        """

        # Return Dataframe b/c some uses will need DB-style loc lookups
        route_vert_ids = gpd.read_postgis(text(sql), con, geom_col="point", crs=4326).set_index("point_id")

    return route_vert_ids


def routing_distances_for_points(
    engine, pairs_to_load, points_to_route, max_distance: float = 1000
) -> pd.Series:
    """
    Args:
        engine:
        pairs_to_load:
        points_to_route:
        max_distance:

    Returns:

    """
    routing_points = swiss_routing_vertex_for_points(engine, points_to_route).routing_vertex_id
    pairs_to_load["source_vert"] = routing_points[pairs_to_load.id_from].values
    pairs_to_load["target_vert"] = routing_points[pairs_to_load.id_to].values
    # NOTE: currently approximating the max distance by applying it to the search buffer
    pairs_to_load["distance"] = road_distances_for_edges(
        pairs_to_load[["source_vert", "target_vert"]], engine, search_buffer=max_distance
    ).astype(float)
    distances = pairs_to_load.set_index(["id_from", "id_to"])["distance"]
    # limit max dist, note that this doesn't prevent buildings from being loaded anyway
    # pairs_to_load = pairs_to_load[pairs_to_load < max_distance]
    return distances


def distance_matrix_for_buildings(edges, buildings, n_jobs=-1, limit=np.inf):
    edges = edges[edges.egid_a.isin(buildings.index)]
    edges = edges[edges.egid_b.isin(buildings.index)]
    graph = adjacency_matrix_from_edges(
        edges.egid_a.to_numpy(),
        edges.egid_b.to_numpy(),
        edges.full_distance.to_numpy(),
        buildings.index.to_numpy(),
    )
    dist = labelled_distance_matrix(
        graph,
        (buildings.index, buildings.index),
        dims=("id_from", "id_to"),
        name="distance",
        n_jobs=n_jobs,
        limit=limit,
    )
    return dist


def connected_egids_for_network(routes):
    """
    NOTE: since we add a max distance filter before calculating routes,
    output might not include every egid in the input. This function
    returns the list of egids that are included in the routing.
    """
    tmp = routes.data.tocoo()
    used_coords = np.unique(np.concatenate([tmp.coords[0], tmp.coords[1]]))
    connected_egids = routes.id_from[used_coords]
    return connected_egids


def egid_pairs_for_network(routes):
    """
    NOTE: since we add a max distance filter before calculating routes,
    output might not include every egid in the input. This function
    returns the list of egids that are included in the routing.
    """
    if not isinstance(routes.data, sparse.COO):
        tmp = routes.data.tocoo()
    else:
        tmp = routes.data
    id_from = routes.id_from.data[tmp.coords[0]]
    id_to = routes.id_to.data[tmp.coords[1]]
    return pd.DataFrame({"id_from": id_from, "id_to": id_to})


def edges_for_egids(egids, db):
    with db.connect() as con:
        edges = pd.read_sql(
            text(
                """
select index, egid_a, egid_b, route_distance, full_distance
from prj_geneap.building_delaunay_edges
where egid_a in :egids
  and egid_b in :egids
    """
            ),
            con,
            params={
                #                 'edge_table': edge_table,
                "egids": tuple(egids)
            },
            index_col="index",
        )
    return edges


def edges_for_cluster(cluster_id, db):
    with db.connect() as con:
        edges = pd.read_sql(
            text(
                """
with cluster as (select egid from prj_geneap.building_clusters_100m where cluster_label=:cluster_id)
select index, egid_a, egid_b, route_distance, full_distance
from prj_geneap.building_delaunay_edges
where egid_a in (select egid from cluster)
  and egid_b in (select egid from cluster)
    """
            ),
            con,
            params={"cluster_id": int(cluster_id)},
        )
    return edges
